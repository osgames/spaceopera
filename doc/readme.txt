SpaceOpera Readme.txt Version 0.5.xx, May 2008
==============================================

Welcome to Space Opera and thanks for downloading the game. 

The file SpaceOpera.jar contains this readme file, the source code, the compiled classes 
(compiled with IBM's JDK 1.4.1). 

All the multimedia files are in the SpaceOpera_Resources.jar (animations, images and sound).

To run the game you need 
- version 1.4.1 or later of the Java Runtime Environment 
- version 2.1.1 of the Java Media Framework (sound and animations)
- a Xerxes 1 or 2 installation (XML support)

How to install Space Opera: 
===========================

1) Create a working directory like

 /home/lorenz/spaceopera/
 - or -
 C:\games\spaceopera\
			

2) Store the SpaceOpera.jar and the SpaceOpera_Resources.jar 
files in this working directory.

Unpack the SpaceOpera_Resources.jar file. This will produce the
following subdirectories:
 - animations 
 - effects
 - images 
 - music (currently empty)
  

How to install the necessary Java resources:
============================================
1) Java 1.4.1 Runtime Environment
   Download and install from http://java.sun.com/products/archive/

2) Java Media Framework (V 2.1.1)
   Goto http://java.sun.com/products/java-media/jmf/ for this download

3) Xerxes 1 or 2 implementation
   Get "Xerces-J-bin.2.5.0.zip" or later from http://xml.apache.org/dist/xerces-j/


Run the Space Opera from the jar-file:
======================================
1) The java command does not resolve the classpath completely when running a jar-file,
   therefore you must copy the JMF and Xerces Libraries to a special directory:

1a) Create a subdirectory, e.g.  C:\games\spaceopera\ext

1b) Copy the following files to this directory.
	xercesImpl.jar 
	customizer.jar 
	jmf.jar 
	mediaplayer.jar 
	multiplayer.jar 
	sound.jar 
   (Those files were downloaded with jmf and xerces. Sorry, because this game
   is open-sourced, I cannot easily distribute copyrighted code. Download the files
   as instructed above.)
   (The game should work without these files, but then animations won't show and
   sound won't play...)


2) Enter the following command: 

   java -jar SpaceOpera.jar  (with JRE 1.4 +)

3) On Windows systems, you can doubleclick on SpaceOpera.jar to start the
   game. (If all is configured correctly, that is.)
 
have fun!

Note: The source in the jar was compiled with JDK 1.4.1.


How to update from an earlier version:
======================================
Starting with SpaceOpera Version 0.5.xx, there will be different 
packages to download: 

-SpaceOpera.jar: the source code and the compiled classes
-SpaceOpera_Resources.jar: all the multimedia files

So you may download e.g. only the new SpaceOpera.jar to
run a new version. Also, the multimedia files must only be downloaded
when new animations or images are available.


Programmers Reference:
======================

How to edit, modify and compile the source:
===========================================

1) Extract the files from the jar-files with the following commands:
   (you can also use winzip to extract the files)

 jar -xvf spaceopera.jar
 jar -xvf spaceopera_resources.jar

This will create the following directories:

 /home/lorenz/spaceopera/source/                 - this is the source directory
 /home/lorenz/spaceopera/spaceopera/             - this is the classes directory
 /home/lorenz/spaceopera/images/                 - this is the images directory
 ...

2) Modify the source in source/spaceopera 

3) Now you can compile the source files with javac *.java with your own
JDK. Move the class files to the classes directory to run them.


How to run the program from your compiled code:
===============================================

1) go to the working directory, i.e. /home/lorenz/spaceopera

2) call the program with 'java spaceopera.gui.SpaceOpera'

have fun!


How to create new distributable *.jar files:
============================================

1) The file MANIFEST.MF specifies which class contains the main() method.

2) Create the new executable jar-file with the following command:
  
   jar -cvfm SpaceOpera.jar MANIFEST.MF spaceopera source license readme.txt. (*)

   which adds the manifest file, the directories spaceopera, images, source and license, plus the readme file to the jar-file.

3) Create the new SpaceOpera_Resources.jar file:

   jar -cvf SpaceOpera_Resources.jar animations config effects images music license readme.txt.

   which adds the multimedia and configuration directories to the jar-file.


*) As of version 0.5.186, the jar files can be created with an ant procedure.

How to upload a directory to subversion:
========================================

see https://sourceforge.net/svn/?group_id=45664


How to upload a file to the project homepage:
============================================

1) Log into a shell on sourceforge:

   ssh -l loginname yourproject.sourceforge.net

2) Upload localfile to your projects' webspace:

   scp localfile loginname@yourproject.sourceforge.net:/home/groups/y/yo/yourproject/htdocs/


How to upload files to the sourceforge FRS:
===========================================
   1. FTP to upload.sourceforge.net
   2. Login as "anonymous"
   3. Use your e-mail address as the password for this login
   4. Set your client to binary mode ("bin" on command-line clients)
   5. Change your current directory to /incoming ("cd /incoming")
   6. Upload the desired files for the release ("put filename")
   7. Move the uploaded files quickly away from the /incoming directory
      to the project's file area.



Troubleshooting
===============

Game does not start correctly. 
------------------------------
The main window shows with background stars and such, but nothing happens
when pressing 'next turn'. Also the popup window 'Welcome to the SpaceOpera Universe'
does not appear.

Possible cause: the 'ext' directory with the necessary JMF and Xerces Libraries
was not installed.


Sound does not work
-------------------
TODO


If you run into problems look into the newsgroups or the bug reports in 
http://spaceopera.sourceforge.net/ or write an email to the project admin.


-- good luck.