//The class Player is the parent class for all players in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import java.awt.Color;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.objects.battle.FightShip;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Technology;
import spaceopera.universe.Universe;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.ShipDesign;
import spaceopera.universe.ships.ShipOrderItem;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Player class is the parent class for human players and computer players.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class Player implements SOConstants {
	String name;
	private List<Building> possibleBuildings;
	private Vector possibleProducts;
	private Hashtable possibleComponents;
	private List<Technology> possibleTechnologies;
	List<Colony> colonies;
	Hashtable<String, ShipDesign> shipDesigns;
	List<SpaceCraft> ships;
	Hashtable shipsTo;
	Vector planetList;
	Ship _ship;
	private Colony currentColony;
	private Planet currentPlanet;
	Color color;
	Universe universe;
	private double researchProduction = 0.0;
	private double treasury = 0.0;
	double distMin = 0.0;
	Sun selectedSun;
	String scoutingStrategy = NEXT;
	String colonizingStrategy = FIRST;
	private Vector playerRelations;
	private int coloniesBuilt = 0;
	PreferredPlanetValues preferredPlanetValues = new PreferredPlanetValues();

	// XXX: first contact with, relation to, treaties with other races ....
	private Hashtable knownOtherPlayers; // contains names of known other
	// players
	private int[] usedColonyImages;
	private int nbrUsedColonyImages;
	private int physicsPercent = 20;
	private int biologyPercent = 16;
	private int mathematicsPercent = 16;
	private int socialPercent = 16;
	private int economyPercent = 16;
	private int militaryPercent = 16;

	// initial science projects
	private String currentPhysics = "";
	private String currentBiology = "";
	private String currentMathematics = "";
	private String currentSocial = "";
	private String currentEconomy = "";
	private String currentMilitary = "";

	String preferredPlanetType = null;

	public void addColony(Colony c) {
		colonies.add(c);
	}

	public void addPlanet(Planet p) {
		if (!planetList.contains(p)) {
			planetList.addElement(p);
		}
	}

	public void addProduct(Product p) {
		possibleProducts.addElement(p);
	}

	public void addResearchProduction(double r) {
		researchProduction += r;
	}

	public void addShipDesign(String name, ShipDesign sd) {
		shipDesigns.put(name, sd);
	}

	public void addSpaceCraft(SpaceCraft rs) {
		ships.add(rs);
	}

	public void addTreasury(double d) {
		treasury += d;
		// System.out.println("treasury: " + treasury);
	}

	public void addToShipsTo(int s) {
		shipsTo.put(s, "");
	}

	public void buildShips() { /* computerplayer AI-method */
	}

	// called by FightShip.executeStep()
	public void checkOrders(FightShip fs, Vector fightingShips, Sun s) {
		if (fs.getOrderTarget() == null) {
			if (fs.getShipAttacks()) {
				setOrderAttackShip(fs, fightingShips, s);
			}
		}
		if (fs.getOrderTarget() == null) {
			if (fs.getPlanetAttacks()) {
				setOrderAttackPlanet(fs, s);
			}
		}
	}

	public List<Colony> getColonies() {
		return (colonies);
	}

	public Color getColor() {
		return (color);
	}

	public String getCurrentPhysics() {
		return (currentPhysics);
	}

	public String getCurrentBiology() {
		return (currentBiology);
	}

	public Colony getCurrentColony() {
		return (currentColony);
	}

	public String getCurrentMathematics() {
		return (currentMathematics);
	}

	public Planet getCurrentPlanet() {
		return (currentPlanet);
	}

	public String getCurrentSocial() {
		return (currentSocial);
	}

	public String getCurrentEconomy() {
		return (currentEconomy);
	}

	public String getCurrentMilitary() {
		return (currentMilitary);
	}

	public int getPhysicsPercent() {
		return (physicsPercent);
	}

	public int getBiologyPercent() {
		return (biologyPercent);
	}

	public int getMathematicsPercent() {
		return (mathematicsPercent);
	}

	public int getSocialPercent() {
		return (socialPercent);
	}

	public int getEconomyPercent() {
		return (economyPercent);
	}

	public int getMilitaryPercent() {
		return (militaryPercent);
	}

	public String getName() {
		return (name);
	}

	public int getNumberOfUsedColonyImages() {
		return (nbrUsedColonyImages);
	}

	public List<Planet> getPlanetList() {
		return (planetList);
	}

	public void addRelation(PlayerRelation p) {
		playerRelations.addElement(p);
	}

	public float getPlayerRelation(Player p) {
		float relation = 0;
		for (int i = 0; i < playerRelations.size(); i++) {
			PlayerRelation pr = (PlayerRelation) playerRelations.elementAt(i);
			if (pr.getRelationTo().equals(p)) {
				relation = pr.getRelationLevel();
			}
		}
		return relation;
	}

	public String[] getColonizablePlanetNames(SpaceCraft ship) {
		String tempNames[] = new String[planetList.size()];
		int j = 0;
		for (int i = 0; i < planetList.size(); i++) {
			Planet planet = (Planet) planetList.elementAt(i);
			if (ship.canColonize(planet)) {
				if (!planet.isColonized()) {
					tempNames[j] = planet.getName();
					// System.out.println("can colonize: " + tempNames[j]);
					j++;
				}
			}
		}
		String colonizeNames[] = new String[j];
		for (int i = 0; i < j; i++) {
			colonizeNames[i] = tempNames[i];
			// System.out.println("tempNames: " + tempNames[i] + ",
			// colonizeNames: " + colonizeNames[i]);
		}
		if (j == 0) {
			colonizeNames = new String[] { NONE_AVAILABLE };
		}
		java.util.Arrays.sort(colonizeNames);
		return (colonizeNames);
	}

	/** use the planet names (e.g. "Sol 3") instead of "Colony 37" */
	public String[] getColonyPlanetNames() {
		String colonyNames[] = new String[colonies.size()];
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony = colonies.get(i);
			colonyNames[i] = colony.getPlanetName();
		}
		return (colonyNames);
	}

	public List<Building> getPossibleBuildings() {
		return (possibleBuildings);
	}

	public Hashtable getPossibleComponents() {
		return (possibleComponents);
	}

	public Vector getPossibleProducts() {
		return (possibleProducts);
	}

	public List<Technology> getPossibleTechnologies() {
		return (possibleTechnologies);
	}

	public double getResearchProduction() {
		return (researchProduction);
	}

	public List<SpaceCraft> getShips() {
		return (ships);
	}

	public Hashtable<String, ShipDesign> getShipDesigns() {
		return (shipDesigns);
	}

	public PreferredPlanetValues getPreferredPlanetValues() {
		return preferredPlanetValues;
	}

	public double getTreasury() {
		return (treasury);
	}

	public int getUsedColonyImages(int i) {
		return (usedColonyImages[i]);
	}

	public boolean hasShip(SpaceCraft s) {
		return (ships.contains(s));
	}

	public void incNbrUsedColonyImages() {
		nbrUsedColonyImages++;
	}

	public void removeColony(Colony c) {
		colonies.remove(c);
	}

	public void removeFromShipsTo(int s) {
		shipsTo.remove(s);
	}

	public void removeProduct(String s) {
		for (int i = 0; i < possibleProducts.size(); i++) {
			Product p = (Product) possibleProducts.elementAt(i);
			if (p.getName().equals(s)) {
				possibleProducts.remove(p);
			}
		}
	}

	public void removeShipDesign(String name) {
		shipDesigns.remove(name);
	}

	public void removeSpaceCraft(SpaceCraft rs) {
		ships.remove(rs);
	}

	public ShipComponent getShipComponent(String name) {
		// ShipComponent sc=null;
		// for (int i=0; i<possibleComponents.size(); i++) {
		// ShipComponent s = (ShipComponent)possibleComponents.elementAt(i);
		// if (name.equals(s.getName())){
		// sc=s;
		// break;
		// }
		// }
		// return sc;
		return ((ShipComponent) possibleComponents.get(name));
	}

	private Sun isThisSunCloserThanThePrevious(Ship ship, Sun sun) {
		double distNew = 0.0;
		if (selectedSun == null) {
			selectedSun = sun;
			distMin = Math.sqrt((ship.getX() - sun.getX()) * (ship.getX() - sun.getX()) + (ship.getY() - sun.getY())
					* (ship.getY() - sun.getY()));
		} else {
			distNew = Math.sqrt((ship.getX() - sun.getX()) * (ship.getX() - sun.getX()) + (ship.getY() - sun.getY())
					* (ship.getY() - sun.getY()));
			if (distNew < distMin) {
				distMin = distNew;
				selectedSun = sun;
			}
		}
		return selectedSun;
	}

	private boolean isThisPlanetCloser(Ship ship, Planet previous, Planet planet) {
		Sun previousSun = previous.getSun();
		Sun newSun = planet.getSun();
		double previousDistance = Math.sqrt((ship.getX() - previousSun.getX()) * (ship.getX() - previousSun.getX())
				+ (ship.getY() - previousSun.getY()) * (ship.getY() - previousSun.getY()));
		double newDistance = Math.sqrt((ship.getX() - newSun.getX()) * (ship.getX() - newSun.getX()) + (ship.getY() - newSun.getY())
				* (ship.getY() - newSun.getY()));
		// Test
		// System.out.println("distance to " +previousSun.getName() + " is " +
		// previousDistance);
		// System.out.println("distance to " +newSun.getName() + " is " +
		// newDistance);
		return (newDistance < previousDistance);
	}

	public boolean knowsPlayer(String name) {
		boolean knows = false;
		if (knownOtherPlayers.containsKey(name)) {
			knows = true;
		}
		return (knows);
	}

	public void meetsPlayer(String name) {
		// XXX: for now the object is just the name, later it might be some
		// kind of 'relation' class
		knownOtherPlayers.put(name, name);
	}

	public Player(Universe u, String player, Color c) {
		Product p = null;
		color = c;
		universe = u;
		name = player;
		possibleBuildings = new Vector();
		possibleProducts = new Vector();
		possibleComponents = new Hashtable();
		possibleTechnologies = new Vector();
		knownOtherPlayers = new Hashtable();
		shipDesigns = new Hashtable();
		colonies = new Vector();
		ships = new Vector();
		shipsTo = new Hashtable();
		planetList = new Vector();
		playerRelations = new Vector();

		nbrUsedColonyImages = 0;
		usedColonyImages = new int[COLONYIMAGES + 1];
		for (int i = 1; i < COLONYIMAGES; i++) {
			usedColonyImages[i] = 0;
		}

		// add buildings of Universe for player
		Vector buildings = universe.getBuildings();
		for (int i = 0; i < buildings.size(); i++) {
			Building e = (Building) buildings.elementAt(i);

			// new: randomize construction cost
			Building copy = (Building) e.clone();
			if (Math.random() > 0.5) {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 1.1);
				copy.setProductionCost(R_WORKUNITS_OPERATION, copy.getProductionCost(R_WORKUNITS_OPERATION) * 1.1);
			} else {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 0.9);
				copy.setProductionCost(R_WORKUNITS_OPERATION, copy.getProductionCost(R_WORKUNITS_OPERATION) * 0.9);
			}

			possibleBuildings.add(copy);
		}
		// add products of Universe
		Vector products = universe.getProducts();
		for (int i = 0; i < products.size(); i++) {
			Product prod = (Product) products.elementAt(i);
			// new: randomize construction cost
			Product copy = (Product) prod.clone();
			if (Math.random() > 0.5) {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 1.1);
			} else {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 0.9);
			}
			possibleProducts.addElement(copy);
		}
		// add Components of Universe
		Vector components = universe.getSOComponents();
		for (int i = 0; i < components.size(); i++) {
			ShipComponent k = (ShipComponent) components.elementAt(i);

			// new: randomize construction cost
			ShipComponent copy = (ShipComponent) k.clone();
			if (Math.random() > 0.5) {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 1.1);
			} else {
				copy.setProductionCost(R_WORKUNITS_CONSTRUCTION, copy.getProductionCost(R_WORKUNITS_CONSTRUCTION) * 0.9);
			}

			possibleComponents.put(k.getName(), copy);
		}
		// add technologies of Universe
		Vector technologies = universe.getTechnologies();
		for (int i = 0; i < technologies.size(); i++) {
			Technology tech = (Technology) technologies.elementAt(i);
			// new: randomize research cost of technology
			Technology copy = (Technology) tech.clone();
			// XXX: researchcost depending on personality (for some, biology is
			// cheaper...)
			// or depending on difficulty level...
			int randomize = (int) (10 * Math.random());
			switch (randomize) {
			case 0:
				copy.setResearchCost(copy.getResearchCost() * 0.4);
				break;
			case 1:
				copy.setResearchCost(copy.getResearchCost() * 0.6);
				break;
			case 2:
				copy.setResearchCost(copy.getResearchCost() * 0.8);
				break;
			case 3:
				copy.setResearchCost(copy.getResearchCost() * 1.2);
				break;
			case 4:
				copy.setResearchCost(copy.getResearchCost() * 1.4);
				break;
			case 5:
				copy.setResearchCost(copy.getResearchCost() * 1.6);
				break;
			default:
				// no change
			}
			possibleTechnologies.add(copy);
		}

		if (this instanceof MPlayer) {
			// XXX: change as needed for test purposes
			// Define standard ships: scout, colony ship, transport
			/** Scoutship */
			ShipDesign sd = new ShipDesign(SCOUT_SHIP);
			sd.setHull(SCOUT_HULL);
			sd.addDrive(this, SUBLIGHT_DRIVE);
			sd.addDrive(this, "Warp 1 drive");
			sd.addWeapon(this, LASER);
			// sd.addWeapon(LASER_CANNON);
			// sd.addWeapon("Atomic bomb");
			// sd.addWeapon(TORPEDO);
			sd.addSpecial(this, FUEL_SCOOP);
			sd.addElectronics(this, "Scanner I");
			// sd.addShielding("Electronic Shield I");
			shipDesigns.put(SCOUT_SHIP, sd);
			p = new Product(-1, sd.getShipClass(), sd.getDescription(), false, true, "s_scoutship", SHIP);
			for (int i = R_MIN; i < R_MAX; i++) {
				if (sd.getCost(this, i) > 0) {
					p.setProductionCost(i, sd.getCost(this, i));
					p.setSupportCost(i, sd.getSupportCost(this, i));
				}
			}
			possibleProducts.addElement((Product) p.clone());
			/** Colonyship */
			sd = new ShipDesign(COLONY_SHIP);
			sd.setHull(COLONY_HULL);
			sd.addDrive(this, SUBLIGHT_DRIVE);
			sd.addDrive(this, "Warp 1 drive");
			sd.addWeapon(this, LASER);
			// sd.addWeapon(this, LASER_CANNON);
			// sd.addWeapon(BUNDLED_LASER);
			// sd.addWeapon(this, TORPEDO);
			// sd.addWeapon(HIGH_SPEED_TORPEDO);
			// sd.addWeapon("Atomic bomb");
			// sd.addWeapon(this, PELLET_GUN);
			sd.addWeapon(this, RAIL_GUN);
			// sd.addShieldingOrArmor(this, "Electronic Shield I");
			// sd.addShieldingOrArmor(this, ARMOR_CLASS_I);
			sd.addSpecial(this, FUEL_SCOOP);
			sd.addSpecial(this, COLONY_MODULE);
			sd.addElectronics(this, "Scanner I");
			// sd.addElectronics(this, "Target computer II");
			shipDesigns.put(COLONY_SHIP, sd);
			p = new Product(-1, sd.getShipClass(), sd.getDescription(), false, false, "s_colonyship", SHIP);
			for (int i = R_MIN; i < R_MAX; i++) {
				if (sd.getCost(this, i) > 0) {
					p.setProductionCost(i, sd.getCost(this, i));
					p.setSupportCost(i, sd.getSupportCost(this, i));
				}
			}
			possibleProducts.addElement((Product) p.clone());
			/**
			 * Transport ship sd = new ShipDesign("Transporter");
			 * sd.setHull(SCOUT_HULL); sd.addDrive(this, SUBLIGHT_DRIVE);
			 * sd.addDrive(this, "Warp 1 drive"); //sd.addWeapon(LASER);
			 * //sd.addWeapon(LASER_CANNON); //sd.addWeapon("Atomic bomb");
			 * //sd.addWeapon(TORPEDO); //sd.addSpecial("Fuel Tank I");
			 * sd.addSpecial("Cargo Container (100 tons)");
			 * //sd.addElectronics("Scanner I"); //sd.addShielding("Electronic
			 * Shield I"); shipDesigns.put("Transporter", sd); p = new
			 * Product(-1
			 * ,sd.getShipClass(),sd.getDescription(),false,true,"s_transport"
			 * ,SHIP); for (int i=R_MIN; i<R_MAX; i++) { if
			 * (sd.getCost(this,i)>0) {
			 * p.setProductionCost(i,sd.getCost(this,i));
			 * p.setSupportCost(i,sd.getSupportCost(this,i)); } }
			 * possibleProducts.addElement((Product)p.clone());
			 */
		}
	}

	public void setCurrentBiology(String s) {
		currentBiology = s;
	}

	public void setCurrentColony(Colony c) {
		currentColony = c;
	}

	public void setCurrentPhysics(String s) {
		currentPhysics = s;
	}

	public void setCurrentPlanet(Planet p) {
		currentPlanet = p;
	}

	public void setCurrentMathematics(String s) {
		currentMathematics = s;
	}

	public void setCurrentSocial(String s) {
		currentSocial = s;
	}

	public void setCurrentEconomy(String s) {
		currentEconomy = s;
	}

	public void setCurrentMilitary(String s) {
		currentMilitary = s;
	}

	public void setiPhysics(int i) {
		physicsPercent = i;
	}

	public void setiBiology(int i) {
		biologyPercent = i;
	}

	public void setiMathematics(int i) {
		mathematicsPercent = i;
	}

	public void setiSocial(int i) {
		socialPercent = i;
	}

	public void setiEconomy(int i) {
		economyPercent = i;
	}

	public void setiMilitary(int i) {
		militaryPercent = i;
	}

	public void setName(String s) {
		name = s;
	}

	public void setNbrUsedColonyImages(int n) {
		nbrUsedColonyImages = n;
	}

	public void setResearchProduction(double r) {
		researchProduction = r;
	}

	public void setUsedColonyImages(int i, int n) {
		usedColonyImages[i] = n;
	}

	public void setPreferredPlanetValues(PreferredPlanetValues t) {
		preferredPlanetValues = t;
	}

	// protected void setColonyOrders() {/* computerplayer AI-method */ }

	// called by checkOrders
	public void setOrderAttackPlanet(FightShip fs, Sun sun) {
		Vector planets = sun.getPlanets();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			if (planet.isColonized()) {
				if (!planet.getPlayerName().equals(name)) {
					if (getPlayerRelation(planet.getPlayer()) < NEUTRAL) {
						fs.setOrderTarget(planet);
					}
				}
			}
		}
	}

	// called by checkOrders
	public void setOrderAttackShip(FightShip fs, Vector fightingShips, Sun s) {
		// simplest Strategy: ship gets "attack" order without a target
		// next to simple strategy: ship gets attack order for closest enemy
		// ship or planet
		// Strategies depend on
		// - ship type
		// - enemy ship type
		// - CPlayer-type
		// - others?
		FightShip ship = null;
		float previousDistance, distance;
		previousDistance = 9999.9f;
		// are there more enemy ships to fight?
		for (int i = 0; i < fightingShips.size(); i++) {
			ship = (FightShip) fightingShips.elementAt(i);
			// System.out.println("Player.setOrderAttackShip: ship is " +
			// ship.getShipType());
			if (!ship.getPlayerName().equals(name)) {
				if (fs.getOrderTarget() == null) {
					// System.out.println("Player.setOrderAttackShip: relation 1
					// is " + getPlayerRelation(ship.getPlayer()));
					// System.out.println("Player.setOrderAttackShip: relation 2
					// is " + ship.getPlayer().getPlayerRelation(this));
					if ((getPlayerRelation(ship.getPlayer()) < NEUTRAL) || (ship.getPlayer().getPlayerRelation(this) < NEUTRAL)) {
						fs.setOrderTarget(ship);
						distance = (float) Math.sqrt((ship.getX() - fs.getX()) ^ 2 + (ship.getY() - fs.getY()) ^ 2);
						if (distance < previousDistance) {
							distance = previousDistance;
							fs.setOrderTarget(ship);
						}
					}
				}
			}
		}
	}

	public void setShipOrders() {
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft sc = ships.get(i);
			if (!sc.isTraveling()) {
				setThisShipsOrders(sc);
			}
		}
	}

	/* method implemented by CPlayer and MPlayer */
	protected abstract void setThisShipsOrders(SpaceCraft sc);

	protected void scoutForHabitablePlanets(Ship ship) {

		Sun sun = null;
		selectedSun = null;
		int radius = 6 * universe.getPixelPerLightYear();
		int increment = 2 * universe.getPixelPerLightYear();
		// Test:
		// System.out.println("Scouting strategy used: " + scoutingStrategy);
		if (scoutingStrategy.equals(NEXT)) {

			List suns = universe.getSuns();
			for (int j = 0; j < suns.size(); j++) {
				sun = (Sun) suns.get(j);
				if (!sunExplored(sun)) {
					selectedSun = isThisSunCloserThanThePrevious(ship, sun);
				}
			}

		} else if (scoutingStrategy.equals(AREA)) {

			Vector foundSuns = new Vector();
			List suns = universe.getSuns();
			while ((foundSuns.size() < 1) && (radius < 500)) {
				// scout a given radius around first colony, second colony....
				if (colonies.size() > 0) {
					for (int i = 0; i < colonies.size(); i++) {
						Colony colony = colonies.get(i);
						Sun colonySun = colony.getSun();
						for (int j = 0; j < suns.size(); j++) {
							sun = (Sun) suns.get(j);
							if (!sunExplored(sun)) {
								double distance = Math.sqrt((colonySun.getX() - sun.getX()) * (colonySun.getX() - sun.getX())
										+ (colonySun.getY() - sun.getY()) * (colonySun.getY() - sun.getY()));
								if (distance < radius) {
									foundSuns.addElement(sun);
								}
							}
						}
					}
				}
				// then scout a given radius around current ship
				for (int j = 0; j < suns.size(); j++) {
					sun = (Sun) suns.get(j);
					if (!sunExplored(sun)) {
						if (selectedSun == null) {
							double distance = Math.sqrt((ship.getX() - sun.getX()) * (ship.getX() - sun.getX())
									+ (ship.getY() - sun.getY()) * (ship.getY() - sun.getY()));
							if (distance < radius) {
								foundSuns.addElement(sun);
							}
						}
					}
				}
				// if nothing found increase radius
				radius += increment;
			}
			// from the found suns, take the closest one depending on
			// preferences
			if (selectedSun == null) {
				for (int j = 0; j < foundSuns.size(); j++) {
					sun = (Sun) foundSuns.elementAt(j);
					if (!sunExplored(sun)) {
						selectedSun = isThisSunCloserThanThePrevious(ship, sun);
					}
				}
			}
		} else { // colors preferred

			// first scan after area strategy
			Color preferredColor = null;
			if (scoutingStrategy.equals(WHITE)) {
				preferredColor = Color.white;
			} else if (scoutingStrategy.equals(YELLOW)) {
				preferredColor = Color.yellow;
			} else if (scoutingStrategy.equals(RED)) {
				preferredColor = Color.red;
			} else if (scoutingStrategy.equals(BLUE)) {
				preferredColor = Color.blue;
			} else if (scoutingStrategy.equals(GREEN)) {
				preferredColor = Color.green;
			}
			List suns = universe.getSuns();
			// look ferst for matching colors
			for (int j = 0; j < suns.size(); j++) {
				sun = (Sun) suns.get(j);
				if (preferredColor.equals(sun.getColor())) {
					if (!sunExplored(sun)) {
						if (selectedSun == null) {
							distMin = Math.sqrt((ship.getX() - sun.getX()) * (ship.getX() - sun.getX()) + (ship.getY() - sun.getY())
									* (ship.getY() - sun.getY()));
							if (distMin < 2 * radius) {
								selectedSun = sun;
							}
						} else {
							double distNew = Math.sqrt((ship.getX() - sun.getX()) * (ship.getX() - sun.getX())
									+ (ship.getY() - sun.getY()) * (ship.getY() - sun.getY()));
							if (distNew < distMin) {
								distMin = distNew;
								selectedSun = sun;
							}
						}
					}
				}
			}
			// only if no 'color' suns found, take any one
			if (selectedSun == null) {
				for (int j = 0; j < suns.size(); j++) {
					sun = (Sun) suns.get(j);
					if (!sunExplored(sun)) {
						selectedSun = isThisSunCloserThanThePrevious(ship, sun);
					}
				}
			}
		}
		if (selectedSun != null) {
			String opt = ship.getCurrentOrder().getOption();
			String command = ship.getCurrentOrder().getCommand();
			ship.setCurrentOrder(new ShipOrderItem(opt, command, selectedSun.getName()));
		} else {
			// TEST
			// System.out.println("scoutForHabitablePlanets: All sunsystems
			// scouted!");
		}
	}

	private boolean sunExplored(Sun sun) {
		boolean explored = false;
		if (sun.isExploredBy(name)) {
			explored = true;
		}
		if (shipsTo.containsKey(sun.getNumber())) {
			explored = true;
		}
		return (explored);
	}

	public int getNextColonyNumber() {
		coloniesBuilt++;
		return coloniesBuilt;
	}

	protected void colonizePlanet(Planet planet, Ship ship) {
		planetList.remove(planet);
		if (!planet.isColonized()) {
			planet.colonize(ship);
		}
	}

	// called by MPlayer.setShipOrders
	// called by CPlayer.setShipOrders
	protected boolean setOrderToColonizePlanet(Ship ship, String target) {
		boolean foundOne = false;
		Planet colonizeThisPlanet = null;
		// auto colonize best (either from list or if found more than one??)
		if (target.equals(BEST)) {
			for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
				Planet planet = (Planet) planetList.elementAt(planetLoop);
				if (ship.canColonize(planet) && !planet.isColonized()) {
					planet.getValue(this);
					if (colonizeThisPlanet == null) {
						colonizeThisPlanet = planet;
					} else if (planet.getValue(this) > colonizeThisPlanet.getValue(this)) {
						colonizeThisPlanet = planet;
					}
				}
			}
		}
		// auto colonize next (found planet) in list
		if (target.equals(FIRST)) {
			// this Strategy ("next"/"first") has the limitation, that only the
			// next planet (in the
			// order they were discovered) gets colonized, but not necessarily
			// the
			// closest nor the best 8-(
			//
			// this leads to the error that ship B discovers a habitable planet
			// in
			// sunsystem D but returns to sunsystem C where ship A discovered
			// two habitable planets!
			for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
				Planet planet = (Planet) planetList.elementAt(planetLoop);
				if (ship.canColonize(planet) && !planet.isColonized()) {
					colonizeThisPlanet = planet;
					break;
				}
			}
		}
		// colonize the planet closest to the current position (if any, and of
		// those the first)
		if (target.equals(CLOSEST)) {
			for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
				Planet planet = (Planet) planetList.elementAt(planetLoop);
				if (!planet.isColonized()) {
					if (colonizeThisPlanet == null) {
						colonizeThisPlanet = planet;
					}
					if (ship.canColonize(planet)) {
						if (isThisPlanetCloser(ship, colonizeThisPlanet, planet)) {
							colonizeThisPlanet = planet;
						}
					}
				}
			}
			// System.out.println("Selected: " + colonizeThisPlanet.getName());
		}
		// TEST
		// System.out.println("target type: " + target);
		// System.out.println("colonize planet " +
		// colonizeThisPlanet.getName());
		// System.out.println("Planetlist contains:");
		// Test
		// for (int planetLoop=0; planetLoop<planetList.size(); planetLoop++) {
		// System.out.println(((Planet)planetList.elementAt(planetLoop)).getName());
		// }
		if (colonizeThisPlanet != null) {
			if (!shipsTo.containsKey(colonizeThisPlanet.getSun().getNumber())) {
				foundOne = true;
				ship.setCurrentOrder(new ShipOrderItem("", COLONIZE, colonizeThisPlanet.getName()));
			}
		}
		return foundOne;
	}

	public String getPreferredPlanetType() {
		return preferredPlanetType;
	}

	/**
	 * Position the first colony on a habitable planet or the first ship close
	 * to it
	 * 
	 * @param universe
	 * @param player
	 */
	protected void setPlayerPosition(Universe universe, Player player) {
		int x = 20;
		int y = 20;
		boolean found = false;
		int count = 0;
		List suns = universe.getSuns();
		if (suns.size() <= 1) {
			// ignore game initialization
			return;
		}
		while (!found) {
			count++;
			// random start so not all players start in the same sunsystems
			int start = (int) (Math.random() * suns.size());
			search: for (int i = start; i < suns.size(); i++) {
				Sun sun = (Sun) suns.get(i);
				Vector planets = sun.getPlanets();
				for (int j = 0; j < planets.size(); j++) {
					Planet planet = (Planet) planets.elementAt(j);
					if (planet.getType().equals(player.getPreferredPlanetType()) && planet.getNecessaryColonyModuleNr(player) == 1
							&& !planet.isColonized()) {
						if (sun.getX() < universe.getUniverseSize() - MAXSTARDIST) {
							x = sun.getX() + MINSTARDIST;
						} else {
							x = sun.getX() - MINSTARDIST;
						}
						y = sun.getY();
						planet.colonize(player);
						planet.addExploredByPlayer(player.getName());
						for (int k = 0; k < planets.size(); k++) {
							Planet add = (Planet) planets.elementAt(k);
							player.addPlanet(add);
						}
						found = true;
						break search;
					}
				}
			}
			if (count > 10) {
				found = true;
				System.out.println("Warning: Player " + player.getName() + " could not be positioned.");
			}
		}
		// Test
		if (player instanceof MPlayer) {
			Colony c = player.getColonies().get(0); // ugly
			// TODO: depending on difficulty level
			_ship = new Ship(this, (ShipDesign) shipDesigns.get(COLONY_SHIP), null, universe);
			_ship.setPosition(x, y);
			_ship.setTarget(x, y);
			_ship.setColony(c);
			addSpaceCraft(_ship);
			// _ship = new Ship(this, (ShipDesign) shipDesigns.get(SCOUT_SHIP),
			// null, universe);
			// _ship.setPosition(x, y);
			// _ship.setTarget(x, y);
			// _ship.setColony(c);
			// addSpaceCraft(_ship);
		}
		return;
	}

	public void setResearch() {
		List<Technology> possibleTechnologies = getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = possibleTechnologies.get(i);
			if (tech.getAllowStudy()) {
				if ((tech.getCls().equals("Physics")) && (getCurrentPhysics().equals(""))) {
					setCurrentPhysics(tech.getName());
				}
				if ((tech.getCls().equals("Biology")) && (getCurrentBiology().equals(""))) {
					setCurrentBiology(tech.getName());
				}
				if ((tech.getCls().equals("Mathematics")) && (getCurrentMathematics().equals(""))) {
					setCurrentMathematics(tech.getName());
				}
				if ((tech.getCls().equals("Social")) && (getCurrentSocial().equals(""))) {
					setCurrentSocial(tech.getName());
				}
				if ((tech.getCls().equals("Economy")) && (getCurrentEconomy().equals(""))) {
					setCurrentEconomy(tech.getName());
				}
				if ((tech.getCls().equals("Military")) && (getCurrentMilitary().equals(""))) {
					setCurrentMilitary(tech.getName());
				}
			}
		}
	}

}