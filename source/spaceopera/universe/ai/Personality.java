//The class Personality is the random personality for computer players in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import java.util.List;
import java.util.Vector;

import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;

/**
 * Each player has a personality. Currently only implemented for computer
 * players.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Personality implements SOConstants {

	private Universe universe = null;
	private Player player = null;
	private String type = "";
	private String attitude = "";
	private String scoutingStrategy = "";
	private int randomTraits = 0;
	private int preferredNumberOfColonies = 0;
	private int productionStrategy = 0;
	private String preferredPlanetType = "";
	private String colonizingStrategy = "";

	// Accessors
	public String getColonizingStrategy() {
		return (colonizingStrategy);
	}

	public String getPreferredPlanetType() {
		return (preferredPlanetType);
	}

	public int getPreferredNumberOfColonies() {
		return (preferredNumberOfColonies);
	}

	public String getScoutingStrategy() {
		return (scoutingStrategy);
	}

	// Constructor
	public Personality(Universe u, Player p) {
		// Personality 1: defensive, many arms, rather little industry and
		// research
		// Personality 2: expansionist, rather little industry and research
		// Personality 3: research-intensive
		// Personality 4: production-intensive
		// Personality 5..n:
		universe = u;
		player = p;
		// todo: personality strength depending on difficulty level and number
		// of opponents
		setPersonalityType();
		setPersonalityAttitude();
		setRandomTraits();
		setSciencePercentage();
		setResearchSettings();
		setColonizingStrategy();
		setScoutingStrategy();
		setOtherPlayerRelations();
		setProductionStrategy();
		// setColonyWorkDistribution(null);

		// Test
		// System.out.println("New personality for player " + player.getName() +
		// ": " +
		// attitude + ", " + type + ", " + randomTraits);
	}

	// Methods
	private void addBuildings() {

		// for each colony, check wether there are less than a number, say 6,
		// items in the
		// production queue. If so, add new items according to the rules below.
		//
		// Strategy 1: just add a number of random buildings
		// Strategy 2: add buildings according to preferences of personality
		// type (militarist, economist,...)
		// XXX: Other advanced strategy: For each building, check player
		// preferences and add if needed (???)
		// ...
		// XXX: compare/merge with Colony.effectsOfColonyManager(), especially
		// buildings like habitations and farms

		List<Colony> colonies = player.getColonies();
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony = colonies.get(i);
			buildShipYard(colony); // see if colony needs a shipyard
			// try to add some buildings if the productionQueue is not too full
			for (int j = colony.getProductionQueue().size(); j < 10; j++) {
				switch (productionStrategy) {
				case 0:
					// simplest strategy
					selectRandomBuilding(colony);
					break;
				case 1:
					// depending on personalityType
					selectBuildingForPersonality(colony);
					break;
				default:
					// simplest strategy
					selectRandomBuilding(colony);
				}
			}
		}
	}

	private void setColonizingStrategy() {
		// strategy:
		// "next": colonize next planet in list
		// "closest": colonize closest planet in list
		// XXX: "best": colonize 'best' planet in list, depending on player
		// attributes
		// and planet environment, resources, ...
		// Preferred planet types:
		// EARTHLIKE...
		int selectColonizingStrategy = (int) (Math.random() * 10);
		int planetType = (int) (Math.random() * 10);

		if (planetType < 1) {
			// 10%
			preferredPlanetType = MERCURYLIKE;
		} else if (planetType < 4) {
			// 30%
			preferredPlanetType = VENUSLIKE;
		} else if (planetType < 8) {
			// 40%
			preferredPlanetType = EARTHLIKE;
		} else {
			// 20%
			preferredPlanetType = MARSLIKE;
		}
		// Test
		// System.out.println("Preferred Planet type = " + preferredPlanetType);
		if (selectColonizingStrategy < 3) {
			// 30%
			colonizingStrategy = FIRST;
		} else if (selectColonizingStrategy < 8) {
			// 50%
			colonizingStrategy = CLOSEST;
		} else {
			// 20%
			colonizingStrategy = BEST;
		}
		// Test:
		// System.out.println("Colonizing strategy: " + colonizingStrategy);

	}

	public void setColonyBuildingList() {
		// take the preferences from class player
		// XXX: more variations in personality
		if (attitude.equals(AGGRESSIVE_ATTITUDE)) {
			// ships first, heavy on expansion and attack
			player.buildShips();
			addBuildings();
		}
		if (attitude.equals(DEFENSIVE_ATTITUDE)) {

			addBuildings();
			// if ok, build some ships
			// ships last, slow on expansion and attack
			if ((int) (Math.random() * randomTraits) < 3) {
				player.buildShips();
			}
		}
		if (attitude.equals(NEUTRAL_ATTITUDE)) {
			// mixed, some expansion, some attack.
			// add some ships, add some other buildings, depending on mood
			if ((int) (Math.random() * 6) > 2) {
				addBuildings();
			} else {
				player.buildShips();
			}
		}
	}

	public void setColonyWorkDistribution(Colony colony) {
		// first version: static allocation,
		// XXX respect necessities (colony with x population needs y farming),
		// adapt to changing situation (more population, empty production
		// queue...)
		int hours = 0;
		if (randomTraits > 5) {
			hours = (int) (Math.random() * 4);
		}

		if (type.equals("Warrior")) {
			colony.setProduction(80);
			colony.setResearch(20);
			colony.setCulture(0);
		} else if (type.equals("Researcher")) {
			colony.setProduction(60);
			colony.setResearch(30);
			colony.setCulture(10);
		} else if (type.equals("Trader")) {
			colony.setProduction(70);
			colony.setResearch(20);
			colony.setCulture(10);
		} else if (type.equals("Industrial")) {
			colony.setProduction(90);
			colony.setResearch(10);
			colony.setCulture(0);
		} else if (type.equals("Harmonic")) {
			colony.setProduction(60);
			colony.setResearch(20);
			colony.setCulture(20);
		}
	}

	public void setOtherPlayerRelations() {
		// XXX: other players relations
	}

	private void setPersonalityAttitude() {
		String[] attitudes = new String[] { AGGRESSIVE_ATTITUDE, DEFENSIVE_ATTITUDE, NEUTRAL_ATTITUDE };
		int i = (int) (Math.random() * attitudes.length);
		switch (i) {
		case 0:
			attitude = attitudes[i];
			preferredNumberOfColonies = 50 + (int) (Math.random() * 9999);
			break;
		case 1:
			attitude = attitudes[i];
			preferredNumberOfColonies = 3 + (int) (Math.random() * 25);
			break;
		case 2:
			attitude = attitudes[i];
			preferredNumberOfColonies = 30 + (int) (Math.random() * 999);
			break;
		}
	}

	// XXX: (???) "developer" (terraforming, ...), "diplomat" (trade, espionage,
	// ...), others?
	private void setPersonalityType() {
		String[] types = new String[] { "Warrior", "Researcher", "Trader", "Industrial", "Harmonic" };
		int i = (int) (Math.random() * types.length);
		type = types[i];
	}

	private void setProductionStrategy() {
		productionStrategy = (int) (Math.random() * 2); // 0=default,
														// 1=depending on
														// personality
		// TEST
		// System.out.println("production strategy = " + productionStrategy);

	}

	private void setRandomTraits() {
		randomTraits = (int) (Math.random() * 11);
	}

	public void setResearchSettings() {
		// first version: static allocation,
		// XXX: adapt to research progress (correct if too far in one area,
		// underdeveloped in another)
		if (type.equals("Warrior")) {
			player.setiPhysics(20);
			player.setiBiology(10);
			player.setiMathematics(16);
			player.setiSocial(10);
			player.setiEconomy(10);
			player.setiMilitary(34);
		} else if (type.equals("Researcher")) {
			player.setiPhysics(20);
			player.setiBiology(16);
			player.setiMathematics(20);
			player.setiSocial(14);
			player.setiEconomy(18);
			player.setiMilitary(12);
		} else if (type.equals("Trader")) {
			player.setiPhysics(14);
			player.setiBiology(14);
			player.setiMathematics(14);
			player.setiSocial(14);
			player.setiEconomy(30);
			player.setiMilitary(14);
		} else if (type.equals("Industrial")) {
			player.setiPhysics(16);
			player.setiBiology(14);
			player.setiMathematics(16);
			player.setiSocial(16);
			player.setiEconomy(24);
			player.setiMilitary(14);
		} else if (type.equals("Harmonic")) {
			player.setiPhysics(20);
			player.setiBiology(16);
			player.setiMathematics(16);
			player.setiSocial(16);
			player.setiEconomy(16);
			player.setiMilitary(16);
		}
		// Test
		// System.out.println("Player research settings: " +
		// player.getiPhysics()+", " +
		// player.getiBiology()+", " +
		// player.getiMathematics()+", " +
		// player.getiSocial()+", " +
		// player.getiEconomy()+", " +
		// player.getiMilitary() );
	}

	private void setSciencePercentage() {
		// set the percentage of research
		// XXX: select a research project from each list initially and when a
		// project completes
		// (the first one gets taken by default)
		if (type.equals("Warrior")) {
			player.setiPhysics(15);
			player.setiBiology(10);
			player.setiMathematics(20);
			player.setiSocial(5);
			player.setiEconomy(10);
			player.setiMilitary(40);
		}
		if (type.equals("Researcher")) {
			player.setiPhysics(20);
			player.setiBiology(15);
			player.setiMathematics(20);
			player.setiSocial(15);
			player.setiEconomy(15);
			player.setiMilitary(15);
		}
		if (type.equals("Trader")) {
			player.setiPhysics(15);
			player.setiBiology(15);
			player.setiMathematics(15);
			player.setiSocial(10);
			player.setiEconomy(30);
			player.setiMilitary(15);
		}
		if (type.equals("Industrial")) {
			player.setiPhysics(25);
			player.setiBiology(10);
			player.setiMathematics(20);
			player.setiSocial(10);
			player.setiEconomy(25);
			player.setiMilitary(10);
		}
		if (type.equals("Harmonic")) {
			player.setiPhysics(20);
			player.setiBiology(16);
			player.setiMathematics(16);
			player.setiSocial(16);
			player.setiEconomy(16);
			player.setiMilitary(16);
		}
		// randomize some
		if (randomTraits > 2) {
			int area = (int) (Math.random() * 6);
			switch (area) {
			case 0:
				player.setiBiology(player.getBiologyPercent() - randomTraits);
				break;
			case 1:
				player.setiEconomy(player.getEconomyPercent() - randomTraits);
				break;
			case 2:
				player.setiMathematics(player.getMathematicsPercent() - randomTraits);
				break;
			case 3:
				player.setiMilitary(player.getMilitaryPercent() - randomTraits);
				break;
			case 4:
				player.setiPhysics(player.getPhysicsPercent() - randomTraits);
				break;
			case 5:
				player.setiSocial(player.getSocialPercent() - randomTraits);
				break;
			default:
				player.setiSocial(player.getSocialPercent() - randomTraits);
			}
			area = (int) (Math.random() * 6);
			switch (area) {
			case 0:
				player.setiBiology(player.getBiologyPercent() + randomTraits);
				break;
			case 1:
				player.setiEconomy(player.getEconomyPercent() + randomTraits);
				break;
			case 2:
				player.setiMathematics(player.getMathematicsPercent() + randomTraits);
				break;
			case 3:
				player.setiMilitary(player.getMilitaryPercent() + randomTraits);
				break;
			case 4:
				player.setiPhysics(player.getPhysicsPercent() + randomTraits);
				break;
			case 5:
				player.setiSocial(player.getSocialPercent() + randomTraits);
				break;
			default:
				player.setiSocial(player.getSocialPercent() + randomTraits);
			}
		}
		// Test
		// System.out.println("Science Settings:" +
		// "\nBiology : " + player.getiBiology() +
		// "\nEconomy : " + player.getiEconomy() +
		// "\nMathematics: " + player.getiMathematics() +
		// "\nMilitary : " + player.getiMilitary() +
		// "\nPhysics : " + player.getiPhysics() +
		// "\nSocial : " + player.getiSocial());
	}

	private void setScoutingStrategy() {
		// different possible strategies:
		// "next": scout next closest unexplored sun
		// "area": scout all suns in a given area, increase area, repeat
		// "yellow", "red", "blue", "green": scout all yellow, red, blue suns
		// first
		// XXX: also consider ship range in algorithms
		// maybe scout only suns with at least one planet or so, depending on
		// on AI difficulty level?
		int selectScoutingStrategy = (int) (Math.random() * 15);
		if (selectScoutingStrategy < 6) {
			scoutingStrategy = NEXT;
		} else if (selectScoutingStrategy < 10) {
			scoutingStrategy = AREA;
		} else if (selectScoutingStrategy == 10) {
			scoutingStrategy = WHITE;
		} else if (selectScoutingStrategy == 11) {
			scoutingStrategy = YELLOW;
		} else if (selectScoutingStrategy == 12) {
			scoutingStrategy = RED;
		} else if (selectScoutingStrategy == 13) {
			scoutingStrategy = BLUE;
		} else if (selectScoutingStrategy == 14) {
			scoutingStrategy = GREEN;
		}
		// Test:
		// System.out.println("Scouting strategy set: " + scoutingStrategy);

	}

	private void selectRandomBuilding(Colony colony) {
		Vector possibleBuildings = colony.getPossibleBuildings();
		for (int numtries = 0; numtries < 5; numtries++) {
			int select = (int) (Math.random() * (possibleBuildings.size()));
			// in the first 100 to 200 turns, build some extra factories and
			// mines...
			if (universe.getTurnNr() < (100 + (int) (Math.random() * 100))) {
				if ((int) (Math.random() * 3) > 1) {
					select = (int) (Math.random() * 2);
				}
			}
			Building building = (Building) possibleBuildings.elementAt(select);
			if (permitted(building)) {
				addSelectedBuilding(colony, building);
				break;
			}
		}
	}

	private void selectBuildingForPersonality(Colony colony) {
		if (Math.random() < 0.2) {
			// 20% chance to add a building preferred by personality type
			if (type.equals("Warrior")) {
				selectMilitaryBuilding(colony);
			}
			if (type.equals("Researcher")) {
				selectResearchBuilding(colony);
			}
			if (type.equals("Trader")) {
				selectTradeBuilding(colony);
			}
			if (type.equals("Industrial")) {
				selectIndustrialBuilding(colony);
			}
			if (type.equals("Harmonic")) {
				selectRandomBuilding(colony);
			}
		} else {
			// 80% fallback to default building strategy
			selectRandomBuilding(colony);
		}
	}

	private void selectMilitaryBuilding(Colony colony) {
		Vector possibleBuildings = colony.getPossibleBuildings();
		Building building = null;
		for (int numtries = 0; numtries < 5; numtries++) {
			int select = (int) (Math.random() * (possibleBuildings.size()));
			building = (Building) possibleBuildings.elementAt(select);
			if (building.getType().equals(MILITARY)) {
				// first choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
			if (building.getType().equals(PHYSICS) || building.getType().equals(BIOLOGY) || building.getType().equals(MATHEMATICS)
					|| building.getType().equals(ECONOMY)) {
				// second choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
		}
	}

	private void selectResearchBuilding(Colony colony) {
		Vector possibleBuildings = colony.getPossibleBuildings();
		Building building = null;
		for (int numtries = 0; numtries < 5; numtries++) {
			int select = (int) (Math.random() * (possibleBuildings.size()));
			building = (Building) possibleBuildings.elementAt(select);
			if (building.getType().equals(SOCIAL)) {
				// first choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
			if (building.getType().equals(PHYSICS) || building.getType().equals(BIOLOGY) || building.getType().equals(MATHEMATICS)) {
				// second choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
		}
	}

	private void selectTradeBuilding(Colony colony) {
		Vector possibleBuildings = colony.getPossibleBuildings();
		Building building = null;
		for (int numtries = 0; numtries < 5; numtries++) {
			int select = (int) (Math.random() * (possibleBuildings.size()));
			building = (Building) possibleBuildings.elementAt(select);
			if (building.getType().equals(ECONOMY)) {
				// first choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
			if (building.getType().equals(SOCIAL) || building.getType().equals(BIOLOGY) || building.getType().equals(PHYSICS)) {
				// second choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
		}
	}

	private void selectIndustrialBuilding(Colony colony) {
		Vector possibleBuildings = colony.getPossibleBuildings();
		Building building = null;
		for (int numtries = 0; numtries < 5; numtries++) {
			int select = (int) (Math.random() * (possibleBuildings.size()));
			building = (Building) possibleBuildings.elementAt(select);
			if (building.getType().equals(ECONOMY)) {
				// first choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
			if (building.getType().equals(PHYSICS)) {
				// second choice
				if (permitted(building)) {
					addSelectedBuilding(colony, building);
					break;
				}
			}
		}
	}

	private void addSelectedBuilding(Colony colony, Building building) {
		if (colony.getProductionQueueCount(building.getName()) < 1) {
			colony.addProductionQueue(building.clone());
			colony.removePossibleBuildings(building);
			// TEST
			// System.out.println("Add building to colony queue: " +
			// building.getName());
		} else {
			// TEST
			// System.out.println("Building not added: " + building.getName());
		}
	}

	private void buildShipYard(Colony colony) {
		// XXX: with a Hashtable, this loop could be eliminated!
		Vector possibleBuildings = colony.getPossibleBuildings();
		for (int j = 0; j < possibleBuildings.size(); j++) {
			Building building = (Building) possibleBuildings.elementAt(j);
			if (building.getName().equals("Orbital shipyard")) {
				colony.addProductionQueue(building.clone());
				colony.removePossibleBuildings(building);
				break;
			}
		}

	}

	// XXX: exclude other buildings managed by colony manager
	// (colony.effectsOfColonyManager()) if needed
	private boolean permitted(Building building) {
		return building.getAllowBuild() && !building.getName().equals(HABITAT) && !building.getName().equals(FARM);

	}

	public String getAttitude() {
		return attitude;
	}
}