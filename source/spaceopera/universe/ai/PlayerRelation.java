//The class PlayerRelation describes the relation between two SpaceOpera players
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;


/**
 * A PlayerRelation describes the relation from one to another player.
 * Player A may have other feelings for player B than vice versa.
 *  
 */
public class PlayerRelation {
	private Player relationTo;
	private float relationLevel;  
	// -1 = war
	//  0 = neutral
	// +1 = peace  
	
	//more ideas:
	//trade Agreement
	//alliance
	//technology Exchange
	//peace treaty
	//...
	
	
	public Player getRelationTo()       {return(relationTo);}
	public float getRelationLevel()     {return(relationLevel);}
	public void changeRelation(float f) {
		relationLevel+=f;
		if (relationLevel<-1) {
			relationLevel=-1;
		}
		if (relationLevel>1) {
			relationLevel=1;
		}
	}
	
	
	public PlayerRelation(Player player) {
		relationTo = player;
		relationLevel = (float)(0.5 - Math.random()*1.0);
		//XXX: some players can't stand each other ...
		//System.out.println("Player " + player.getName() + "relationLevel = " + relationLevel);
	}
	
}
