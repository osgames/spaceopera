//The class CPlayer is the main class for Computerplayers with AI and so in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import java.util.*;
import java.awt.*;

import spaceopera.universe.Universe;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.SpaceCraft;

/**
 * Class CPlayer is used for computer players and contains some AI for
 * setThisShipsOrders() and setColonyOrders(), later also for setResearch(), and
 * other functions; Todo: different AI versions
 */
@SuppressWarnings("rawtypes")
public class CPlayer extends Player {

	private Planet selectedPlanet = null;
	private Personality personality = null;
	private AiShipBuilder shipBuilder = null;
	int orderedDefenders = 0;
	int orderedScoutShips = 0;
	int orderedColonyShips = 0;

	// Methods

	// XXX: when building new ships, sometimes it may be necessary to scrap old
	// ones?
	public void buildShips() {
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony =  colonies.get(i);
			Vector possibleProducts = colony.getPossibleProducts();
			for (int j = 0; j < possibleProducts.size(); j++) {
				Product product = (Product) possibleProducts.elementAt(j);
				// TEST
				// System.out.println("CPlayer.buildShips: " +
				// product.getName());

				if (product.getName().equals(SCOUT_SHIP)) {
					if ((countScoutShips() + orderedScoutShips < 2)) {
						if (colony.getProductionQueueCount(SCOUT_SHIP) < 1) {
							orderedScoutShips++;
							colony.addProductionQueue(product.clone());
							break;
						}

					}
				} else if (product.getName().equals(COLONY_SHIP)) {
					if (countColonizablePlanets() > countColonyShips() + orderedColonyShips) {
						if ((colonies.size() + countColonyShips() + orderedColonyShips) <= personality
								.getPreferredNumberOfColonies()) {
							if (colony.getProductionQueueCount(COLONY_SHIP) < 1) {
								orderedColonyShips++;
								colony.addProductionQueue(product.clone());
								break;
							}
						}
					}
				} else if (product.getName().equals(DEFENDER)) {
					if (countScoutShips() > 0 && colony.getProductionQueueCount(DEFENDER) < 1 && orderedDefenders < 5) {
						orderedDefenders++;
						// XXX: rebuild destroyed defenders.
						colony.addProductionQueue(product.clone());
						break;
					}
				}
			}
		}
	}

	private int countColonizablePlanets() {
		int count = 0;
		for (int i = 0; i < planetList.size(); i++) {
			Planet planet = (Planet) planetList.elementAt(i);
			if (planet.getType().equals(personality.getPreferredPlanetType())) {
				count++;
			}
		}
		return (count);
	}

	// called by SpaceOpera.otherPlayersMakeTheirTurns
	public void colonizePlanetNow() {
		for (int i = 0; i < ships.size(); i++) {
			Ship ship = (Ship) ships.get(i);
			if ((ship.getCurrentOrder().getCommand().equals(COLONIZE)) && !ship.isTraveling()) {
				if ((ship.getTargetSun() != null) && (ship.getPlanet() != null)) {
					Planet planet = ship.getPlanet();
					if (ship.getShipDesign().getColonyModuleNumber() >= planet.getNecessaryColonyModuleNr(this)) {
						colonizePlanet(planet, ship);
					} else {
						this.setOrderToColonizePlanet(ship, colonizingStrategy);
					}
				}
			}
		}
	}

	private void correctPossibleBuildings() {
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony = colonies.get(i);
			Vector possibleBuildings = colony.getPossibleBuildings();
		}
	}

	private int countScoutShips() {
		int numScoutShips = 0;
		for (int i = 0; i < ships.size(); i++) {
			Ship rs = (Ship) ships.get(i);
			if (rs.getShipClass().equals(SCOUT_SHIP)) {
				numScoutShips++;
			}
		}
		return numScoutShips;
	}

	private int countColonyShips() {
		int numColonyShips = 0;
		for (int i = 0; i < ships.size(); i++) {
			Ship rs = (Ship) ships.get(i);
			if (rs.getShipClass().equals(COLONY_SHIP)) {
				numColonyShips++;
			}
		}
		return numColonyShips;
	}

	public CPlayer(Universe u, String playerName, Color c) {
		super(u, playerName, c);
		personality = new Personality(u, this);
		shipBuilder = new AiShipBuilder(this);
		shipBuilder.createBasicShipDesigns();
		scoutingStrategy = personality.getScoutingStrategy();
		colonizingStrategy = personality.getColonizingStrategy();
		preferredPlanetType = personality.getPreferredPlanetType();
		setPreferredPlanetValues();
		setPlayerPosition(u, this);
	}

	private void distributeColonyProduction() {
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony =  colonies.get(i);
			personality.setColonyWorkDistribution(colony);
		}
	}

	// XXX: set other preferred values
	// XXX: introduce some randomness for different species? (also for
	// earthlike planets)
	// (some ideals might center around 0.0 - 0.3, others around 1.7 - 2.0 ...)
	private void setPreferredPlanetValues() {
		if (preferredPlanetType.equals(MERCURYLIKE)) {
			// temperature
			preferredPlanetValues.setLowerExtremeTemperature(-10);
			preferredPlanetValues.setLowerAcceptableTemperature(10);
			preferredPlanetValues.setLowerGoodTemperature(25);
			preferredPlanetValues.setLowerIdealTemperature(40);
			preferredPlanetValues.setUpperIdealTemperature(80);
			preferredPlanetValues.setUpperGoodTemperature(120);
			preferredPlanetValues.setUpperAcceptableTemperature(150);
			preferredPlanetValues.setUpperExtremeTemperature(200);
			// atmospheric pressure 0..5?
			preferredPlanetValues.setLowerExtremeAtmosphericPressure(0.2f);
			preferredPlanetValues.setLowerAcceptableAtmosphericPressure(0.3f);
			preferredPlanetValues.setLowerIdealAtmosphericPressure(0.5f);
			preferredPlanetValues.setUpperIdealAtmosphericPressure(0.6f);
			preferredPlanetValues.setUpperAcceptableAtmosphericPressure(0.7f);
			preferredPlanetValues.setUpperExtremeAtmosphericPressure(0.8f);
			// radiation 0..5000?
			preferredPlanetValues.setLowerExtremeRadiation(500);
			preferredPlanetValues.setLowerAcceptableRadiation(800);
			preferredPlanetValues.setLowerIdealRadiation(1000);
			preferredPlanetValues.setUpperIdealRadiation(1200);
			preferredPlanetValues.setUpperAcceptableRadiation(1400);
			preferredPlanetValues.setUpperExtremeRadiation(1600);
			// atmospheric composition 0..2
			preferredPlanetValues.setLowerExtremeAtmosphericComposition(0.0f);
			preferredPlanetValues.setLowerAcceptableAtmosphericComposition(0.1f);
			preferredPlanetValues.setLowerIdealAtmosphericComposition(0.3f);
			preferredPlanetValues.setUpperIdealAtmosphericComposition(0.5f);
			preferredPlanetValues.setUpperAcceptableAtmosphericComposition(0.8f);
			preferredPlanetValues.setUpperExtremeAtmosphericComposition(1.2f);
			// gravitation
			preferredPlanetValues.setLowerExtremeGravitation(0.2f);
			preferredPlanetValues.setLowerAcceptableGravitation(0.3f);
			preferredPlanetValues.setLowerIdealGravitation(0.4f);
			preferredPlanetValues.setUpperIdealGravitation(0.6f);
			preferredPlanetValues.setUpperAcceptableGravitation(0.7f);
			preferredPlanetValues.setUpperExtremeGravitation(0.8f);
			// seasons 0-2
			preferredPlanetValues.setLowerPoorSeasons(0.0f);
			preferredPlanetValues.setLowerAcceptableSeasons(0.1f);
			preferredPlanetValues.setLowerIdealSeasons(0.2f);
			preferredPlanetValues.setUpperIdealSeasons(0.6f);
			preferredPlanetValues.setUpperAcceptableSeasons(0.8f);
			preferredPlanetValues.setUpperPoorSeasons(1.1f);
			// water-land ratio mercury 0-0.6, venus: 0.6-1.2, mars: 0.3 - 0.6
			preferredPlanetValues.setLowerExtremeWaterLandRatio(0.0f);
			preferredPlanetValues.setLowerAcceptableWaterLandRatio(0.1f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(0.2f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(0.4f);
			preferredPlanetValues.setUpperAcceptableWaterLandRatio(0.5f);
			preferredPlanetValues.setUpperExtremeWaterLandRatio(0.6f);
			// windSpeed

		} else if (preferredPlanetType.equals(VENUSLIKE)) {
			// temperature
			preferredPlanetValues.setLowerExtremeTemperature(0);
			preferredPlanetValues.setLowerAcceptableTemperature(10);
			preferredPlanetValues.setLowerGoodTemperature(25);
			preferredPlanetValues.setLowerIdealTemperature(40);
			preferredPlanetValues.setUpperIdealTemperature(55);
			preferredPlanetValues.setUpperGoodTemperature(70);
			preferredPlanetValues.setUpperAcceptableTemperature(90);
			preferredPlanetValues.setUpperExtremeTemperature(100);
			// atmospheric pressure
			preferredPlanetValues.setLowerExtremeAtmosphericPressure(0.2f);
			preferredPlanetValues.setLowerAcceptableAtmosphericPressure(0.4f);
			preferredPlanetValues.setLowerIdealAtmosphericPressure(0.6f);
			preferredPlanetValues.setUpperIdealAtmosphericPressure(1.0f);
			preferredPlanetValues.setUpperAcceptableAtmosphericPressure(1.3f);
			preferredPlanetValues.setUpperExtremeAtmosphericPressure(1.5f);
			// radiation
			preferredPlanetValues.setLowerExtremeRadiation(300);
			preferredPlanetValues.setLowerAcceptableRadiation(500);
			preferredPlanetValues.setLowerIdealRadiation(600);
			preferredPlanetValues.setUpperIdealRadiation(800);
			preferredPlanetValues.setUpperAcceptableRadiation(1000);
			preferredPlanetValues.setUpperExtremeRadiation(1200);
			// atmospheric composition 0..2
			preferredPlanetValues.setLowerExtremeAtmosphericComposition(0.1f);
			preferredPlanetValues.setLowerAcceptableAtmosphericComposition(0.3f);
			preferredPlanetValues.setLowerIdealAtmosphericComposition(0.5f);
			preferredPlanetValues.setUpperIdealAtmosphericComposition(0.8f);
			preferredPlanetValues.setUpperAcceptableAtmosphericComposition(1.2f);
			preferredPlanetValues.setUpperExtremeAtmosphericComposition(1.6f);
			// XXX: gravitation
			preferredPlanetValues.setLowerExtremeGravitation(0.4f);
			preferredPlanetValues.setLowerAcceptableGravitation(0.5f);
			preferredPlanetValues.setLowerIdealGravitation(0.6f);
			preferredPlanetValues.setUpperIdealGravitation(0.9f);
			preferredPlanetValues.setUpperAcceptableGravitation(1.1f);
			preferredPlanetValues.setUpperExtremeGravitation(1.3f);
			// seasons 0.1 - 2
			preferredPlanetValues.setLowerPoorSeasons(0.3f);
			preferredPlanetValues.setLowerAcceptableSeasons(0.5f);
			preferredPlanetValues.setLowerIdealSeasons(0.7f);
			preferredPlanetValues.setUpperIdealSeasons(0.8f);
			preferredPlanetValues.setUpperAcceptableSeasons(1.0f);
			preferredPlanetValues.setUpperPoorSeasons(1.3f);
			// water-land ratio mercury 0-0.6, venus: 0.6-1.2, mars: 0.3 - 0.6
			preferredPlanetValues.setLowerExtremeWaterLandRatio(0.6f);
			preferredPlanetValues.setLowerAcceptableWaterLandRatio(0.7f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(0.8f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(1.0f);
			preferredPlanetValues.setUpperAcceptableWaterLandRatio(1.1f);
			preferredPlanetValues.setUpperExtremeWaterLandRatio(1.2f);

		} else if (preferredPlanetType.equals(EARTHLIKE)) {
			// do nothing: default values from class PreferredPlanetValues are
			// ok
		} else if (preferredPlanetType.equals(MARSLIKE)) {
			// temperature
			preferredPlanetValues.setLowerExtremeTemperature(-20);
			preferredPlanetValues.setLowerAcceptableTemperature(0);
			preferredPlanetValues.setLowerGoodTemperature(10);
			preferredPlanetValues.setLowerIdealTemperature(20);
			preferredPlanetValues.setUpperIdealTemperature(30);
			preferredPlanetValues.setUpperGoodTemperature(50);
			preferredPlanetValues.setUpperAcceptableTemperature(60);
			preferredPlanetValues.setUpperExtremeTemperature(80);
			// atmospheric pressure
			preferredPlanetValues.setLowerExtremeAtmosphericPressure(0.3f);
			preferredPlanetValues.setLowerAcceptableAtmosphericPressure(0.5f);
			preferredPlanetValues.setLowerIdealAtmosphericPressure(0.7f);
			preferredPlanetValues.setUpperIdealAtmosphericPressure(1.1f);
			preferredPlanetValues.setUpperAcceptableAtmosphericPressure(1.4f);
			preferredPlanetValues.setUpperExtremeAtmosphericPressure(1.8f);
			// radiation
			preferredPlanetValues.setLowerExtremeRadiation(300);
			preferredPlanetValues.setLowerAcceptableRadiation(400);
			preferredPlanetValues.setLowerIdealRadiation(500);
			preferredPlanetValues.setUpperIdealRadiation(700);
			preferredPlanetValues.setUpperAcceptableRadiation(800);
			preferredPlanetValues.setUpperExtremeRadiation(900);
			// atmospheric composition 0..2
			preferredPlanetValues.setLowerExtremeAtmosphericComposition(0.2f);
			preferredPlanetValues.setLowerAcceptableAtmosphericComposition(0.4f);
			preferredPlanetValues.setLowerIdealAtmosphericComposition(0.6f);
			preferredPlanetValues.setUpperIdealAtmosphericComposition(1.0f);
			preferredPlanetValues.setUpperAcceptableAtmosphericComposition(1.3f);
			preferredPlanetValues.setUpperExtremeAtmosphericComposition(1.7f);
			// gravitation
			preferredPlanetValues.setLowerExtremeGravitation(0.4f);
			preferredPlanetValues.setLowerAcceptableGravitation(0.5f);
			preferredPlanetValues.setLowerIdealGravitation(0.6f);
			preferredPlanetValues.setUpperIdealGravitation(0.8f);
			preferredPlanetValues.setUpperAcceptableGravitation(1.0f);
			preferredPlanetValues.setUpperExtremeGravitation(1.3f);
			// seasons 0.5 - 2
			preferredPlanetValues.setLowerPoorSeasons(0.8f);
			preferredPlanetValues.setLowerAcceptableSeasons(1.2f);
			preferredPlanetValues.setLowerIdealSeasons(1.4f);
			preferredPlanetValues.setUpperIdealSeasons(1.7f);
			preferredPlanetValues.setUpperAcceptableSeasons(1.8f);
			preferredPlanetValues.setUpperPoorSeasons(2.0f);
			// water-land ratio mercury 0-0.6, venus: 0.6-1.2, mars: 0.3 - 0.9
			preferredPlanetValues.setLowerExtremeWaterLandRatio(0.3f);
			preferredPlanetValues.setLowerAcceptableWaterLandRatio(0.4f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(0.5f);
			preferredPlanetValues.setLowerIdealWaterLandRatio(0.7f);
			preferredPlanetValues.setUpperAcceptableWaterLandRatio(0.8f);
			preferredPlanetValues.setUpperExtremeWaterLandRatio(0.9f);
		} else {
			// do nothing: default values from class PreferredPlanetValues are
			// ok
		}

	}

	public void setColonyOrders() {
		distributeColonyProduction();
		correctPossibleBuildings();
		personality.setColonyBuildingList();
	}

	/**
	 * Orders for one ship
	 * 
	 * @param ship //
	 *            XXX: add transport, defend, attack... commands as defined in
	 *            ShipOrders
	 */
	protected void setThisShipsOrders(SpaceCraft sc) {
		if (sc.isShip()) {
			Ship ship = (Ship) sc;
			if (ship.getShipDesign().getColonyModuleNumber() > 0) {
				// System.out.println("colonizingstrategy="
				// +colonizingStrategy);
				if (!setOrderToColonizePlanet(ship, colonizingStrategy)) {
					// System.out.println("found none");
					scoutForHabitablePlanets(ship);
				}
			} else {
				if (ship.hasInterstellarDrive()) {
					scoutForHabitablePlanets(ship);
				}
			}
		}
	}

	public void newShipsCanBeBuilt(ShipComponent shipComp) {
		shipBuilder.newShipsCanBeBuilt(shipComp);
	}

	public Personality getPersonality() {
		return personality;
	}

	/**
	 * @return Returns the orderedColonyShips.
	 */
	public int getOrderedColonyShips() {
		return orderedColonyShips;
	}

	/**
	 * @return Returns the orderedDefenders.
	 */
	public int getOrderedDefenders() {
		return orderedDefenders;
	}

	/**
	 * @return Returns the orderedScoutShips.
	 */
	public int getOrderedScoutShips() {
		return orderedScoutShips;
	}

	/**
	 * @param orderedColonyShips The orderedColonyShips to set.
	 */
	public void setOrderedColonyShips(int orderedColonyShips) {
		this.orderedColonyShips = orderedColonyShips;
	}

	/**
	 * @param orderedDefenders The orderedDefenders to set.
	 */
	public void setOrderedDefenders(int orderedDefenders) {
		this.orderedDefenders = orderedDefenders;
	}

	/**
	 * @param orderedScoutShips The orderedScoutShips to set.
	 */
	public void setOrderedScoutShips(int orderedScoutShips) {
		this.orderedScoutShips = orderedScoutShips;
	}

}
