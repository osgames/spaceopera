//The class AiShipTemplate is used as template for every shiptype like 'defender', 'colonyship', ...
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import spaceopera.universe.SOConstants;

public class AiShipTemplate implements SOConstants {

    //The used shipComponents for this shiptype are stored in this array e.g. as true,true,false,true,false....
	//codes from SOCOnstants:
	//	  int SUBLIGHT_DRIVE_TYPE = 1;
	//	  int FTL_DRIVE_TYPE = 2;
	//	  int BEAM_WEAPON_TYPE = 3;
	//	  int MISSILE_WEAPON_TYPE = 4;
	//	  int PARTICLE_WEAPON_TYPE = 5;
	//	  int BOMB_WEAPON_TYPE = 6;
	//	  int ORE_SEEKER_TYPE = 7;
	//	  int TACTICAL_TYPE = 8;
	//	  int FUEL_TANK_TYPE = 9;
	//	  int FUEL_SCOOP_TYPE = 10;
	//	  int CARGO_TYPE = 11;
	//	  int MINE_DEPLOYER_TYPE = 12;
	//	  int TARGET_COMPUTER_TYPE = 13;
	//	  int SCANNER_TYPE = 14;
	//	  int ARMOUR_TYPE = 15;
	//	  int SHIELDING_TYPE = 16;
	//	  int COLONY_MODULE_TYPE = 17;			
	
	//XXX: currently, only 17 shipComponent types, but watch this
	private boolean [] componentTypes = new boolean[AI_SHIP_TEMPLATE_ARRAY_SIZE];	
	private String shipDesignName = "";
	private String hullType = "";
	
	//XXX: instead of a boolean, define with what to fill (e.g. weapons, cargo, armour)??
	boolean fill = false; //fill the ship with weapons and such up to capacity
	
	
	public void setComponentType(int i) {
		componentTypes[i]=true;
	}
	public boolean getComponentType(int i) {
		return(componentTypes[i]);
	}
	public String getHullType() {
		return hullType;
	}
	public void setHullType(String hullType) {
		this.hullType = hullType;
	}
	public String getShipDesignName() {
		return shipDesignName;
	}
	public void setShipDesignName(String shipDesignName) {
		this.shipDesignName = shipDesignName;
	}
	public int getComponentTypesLength() {
		return componentTypes.length;
	}
	public boolean isFill() {
		return fill;
	}
	public void setFill(boolean fill) {
		this.fill = fill;
	}
	
}
