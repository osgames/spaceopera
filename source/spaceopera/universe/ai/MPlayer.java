//The class MPlayer is the huMan player (Menschlicher Spieler) in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import java.awt.Color;
import java.util.List;
import java.util.Vector;

import spaceopera.universe.Universe;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipOrderItem;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The human player class
 */
@SuppressWarnings({"rawtypes"})
public class MPlayer extends Player {

	public MPlayer(Universe universe, String player, Color c) {
		super(universe, player, c);
		preferredPlanetType = EARTHLIKE;
		setPlayerPosition(universe, this);
		setResearch();
	}

	/**
	 * XXX: synchronize this with CPlayer Make the first of the nextOrderItems
	 * to the currentOrder, except when there is a colonizable planet in the
	 * current system and the order is colonize
	 */
	protected void setThisShipsOrders(SpaceCraft sc) {
		if (sc.isShip()) {
			Ship ship = (Ship) sc;
			Vector nextOrderItems = ship.getNextOrderItems();
			if (nextOrderItems.size() > 0) {
				// make the first 'next order' to the 'current order'
				ShipOrderItem order = (ShipOrderItem) nextOrderItems.elementAt(0);
				String option = order.getOption();
				String command = order.getCommand();
				String target = order.getTarget();
				String cargo = order.getCargo();
				String fill = order.getFill();

				if (command.equals(COLONIZE)) {
					if (!colonizePlanetNow(ship, option, command, target)) {
						if (option.equals(AUTO)) {
							// XXX: check planetList first: are there
							// suitable planets to colonize?
							if (!setOrderToColonizePlanet(ship, target)) {
								scoutForHabitablePlanets(ship);
							}
						} else {
							// find target to colonize
							for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
								Planet planet = (Planet) planetList.elementAt(planetLoop);
								if (planet.getName().equals(target) && !planet.isColonized()) {
									ship.setCurrentOrder(new ShipOrderItem("", COLONIZE, planet.getName()));
									break; // planet loop
								}
							}
						}
					}
				}

				if (command.equals(EXPLORE)) {
					if (option.equals(AUTO)) {
						scoutForHabitablePlanets(ship);
					}
					// Warning: else part is in the next if statement
				}

				if (command.equals(GOTO) || command.equals(EXPLORE)) {
					// goto target (explore, transport, ....)
					List suns = universe.getSuns();
					for (int sunSystemLoop = 0; sunSystemLoop < suns.size(); sunSystemLoop++) {
						Sun sun = (Sun) suns.get(sunSystemLoop);
						if (sun.getName().equals(target)) {
							ship.setCurrentOrder(new ShipOrderItem("", EXPLORE, sun.getName()));
							nextOrderItems.removeElementAt(0);
						}
					}
				}
				if (command.equals(TRANSPORT)) {
					// sample: transport iron to Money 3, fill
					for (int colonyLoop = 0; colonyLoop < colonies.size(); colonyLoop++) {
						Colony colony = colonies.get(colonyLoop);
						if (colony.getPlanetName().equals(target)) {
							ship.setCurrentOrder(order);
							nextOrderItems.removeElementAt(0);
						}
					}
				}
			}
		}
	}

	// When colonizing a planet in a newly arrived-at system, the 'current'
	// order will be the final order
	// called by MPlayer.setThisShipsOrders()
	private boolean colonizePlanetNow(Ship ship, String option, String command, String target) {
		boolean lastOrder = false;
		if (command.equals(COLONIZE)) {
			// colonize planet in current system?
			if (option.equals(AUTO)) {
				if (target.equals(BEST)) { // auto colonize best (either from
					// list or if found more than one??)
					Planet colonizeThisPlanet = null;
					for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
						Planet planet = (Planet) planetList.elementAt(planetLoop);
						planet.getValue(ship.getPlayer());
						// System.out.println("Planet " + planet.getName() + "
						// of type " + planet.getType() + " in list.");
						if (planet.getSunSystem().equals(ship.getFromSunSystem())) {
							if (ship.canColonize(planet)) {
								if (colonizeThisPlanet == null) {
									colonizeThisPlanet = planet;
								} else if (planet.getValue(this) > colonizeThisPlanet.getValue(this)) {
									colonizeThisPlanet = planet;
								}
							}
						}
					}
					if (colonizeThisPlanet != null) {
						colonizePlanet(colonizeThisPlanet, ship);
						lastOrder = true;
					}
				}
				if (target.equals(FIRST)) { // auto colonize next (found planet)
					for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
						Planet planet = (Planet) planetList.elementAt(planetLoop);
						// System.out.println("Planet " + planet.getName() + "
						// of type " + planet.getType() + " in list.");
						if (planet.getSunSystem().equals(ship.getFromSunSystem())) {
							if (ship.canColonize(planet)) {
								colonizePlanet(planet, ship);
								lastOrder = true;
							}
						}
					}
				}
				if (target.equals(CLOSEST)) {

					for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
						Planet planet = (Planet) planetList.elementAt(planetLoop);
						if (planet.getName().equals(ship.getCurrentOrder().getTarget())) {
							colonizePlanet(planet, ship);
							lastOrder = true;
							break;
						}
					}
				}

			} else {
				for (int planetLoop = 0; planetLoop < planetList.size(); planetLoop++) {
					Planet planet = (Planet) planetList.elementAt(planetLoop);
					// colonize if already there
					if (planet.getSunSystem().equals(ship.getFromSunSystem())) {
						if (planet.getName().equals(target)) {
							colonizePlanet(planet, ship);
							lastOrder = true;
						}
					}
				}
			}
		}
		return lastOrder;
	}

}