//The class AiShipBuilder defines the possible shipDesigns for computer players
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ai;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.Product;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.ShipDesign;

/**
 * The class AiShipBuilder defines the possible shipDesigns for computer
 * players, selects 'random' designs depending on personality type and
 * adds/removes the shipbuilding projects to the computerplayer's colonies
 * 
 * ships consist of a hull and several components in the following areas:
 * DRIVES, ELECTRONICS, SHIELDING, SPECIAL, WEAPONS
 * 
 * satellites consist of a hull and several components in the following areas:
 * ELECTRONICS, SHIELDING, WEAPONS
 * 
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class AiShipBuilder implements SOConstants {

	private CPlayer player;

	private AiShipTemplate scoutTemplate;
	private AiShipTemplate defenderTemplate;
	private AiShipTemplate escortTemplate;
	private AiShipTemplate smallTransportTemplate;
	private AiShipTemplate frigateTemplate;
	private AiShipTemplate colonyShipTemplate;

	private Hashtable components;
	private Vector products;
	private Hashtable shipDesigns;

	public AiShipBuilder(CPlayer player) {
		this.player = player;
		components = player.getPossibleComponents(); // available
		// shipcomponents
		products = player.getPossibleProducts(); // existing shipdesigns
		shipDesigns = player.getShipDesigns(); // available shipDesigns

		scoutTemplate = createScout();
		defenderTemplate = createDefender();
		escortTemplate = createEscort();
		smallTransportTemplate = createSmallTransport();
		frigateTemplate = createFrigate();
		colonyShipTemplate = createColonyShip();

		// XXX: satellites

	}

	public void createBasicShipDesigns() {
		ShipDesign design;
		Product product;

		design = createShipDesign(SCOUT_SHIP, scoutTemplate);
		product = createProduct(design);
		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);
		// TEST
		//System.out.println("Player name = " + player.getName() + ", attitude = " + player.getPersonality().getAttitude());
		//System.out.println("\nSCOUT_SHIP: \n" + design);

		design = createShipDesign(DEFENDER, defenderTemplate);
		product = createProduct(design);
		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);
		// TEST
		//System.out.println("\nDEFENDER: \n" + design);

		design = createShipDesign(ESCORT, escortTemplate);
		product = createProduct(design);
		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);
		// TEST
		//System.out.println("\nESCORT: \n" + design);

		// XXX: large transport or only one transport
		design = createShipDesign(SMALL_TRANSPORT, smallTransportTemplate);
		product = createProduct(design);
		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);
		// TEST
		//System.out.println("\nSMALL_TRANSPORT: \n" + design);

		design = createShipDesign(FRIGATE, frigateTemplate);
		product = createProduct(design);
		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);
		// TEST
		//System.out.println("Player attitude = " + player.getPersonality().getAttitude());
		//System.out.println("\nFRIGATE: \n" + design);

		design = createShipDesign(COLONY_SHIP, colonyShipTemplate);
		product = createProduct(design);
		// TEST
		//System.out.println("\nCOLONY_SHIP: \n" + design +"\n\n");

		shipDesigns.put(design.getShipClass(), design);
		products.addElement(product);

	}

	private ShipDesign createShipDesign(String name, AiShipTemplate template) {
		ShipDesign newDesign = new ShipDesign(name);
		newDesign.setHull(template.getHullType());
		addFirstPartOfEachType(template, newDesign);
		if (template.isFill()) {
			addMorePartsOfEachType(template, newDesign);
		}
		return newDesign;
	}

	/**
	 * add more weapons for warships, more cargo units for transport ships <br>
	 * XXX: other components?
	 */
	private void addMorePartsOfEachType(AiShipTemplate template, ShipDesign newDesign) {
		float defensive = 0;
		boolean isFull = false;
		if (player.getPersonality().getAttitude().equals(AGGRESSIVE_ATTITUDE)) {
			defensive = 0.02f;
		} else if (player.getPersonality().getAttitude().equals(NEUTRAL_ATTITUDE)) {
			defensive = 0.05f;
		} else {
			defensive = 0.1f;
		}
		int breakOff = 0;
		while (!isFull) {
			for (int i = 0; i < template.getComponentTypesLength(); i++) {
				if (template.getComponentType(i)) {
					Collection c = components.values();
					for (Iterator cit = c.iterator(); cit.hasNext();) {
						ShipComponent sc = (ShipComponent) cit.next();
						if (sc.getSubType() == i && sc.getAllowBuild()) {
							// if (sc.getSubType()==i) {
							if (newDesign.getShipClass().equals(SMALL_TRANSPORT)) {
								if (sc.getSubType() == CARGO_TYPE) {
									if (hasRoom(newDesign, sc)) {
										newDesign.addSpecial(player, sc.getName());
									} else {
										isFull = true;
									}
								}
							} else {
								if (Math.random() > defensive) {
									if (sc.getType().equals(WEAPONS)) {
										if (hasRoom(newDesign, sc)) {
											addIfBetterThanExistingWeapon(template, newDesign, sc);
										} else {
											isFull = true;
										}
									}
								} else {
									if (Math.random() > 0.5) {
										if (sc.getSubType() == ARMOR_TYPE) {
											if (hasRoom(newDesign, sc)) {
												addIfBetterThanExistingArmor(newDesign, sc);
											} else {
												isFull = true;
											}
										}
									} else {
										if (sc.getSubType() == SHIELDING_TYPE) {
											if (hasRoom(newDesign, sc)) {
												addIfBetterThanExistingShielding(newDesign, sc);
											} else {
												isFull = true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (breakOff++ > 20) {
				isFull = true;
			}
		}

	}

	/**
	 * Add component only if its the same as already installed or add better
	 * components and remove previously installed
	 */
	private void addIfBetterThanExistingShielding(ShipDesign design, ShipComponent newComponent) {
		if (!design.addShielding(newComponent.getName())) {
			ShipComponent oldComponent = (ShipComponent) components.get(design.getShieldType());
			if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
				while (design.getShieldCount() > 0) {
					design.removeShielding(oldComponent.getName());
				}
				design.addShielding(newComponent.getName());

			}
		}
	}

	private void addIfBetterThanExistingArmor(ShipDesign design, ShipComponent newComponent) {
		if (!design.addArmor(newComponent.getName())) {
			ShipComponent oldComponent = (ShipComponent) components.get(design.getArmorType());
			if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
				while (design.getArmorCount() > 0) {
					design.removeArmor(oldComponent.getName());
				}
				design.addArmor(newComponent.getName());
			}
		}
	}

	/**
	 * Add component only if its the same as already installed or add better
	 * components and remove previously installed
	 */
	private void addIfBetterThanExistingWeapon(AiShipTemplate template, ShipDesign design, ShipComponent newComponent) {
		ShipComponent oldComponent = null;
		if (template.getComponentType(newComponent.getSubType())) {
			// this design needs a component of this subtype, e.g. a beamweapon
			if (newComponent.getSubType() == BEAM_WEAPON_TYPE) {
				if ("".equals(design.getBeamWeaponType()) || newComponent.getName().equals(design.getBeamWeaponType())) {
					design.addWeapon(player, newComponent.getName());
				} else {
					oldComponent = (ShipComponent) components.get(design.getBeamWeaponType());
					if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
						while (design.getBeamWeaponCount() > 0) {
							design.removeWeapon(player, oldComponent.getName());
						}
						design.addWeapon(player, newComponent.getName());
					}
				}
			}
			if (newComponent.getSubType() == PARTICLE_WEAPON_TYPE) {
				if ("".equals(design.getParticleWeaponType()) || newComponent.getName().equals(design.getParticleWeaponType())) {
					design.addWeapon(player, newComponent.getName());
				} else {
					oldComponent = (ShipComponent) components.get(design.getParticleWeaponType());
					if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
						while (design.getParticleWeaponCount() > 0) {
							design.removeWeapon(player, oldComponent.getName());
						}
						design.addWeapon(player, newComponent.getName());
					}
				}
			}
			if (newComponent.getSubType() == MISSILE_WEAPON_TYPE) {
				if ("".equals(design.getMissileWeaponType()) || newComponent.getName().equals(design.getMissileWeaponType())) {
					design.addWeapon(player, newComponent.getName());
				} else {
					oldComponent = (ShipComponent) components.get(design.getMissileWeaponType());
					if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
						while (design.getMissileWeaponCount() > 0) {
							design.removeWeapon(player, oldComponent.getName());
						}
						design.addWeapon(player, newComponent.getName());
					}
				}
			}
			if (newComponent.getSubType() == BOMB_WEAPON_TYPE) {
				if ("".equals(design.getBombWeaponType()) || newComponent.getName().equals(design.getBombWeaponType())) {
					design.addWeapon(player, newComponent.getName());
				} else {
					oldComponent = (ShipComponent) components.get(design.getBombWeaponType());
					if (newComponent.getTechLevel() > oldComponent.getTechLevel()) {
						while (design.getBombWeaponCount() > 0) {
							design.removeWeapon(player, oldComponent.getName());
						}
						design.addWeapon(player, newComponent.getName());
					}
				}
			}

		}
	}

	/**
	 * drives and such are added only once
	 */
	private void addFirstPartOfEachType(AiShipTemplate template, ShipDesign newDesign) {
		for (int i = 0; i < template.getComponentTypesLength(); i++) {
			if (template.getComponentType(i)) { // e.g. BEAMWEAPONS = true
				Collection c = components.values();
				for (Iterator cit = c.iterator(); cit.hasNext();) {
					ShipComponent sc = (ShipComponent) cit.next();
					if (sc.getSubType() == i && sc.getAllowBuild()) {
						// if (sc.getSubType()==i) {
						if (sc.getType().equals(DRIVES)) {
							newDesign.addDrive(player, sc.getName());
						}
						if (sc.getType().equals(WEAPONS)) {
							addIfBetterThanExistingWeapon(template, newDesign, sc);
						}
						if (sc.getType().equals(ELECTRONICS)) {
							newDesign.addElectronics(player, sc.getName());
						}
						if (sc.getSubType() == ARMOR_TYPE) {
							addIfBetterThanExistingArmor(newDesign, sc);
						}
						if (sc.getSubType() == SHIELDING_TYPE) {
							addIfBetterThanExistingShielding(newDesign, sc);
						}
						if (sc.getType().equals(SPECIAL)) {
							newDesign.addSpecial(player, sc.getName());
						}
					}
				}
			}
		}
	}

	// XXX when a new shipcomponent is available, redefine all shipdesigns.
	// If the newly defined is bigger/better/faster, then obsolete the old
	// shipdesign and product
	public void newShipsCanBeBuilt(ShipComponent shipComp) {

		// XXX when a new shipdesign version (e.g. a new colony ship) is
		// available,
		// the old design could be obsoleted, the product obsoleted and a new
		// product with
		// the same name added. So the 'build ship' logic could stay the same
		// and existing construction orders continue
		// XXX this method is similar to what is done in shipyard, but the
		// existing
		// code looks not very much reusable. Maybe refactor later?

		// TEST
		// System.out.println("XXX: Technology " + tech.getName() + " allows
		// new ships to be built???");
		// if (shipComp.getType().equals(DRIVES)) {
		// System.out.println("new ships can be built with " +
		// shipComp.getName() + ", " + shipComp.getType());
		// }

		// possible algorithm: (different rules or weights for different
		// personalities)
		// -select shipclass, e.g. scout, colonyship, destroyer, ...
		// -select hull for this class
		// -add drives
		// -depending on usage, add
		// - colony module
		// - weapons (select random weapons of some or all categories
		// (beamweapons, missiles, bombs, ...)
		// - specials (cargo, battle enhancement, fuel scoop, ....
		// - electronics
		// - shielding
		// do this in a loop, as long as used payload < possible payload
		// verify: if the planned shipDesign is possible, check if an older
		// version exists and
		// whether there is some improvement, obsolete the old one and add the
		// new one

		// XXX print old & new design for test purpose
	}

	private AiShipTemplate createScout() {
		AiShipTemplate template = new AiShipTemplate();
		template.setFill(false);
		template.setHullType(SCOUT_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(FTL_DRIVE_TYPE);
		template.setComponentType(SCANNER_TYPE);
		// XXX: fuel tank???
		if (Math.random() > 0.5) {
			if (Math.random() > 0.5) {
				template.setComponentType(BEAM_WEAPON_TYPE);
			} else {
				template.setComponentType(MISSILE_WEAPON_TYPE);
			}
		}
		return template;
	}

	private AiShipTemplate createDefender() {
		boolean addedWeapon = false;
		AiShipTemplate template = new AiShipTemplate();
		template.setFill(true);
		template.setHullType(SCOUT_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(SCANNER_TYPE);
		template.setComponentType(TARGET_COMPUTER_TYPE);
		template.setComponentType(ARMOR_TYPE);
		template.setComponentType(MISSILE_WEAPON_TYPE);
		addedWeapon = true;
		if (Math.random() > 0.3) {
			template.setComponentType(BEAM_WEAPON_TYPE);
		} else {
			template.setComponentType(PARTICLE_WEAPON_TYPE);
		}
		if (Math.random() > 0.7) {
			template.setComponentType(TACTICAL_TYPE);
		}
		return template;
	}

	// XXX: tune
	private AiShipTemplate createEscort() {
		boolean addedWeapon = false;
		AiShipTemplate template = new AiShipTemplate();
		template.setFill(true);
		template.setHullType(ESCORT_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(FTL_DRIVE_TYPE);
		template.setComponentType(SCANNER_TYPE);
		template.setComponentType(TARGET_COMPUTER_TYPE);
		template.setComponentType(TACTICAL_TYPE);
		template.setComponentType(BEAM_WEAPON_TYPE);
		if (Math.random() > 0.3) {
			template.setComponentType(MISSILE_WEAPON_TYPE);
		} else {
			template.setComponentType(PARTICLE_WEAPON_TYPE);
		}
		return template;
	}

	// XXX: tune
	private AiShipTemplate createSmallTransport() {
		AiShipTemplate template = new AiShipTemplate();
		template.setFill(true);
		template.setHullType(SMALL_TRANSPORT_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(FTL_DRIVE_TYPE);
		template.setComponentType(CARGO_TYPE);
		if (Math.random() > 0.3) {
			template.setComponentType(SCANNER_TYPE);
		}
		if (Math.random() > 0.5) {
			// some chance to add weapons
			if (Math.random() > 0.5) {
				template.setComponentType(BEAM_WEAPON_TYPE);
			} else {
				template.setComponentType(MISSILE_WEAPON_TYPE);
			}
		}
		return template;
	}

	// XXX: tune
	private AiShipTemplate createFrigate() {
		AiShipTemplate template = new AiShipTemplate();
		template.setFill(true);
		template.setHullType(FRIGATE_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(FTL_DRIVE_TYPE);
		template.setComponentType(SCANNER_TYPE);
		template.setComponentType(TARGET_COMPUTER_TYPE);
		template.setComponentType(TACTICAL_TYPE);
		template.setComponentType(MISSILE_WEAPON_TYPE);
		template.setComponentType(BEAM_WEAPON_TYPE);
		template.setComponentType(PARTICLE_WEAPON_TYPE);
		template.setComponentType(ARMOR_TYPE);
		template.setComponentType(SHIELDING_TYPE);
		return template;
	}

	// XXX: tune
	private AiShipTemplate createColonyShip() {
		AiShipTemplate template = new AiShipTemplate();
		template.setHullType(COLONY_HULL);
		template.setComponentType(SUBLIGHT_DRIVE_TYPE);
		template.setComponentType(FTL_DRIVE_TYPE);
		template.setComponentType(SCANNER_TYPE);
		template.setComponentType(COLONY_MODULE_TYPE);
		if (Math.random() > 0.4) {
			// some chance to add weapons
			if (Math.random() > 0.5) {
				template.setComponentType(BEAM_WEAPON_TYPE);
			} else {
				template.setComponentType(MISSILE_WEAPON_TYPE);
			}
		}
		return template;
	}

	private Product createProduct(ShipDesign sd) {
		boolean canBuild;
		if (sd.canLandOnPlanets()) {
			canBuild = true;
		} else {
			canBuild = false;
		}
		Product p;
		if (sd.isShip()) {
			p = new Product(-1, sd.getShipClass(), sd.getDescription(), false, canBuild, sd.getShipClass(), SHIP);
		} else {
			p = new Product(-1, sd.getShipClass(), sd.getDescription(), false, true, sd.getShipClass(), SATELLITE);
		}
		for (int i = R_MIN; i < R_MAX; i++) {
			if (sd.getCost(player, i) > 0) {
				p.setProductionCost(i, sd.getCost(player, i));
				p.setSupportCost(i, sd.getSupportCost(player, i));
			}
		}
		return p;
	}

	private boolean hasRoom(ShipDesign newDesign, ShipComponent sc) {
		return newDesign.getMaxPayload(player) > (newDesign.getPayload(player) + sc.getWeight());
	}
}
