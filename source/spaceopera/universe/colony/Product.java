//The class Product is used to produce things on colonies in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.colony;

/** The Product is a buildProject that needs no support cost, is built
 *  and forgotten, like things that are being sold.
 *  
 *  Ships are also built as products, even if they need some support cost from the
 *  home planet, e.g. spare parts, maintenance personnel and such
 */
public class Product extends BuildProject {

    //Methods
    public Product(int id, String title, String description, boolean unique, boolean allow,
                   String picture, String type){
        setId(id);
        setName(title);
        setDescription(description);
        //this.constructionCost = kosten;
        //this.maintenance = maint;
        setUnique(unique);
        setAllowBuild(allow);
        setPicture(picture);
        setType(type);
    }

    public String toString() {
    	return "Product " + getName() + ", " + getDescription() + ", " + getType();
    }
}