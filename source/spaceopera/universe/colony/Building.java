//The class Building defines buildings for colonies.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.colony;


/** The Buildings class is the base class for colony buildings like Factory,
 *  Orbital Shipyard and such. Buildings belong to a colony or a outpost.
 */
public class Building extends BuildProject  {

  private int counter = 1;     // Buildings like factory only once

  //Accessors
  //public String getCls()          {return(cls);}
  public int    getCounter()      {return(counter);}
  public void   setCounter(int c) {counter=c;}
  public void   incCounter()      {counter++;}

  //Methods
  public Building(int id, String title, String cls, String description,
                  int product, int comp, boolean unique, boolean allow, String picture) {
        setId(id);
        setName(title);
        setType(cls);
        setDescription(description);
        setProduct(product);
        setComponent(comp);              
        setUnique(unique);
        setAllowBuild(allow);
        setPicture(picture);
  }

  //XXX: new functions
  private double defenseCount = 0.0;
  private double happinessCount = 0.0;
  private double housingCount = 0.0;
  private double populationCount = 0.0;

  private double tradeCount = 0.0;

  private double happinessFactor = 1.0;
  private double researchFactor = 1.0;

  //XXX: terraforming projects improve planet values (repeatable until max/ideal)
  private double fertilityIncrease = 0.0;   
  private double temperatureImprovement = 0.0;
  private double radiationReduction = 0.0;  
  private double atmosphericPressureCorrection = 0.0;
  private double weatherCorrection = 0.0;
  private double atmosphericCompositionImprovement = 0.0;
  private double gravitationImprovement = 0.0;
  private double waterLandRatioImprovement = 0.0;
  private double faunaImprovement = 0.0;
  private double floraImprovement = 0.0;    
  
  private double tradeFactor = 1.0;
  
  public void addDefense(double d) {defenseCount+=d;}
  public void addHappiness(double d) {happinessCount+=d;}
  public void addHousing(double d) {housingCount+=d;}
  public void addPopulation(double d) {populationCount+=d;}
  public void addTrade(double d) {tradeCount+=d;}
 
  public void multiplyHappiness(double d) {happinessFactor*=d;}
  public void multiplyResearch(double d) {researchFactor*=d;}
  public void multiplyTrade(double d) {tradeFactor*=d;}
  
  public double getDefenseCount() {return(defenseCount);}
  public double getHappinessCount() {return(happinessCount);}
  public double getHousingCount() {return(housingCount);}
  public double getPopulationCount() {return(populationCount);}

  public double getTradeCount() {return(tradeCount);}
  
  public double getHappinessFactor() {return(happinessFactor);}
  public double getResearchFactor() {return(researchFactor);}
  public double getTradeFactor() {return(tradeFactor);}

  //TERRAFORMING getters and setters
  public void setFertilityIncrease(double d) {
    fertilityIncrease = d;
  }
  public double getFertilityIncrease() {
    return fertilityIncrease;
  }
  public double getTemperatureImprovement() {
    return temperatureImprovement;
  }
  public void setTemperatureImprovement(double temperatureImprovement) {
    this.temperatureImprovement = temperatureImprovement;
  }
  public double getRadiationReduction() {
    return radiationReduction;
  }
  public void setRadiationReduction(double radiationReduction) {
    this.radiationReduction = radiationReduction;
  }
  public double getAtmosphericPressureCorrection() {
    return atmosphericPressureCorrection;
  }
  public void setAtmosphericPressureCorrection(double atmosphericPressureCorrection) {
    this.atmosphericPressureCorrection = atmosphericPressureCorrection;
  }
  public double getWeatherCorrection() {
    return weatherCorrection;
  }
  public void setWeatherCorrection(double weatherCorrection) {
    this.weatherCorrection = weatherCorrection;
  }
  public double getAtmosphericCompositionImprovement() {
    return atmosphericCompositionImprovement;
  }
  public void setAtmosphericCompositionImprovement(double atmosphericCompositionImprovement) {
    this.atmosphericCompositionImprovement = atmosphericCompositionImprovement;
  }
  public double getGravitationImprovement() {
    return gravitationImprovement;
  }
  public void setGravitationImprovement(double gravitationImprovement) {
    this.gravitationImprovement = gravitationImprovement;
  }

  public void setWaterLandRatioImprovement(double waterLandRatioImprovement) {
    this.waterLandRatioImprovement = waterLandRatioImprovement;
  }
  public double getWaterLandRatioImprovement() {
    return waterLandRatioImprovement;
  }

  public void setFaunaImprovement(double faunaImprovement) {
    this.faunaImprovement = faunaImprovement;    
  }
  public double getFaunaImprovement() {
      return faunaImprovement;
  }

  public void setFloraImprovement(double floraImprovement) {
    this.floraImprovement = floraImprovement;
  }
  public double getFloraImprovement() {
    return floraImprovement;
  }
  
  public String toString() {
  	return "Building " + getName() + ", " + getType() + ", " + this.getCounter();
  }  
  
}
