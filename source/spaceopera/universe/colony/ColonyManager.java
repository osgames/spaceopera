package spaceopera.universe.colony;

public class ColonyManager {

	private boolean auto; // ColonyManager does everything
	private boolean advice; // ColonyManager only talks
	private boolean shutup; // ColonyManager does nothing

	public boolean getAuto() {
		return auto;
	}

	public boolean getAdvice() {
		return advice;
	}

	public boolean getShutup() {
		return shutup;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public void setAdvice(boolean advice) {
		this.advice = advice;
	}

	public void setShutup(boolean shutup) {
		this.shutup = shutup;
	}
}
