//The class BuildProject is used for production orders on colonies.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.colony;

import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;

public class BuildProject implements Cloneable, SOConstants {

	private int id = 0;
	private String name = "";
	private String type = "";
	private String description = "";
	private String picture = "";
	private int productId = 0;
	private int componentId = 0;

	// private int count=0; // how many (eg. Torpedos)
	private double[] productionCost; // array for R_MAX different resource
										// usage
	private double[] productionCostPaid;
	private double[] supportCost; // "
	private double[] producedResource; // array for R_MAX different resource
										// production
	private double percentage; // how much of the resources/effects can be
								// realized?
	private double percentComplete; // how far is the construction of this
									// object completed?
	private boolean unique = false; // can only be built once per colony (but
									// can be rebuilt if deleted)
	private boolean useOnlyOnce = false; // can be build only once per
											// colony, cannot be rebuilt if
											// deleted
	private boolean allowBuild = false;
	private double educationLevel = 10.0; // 0.0 to 20.0, see Resources.java
	private Universe universe;
	private double scanningCount = 0.0;

	// Accessors
	public void setScanner(double d) {
		scanningCount += d;
	}

	public boolean getAllowBuild() {
		return (allowBuild);
	}

	// public double getAmount() {return(amount);}
	public String getType() {
		return (type);
	}

	public int getComponent() {
		return (componentId);
	}

	public String getDescription() {
		return (description);
	}

	public double getEducationLevel() {
		return (educationLevel);
	}

	// public int getEffect() {return(effectId);}
	public int getId() {
		return (id);
	}

	public double getMaintenance() {
		return (supportCost[R_WORKUNITS]);
	}

	public String getName() {
		return (name);
	}

	// public int getOper() {return(oper);}
	public double getEfficiency() {
		return (percentage);
	}

	// public double getPercentComplete() {return(percentComplete);}
	public String getPicture() {
		return (picture);
	}

	public int getProduct() {
		return (productId);
	}

	public double getProductionCost(int resource) {
		return (productionCost[resource]);
	}

	public double getProductionCostPaid(int resource) {
		return (productionCostPaid[resource]);
	}

	public double getResourceProduction(int resource) {
		return (producedResource[resource]);
	}

	public double getScanningCount() {
		return (scanningCount);
	}

	public double getSupportCost(int resource) {
		return (supportCost[resource]);
	}

	public boolean isUnique() {
		return (unique);
	}

	public boolean useOnlyOnce() {
		return (useOnlyOnce);
	}

	// public void setConstructionCost(double c) {constructionCost=c;}
	public void setAllowBuild(boolean b) {
		allowBuild = b;
	}

	// public void setAmount(double a) {amount=a;}
	public void setType(String c) {
		type = c;
	}

	public void setComponent(int c) {
		componentId = c;
	}

	public void setDescription(String d) {
		description = d;
	}

	public void setEducationLevel(double e) {
		educationLevel = e;
	}

	// public void setEffect(int e) {effectId=e;}
	public void setId(int i) {
		id = i;
	}

	// public void setCount(int c) {count=c;}
	public void setName(String n) {
		name = n;
	}

	// public void setOper(int o) {oper=o;}
	public void setEfficiency(double p) {
		percentage = p;
	}

	public void setPercentComplete(double p) {
		percentComplete = p;
	}

	public void setPicture(String p) {
		picture = p;
	}

	public void setProduct(int p) {
		productId = p;
	}

	public void setProductionCostPaid(int res, double c) {
		productionCostPaid[res] = c;
	}

	// public void setPercentComplete(double p) {percentComplete=p;}
	public void setUnique(boolean u) {
		unique = u;
	}

	public void setUniverse(Universe u) {
		universe = u;
	}

	public void setUseOnlyOnce(boolean u) {
		useOnlyOnce = u;
	}

	// Methods
	public BuildProject() {
		productionCost = new double[R_MAX];
		producedResource = new double[R_MAX];
		supportCost = new double[R_MAX];
		productionCostPaid = new double[R_MAX];
		for (int i = R_MIN; i < R_MAX; i++) {
			productionCost[i] = 0.0;
			producedResource[i] = 0.0;
			supportCost[i] = 0.0;
			productionCostPaid[i] = 0.0;
		}
		percentage = 100;
	}

	public BuildProject clone() {
		BuildProject object = null;
		try {
			object = (BuildProject) super.clone();
			object.productionCost = (double[]) productionCost.clone();
			object.producedResource = (double[]) producedResource.clone();
			object.supportCost = (double[]) supportCost.clone();
			object.productionCostPaid = (double[]) productionCostPaid.clone();
		} catch (Exception e) {
			System.out.println("Buildproject clone failed. " + e);
		}
		return object;
	}

	public double getRushJobCost() {
		double cost = 0.0;
		for (int i = R_MIN; i < R_MAX; i++) {
			if (productionCost[i] > 0) {
				cost += productionCost[i];
			}
		}
		return cost;
	}

	public double getPercentComplete() {
		double percent = 0.0;
		double minPercent = 100.0;
		double prod = 0.0;
		double paid = 0.0;
		for (int i = R_MIN; i < R_MAX; i++) {

			if (productionCost[i] > 0) {
				prod = productionCost[i];
				paid = productionCostPaid[i];
				percent = 100.0 / prod * paid;
				if (percent > 99.9999) {
					percent = 100.0;
				}
				if (percent < minPercent) {
					minPercent = percent;
				}
			}
		}

		return (minPercent);
	}

	public double getPercentComplete(int resource) {
		double percent = 0.0;
		double minPercent = 100.0;
		double prod = 0.0;
		double paid = 0.0;
		if (productionCost[resource] > 0) {
			prod = productionCost[resource];
			paid = productionCostPaid[resource];
			percent = 100.0 / prod * paid;
			if (percent > 99.9999) {
				percent = 100.0;
			}
		}
		return (percent);
	}

	public void setProductionCost(int resource, double amount) {
		productionCost[resource] = amount;
	}

	public void setResourceProduction(int resource, double amount) {
		producedResource[resource] = amount;
	}

	public void setSupportCost(int resource, double amount) {
		supportCost[resource] = amount;
	}

	public void setAllProductionCostPaid() {
		for (int i = R_MIN; i < R_MAX; i++) {
			productionCostPaid[i] = productionCost[i];
		}
	}

}