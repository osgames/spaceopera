//The class Colony is used for colonizing planets and colony management.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.colony;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.objects.weapons.BeamWeapon;
import spaceopera.gui.objects.weapons.Bomb;
import spaceopera.gui.objects.weapons.Torpedo;
import spaceopera.gui.objects.weapons.Weapon;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ai.PreferredPlanetValues;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.elements.SunSystem;
import spaceopera.universe.ships.Satellite;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.ShipDesign;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Colony class is used for colony management: population count and growth, manage the production settings, manage the production
 * Queue, calculate resource usage and output of buildings,
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Colony implements SOConstants {
	private String name = "";
	private String planetName = "";
	private String image = "";
	private int colonyAge = 0;
	private Planet planet = null;
	private Player player = null;
	private Fullscreen fs;
	private List<BuildProject> productionQueue;
	private List<BuildProject> buildings;
	private Vector weapons;
	private Vector possibleBuildings;
	private Vector possibleProducts;
	private Hashtable possibleComponents;
	private long populationCount;
	private double growthFactor = 1.0;
	private double oldGrowthFactor = 1.0;

	private int production = 80; // build and run
	private int research = 10;
	private int culture = 10;
	private int taxBase = 10;

	private double totalProducedWork;
	private double researchBonus;
	private double researchFactor;
	private double storedResources[] = new double[R_MAX];
	private double producedResources[] = new double[R_MAX];
	private double consumedResources[] = new double[R_MAX];
	private double demandedResources[] = new double[R_MAX];
	private double averageProducedResources[] = new double[R_MAX];
	private double averageDemandedResources[] = new double[R_MAX];
	private String resourceDemandString[] = new String[R_MAX];
	private double resourceDemand[] = new double[R_MAX];
	private int torpedoStorage = 0;
	// storage of colony data
	// XXX: difference between colony and player/personality/race data
	// e.g. happiness of overall race might differ a bit on planets?
	private double colonyHappiness = 1.0; // 0.5 is quite bad, 1.0 is average,
											// 1.5 is very happy...
	private double longTimeHappiness = 1.0;
	private double maxHousing = 0.0;
	private double usableHousing = 0.0;
	// XXX: Defense part 1:
	// sum of all defensive buildings and factors
	// buildings can 1 to 10 points each, some accumulate, a possible total may
	// be 200...
	// Defense part one: buildings like barracks, bunkers, shields, weapons
	// define a defense level
	private double defenseFactor = 0.0;
	// Defense part two:
	// the army size is the number of colonists armed and educated in warfare
	// Note: the army size does not contain the working population part than
	// mans and operates the army buildings
	private double armySize = 0.0; // anything from 50 to 500'000 ?
	// Defense part three:
	// parts one and two added somehow together?
	private int initialScanRange = 3;
	private int scanRange = initialScanRange;
	private boolean isScanned = false;

	private double educationLevel = 0.0; // 0.0 = Stone Age, 10.0 = 21st
											// Century, 20.0 = Utopia

	private ColonyManager colonyManager;

	// Accessors
	// public void addBuildingToColony(Object e) {buildings.addElement(e);}
	public void addProductionQueue(BuildProject p) {
		productionQueue.add(p);
	}

	public void addPossibleBuildings(Object b) {
		possibleBuildings.addElement(b);
	}

	public void addPossibleComponents(String name, Object object) {
		possibleComponents.put(name, object);
	}

	public void addPossibleProducts(Object p) {
		possibleProducts.addElement(p);
	}

	public void addStoredResources(int i, double r) {
		storedResources[i] += r;
	}

	public List<BuildProject> getBuildings() {
		return (buildings);
	}

	public double getEducationLevel() {
		return (educationLevel);
	}

	public int getProduction() {
		return ((int) production);
	}

	public int getResearch() {
		return ((int) research);
	}

	public String getImage() {
		return (image);
	}

	public String getName() {
		return (name);
	}

	public Fullscreen getParent() {
		return (fs);
	}

	public Planet getPlanet() {
		return (planet);
	}

	public int getPlanetNumber() {
		return (planet.getNumber());
	}

	public Player getPlayer() {
		return (player);
	}

	public String getPlayerName() {
		return (player.getName());
	}

	public long getPopulationCount() {
		return (populationCount);
	}

	public Vector getPossibleBuildings() {
		return (possibleBuildings);
	}

	public Hashtable getPossibleComponents() {
		return (possibleComponents);
	}

	public Vector getPossibleProducts() {
		return (possibleProducts);
	}

	public double getProducedResources(int i) {
		return (producedResources[i]);
	}

	public List<BuildProject> getProductionQueue() {
		return (productionQueue);
	}

	// XXX: changed from resourceDemand
	public String getResourceDemandString(int i) {
		return (resourceDemandString[i]);
	}

	public boolean getScanned() {
		return (isScanned);
	}

	public int getScanRange() {
		return (scanRange);
	}

	public double getStoredResources(int i) {
		return (storedResources[i]);
	}

	public Sun getSun() {
		return planet.getSun();
	}

	public SunSystem getSunSystem() {
		return (planet.getSunSystem());
	}

	public int getSunX() {
		return (getSun().getX());
	}

	public int getSunY() {
		return (getSun().getY());
	}

	public double getConsumedResources(int i) {
		return (consumedResources[i]);
	}

	public double getTotalProduction() {
		return (totalProducedWork);
	}

	public Vector getWeapons() {
		return (weapons);
	}

	public void removeBuildings(Building e) {
		buildings.remove(e);
	}

	public void removePossibleBuildings(Building e) {
		possibleBuildings.removeElement(e);
	}

	public void removePossibleComponents(ShipComponent k) {
		possibleComponents.remove(k);
	}

	public void removePossibleProducts(Product p) {
		possibleProducts.removeElement(p);
	}

	public void removeProductionQueue(String name) {
		BuildProject toRemove = null;
		for (BuildProject bp : productionQueue) {
			if (bp.getName().equals(name)) {
				toRemove = bp;
				break;
			}
		}
		if (toRemove != null) {
			productionQueue.remove(toRemove);
		}
	}

	public void removeStoredResources(int i, double r) {
		storedResources[i] -= r;
	}

	public void setEducationLevel(double e) {
		educationLevel = e;
	}

	public void setProduction(int f) {
		production = f;
	}

	public void setResearch(int s) {
		research = s;
	}

	public void setName(String n) {
		name = n;
	}

	public void setPlanetSelected(boolean b) {
		planet.setSelected(b);
	}

	public void setPopulationCount(double p) {
		populationCount = (int) p;
	}

	public void setScanned(boolean b) {
		isScanned = b;
		getSun().setScanned(b);
	}

	public void setSunSelected(boolean b) {
		planet.setSunSelected(b);
	}

	// Methods
	public Colony(Fullscreen fs, Player sp, Planet p, long initialPopulation) {
		this.fs = fs;
		player = sp;
		planet = p;
		populationCount = initialPopulation;
		// productionFactor = PRODUCTIONFACTOR;
		// growthFactor = GROWTHFACTOR;
		productionQueue = new Vector();
		buildings = new Vector();
		weapons = new Vector();
		possibleBuildings = new Vector();
		possibleProducts = new Vector();
		possibleComponents = new Hashtable();
		for (int i = 0; i < R_MAX; i++) {
			resourceDemand[i] = 0;
			resourceDemandString[i] = NONE;
		}
		planetName = planet.getName();
		name = "Colony " + player.getNextColonyNumber();
		if (player.getNumberOfUsedColonyImages() == COLONYIMAGES) {
			for (int i = 1; i < COLONYIMAGES; i++) {
				player.setUsedColonyImages(i, 0);
			}
			player.setNbrUsedColonyImages(0);
		}
		// if (player instanceof MPlayer) {
		// temporary for all players
		boolean imgNbrUsed = true;
		int imgNbr = 0;
		while (imgNbrUsed) {
			imgNbr = (int) (Math.random() * COLONYIMAGES + 1);
			imgNbrUsed = false;
			for (int i = 1; i < COLONYIMAGES; i++) {
				if (player.getUsedColonyImages(i) == imgNbr) {
					imgNbrUsed = true;
				}
			}
		}
		player.incNbrUsedColonyImages();
		player.setUsedColonyImages(player.getNumberOfUsedColonyImages(), imgNbr);
		image = "colony" + imgNbr + ".jpg";
		// }
		refreshPossibilities();
		// add initial buildings to colony
		// XXX: make this depending on difficulty level
		for (int j = 0; j < possibleBuildings.size(); j++) {
			Building b = (Building) possibleBuildings.elementAt(j);
			if ((b.getName().equals(POWER_PLANT)) || (b.getName().equals(HABITAT)) || (b.getName().equals(FARM))
					|| (b.getName().equals(STRIP_MINING_UNIT))) {
				buildings.add(b);
			}
			// TODO if ((spaceOpera.getDifficultyLevel() == D_KINDERGARDEN) ||
			// (spaceOpera.getDifficultyLevel() == D_EASY)) {
			if (b.getName().equals(COLONY_MANAGER)) {
				buildings.add(b);
			}
			if (b.getName().equals(ORBITAL_SHIPYARD)) {
				buildings.add(b);
			}
			// }
		}
		colonyManager = new ColonyManager();
		colonyManager.setAdvice(true);
	}

	public void destroy() {
		productionQueue.clear();
		buildings.clear();
		weapons.removeAllElements();
		possibleBuildings.removeAllElements();
		possibleProducts.removeAllElements();
		possibleComponents.clear();
		transferShipsToOtherColony();
		// XXX: who else has a reference to this colony?
	}

	public void nextTurn() {
		colonyAge++;
		cleanupNumbersFromPreviousTurn(); // 1 sequence matters!
		addPopulationGrowth(); // 2
		getProducedWork(); // 3
		buildingsAddSomething(); // 4
		subtractTaxes(); // 5
		distributeWork(); // 6 (XXX: test distributeProducedWork)
		prospectorsAndShops(); // 7
		maintainBuildings(); // 8
		buildingsMultiplySomething(); // 9
		addResearchBonus(); // 10
		maintainShips(); // 11
		maintainWeapons();
		constructionWork(); // 12
		useUnusedResources(); // 13
		storeResourcesForNextTurn(); // 14
		buildingsAllowConstructionOf(); // 15
		buildingsWithSpecialEffects(); // 16
		refreshPossibilities(); // for AI colonies
		calculateDemand(); // 17
		educationLevelChange(); // 18
		refreshDisplay(); // 19
	}

	private void addBuildingToColony(BuildProject buildProject) {
		Building building = null;
		boolean exists = false;
		// increment buildings
		for (int j = 0; j < buildings.size(); j++) {
			building = (Building) buildings.get(j);
			if (building.getName().equals(buildProject.getName())) {
				building.incCounter();
				exists = true;
				break;
			}
		}
		// add buildings
		if (!exists) {
			buildings.add(buildProject);
		}
		// public Torpedo(String name, int ammo, int battleSpeed, int force, int
		// lifeCycles) {
		if (buildProject.getName().equals(DEFENSE_BATTERY)) {
			Weapon w = new Torpedo(TORPEDO, 2, 1, 8, 3);
			weapons.addElement(w);
		}
		if (buildProject.getName().equals(DEFENSE_BATTERY_2)) {
			Weapon w = new Torpedo(TORPEDO, 4, 1, 10, 4);
			weapons.addElement(w);
		}
		if (buildProject.getName().equals(DEFENSE_BATTERY_3)) {
			Weapon w = new Torpedo(TORPEDO, 8, 1, 15, 5);
			weapons.addElement(w);
		}
	}

	private void addBuildingOutputToColony(Building building) {
		int count = building.getCounter();
		double efficiency = building.getEfficiency();
		double supply = 1;
		for (int j = R_MIN; j < R_MAX; j++) {
			supply = getPlanetarySupply(j);
			if (j == R_ELECTRICITY) {
				producedResources[j] += supply * count * building.getResourceProduction(j) * (efficiency / 100.0);
			} else {
				double prod = supply * count * building.getResourceProduction(j) * (efficiency / 100.0);
				producedResources[j] += prod / 2.0 + prod / 2.0 * Math.random();
			}
		}
	}

	private void addShipToColony(BuildProject buildProject) {
		String buildProjectName = buildProject.getName();
		boolean isShip = false;
		boolean isSatellite = false;
		ShipDesign shipDesign = null;
		// XXX: optimize this
		Hashtable shipDesigns = player.getShipDesigns();

		// TEST
		// System.out.println("Completed " + buildProject.getName());
		// System.out.println(shipDesigns.toString());

		if (shipDesigns.containsKey(buildProjectName)) {
			shipDesign = (ShipDesign) shipDesigns.get(buildProjectName);
			int x = planet.getSunSystemX();
			int y = planet.getSunSystemY();
			if (shipDesign.isShip()) {
				Ship ship = new Ship(player, shipDesign, this, fs.getUniverse());
				// XXX: change that
				if (shipDesign.getColonyModuleNumber() > 0 && populationCount > 2 * NUMCOLONISTS) {
					populationCount -= NUMCOLONISTS;
					if (player instanceof CPlayer) {
						if (((CPlayer) player).getOrderedColonyShips() > 0) {
							((CPlayer) player).setOrderedColonyShips(((CPlayer) player).getOrderedColonyShips() - 1);
						}
					}

				}
				if (shipDesign.getShipClass().equals(SCOUT_SHIP)) {
					if (player instanceof CPlayer) {
						if (((CPlayer) player).getOrderedScoutShips() > 0) {
							((CPlayer) player).setOrderedScoutShips(((CPlayer) player).getOrderedScoutShips() - 1);
						}
					}

				}
				ship.setPosition(x + 12, y - 8);
				ship.setTarget(x + 12, y - 8);
				ship.setSunSystem(planet.getSunSystem());
				ship.setColony(this);
				planet.addSpaceCraft(ship);
				player.addSpaceCraft(ship);
				// TODO spaceOpera.addShipToUniverse(ship);
			} else {
				Satellite sat = new Satellite(player, shipDesign, this, fs.getUniverse());

				sat.setPosition(x - 4, y + 8);
				sat.setSunSystem(planet.getSunSystem());
				sat.setColony(this);
				planet.addSpaceCraft(sat);
				player.addSpaceCraft(sat);
				// TODO spaceOpera.addShipToUniverse(sat);
				// XXX add to colony list
				// addBuildingToColony(buildProject);
			}
		}
	}

	private void addFarmerOutputToColony() {
		// XXX: aliens on their colonies have other requirements
		double productionFactor = getProductionFactor();
		double supply = planet.getStone();
		producedResources[R_STONE] += production / 100 * (productionFactor * supply * Math.random() * 50 + 25);
		supply = planet.getWood();
		producedResources[R_WOOD] += production / 100 * (productionFactor * supply * Math.random() * 50 + 50);
		supply = planet.getFauna();
		producedResources[R_MEAT] += production / 100 * (productionFactor * supply * Math.random() * 20 + 25);
		producedResources[R_FISH] += production / 100 * (productionFactor * supply * Math.random() * 2 + 25);
		supply = planet.getFlora();
		producedResources[R_RICE] += production / 100 * (productionFactor * supply * Math.random() * 10 + 10);
		producedResources[R_CROP] += production / 100 * (productionFactor * supply * Math.random() * 10 + 10);
		producedResources[R_DAIRY] += production / 100 * (productionFactor * supply * Math.random() * 10 + 10);
		producedResources[R_VEGETABLES] += production / 100 * (productionFactor * supply * Math.random() * 20 + 25);
		producedResources[R_FRESHWATER] += production / 100 * (productionFactor * supply * Math.random() * 150 + 50);
		producedResources[R_WINE] += production / 100 * (productionFactor * supply * Math.random() * 20 + 25);
	}

	private void addProspectorOutputToColony() {
		// XXX: aliens on their colonies have other requirements
		double productionFactor = getProductionFactor();
		double supply = planet.getStone();
		producedResources[R_STONE] += production / 100 * (productionFactor * supply * Math.random() * 50 + 25);
		supply = planet.getWood();
		producedResources[R_WOOD] += production / 100 * (productionFactor * supply * Math.random() * 50 + 50);
		supply = planet.getFossils();
		producedResources[R_COAL] += productionFactor * supply * Math.random() * 2 + 1;
		producedResources[R_OIL] += productionFactor * supply * Math.random() * 2;
		supply = planet.getMetals();
		producedResources[R_IRON] += productionFactor * supply * Math.random() * 5 + 2;
		producedResources[R_COPPER] += productionFactor * supply * Math.random() * 1 + 1;
		producedResources[R_ALUMINUM] += productionFactor * supply * Math.random() * 1 + 1;
		producedResources[R_GOLD] += productionFactor * supply * Math.random() * 0.15;
		producedResources[R_SILVER] += productionFactor * supply * Math.random() * 0.5;
		producedResources[R_SILICIUM] += productionFactor * supply * Math.random() * 0.5;
		supply = planet.getUranium();
		producedResources[R_URANIUM] += productionFactor * supply * Math.random() * 0.5;
		supply = planet.getSalt();
		producedResources[R_SALT] += production / 100 * (productionFactor * supply * Math.random() * 5 + 5);
	}

	private void addShopOutputToColony() {
		// XXX: aliens on their colonies have other requirements
		double productionFactor = getProductionFactor();
		double supply = productionFactor;
		producedResources[R_GOODS] += productionFactor * supply * Math.random() * 5 + 5;
		supply = planet.getMetals();
		producedResources[R_TOOLS] += productionFactor * supply * Math.random() * 0.5;
		supply = planet.getFlora();
		producedResources[R_GARMENTS] += productionFactor * supply * Math.random() * 2;
		producedResources[R_DRUGS] += productionFactor * supply * Math.random() * 0.1;
		supply = planet.getStone();
		producedResources[R_GLASS] += productionFactor * supply * Math.random() * 1;
	}

	private void addTinkererOutputToColony() {
		// XXX: aliens on their colonies have other requirements
		double productionFactor = getProductionFactor();
		double supply = productionFactor;
		producedResources[R_GOODS] += productionFactor * supply * Math.random() * 5 + 5;
		supply = planet.getMetals();
		producedResources[R_STEEL] += productionFactor * supply * Math.random() * 0.2;
		producedResources[R_TOOLS] += productionFactor * supply * Math.random() * 0.5;
		producedResources[R_ALLOY] += productionFactor * supply * Math.random() * 0.2;
		producedResources[R_MACHINES] += productionFactor * supply * Math.random() * 0.2;
		producedResources[R_ELECTRONICS] += productionFactor * supply * Math.random() * 0.1;
		supply = planet.getFossils();
		producedResources[R_CHEMICALS] += productionFactor * supply * Math.random() * 0.5;
		producedResources[R_PLASTIC] += productionFactor * supply * Math.random() * 0.1;
		producedResources[R_GARMENTS] += productionFactor * supply * Math.random() * 2;
		supply = planet.getFlora();
		producedResources[R_DRUGS] += productionFactor * supply * Math.random() * 0.1;
		supply = planet.getStone();
		producedResources[R_GLASS] += productionFactor * supply * Math.random() * 1;
		// even colonies without weapon factory produce or buy some
		if (torpedoStorage < 500) {
			torpedoStorage += 1;
		}
	}

	private void addBuildingToProductionQueue(String buildingName) {
		for (int k = 0; k < possibleBuildings.size(); k++) {
			Building kBuilding = (Building) possibleBuildings.elementAt(k);
			if (kBuilding.getName().equals(buildingName)) {
				addProductionQueue(kBuilding.clone());
				break;
			}
		}
	}

	private void prospectorsAndShops() {
		addFarmerOutputToColony();
		addProspectorOutputToColony();
		addShopOutputToColony();
		addTinkererOutputToColony();
	}

	private void applyDamageToBuildings() {
		int damageToBuildingNo = (int) (Math.random() * buildings.size());
		Building building = (Building) buildings.get(damageToBuildingNo);
		int counter = building.getCounter();
		if (counter > 1) {
			building.setCounter(counter - 1);
		} else {
			buildings.remove(damageToBuildingNo);
		}
	}

	public double availableHousing() {
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (building.getHousingCount() > 0) {
				maxHousing += building.getCounter() * building.getHousingCount();
				if (building.getEfficiency() < 50) {
					usableHousing += 0.5 * building.getCounter() * building.getHousingCount();
				} else if (building.getEfficiency() < 80) {
					usableHousing += 0.8 * building.getCounter() * building.getHousingCount();
				} else {
					usableHousing += building.getCounter() * building.getHousingCount();
				}
			}
		}
		return (usableHousing / populationCount); // eg 8000/10000 (=80%)
	}

	public double availableFood() {
		double demand = 0.0, produce = 0.0;
		for (int k = 1; k < 7; k++) {
			demand += averageDemandedResources[R_FOOD + k];
			produce += averageProducedResources[R_FOOD + k];
		}
		// TODO if ((demand < 0.01) || (spaceOpera.getTurnNr() < 3)) {
		// return (1.0);
		// } else {
		// if (availableHousing() > 1) {
		// demand /= availableHousing();
		// }
		// return (produce / demand);
		// }
		return 0.8;
	}

	private void buildCompleted(BuildProject buildProject) {
		if (buildProject instanceof Building) {
			addBuildingToColony(buildProject);
			enableFollowupProductsOrComponents(buildProject);
		} else {
			addShipToColony(buildProject);
		}
		productionQueue.remove(buildProject);
	}

	private void getProducedWork() {
		// XXX tune production
		totalProducedWork = (Math.random() + 0.5) * populationCount;

		// effects of happiness on production
		if (longTimeHappiness > 1.5) {
			totalProducedWork *= 1.1;
		}
		if (longTimeHappiness < 0.5) {
			totalProducedWork *= 0.8;
		}
	}

	private void buildingsAddSomething() {
		maxHousing = 0.0;
		usableHousing = 0.0;
		defenseFactor = 0.0;
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (building.getHappinessCount() > 0) {
				colonyHappiness += building.getCounter() * building.getHappinessCount();
			}
			if (building.getDefenseCount() > 0) {
				defenseFactor += building.getCounter() * building.getDefenseCount();
			}
			if (scanRange < initialScanRange + building.getScanningCount()) {
				scanRange = initialScanRange + (int) building.getScanningCount();
			}
			if (building.getName().equals(WEAPONS_FACTORY)) {
				// XXX: torpedos stored are of any kind, for now
				if (torpedoStorage < 500) {
					torpedoStorage += (int) (building.getEfficiency() / 100 * 10);
					// test
					// System.out.println("Torpedos stored: " + torpedoStorage);
				}
			}
			// XXX: housing, population, trade ???
		}
	}

	private void buildingsMultiplySomething() {
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			researchFactor *= building.getResearchFactor();
			colonyHappiness *= building.getHappinessFactor();
			// XXX: terraform, trade ???
		}
		longTimeHappiness = (4 * longTimeHappiness + colonyHappiness) / 5;
	}

	private void cleanupNumbersFromPreviousTurn() {
		for (int i = R_MIN; i < R_MAX; i++) {
			producedResources[i] = 0;
			consumedResources[i] = 0;
			demandedResources[i] = 0;
		}
		researchFactor = 1.0;
		colonyHappiness = 1.0;
	}

	public void educationLevelChange() {
		// take into account:
		// populationCount
		// producedResources[R_WORKUNITS_RESEARCH]
		// educationLevel
		// number of buildings affecting education

		// assume a decrease by 0.02 points per year if no schooling is done
		// of course special events like disasters or research breakthroughs may
		// affect the educationLevel elsewhere
		double levelChange = -0.01;
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (building.getName().equals(BARRACKS)) {
				levelChange += 0.001;
			}
			if (building.getName().equals(CHURCH)) {
				levelChange += 0.001;
			}
			if (building.getName().equals(TEMPLE)) {
				levelChange += 0.001;
			}
			if (building.getName().equals(SCHOOL)) {
				levelChange += 0.01;
			}
			if (building.getName().equals(BATTLE_SCHOOL)) {
				levelChange += 0.001;
			}
			if (building.getName().equals(LIBRARY)) {
				levelChange += 0.005;
			}
			if (building.getName().equals(UNIVERSITY)) {
				levelChange += 0.01;
			}
		}
		// investment in research also pays off:
		// XXX: simplify this (when producedResources[...] reflects man-years)
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/100) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/50) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/30) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/25) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/20) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/15) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/12) {
		// levelChange += 0.001;
		// }
		// if (producedResources[R_WORKUNITS_RESEARCH] > populationCount/10) {
		// levelChange += 0.001;
		// }
		//
		// change slows when approaching the limits:
		if (educationLevel < 5 || educationLevel > 15) {
			levelChange /= 2;
		}
		if (educationLevel < 4 || educationLevel > 16) {
			levelChange /= 3;
		}
		if (educationLevel < 3 || educationLevel > 17) {
			levelChange /= 4;
		}
		if (educationLevel < 2 || educationLevel > 18) {
			levelChange /= 5;
		}
		if (educationLevel <= 0.1 || educationLevel >= 19.9) {
			levelChange = 0.0;
		} else {
			// System.out.println("Education level: " + educationLevel + ",
			// changes by: " + levelChange);
			educationLevel += levelChange;
		}
	}

	public String getHighestDemand() {
		double max = 0.0;
		String resourceName = "unknown";
		for (int i = R_MIN; i < R_MAX; i++) {
			double demand = averageDemandedResources[i] / averageProducedResources[i];
			if (demand > max) {
				max = demand;
				// TODO resourceName = spaceOpera.getResourceName(i);
			}
		}
		return (resourceName);
	}

	private double getProductionFactor() {
		double productionFactor = 1;
		if (populationCount < 10000) {
			productionFactor = 2;
		}
		if (populationCount > 10000) {
			productionFactor = 1.5;
		}
		if (populationCount > 500000) {
			productionFactor = 1;
		}
		if (populationCount > 1000000) {
			productionFactor = 0.8;
		}
		if (populationCount > 5000000) {
			productionFactor = 0.6;
		}
		return (productionFactor);
	}

	private void calculateDemand() {
		// calculate new average
		for (int i = 1; i < R_MAX; i++) {
			averageProducedResources[i] = (2 * averageProducedResources[i] + producedResources[i]) / 3;
			averageDemandedResources[i] = (2 * averageDemandedResources[i] + demandedResources[i]) / 3;
		}
		// calculate resource demand
		// work
		for (int i = R_MIN; i < R_MAX; i++) {
			resourceDemandString[i] = NONE;
			if (i < R_RAWMATERIAL) {
				// no storage
				resourceDemand[i] = averageDemandedResources[i] / averageProducedResources[i];
			} else {
				resourceDemand[i] = averageDemandedResources[i] / (averageProducedResources[i] + storedResources[i]);
				// TEST THIS:
				// resourceDemand[i] =
				// demandedResources[i]/(producedResources[i]+storedResources[i]);
				// OR THIS
				// resourceDemand[i] =
				// demandedResources[i]/producedResources[i];
				// TEST:
				// if (i==R_MEAT) {
				// System.out.println("Meat: demand = " + demandedResources[i] +
				// ", production = " + producedResources[i]);
				// }

			}
			if (resourceDemand[i] > 0.0001) {
				if (resourceDemand[i] < D_VERY_LOW) {
					resourceDemandString[i] = VERY_LOW;
				} else if (resourceDemand[i] < D_LOW) {
					resourceDemandString[i] = LOW;
				} else if (resourceDemand[i] < D_AVERAGE) {
					resourceDemandString[i] = AVERAGE;
				} else if (resourceDemand[i] < D_HIGH) {
					resourceDemandString[i] = HIGH;
				} else if (resourceDemand[i] < D_VERY_HIGH) {
					resourceDemandString[i] = VERY_HIGH;
				} else if (resourceDemand[i] >= D_VERY_HIGH) {
					resourceDemandString[i] = EXTREMELY_HIGH;
				}
			}
		}
	}

	private void useUnusedOperationResources() {
		if (consumedResources[R_WORKUNITS_OPERATION] <= 0.9 * producedResources[R_WORKUNITS_OPERATION]) {
			double onePercent = 0.01 * producedResources[R_WORKUNITS_OPERATION];
			// TODO if (spaceOpera.getDifficultyLevel() == D_KINDERGARDEN) {
			if (Math.random() * 10 > 5) {
				prospectorsAndShops();
			}
			// demandedResources[R_WORKUNITS]+=10*onePercent;
			// consumedResources[R_WORKUNITS]+=10*onePercent;
			demandedResources[R_WORKUNITS_OPERATION] += 10 * onePercent;
			consumedResources[R_WORKUNITS_OPERATION] += 10 * onePercent;

			// }
			// if (spaceOpera.getDifficultyLevel() == D_EASY) {
			// if (Math.random() * 10 > 8) {
			// prospectorsAndShops();
			// }
			// // demandedResources[R_WORKUNITS]+= 5*onePercent;
			// // consumedResources[R_WORKUNITS]+= 5*onePercent;
			// demandedResources[R_WORKUNITS_OPERATION] += 5 * onePercent;
			// consumedResources[R_WORKUNITS_OPERATION] += 5 * onePercent;
			// if (unEmploymentRate < 8) {
			// unEmploymentRate += 0.1;
			// }
			// }
			// if (spaceOpera.getDifficultyLevel() == D_MEDIUM) {
			// if (unEmploymentRate < 10) {
			// unEmploymentRate += 0.2;
			// }
			// }
			//
			// if (spaceOpera.getDifficultyLevel() == D_HARD ||
			// spaceOpera.getDifficultyLevel() == D_IMPOSSIBLE) {
			// if (unEmploymentRate < 20) {
			// unEmploymentRate += 0.3;
			// }
			// }

			// test unemployment rate:
			// * Notice:
			// * this is just the basic unemployment, the actual rate due to
			// misalignment
			// * in the resource distribution may be much higher!
			// System.out.println("Unemployment rate: " + unEmploymentRate +
			// "%");

		}

		double produced = 0.0;
		double consumed = 0.0;
		double demanded = 0.0;
		// accumulate work demands
		for (int res = R_WORKUNITS_CONSTRUCTION; res <= R_WORKUNITS_ADMINISTRATION; res++) {
			produced += producedResources[res];
			consumed += consumedResources[res];
			demanded += demandedResources[res];
		}
		demandedResources[R_WORKUNITS] = demanded;
		producedResources[R_WORKUNITS] = totalProducedWork; // produced;
		consumedResources[R_WORKUNITS] = consumed;

		// Test
		// if (player instanceof MPlayer) {
		// System.out.println("Colony " + name + ": unemployment rate = " +
		// unEmploymentRate + "%.");
		// }

	}

	private void addResearchBonus() {
		// XXX: research, military, others?
		producedResources[R_WORKUNITS_EDUCATION] += researchBonus;
		producedResources[R_WORKUNITS_EDUCATION] *= researchFactor;
		//

		// if (player instanceof MPlayer){
		// System.out.println("researchBonus = " + researchBonus + ",
		// researchFactor = " + researchFactor);
		// System.out.println("producedResources[R_WORKUNITS_RESEARCH] = " +
		// producedResources[R_WORKUNITS_RESEARCH]);
		// }
	}

	private void maintainBuildings() {
		// some basic buildings like Power Plant have to produce their output
		// first
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (building.getName().equals(POWER_PLANT) || building.getName().equals(SOLAR_POWER_PLANT)
					|| building.getName().equals(TEMPLE)) {
				useResourcesForOperationOf(building);
				addBuildingOutputToColony(building);
			}
		}
		// so that the more advanced buildings later can use it...
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (!building.getName().equals(POWER_PLANT) && !building.getName().equals(SOLAR_POWER_PLANT)
					&& !building.getName().equals(TEMPLE)) {
				useResourcesForOperationOf(building);
				addBuildingOutputToColony(building);
			}
		}
	}

	private void storeResourcesForNextTurn() {
		// production and usage in this turn, don't store work and electricity
		for (int i = R_RAWMATERIAL; i < R_MAX; i++) {
			storedResources[i] += producedResources[i];
			storedResources[i] -= consumedResources[i];
		}
	}

	private void buildingsAllowConstructionOf() {
		// Warning: this function may be called several times per turn
		// especially from ColonyDetail
		for (int i = 0; i < buildings.size(); i++) {
			Building iBuilding = (Building) buildings.get(i);
			// special Building effects:
			if (iBuilding.getName().equals(ORBITAL_SHIPYARD)) {
				effectsOfOrbitalShipYard();
			}
		}
	}

	private void buildingsWithSpecialEffects() {
		// special effects of buildings, eg. allow/disallow construction of...
		// Warning: this function may be called only once per turn
		for (int i = buildings.size() - 1; i >= 0; i--) {
			Building iBuilding = (Building) buildings.get(i);
			PreferredPlanetValues preferences = player.getPreferredPlanetValues();
			// special Building effects:
			if (iBuilding.getName().equals(COLONY_MANAGER)) {
				effectsOfColonyManager();
			}
			// XXX: terraforming: make sure to remove building from
			// possibleBuildings when 'maxed out'
			// terraforming: improve planet fertility ...
			if (iBuilding.getFertilityIncrease() > 0) {
				// ... depending on current fertility
				if (planet.fertilityIsVeryLow()) {
					planet.setFertility(planet.getFertility() + iBuilding.getFertilityIncrease() * 3);
				} else if (planet.fertilityIsLow()) {
					planet.setFertility(planet.getFertility() + iBuilding.getFertilityIncrease() * 2);
				} else if (planet.fertilityIsAverage()) {
					planet.setFertility(planet.getFertility() + iBuilding.getFertilityIncrease());
				} else if (planet.fertilityIsHigh()) {
					planet.setFertility(planet.getFertility() + iBuilding.getFertilityIncrease() / 2);
				} else if (planet.fertilityIsVeryHigh()) {
					planet.setFertility(planet.getFertility() + iBuilding.getFertilityIncrease() / 4);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// terraforming: improve average temperature
			if (iBuilding.getTemperatureImprovement() > 0) {
				// ... depending on current temperature
				if (preferences.isTemperatureOutOfRange(planet.getAvgTemp())) {
					planet.improveTemperature(player, iBuilding.getTemperatureImprovement() * 10);
				} else if (preferences.isTemperatureExtreme(planet.getAvgTemp())) {
					planet.improveTemperature(player, iBuilding.getTemperatureImprovement() * 5);
				} else if (preferences.isTemperatureAcceptable(planet.getAvgTemp())) {
					planet.improveTemperature(player, iBuilding.getTemperatureImprovement() * 3);
				} else if (preferences.isTemperatureGood(planet.getAvgTemp())) {
					planet.improveTemperature(player, iBuilding.getTemperatureImprovement() * 2);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// radiation: improve the radiation level
			if (iBuilding.getRadiationReduction() > 0) {
				// depending on current radiation level
				if (preferences.radiationIsOutOfRange(planet.getRadiation())) {
					planet.improveRadiation(iBuilding.getRadiationReduction());
				} else if (preferences.radiationIsExtreme(planet.getRadiation())) {
					planet.improveRadiation(iBuilding.getRadiationReduction() / 2);
				} else if (preferences.radiationIsAcceptable(planet.getRadiation())) {
					planet.improveRadiation(iBuilding.getRadiationReduction() / 4);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			if (iBuilding.getAtmosphericPressureCorrection() > 0) {
				if (preferences.airPressureOutOfRange(planet.getAtmosphericPressure())) {
					planet.improveAtmosphericPressure(player, iBuilding.getAtmosphericPressureCorrection() * 2);
				} else if (preferences.airPressureIsExtreme(planet.getAtmosphericPressure())) {
					planet.improveAtmosphericPressure(player, iBuilding.getAtmosphericPressureCorrection());
				} else if (preferences.airPressureIsAcceptable(planet.getAtmosphericPressure())) {
					planet.improveAtmosphericPressure(player, iBuilding.getAtmosphericPressureCorrection() / 2);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			if (iBuilding.getWeatherCorrection() > 0) {
				// corrections to wind speed
				// iBuilding.getWeatherCorrection() is 0.1 and windSpeed is from
				// 0..100 and more,
				// so multiply e.g. by 100 for windSpeed
				if (preferences.windSpeedIsExtreme(planet.getWindSpeed())) {
					planet.improveWindSpeed(player, iBuilding.getWeatherCorrection() * 100);
				}
				if (preferences.windSpeedIsPoor(planet.getWindSpeed())) {
					planet.improveWindSpeed(player, iBuilding.getWeatherCorrection() * 50);
				}
				if (preferences.windSpeedIsAcceptable(planet.getWindSpeed())) {
					planet.improveWindSpeed(player, iBuilding.getWeatherCorrection() * 10);
				}
				// corrections to seasons
				if (preferences.seasonsAreExtreme(planet.getAxialTilt())) {
					planet.improveSeasons(player, iBuilding.getWeatherCorrection() * 3);
				}
				if (preferences.seasonsArePoor(planet.getAxialTilt())) {
					planet.improveSeasons(player, iBuilding.getWeatherCorrection() * 2);
				}
				if (preferences.seasonsAreAcceptable(planet.getAxialTilt())) {
					planet.improveSeasons(player, iBuilding.getWeatherCorrection());
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// atmosphericCompositionImprovement is 0.1 and range is from 0 to 2
			if (iBuilding.getAtmosphericCompositionImprovement() > 0) {
				if (preferences.atmosphericCompositionIsOutOfRange(planet.getAtmosphericComposition())) {
					planet.improveAtmosphericComposition(player, iBuilding.getAtmosphericCompositionImprovement() * 2);
				}
				if (preferences.atmosphericCompositionIsExtreme(planet.getAtmosphericComposition())) {
					planet.improveAtmosphericComposition(player, iBuilding.getAtmosphericCompositionImprovement());
				}
				if (preferences.atmosphericCompositionIsAcceptable(planet.getAtmosphericComposition())) {
					planet.improveAtmosphericComposition(player, iBuilding.getAtmosphericCompositionImprovement() / 2);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// gravitation: improvement is 0.1, range is from 0 to ??
			if (iBuilding.getGravitationImprovement() > 0) {
				if (preferences.gravitationIsOutOfRange(planet.getGravitation())) {
					planet.improveGravitation(player, iBuilding.getGravitationImprovement() * 3);
				} else if (preferences.gravitationIsExtreme(planet.getGravitation())) {
					planet.improveGravitation(player, iBuilding.getGravitationImprovement() * 2);
				} else if (preferences.gravitationIsAcceptable(planet.getGravitation())) {
					planet.improveGravitation(player, iBuilding.getGravitationImprovement());
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// water-land ratio (0 is no water, 2 is water only)
			if (iBuilding.getWaterLandRatioImprovement() > 0) {
				if (preferences.waterLandRatioIsExtreme(planet.getWaterLandRatio())) {
					planet.improveWaterLandRatio(player, iBuilding.getWaterLandRatioImprovement() * 2);
				} else if (preferences.waterLandRatioIsPoor(planet.getWaterLandRatio())) {
					planet.improveWaterLandRatio(player, iBuilding.getWaterLandRatioImprovement());
				} else if (preferences.waterLandRatioIsAverage(planet.getWaterLandRatio())) {
					planet.improveWaterLandRatio(player, iBuilding.getWaterLandRatioImprovement() / 2);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// flora
			if (iBuilding.getFloraImprovement() > 0) {
				if (planet.floraIsVeryLow()) {
					planet.setFlora(planet.getFlora() + iBuilding.getFloraImprovement() * 3);
				} else if (planet.floraIsLow()) {
					planet.setFlora(planet.getFlora() + iBuilding.getFloraImprovement() * 2);
				} else if (planet.floraIsAverage()) {
					planet.setFlora(planet.getFlora() + iBuilding.getFloraImprovement());
				} else if (planet.floraIsHigh()) {
					planet.setFlora(planet.getFlora() + iBuilding.getFloraImprovement() / 2);
				} else if (planet.floraIsVeryHigh()) {
					planet.setFlora(planet.getFlora() + iBuilding.getFloraImprovement() / 4);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
			// fauna
			if (iBuilding.getFaunaImprovement() > 0) {
				if (planet.faunaIsVeryLow()) {
					planet.setFauna(planet.getFauna() + iBuilding.getFaunaImprovement() * 3);
				} else if (planet.faunaIsLow()) {
					planet.setFauna(planet.getFauna() + iBuilding.getFaunaImprovement() * 2);
				} else if (planet.faunaIsAverage()) {
					planet.setFauna(planet.getFauna() + iBuilding.getFaunaImprovement());
				} else if (planet.faunaIsHigh()) {
					planet.setFauna(planet.getFauna() + iBuilding.getFaunaImprovement() / 2);
				} else if (planet.faunaIsVeryHigh()) {
					planet.setFauna(planet.getFauna() + iBuilding.getFaunaImprovement() / 4);
				}
				// remove building so it can be built again
				buildings.remove(iBuilding);
			}
		}
	}

	private void distributeWork() {
		// producedResources[R_WORKUNITS] = 0;
		// int levelPercent=0;
		// Universe universe = spaceOpera.getUniverse();
		// levelPercent=educationLevelMatrix[(int)educationLevel][0];
		// producedResources[R_WORKUNITS_NO_EDUCATION] =
		// levelPercent*totalProducedWork/100;
		// levelPercent=educationLevelMatrix[(int)educationLevel][1];
		// producedResources[R_WORKUNITS_BASIC_EDUCATION] =
		// levelPercent*totalProducedWork/100;
		// levelPercent=educationLevelMatrix[(int)educationLevel][2];
		// producedResources[R_WORKUNITS_SECONDARY_EDUCATION] =
		// levelPercent*totalProducedWork/100;
		// levelPercent=educationLevelMatrix[(int)educationLevel][3];
		// producedResources[R_WORKUNITS_TERTIARY_EDUCATION] =
		// levelPercent*totalProducedWork/100;

		// new and improved:
		producedResources[R_WORKUNITS_CONSTRUCTION] = totalProducedWork / 100 * production;
		producedResources[R_WORKUNITS_EDUCATION] = totalProducedWork / 100 * research;
		// TODO Culture

	}

	public void doDamage(Weapon weapon, float damage) {
		// test
		// System.out.println("Damage to colony " + damage + " by weapon " +
		// weapon.name);
		double damageToBuildings = 0.0;
		int damageToBuildingNo = 0;
		long attackedPopulation = populationCount;
		long killedPopulation = 0;
		// check how many people are protected by bomb shelters
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.get(i);
			if (building.getName().equals("Bomb shelter")) {
				attackedPopulation -= building.getCounter() * 10000;
				if (attackedPopulation < 0) {
					attackedPopulation = 0;
				}
			}
		}
		if (weapon instanceof BeamWeapon) {
			// no damage to buildings
			if (attackedPopulation > 10000) {
				killedPopulation = (int) (populationCount * (damage / 1000));
			}
		}
		if (weapon instanceof Torpedo) {
			// damage to max 1 building
			damageToBuildings = Math.random();
			if (damageToBuildings > 0.5) {
				if (buildings.size() > 0) {
					applyDamageToBuildings();
				}
			}
			if (attackedPopulation > 10000) {
				killedPopulation = (int) (populationCount * (damage / 500));
			}
		}
		if (weapon instanceof Bomb) {
			// damage to several buildings
			damageToBuildings = Math.random() * damage / 2;
			if ((int) damageToBuildings > buildings.size()) {
				damageToBuildings = buildings.size();
			}
			for (int i = 0; i < (int) damageToBuildings; i++) {
				applyDamageToBuildings();
			}
			if (attackedPopulation < 1000) {
				// XXX: rethink this. what if damage > 20?, maybe add some
				// randomness?
				killedPopulation = (int) (damage * 50);
			} else if (attackedPopulation < 10000) {
				killedPopulation = (int) (attackedPopulation * (damage / 25));
			} else {
				killedPopulation = (int) (attackedPopulation * (damage / 100));
			}
		}
		if (killedPopulation > attackedPopulation) {
			killedPopulation = attackedPopulation;
		}
		// System.out.println("Colony " + name +
		// ", Total Population: " + populationCount +
		// ", killed: " + killedPopulation +
		// " with " + weapon.getName());
		populationCount -= killedPopulation;
		if (populationCount < 1) {
			populationCount = 0;
		}
	}

	private void effectsOfOrbitalShipYard() {
		// bigger ships can now be built
		for (int j = 0; j < possibleProducts.size(); j++) {
			Product product = (Product) possibleProducts.elementAt(j);
			if (product.getType().equals(SHIP)) {
				product.setAllowBuild(true);
			}
		}
	}

	private void effectsOfColonyManager() {
		// Basic Colony Manager for basic colony requirements:
		double maxHousing = 0.0;
		boolean houseNeeded = true;
		boolean farmNeeded = true;
		boolean powerPlantNeeded = true;
		// boolean factoryNeeded = true;
		// boolean coalMineNeeded = true;
		// boolean oreMineNeeded = true;
		// boolean sawMillNeeded = true;
		// boolean waterPurificatorNeeded = true;
		double maxElectricity = 0;
		int numFactories = 0;
		int numCoalMines = 0;
		int numOreMines = 0;
		int numSawMills = 0;
		int numWaterPurificators = 0;
		// calculate the available housing and power production
		for (int j = 0; j < buildings.size(); j++) {
			Building jBuilding = (Building) buildings.get(j);
			if (jBuilding.getHousingCount() > 0) {
				maxHousing += jBuilding.getCounter() * jBuilding.getHousingCount();
			}
		}
		for (int j = 0; j < buildings.size(); j++) {
			Building jBuilding = (Building) buildings.get(j);
			if (jBuilding.getResourceProduction(R_ELECTRICITY) > 0) {
				maxElectricity += jBuilding.getCounter() * jBuilding.getResourceProduction(R_ELECTRICITY);
				// Test
				// System.out.println("max electricity: " + maxElectricity);
			}
		}
		// calculate demand for housing
		if (0.8 * maxHousing < populationCount) {
			for (int k = 0; k < productionQueue.size(); k++) {
				Object o = productionQueue.get(k);
				if (o instanceof Building) {
					Building building = (Building) o;
					if (building.getHousingCount() > 0) {
						houseNeeded = false;
					}
				}
			}
		} else {
			houseNeeded = false;
		}

		// calculate demand for food
		if ((averageDemandedResources[R_MEAT] > (1.2 * (averageProducedResources[R_MEAT] + storedResources[R_MEAT])))
				&& (averageDemandedResources[R_VEGETABLES] > (1.2 * (averageProducedResources[R_VEGETABLES] + storedResources[R_VEGETABLES])))) {
			for (int k = 0; k < productionQueue.size(); k++) {
				BuildProject project = (BuildProject) productionQueue.get(k);
				if (project.getResourceProduction(R_FOOD) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_MEAT) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_FISH) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_RICE) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_CROP) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_DAIRY) > 0) {
					farmNeeded = false;
				}
				if (project.getResourceProduction(R_VEGETABLES) > 0) {
					farmNeeded = false;
				}
			}
		} else {
			farmNeeded = false;
		}

		// calculate demand for energy
		if (averageDemandedResources[R_ELECTRICITY] > (0.8 * maxElectricity)) {
			for (int k = 0; k < productionQueue.size(); k++) {
				BuildProject project = (BuildProject) productionQueue.get(k);
				if (project.getResourceProduction(R_ELECTRICITY) > 0) {
					powerPlantNeeded = false;
				}
			}
		} else {
			powerPlantNeeded = false;
		}
		// calculate demand for factories, coal mines, ore mines ...
		// count the existing buildings
		for (int j = 0; j < buildings.size(); j++) {
			Building jBuilding = (Building) buildings.get(j);
			if (jBuilding.getName().equals(FACTORY)) {
				numFactories = jBuilding.getCounter();
			}
			if (jBuilding.getName().equals(COAL_MINE)) {
				numCoalMines = jBuilding.getCounter();
			}
			if (jBuilding.getName().equals(STRIP_MINING_UNIT)) {
				numOreMines = jBuilding.getCounter();
			}
			if (jBuilding.getName().equals(SAW_MILL)) {
				numSawMills = jBuilding.getCounter();
			}
			if (jBuilding.getName().equals(WATER_PURIFICATOR)) {
				numWaterPurificators = jBuilding.getCounter();
			}
		}
		// test: demand one building per 5000 colonists
		// maybe add only to productionqueue if less than n items are in queue?
		// add buildings if needed
		// XXX: select the best from all the existing buildings
		if ((numFactories * 5000 < populationCount) && (getProductionQueueCount(FACTORY) < 1)) {
			addBuildingToProductionQueue(FACTORY);
		}
		if ((numCoalMines * 5000 < populationCount) && (getProductionQueueCount(COAL_MINE) < 1)) {
			addBuildingToProductionQueue(COAL_MINE);
		}
		if ((numOreMines * 5000 < populationCount) && (getProductionQueueCount(STRIP_MINING_UNIT) < 1)) {
			addBuildingToProductionQueue(STRIP_MINING_UNIT);
		}
		if ((numSawMills * 5000 < populationCount) && (getProductionQueueCount(SAW_MILL) < 1)) {
			addBuildingToProductionQueue(SAW_MILL);
		}
		if ((numWaterPurificators * 5000 < populationCount) && (getProductionQueueCount(WATER_PURIFICATOR) < 1)) {
			addBuildingToProductionQueue(WATER_PURIFICATOR);
		}
		// add some more buildings
		if (houseNeeded) {
			for (int k = 0; k < possibleBuildings.size(); k++) {
				Building kBuilding = (Building) possibleBuildings.elementAt(k);
				if (kBuilding.getHousingCount() > 0) {
					addProductionQueue(kBuilding.clone());
					break;
				}
			}
		}
		if (powerPlantNeeded) {
			addBuildingToProductionQueue(POWER_PLANT);
		}
		if (farmNeeded) {
			addBuildingToProductionQueue(FARM);
		}

		oldGrowthFactor = growthFactor;

		// TODO if ((spaceOpera.getDifficultyLevel() == D_KINDERGARDEN) ||
		// (spaceOpera.getDifficultyLevel() == D_EASY)) {
		// adjustResourceDistribution();
		// }

	}

	private void enableFollowupProductsOrComponents(BuildProject buildProject) {
		for (int j = 0; j < possibleBuildings.size(); j++) {
			Building building = (Building) possibleBuildings.elementAt(j);
			if (building.getName().equals(buildProject.getName())) {
				// products
				for (int k = 0; k < possibleProducts.size(); k++) {
					Product p = (Product) possibleProducts.elementAt(k);
					if (building.getProduct() != 0) {
						if (p.getId() == building.getProduct()) {
							p.setAllowBuild(true);
							break;
						}
					}
				}
				// components
				Enumeration pc = possibleComponents.elements();
				while (pc.hasMoreElements()) {
					ShipComponent ko = (ShipComponent) pc.nextElement();
					if (building.getComponent() != 0) {
						if (ko.getId() == building.getComponent()) {
							ko.setAllowBuild(true);
							break;
						}
					}
				}
			}
		}
	}

	private double getPlanetarySupply(int j) {
		double supply = 1.0;
		if (j == R_STONE) {
			supply = 2 - planet.getWaterLandRatio();
		}
		if (j == R_WOOD) {
			supply = planet.getWood();
		}
		if (j == R_OIL) {
			supply = planet.getFossils();
		}
		if (j == R_IRON) {
			supply = planet.getMetals();
		}
		if (j == R_URANIUM) {
			supply = planet.getUranium();
		}
		if (j == R_FOOD) {
			supply = planet.getFauna();
		}
		if (j == R_RICE) {
			supply = planet.getFlora();
		}
		if (j == R_SALT) {
			supply = planet.getSalt();
		}
		if (j == R_GOODS) {
			supply = 4 * Math.random();
		}
		return (supply);
	}

	public int getProductionQueueCount(String buildingName) {
		int count = 0;
		for (int k = 0; k < productionQueue.size(); k++) {
			BuildProject buildProject = (BuildProject) productionQueue.get(k);
			if (buildProject.getName().equals(buildingName)) {
				count++;
			}
		}
		return (count);
	}

	private void addPopulationGrowth() {
		growthFactor = 1.00;
		// Difficulty level
		// TODO if (spaceOpera.getDifficultyLevel() == D_KINDERGARDEN) {
		growthFactor = 1.05;
		// } else if (spaceOpera.getDifficultyLevel() == D_EASY) {
		// growthFactor = 1.03;
		// } else if (spaceOpera.getDifficultyLevel() == D_MEDIUM) {
		// growthFactor = 1.015;
		// } else if (spaceOpera.getDifficultyLevel() == D_HARD) {
		// growthFactor = 1.005;
		// } else if (spaceOpera.getDifficultyLevel() == D_IMPOSSIBLE) {
		// growthFactor = 1.001;
		// }
		// available food, housing and planet fertility affect population growth
		// Food & Housing
		if (availableHousing() < 0.5) {
			growthFactor *= 0.95;
		} else if (availableHousing() < 0.8) {
			growthFactor *= 0.98;
		} else if (availableHousing() > 1.2) {
			growthFactor *= 1.015;
		} else if (availableHousing() >= 1.5) {
			growthFactor *= 1.025;
		}
		if (availableFood() < 0.3) {
			growthFactor *= 0.95;
		} else if (availableFood() < 0.6) {
			growthFactor *= 0.98;
		} else if (availableFood() > 1.2) {
			growthFactor *= 1.015;
		} else if (availableFood() > 1.5) {
			growthFactor *= 1.015;
		}
		// Fertility
		if (planet.getFertility() <= 0.4) {
			growthFactor *= 0.94;
		} else if (planet.getFertility() < 0.8) {
			growthFactor *= 0.97;
		} else if (planet.getFertility() <= 1.2) {
			growthFactor *= 1.001;
		} else if (planet.getFertility() > 1.2) {
			growthFactor *= 1.005;
		} else if (planet.getFertility() > 1.5) {
			growthFactor *= 1.01;
		} else if (planet.getFertility() > 1.8) {
			growthFactor *= 1.02;
		}
		if (longTimeHappiness > 1.5) {
			growthFactor *= 1.01;
		}
		if (longTimeHappiness < 0.5) {
			growthFactor *= 0.99;
		}
		populationCount = (long) ((double) populationCount * growthFactor);
		// add some luck factor
		if ((Math.random() * 10) > 5) {
			populationCount += (int) (Math.random() * 100);
		} else {
			populationCount -= (int) (Math.random() * 100);
		}
		if (populationCount < 0) {
			populationCount = 0;
		}
		if (populationCount > planet.getMaxPopulationCount(player)) {
			// XXX: population can grow over this limit when protective
			// dwelling is available
			populationCount = planet.getMaxPopulationCount(player);
		}

		// Test
		// System.out.println("Colony " + player.getName() + ", growthFactor = "
		// + growthFactor);
	}

	private void refreshDisplay() {
		// TODO if (spaceOpera.getColonyDetail() != null) {
		// if (spaceOpera.getColonyDetail().getColony().equals(this)) {
		// spaceOpera.getColonyDetail().refresh();
		// }
		// }
	}

	// resynch buildings and products between player an colony
	public void refreshPossibilities() {
		possibleBuildings.removeAllElements();
		possibleProducts.removeAllElements();
		possibleComponents.clear();
		List<Building> playerBuildings = player.getPossibleBuildings();
		for (int i = 0; i < playerBuildings.size(); i++) {
			boolean allow = true;
			Building building = (Building) playerBuildings.get(i);
			if (building.isUnique()) {
				// already built?
				for (int j = 0; j < buildings.size(); j++) {
					Building b = (Building) buildings.get(j);
					if (b.getName().equals(building.getName())) {
						allow = false;
					}
				}
				// or already in production queue?
				if (getProductionQueueCount(building.getName()) > 0) {
					allow = false;
				}
			}
			if (allow) {
				// do not add terraform buildings if already 'maxed out'
				if (building.getName().equals(TERRAFORM_FERTILITY)) {
					if (planet.getFertility() >= 2) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_FLORA)) {
					if (planet.getFlora() >= 2) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_FAUNA)) {
					if (planet.getFauna() >= 2) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_TEMPERATURE)) {
					if (player.getPreferredPlanetValues().isTemperatureIdeal(planet.getAvgTemp())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_RADIATION)) {
					if (player.getPreferredPlanetValues().radiationIsIdeal(planet.getRadiation())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_ATMOSPHERIC_PRESSURE)) {
					if (player.getPreferredPlanetValues().airPressureIsIdeal(planet.getAtmosphericPressure())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_WEATHER_CONTROL)) {
					if (player.getPreferredPlanetValues().windSpeedIsIdeal(planet.getWindSpeed())
							&& player.getPreferredPlanetValues().seasonsAreIdeal(planet.getAxialTilt())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_ATMOSPHERIC_COMPOSITION)) {
					if (player.getPreferredPlanetValues().atmosphericCompositionIsIdeal(planet.getAtmosphericComposition())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_GRAVITATION)) {
					if (player.getPreferredPlanetValues().gravitationIsIdeal(planet.getGravitation())) {
						continue;
					}
				}
				if (building.getName().equals(TERRAFORM_WATER_LAND_RATIO)) {
					if (player.getPreferredPlanetValues().waterLandRatioIsIdeal(planet.getWaterLandRatio())) {
						continue;
					}
				}
				possibleBuildings.addElement(building.clone());
			}
		}
		Vector playerProducts = player.getPossibleProducts();
		for (int i = 0; i < playerProducts.size(); i++) {
			Product product = (Product) playerProducts.elementAt(i);
			possibleProducts.addElement(product.clone());
		}
		Enumeration playerComponents = player.getPossibleComponents().elements();
		while (playerComponents.hasMoreElements()) {
			ShipComponent comp = (ShipComponent) playerComponents.nextElement();
			possibleComponents.put(comp.getName(), (ShipComponent) comp.clone());
		}
		buildingsAllowConstructionOf();
	}

	private void subtractTaxes() {
		double taxAmount = totalProducedWork * taxBase / 100.0;
		totalProducedWork -= taxAmount;
		player.addTreasury(taxAmount);

	}

	public void swapProductionQueue(int i, int j) {
		BuildProject buildProject = (BuildProject) productionQueue.get(i);
		productionQueue.set(i, productionQueue.get(j));
		productionQueue.set(j, buildProject);
	}

	private void useUnusedAdministrationResources() {
		double unUsedAdministrationBudget = producedResources[R_WORKUNITS_ADMINISTRATION]
				- consumedResources[R_WORKUNITS_ADMINISTRATION];
		// so far consumed: all buildings that use this resource

		// further usage: colony efficience enhancement
		// XXX: effect on colony efficiency of unusedAdministrationBudget (<5%
		// =bad , >15% =bad ???)

		// use some for admin overhead
		demandedResources[R_WORKUNITS] += unUsedAdministrationBudget * 0.8;
		consumedResources[R_WORKUNITS] += unUsedAdministrationBudget * 0.8;
		demandedResources[R_WORKUNITS_ADMINISTRATION] += unUsedAdministrationBudget * 0.8;
		consumedResources[R_WORKUNITS_ADMINISTRATION] += unUsedAdministrationBudget * 0.8;
	}

	private void useUnusedMilitaryResources() {
		double unUsedDefenseBudget = producedResources[R_WORKUNITS_MILITARY] - consumedResources[R_WORKUNITS_MILITARY];
	}

	private void useUnusedResearchResources() {
		double unUsedEducation = producedResources[R_WORKUNITS_EDUCATION] - consumedResources[R_WORKUNITS_EDUCATION];
		// unused education is simply 'converted' into research points
		if (unUsedEducation > 0) {
			player.addResearchProduction(unUsedEducation);
		}
		demandedResources[R_WORKUNITS] += unUsedEducation;
		consumedResources[R_WORKUNITS] += unUsedEducation;
		demandedResources[R_WORKUNITS_EDUCATION] += unUsedEducation;
		consumedResources[R_WORKUNITS_EDUCATION] += unUsedEducation;
	}

	private void useResourcesForOperationOf(Building building) {
		double efficiency = 100;
		building.setEfficiency(efficiency);
		// a) determine max availability of 'non-food' resources
		for (int j = R_MIN; j < R_MAX; j++) {
			if ((j < R_FOOD) || (j >= R_END_FOOD)) {
				double availableResources = producedResources[j] + storedResources[j] - consumedResources[j];
				double usage = building.getSupportCost(j) * building.getCounter();
				if (usage > 0.8 * availableResources) {
					availableResources *= 0.8; // don't use up everything
					efficiency = 100.0 / usage * availableResources;
					if (efficiency < building.getEfficiency()) {
						building.setEfficiency(efficiency);
						// Warning
						// XXX: news?
						// if (player instanceof MPlayer) {
						// if (building.getName().equals(HABITAT)){
						// System.out.println("Warning: " + building.getName() +
						// ": resource '" + spaceOpera.getResourceName(j) + "'
						// only " + efficiency + " % (of" + usage + ")
						// available.");
						// }
						// }
					}
				}
			}
		}
		// b) availability and use of food
		for (int j = R_MIN; j < R_MAX; j++) {
			if (j == R_FOOD) {
				double availableResources = 0.9 * (producedResources[j] + storedResources[j] - consumedResources[j]);
				double usage = building.getSupportCost(j) * building.getCounter();
				double useFood;
				efficiency = building.getEfficiency(); // 0.. 20.. 70 .. 100
				if (usage > 0) {
					// any food does it
					availableResources = producedResources[R_MEAT] + storedResources[R_MEAT] + producedResources[R_FISH]
							+ storedResources[R_FISH] + producedResources[R_RICE] + storedResources[R_RICE]
							+ producedResources[R_CROP] + storedResources[R_CROP] + producedResources[R_DAIRY]
							+ storedResources[R_DAIRY] + producedResources[R_VEGETABLES] + storedResources[R_VEGETABLES];
					// System.out.println("Building: " + building.getName() + "
					// Resource: " + j + "; Usage: " + usage + ",
					// availableResources: " + availableResources);
					double foodPercentage = 100 / usage * availableResources;
					if (foodPercentage < efficiency) { // eg 70% food, 90%
														// power
						efficiency = foodPercentage;
						building.setEfficiency(efficiency);
						// Warning o
						// if (player instanceof MPlayer) {
						// if (building.getName().equals(HABITAT)){
						// System.out.println("Warning: " + building.getName() +
						// ": resource '" + spaceOpera.getResourceName(j) + "'
						// only " + efficiency + " % available.");
						// }
						// }
					} // else e.g. 90% food, 70% water
					useFood = usage * foodPercentage / 100.0; // V 0.4.440:
																// instead of
																// percentage
					int numtries = 0;
					while ((useFood > 0.2) && (numtries < 100)) {
						for (int k = 1; k < 7; k++) {
							if ((useFood > 10)
									&& ((producedResources[R_FOOD + k] + storedResources[R_FOOD + k] - consumedResources[R_FOOD + k]) > 10)) {
								useFood -= 10;
								consumedResources[R_FOOD + k] += 10;
								// demanded may be higher than consumed, e.g.
								// when foodPercentage is only 70%
								demandedResources[R_FOOD + k] += 10 / (foodPercentage / 100);
							} else if ((useFood > 1)
									&& ((producedResources[R_FOOD + k] + storedResources[R_FOOD + k] - consumedResources[R_FOOD + k]) > 1)) {
								useFood -= 1;
								consumedResources[R_FOOD + k] += 1;
								demandedResources[R_FOOD + k] += 1 / (foodPercentage / 100);
							} else if ((useFood > 0.1)
									&& ((producedResources[R_FOOD + k] + storedResources[R_FOOD + k] - consumedResources[R_FOOD + k]) > 0.1)) {
								useFood -= 0.1;
								consumedResources[R_FOOD + k] += 0.1;
								demandedResources[R_FOOD + k] += 0.1 / (foodPercentage / 100);
								numtries += 1;
							} else {
								numtries += 1;
							}
							if (useFood <= 0) {
								break;
							}
						}
					}
				}
			}
		}
		// c) all other resources than food
		for (int j = R_MIN; j < R_MAX; j++) {
			if ((j < R_FOOD) || (j >= R_END_FOOD)) {
				// double availableResources = 0.9*(producedResources[j] +
				// storedResources[j]);
				double usage = building.getSupportCost(j) * building.getCounter();
				double availableResources = producedResources[j] + storedResources[j] - consumedResources[j];
				efficiency = building.getEfficiency();
				demandedResources[j] += usage;
				if (usage > availableResources) {
					usage *= efficiency / 100.0;
				}
				consumedResources[j] += usage;
			}
		}
	}

	private void useUnusedResources() {
		useUnusedOperationResources();
		useUnusedResearchResources();
		useUnusedMilitaryResources();
		useUnusedAdministrationResources();
	}

	// distribute resources top down in the production queue
	private void constructionWork() {
		for (int i = 0; i < productionQueue.size(); i++) {
			BuildProject buildProject = (BuildProject) productionQueue.get(i);
			for (int j = R_MIN; j < R_MAX; j++) {
				// double maxPercentage = 100;
				double availableResources = storedResources[j] + producedResources[j] - consumedResources[j];
				double neededResources = buildProject.getProductionCost(j) - buildProject.getProductionCostPaid(j);

				double usedResources = 0;
				demandedResources[j] += neededResources;
				if (neededResources > 0) {
					if (availableResources > 0.01) {
						if (neededResources > availableResources) {
							usedResources = availableResources;
						} else {
							usedResources = neededResources;
						}
						buildProject.setProductionCostPaid(j, buildProject.getProductionCostPaid(j) + usedResources);
						consumedResources[j] += usedResources;
					}
				}
			}
		}
		// remove the completed buildings
		for (int i = productionQueue.size() - 1; i >= 0; i--) {
			BuildProject buildProject = (BuildProject) productionQueue.get(i);
			if (buildProject.getPercentComplete() >= 100) {
				if ((buildProject.getName().equals(COLONY_SHIP)) && (populationCount < (2 * NUMCOLONISTS))) {
					// ship is complete, but cannot be stocked with colonists
					if (player instanceof MPlayer) {
						System.out.println("Colony ship cannot be launched because there are not enough colonists!");
					}
				} else {
					buildCompleted(buildProject);
				}
			}
		}
	}

	private void maintainShips() {
		List<SpaceCraft> ships = player.getShips();
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft spaceCraft = (SpaceCraft) ships.get(i);
			if (this.equals(spaceCraft.getColony())) {
				for (int j = R_MIN; j < R_MAX; j++) {
					double efficiency = 0;
					double usage = spaceCraft.getServiceCost(player, j);
					double usedResources = 0.0;
					if (usage > 0) {
						double availableResources = producedResources[j] + storedResources[j] - consumedResources[j];
						demandedResources[j] += usage;
						if (usage < availableResources) {
							efficiency = 100;
							consumedResources[j] += usage;
							usedResources = usage;
						} else {
							efficiency = 100 / usage * availableResources;
							consumedResources[j] += availableResources;
							usedResources = availableResources;
						}
						spaceCraft.setResourcePercent(efficiency);
					}
				}
			}
			// repair ships in orbit
			int repairInPercent = 10;
			if (spaceCraft.getTargetSun() != null && spaceCraft.getTargetSun().getName().equals(this.getSun().getName())
					&& !spaceCraft.isTraveling()) {
				// System.out.println("ship in orbit: " + spaceCraft +
				// " around " + this.getName());
				// XXX: add other buildings when available, e.g. spacedock,
				// repairyard, ..., then tune units
				for (int j = 0; j < buildings.size(); j++) {
					Building iBuilding = (Building) buildings.get(j);
					// special Building effects:
					if (iBuilding.getName().equals(ORBITAL_SHIPYARD)) {
						repairInPercent = 25;
					}
				}
			}
			spaceCraft.repair(repairInPercent);
		}
	}

	private void maintainWeapons() {
		// XXX: priorities ok? (first groundbased, then satellites, then ships
		// in orbit...)
		for (int i = 0; i < weapons.size(); i++) {
			Weapon w = (Weapon) weapons.elementAt(i);
			restockWeapon(w);
		}

		List<SpaceCraft> ships = player.getShips();
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft spaceCraft = ships.get(i);
			if (!spaceCraft.isShip()) {
				// it's a satellite, mrs Walker, it's a satellite...
				restockShip(spaceCraft);
			} else {
				// re-stock ships in orbit
				if (spaceCraft.getTargetSun() != null && spaceCraft.getTargetSun().getName().equals(this.getSun().getName())
						&& !spaceCraft.isTraveling()) {
					restockShip(spaceCraft);
				}
			}
		}

	}

	private void restockShip(SpaceCraft spaceCraft) {
		Vector ws = spaceCraft.getWeapons();
		for (int j = 0; j < ws.size(); j++) {
			Weapon w = (Weapon) ws.elementAt(j);
			restockWeapon(w);
		}
	}

	// XXX: add new types of weapons when they are created
	private void restockWeapon(Weapon w) {
		if ((w.getName().equals(TORPEDO)) || (w.getName().equals(LONG_RANGE_TORPEDO)) || (w.getName().equals(HIGH_SPEED_TORPEDO))
				|| (w.getName().equals(STARBURST_TORPEDO))) {
			if (w.getAmmo() <= 0) {
				int restock = w.getBaseAmmo();
				if (torpedoStorage > restock) {
					// TEST
					// System.out.println("Weapon " + w.getName() + " restocked.
					// ");
					w.restockAmmo(restock);
					torpedoStorage -= restock;
				}
			}
		}
	}

	private void adjustResourceDistribution() {
		if (colonyAge > 3) {
			// System.out.println("Resource demand = " +
			// resourceDemand[R_WORKUNITS]);

			// TODO adjustResourceDistribution

		}
	}

	private void transferShipsToOtherColony() {
		// XXX: ... if there is one, else the ships hang around for a while ...
		List<Colony> colonies = player.getColonies();
		Colony transferColony = null;
		if (colonies.size() > 0) {
			transferColony = colonies.get(0);
		}
		List<SpaceCraft> ships = player.getShips();
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft spaceCraft = ships.get(i);
			if (this.equals(spaceCraft.getColony())) {
				if (transferColony != null && transferColony != this) {
					spaceCraft.setColony(transferColony);
				} else {
					spaceCraft.setColony(null);
				}
			}
		}
	}

	public String getPlanetName() {
		return planetName;
	}

	public BuildProject getBuildProject(String name) {
		System.out.println("getting " + name);
		List<Building> playerBuildings = player.getPossibleBuildings();
		for (int i = 0; i < playerBuildings.size(); i++) {
			boolean allow = true;
			BuildProject temp = (BuildProject) playerBuildings.get(i);
			if (temp.getName().equals(name)) {
				System.out.println(temp.getName());
				return temp;
			}
		}
		Vector playerProducts = player.getPossibleProducts();
		for (int i = 0; i < playerProducts.size(); i++) {
			BuildProject temp = (BuildProject) playerProducts.elementAt(i);
			if (temp.getName().equals(name)) {
				System.out.println(temp.getName());
				return temp;
			}
		}
		System.out.println("why is this null? (" + name + ")");
		return null;
	}

	public int getCulture() {
		return culture;
	}

	public void setCulture(int culture) {
		this.culture = culture;
	}

	public int getTaxBase() {
		return taxBase;
	}

	public void setTaxBase(int tax) {
		taxBase = tax;
	}

	public ColonyManager getColonyManager() {
		return colonyManager;
	}

}
