//The class Orbit defines the orbit for planets.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.elements;

/** Every planet has an orbit around the central sunsystem.
 */
public class Orbit {
  private int radius=0;                     // radius in Pixel (?)
  private int position=0;                   // position in Grad [0..359]

  //Accessors
  public void display() { }
  public int  getPosition()        {return(position);}
  public int  getRadius()          {return(radius);}
  public void setPosition(int pos) {position=pos;}

  //Methods
  public Orbit(int r, int p) {
    radius =r;
    position = p;
  }

  public String toString() {
    return (new String("Radius " + radius + ", Degree " + position));
  }
public int getNumber() {
	// TODO orbit number
	return 0;
}
}