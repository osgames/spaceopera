//The class Sun defines a sun in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.elements;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Sun class is used to display the major sun types on the universe display
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Sun implements SOConstants, Comparable {

	// XXX: move number, planets, ships, and so on to class SunSystem!
	private SunSystem sunSystem;
	private String name = "";
	private String type = "";
	private Color color;
	private int x;
	private int y;
	private int z;
	private boolean selected = false;
	private Graphics g;
	private Vector ships;
	private Hashtable flags; // contains the names of the players and their
								// colors
	// private SpaceOpera spaceOpera;
	private Fullscreen fs;
	private boolean isScanned = false;

	// Accessors
	public void addExploredByPlayer(String n) {
		sunSystem.addExploredByPlayer(n);
	}

	public void addFlag(Player p) {
		flags.put(p.getName(), p);
	}

	public void addSpaceCraft(SpaceCraft rs) {
		ships.addElement(rs);
	}

	public Color getColor() {
		return (color);
	}

	public String getName() {
		return (name);
	}

	public int getNumber() {
		return (sunSystem.getNumber());
	}

	public Vector getPlanets() {
		return (sunSystem.getPlanets());
	}

	public boolean getSelected() {
		return (selected);
	}

	public Vector getSpaceCraft() {
		return (ships);
	}

	public SunSystem getSunSystem() {
		return (sunSystem);
	}

	public int getX() {
		return (x);
	}

	public int getY() {
		return (y);
	}

	public boolean isExploredBy(String n) {
		return (sunSystem.isExploredBy(n));
	}

	public void setScanned(boolean b) {
		isScanned = b;
	}

	public void setSelected(boolean s) {
		selected = s;
	}

	// Methods
	public void cleanup(Graphics g) {
		// paint flags for colonized planets (one per player only)
		int j = -5;
		Enumeration flagList = flags.elements();
		while (flagList.hasMoreElements()) {
			Player player = (Player) flagList.nextElement();
			// clear flag at position x+i, y
			g.setColor(Color.black);
			g.drawLine(x + 1 + j, y - 9, x + 1 + j, y - 2);
			g.drawLine(x + 2 + j, y - 9, x + 2 + j, y - 2);
			g.drawLine(x + 3 + j, y - 9, x + 3 + j, y - 6);
			g.drawLine(x + 4 + j, y - 9, x + 4 + j, y - 6);
			g.drawLine(x + 5 + j, y - 9, x + 5 + j, y - 6);
			j += 4;
		}
	}

	public void display(Graphics g) {
		// Test
		// if (ships.size()>0) {
		// System.out.println(ships.size() + " ships at sunsystem " +
		// this.getName());
		// }

		// paint flags for colonized planets (one per player only)
		int j = -5;
		Enumeration flagList = flags.elements();
		while (flagList.hasMoreElements()) {
			Player player = (Player) flagList.nextElement();
			// XXX if ((player instanceof MPlayer) ||
			// (spaceOpera.getShowColonies()) ||
			// (isScanned)
			// ){
			// // or sun in scannerrange
			// // or was in scannerrange
			// // or spying, union,...
			// Color pcolor =player.getColor();
			// // paint flag at position x+i, y
			// g.setColor(pcolor);
			// g.drawLine(x+1+j, y-9, x+1+j, y-2);
			// g.drawLine(x+2+j, y-9, x+2+j, y-2);
			// g.drawLine(x+3+j, y-9, x+3+j, y-6);
			// g.drawLine(x+4+j, y-9, x+4+j, y-6);
			// g.drawLine(x+5+j, y-9, x+5+j, y-6);
			// j += 4;
			// }
		}
		// paint star
		g.setColor(color);
		int mitte = STARSIZE / 2;
		g.fillOval(x, y, STARSIZE, STARSIZE);
		g.drawLine(x + mitte, y - 2, x + mitte, y + STARSIZE + 2);
		g.drawLine(x - 1, y + mitte, x + STARSIZE + 2, y + mitte);
		if (selected) {
			g.setColor(Color.white);
			g.drawOval(x - 2, y - 2, STARSIZE + 4, STARSIZE + 4);
		} else {
			g.setColor(Color.black);
			g.drawOval(x - 2, y - 2, STARSIZE + 4, STARSIZE + 4);
		}
	}

	// XXX: starsystems should be grouped in clusters. The current approach is
	// not optimal.
	// better would be to seed like 4-10 random stars and group the rest around
	// those?
	public void generate(List suns, int number, Universe u) {
		// position the new sunsystem
		Sun sun;
		boolean tooCloseOrTooFar;
		int oldX = 0;
		int oldY = 0;
		int numOfSuns = 0;
		final int numOfClusters = 4;
		numOfSuns = suns.size();
		//
		// do {
		// tooCloseOrTooFar = false;
		// // random position new starsystem

		// x = (int) (Math.random() * u.getMaxSunsystemSpread()) +
		// u.getSunsystemOffset() / 2; //
		// spaceOpera.getUniverse().getUniverseSize()-STARSIZE));
		// y = (int) (Math.random() * u.getMaxSunsystemSpread()) +
		// u.getSunsystemOffset() / 2;//
		// (spaceOpera.getUniverse().getUniverseSize()-3*STARSIZE));
		//
		// //use the whole 900*900 area
		// //x = (int)(Math.random() * u.getUniverseSize()-25) + 10;
		// //y = (int)(Math.random() * u.getUniverseSize()-25) + 10;
		//
		//
		// // position the new sunsystem close to the previous one (build
		// clusters)
		// if (numOfSuns % 4 == 2) {
		// try {
		// sun = (Sun) suns.get(numOfSuns - 1);
		// oldX = sun.getX();
		// oldY = sun.getY();
		// } catch (Exception e) {
		// System.err.println("what exception" + e);
		// }
		// long aa = (x - oldX) * (x - oldX);
		// long bb = (y - oldY) * (y - oldY);
		// double cc = Math.sqrt(aa + bb);
		// // not too far away from previous sunsystem
		// if (cc > (MAXSTARDIST)) {
		// tooCloseOrTooFar = true;
		// }
		// }
		//
		// // not too close to any sunsystem
		// for (int i = 0; i < suns.size(); i++) {
		// sun = (Sun) suns.get(i);
		// long a = (x - sun.getX()) * (x - sun.getX());
		// long b = (y - sun.getY()) * (y - sun.getY());
		// double c = Math.sqrt(a + b);
		// if (c < MINSTARDIST) {
		// tooCloseOrTooFar = true;
		// }
		// }
		// } while (tooCloseOrTooFar);

		// new logic
		do {
			tooCloseOrTooFar = false;
			// random position new starsystem

			// set the first few ones
			if (number == 0) {
				// top left quadrant
				x = (int) (Math.random() * (u.getUniverseSize() / 2));
				y = (int) (Math.random() * (u.getUniverseSize() / 2));
			} else if (number == 1 && u.getGalaxySize()>1) {
				// bottom left quadrant
				x = (int) (Math.random() * (u.getUniverseSize() / 2));
				y = (int) (Math.random() * (u.getUniverseSize() / 2)) + (u.getUniverseSize() / 2);
			} else if (number == 2&& u.getGalaxySize()>2) {
				// top right quadrant
				x = (int) (Math.random() * (u.getUniverseSize() / 2)) + (u.getUniverseSize() / 2);
				y = (int) (Math.random() * (u.getUniverseSize() / 2));
			} else if (number == 3&& u.getGalaxySize()>3) {
				// bottom right quadrant
				x = (int) (Math.random() * (u.getUniverseSize() / 2)) + (u.getUniverseSize() / 2);
				y = (int) (Math.random() * (u.getUniverseSize() / 2)) + (u.getUniverseSize() / 2);
			} else {
				// set the next one based on the position of the previous one
				int selectOne = (int) (Math.random() * number);
				Sun previous = (Sun) suns.get(selectOne);
				x = previous.getX();
				y = previous.getY();
				if (x < u.getUniverseSize() && Math.random() > 0.5) {
					x += MINSTARDIST + (int) (Math.random() * (u.getDifficultyLevel() * 15));
				} else {
					x -= MINSTARDIST + (int) (Math.random() * (u.getDifficultyLevel() * 15));
				}

				if (y < u.getUniverseSize() && Math.random() > 0.5) {
					y += MINSTARDIST + (int) (Math.random() * (u.getDifficultyLevel() * 15));
				} else {
					y -= MINSTARDIST + (int) (Math.random() * (u.getDifficultyLevel() * 15));
				}

			}
			if (x <= 10 || y <= 10 || x >= u.getUniverseSize() - 20 || y >= u.getUniverseSize() - 20) {
				tooCloseOrTooFar = true;
			}

			// not too close to any sunsystem
			for (int i = 0; i < suns.size(); i++) {
				sun = (Sun) suns.get(i);
				long a = (x - sun.getX()) * (x - sun.getX());
				long b = (y - sun.getY()) * (y - sun.getY());
				double c = Math.sqrt(a + b);
				if (c < MINSTARDIST) {
					tooCloseOrTooFar = true;
				}
			}

		} while (tooCloseOrTooFar);
		sunSystem = new SunSystem(this, fs);
		sunSystem.generate(number);
		int sunType = (int) (Math.random() * SUN_TYPES);
		switch (sunType) {
		case 0:
			type = "G2";
			color = Color.white;
			break;
		case 1:
			type = "White Dwarf";
			color = Color.white;
			break;
		case 2:
			type = "F5";
			color = Color.yellow;
			break;
		case 3:
			type = "Red Dwarf";
			color = Color.red;
			break;
		case 4:
			type = "Blue giant";
			color = Color.blue;
			break;
		case 5:
			type = "Green giant";
			color = Color.green;
			break;
		default:
			type = "White Dwarf";
			color = Color.white;
		}
		return;
	}

	public boolean isClicked(int ix, int iy) {
		// System.out.println("ix=" + ix + " iy=" + iy + ", x=" + x + " y=" +
		// y);
		if ((ix >= x - 15) && (iy >= y - 15) && (ix <= (x + STARSIZE + 15)) && (iy <= (y + STARSIZE + 15))) {
			// System.out.println("selected=true\n\n\n");
			return true;
		} else {
			// System.out.println("selected=false");
			return false;
		}
	}

	public void load() {
	}

	public void removeFlag(String name) {
		int numColonies = 0;
		Vector planets = sunSystem.getPlanets();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			Colony colony = planet.getColony();
			if (colony != null) {
				Player player = colony.getPlayer();
				String pname = player.getName();
				if (pname.equals(name)) {
					numColonies++;
				}
			}
		}
		if (numColonies < 2) {
			flags.remove(name);
			// TODO ? fs.displayUniverse();
		}
	}

	public void save() {
	}

	public Sun(String n, Fullscreen fs) {
		ships = new Vector();
		flags = new Hashtable();
		this.fs = fs;
		name = n;
	}

	public String toString() {
		return (new String("Sun " + name + ", Typ " + type + " Coordinates " + x + ", " + y));
	}

	public int compareTo(Object arg0) {
		return this.getName().compareTo(((Sun) arg0).getName());
	}
}