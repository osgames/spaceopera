//The class Planets is used for planets in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.elements;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

import spaceopera.gui.objects.weapons.Weapon;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ai.PreferredPlanetValues;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Planet class contains the planet details. One instance for each planet in
 * the universe.
 */
@SuppressWarnings({"rawtypes"})
public class Planet implements SOConstants {

	private int mx = 0;
	private int my = 0;
	private int size = 0;
	private int orbitNumber = 0; // Number of planet (Merkur=0, Pluto=8)
	private String name = ""; // Name of planet
	private String image = ""; // Picture of planet
	private String type = ""; // Type of planet: earth-like, mars-like... //
								// Gasriese ... (?)
	private Color color;
	private Orbit orbit = null;
	private Colony colony = null;
	private Fullscreen fs;
	private SunSystem sunSystem = null;
	private Ship colonizingShip = null;
	private boolean selected = false;
	private boolean colonized = false;
	private double avgTemp = 20.0; // average temperature (assumption: 20=Earth)
	private double atmosphericPressure = 1.0; // air pressure (1.0=Earth)
	private double windSpeed = 15.0; // average wind speed in km/h
	private double gravitation = 1.0; // 1.0 = Standard-Earth
	private double density = 4.3; // 4.3 = Standard Earth (?)
	private double waterLandRatio = 1.0; // 0.0 = none, 2.0 = Waterworld
	private double fertility = 1.0; // 0.0 = dead, 1.0 = earth, 2.0 = Jungle
	private double geoAge = 4.3; // 4.3 * 10^9 years (?)
	private double hoursPerDay = 24.0;
	private double daysPerYear = 365.25;
	private double axialTilt = 1.0; // 0.0 = none, 1.0 = earthlike, 2.0 =
									// violent
	private double radiation = 300; // average radiation dose per year (in
									// milliRem)
	private double atmosphericComposition = 1.0; // 0.0 = dead, 1.0 = earthlike,
													// 2.0 = toxic
	private double flora = 1.0; // 1.0 = Earth, 0.0 = none, 2.0 = much
	private double fauna = 1.0; // 0.0 .. 1.0 = Earth .. 2.0
	private double stone = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double metals = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double fossils = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double salt = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double uranium = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double wood = 1.0; // 0.0 .. 1.0=Earth .. 2.0
	private double energy = 1.0; // Solar-, water- and wind-energy
	private Vector moons;
	private String special = "none";

	// Accessors
	public void addExploredByPlayer(String p) {
		getSun().addExploredByPlayer(p);
	}

	public void addSpaceCraft(SpaceCraft rs) {
		sunSystem.addSpaceCraft(rs);
	}

	// TODO public void displayColony() {spaceOpera.displayColony(colony);}
	// public void displayNextPlanet() {spaceOpera.displayNextPlanet();}
	// public void displayPreviousPlanet() {spaceOpera.displayPreviousPlanet();}
	public double getAtmosphericPressure() {
		return (atmosphericPressure);
	}

	public double getAtmosphericComposition() {
		return (atmosphericComposition);
	}

	public double getAvgTemp() {
		return (avgTemp);
	}

	public double getAvgAirSpeed() {
		return (windSpeed);
	}

	public Colony getColony() {
		return (colony);
	}

	public double getDaysPerYear() {
		return (daysPerYear);
	}

	public double getDensity() {
		return (density);
	}

	public double getEnergy() {
		return (energy);
	}

	public double getFauna() {
		return (fauna);
	}

	public double getFertility() {
		return (fertility);
	}

	public double getFlora() {
		return (flora);
	}

	public double getFossils() {
		return (fossils);
	}

	public double getGeoAge() {
		return (geoAge);
	}

	public double getGravitation() {
		return (gravitation);
	}

	public double getHoursPerDay() {
		return (hoursPerDay);
	}

	public String getImage() {
		return (image);
	}

	public double getMetals() {
		return (metals);
	}

	public double getRadiation() {
		return (radiation);
	}

	public String getName() {
		return (name);
	}

	public int getNumber() {
		return (orbitNumber);
	}

	public Orbit getOrbit() {
		return (orbit);
	}

	public Player getPlayer() {
		return (colony.getPlayer());
	}

	public String getPlayerName() {
		return (colony.getPlayerName());
	}

	public double getSalt() {
		return (salt);
	}

	public double getAxialTilt() {
		return (axialTilt);
	}

	public boolean getSelected() {
		return (selected);
	}

	public Vector getSpaceCraft() {
		return (sunSystem.getSpaceCraft());
	}

	public String getSpecial() {
		return (special);
	}

	public double getStone() {
		return (stone);
	}

	public Sun getSun() {
		return (sunSystem.getSun());
	}

	public String getSunName() {
		return (sunSystem.getName());
	}

	public SunSystem getSunSystem() {
		return (sunSystem);
	}

	public int getSunSystemX() {
		return (sunSystem.getX());
	}

	public int getSunSystemY() {
		return (sunSystem.getY());
	}

	public double getUranium() {
		return (uranium);
	}

	public String getType() {
		return (type);
	}

	public double getWaterLandRatio() {
		return (waterLandRatio);
	}

	public double getWood() {
		return (wood);
	}

	public int getX() {
		return (mx);
	}

	public int getY() {
		return (my);
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public boolean isColonized() {
		return (colonized);
	}

	public boolean isExploredBy(String p) {
		return (getSun().isExploredBy(p));
	}

	public void setName(String n) {
		name = n;
	}

	public void setSelected(boolean b) {
		selected = b;
	}

	public void setSunSelected(boolean b) {
		sunSystem.setSunSelected(b);
	}

	public void setWindSpeed(double w) {
		windSpeed = w;
	}

	// Methods
	private void addColony(Player player, double educationLevel) {
		colonized = true;
		colony = new Colony(fs, player, this, NUMCOLONISTS);
		colony.refreshPossibilities();
		colony.setEducationLevel(educationLevel);

		player.addColony(colony);
		sunSystem.addFlag(player);
	}

	private void initializeFloraFauna() {
		// valid on all planet types
		if (fertilityIsVeryLow() || fertilityIsLow()) {
			flora = fertility * 0.4;
		}
		if (fertilityIsAverage()) {
			flora = 0.5 + fertility * 0.8;
		}
		if (fertilityIsHigh() || fertilityIsVeryHigh()) {
			flora = 1.0 + fertility * 0.5;
		}

		// XXX: refactor flora <= x etc.
		if (flora < 0.5) {
			fauna = flora * 0.5;
			wood = 0.0;
		} else if (flora < 1.0) {
			fauna = 0.5 + flora * 0.8;
			wood = flora * 0.5;
		} else {
			fauna = 1.0 + flora * 0.5;
			wood = 1.0 + flora * 0.4;
		}

		if (avgTempOutOfRange()) {
			flora /= 4;
			fauna /= 5;
		}
		if (avgTempExtreme()) {
			fauna /= 2;
			flora /= 1.5;
		}
		if (atmPressureOutOfRange()) {
			atmosphericComposition = 0.3;
			fauna /= 3;
			flora /= 2;
		}
	}

	/**
	 * Called from Player.setPlayerPosition() and from Cheatwindow
	 * 
	 * @param player
	 */
	public void colonize(Player player) {
		addColony(player, 10.0);
	}

	public void colonize(Ship ship) {
		colonizingShip = ship;
		Player player = ship.getPlayer();
		addColony(player, ship.getEducationLevel());
		// TODO spaceOpera.deleteShipFromUniverse(sunSystem, colonizingShip);
		// spaceOpera.displayUniverse();
	}

	public void display(Graphics g, int x, int y) {
		mx = x - size / 2;
		my = y - size / 2;
		g.setColor(color);
		g.fillOval(mx, my, size, size);
		if (selected) {
			g.setColor(Color.white);
			g.drawOval(mx - 3, my - 3, size + 5, size + 5);
		}
	}

	public void doDamage(Weapon w, float damage) {
		// test
		// System.out.println("Damage to colony " + damage + " by weapon " +
		// w.name);
		// other weapons?
		// effects on planet
		// **************************
		// XXX: not all weapons affect planet in the same way
		// e.g. lasers have no big effect
		// bombs may increase radioactivity
		// bombs may affect weather, habitability....
		// increase of airspeed and avgtemp, decrease of fertility?
		if (damage > 20) {
			windSpeed += 0.1;
			avgTemp += 0.1;
			if (fertilityIsVeryHigh()) {
				fertility -= 0.1;
			}
			if (fertilityIsHigh()) {
				fertility -= 0.01;
			}
			if (fertilityIsAverage()) {
				fertility -= 0.005;
			}
			if (fertilityIsLow()) {
				fertility -= 0.001;
			}
		}
		if (colonized) {
			colony.doDamage(w, damage);
			if (colony.getPopulationCount() == 0) {
				colonized = false;
				Player player = colony.getPlayer();
				colony.getSun().removeFlag(player.getName());
				player.removeColony(colony);
				System.out.println("Colony " + colony.getName() + " of player " + player.getName() + " destroyed.");

				// TODO ColonyDetail c = spaceOpera.getColonyDetail();
				// if (c!=null) {
				// if (c.getColony().equals(colony)) {
				// spaceOpera.disposeColonyDetail();
				// //spaceOpera.resetColonyDetail();
				// }
				// }
				colony = null;
			} else {
				// TODO ColonyDetail c = spaceOpera.getColonyDetail();
				// if (c != null) {
				// if (c.getColony().equals(colony)) {
				// c.refresh();
				// }
				// }
			}
		}
	}

	// generateNewPlanets
	public void generate() {
		// No gas giants on inner orbits
		// usually no earthlike planets on outer orbits? (except when playin
		// kindergarten or so?)
		int planetenTyp = 0;
		if (orbitNumber < 3) {
			planetenTyp = (int) (Math.random() * 3); // Mercury or Venus
		} else if (orbitNumber < 7) {
			// TODO if (spaceOpera.getDifficultyLevel()==D_KINDERGARDEN) {
			planetenTyp = 2; // Earth
			// } else {
			// planetenTyp = (int) (Math.random() * 4 + 1); // Venus, Earth or
			// Mars
			// }
		} else if (orbitNumber < 9) {
			// TODO if (spaceOpera.getDifficultyLevel()<=D_EASY) {
			planetenTyp = (int) (Math.random() * 2 + 3); // Earth, Mars, Belt
			// } else {
			// planetenTyp = (int) (Math.random() * 2 + 5); // Belt or Gasgiant
			// }
		} else {
			planetenTyp = (int) (Math.random() * 2 + 6); // Gasgiant or
															// Transsaturn
		}
		int radius = (1 + orbitNumber) * ORBITAL_DISTANCE;
		int degree = 0;
		switch (planetenTyp) {
		case 0:
			type = MERCURYLIKE;
			image = "grey" + (int) (Math.random() * NUMGREYPLANETS + 1) + ".jpg";
			color = Color.lightGray;
			size = (int) (Math.random() * 5) + RADIUSMERCURY;
			radius = radius + (int) (Math.random() * RADIUSMERCURY);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = -50.0 + Math.random() * 450; // -50 .. 400
			gravitation = 0.3 + 0.6 * Math.random();
			density = 0.5 + gravitation * 3.0 * Math.random();
			atmosphericPressure = Math.abs(gravitation - 0.5 + gravitation * Math.random());
			atmosphericComposition = 2.0 * Math.random();
			windSpeed = atmosphericPressure * (5.0 + Math.random() * 50);
			waterLandRatio = 0.6 * Math.random();
			fertility = waterLandRatio * Math.random() / 4 + 0.2 * Math.random();
			geoAge = 2.3 + (Math.random() * 5);
			hoursPerDay = (6.0 + 16.0 * Math.random());
			daysPerYear = orbitNumber * 100 * gravitation + orbitNumber * 50.0 * Math.random();
			axialTilt = 2.0 * Math.random();
			radiation = 200.0 + 1600.0 * Math.random();
			metals = 0.4 + 1.6 * Math.random();
			stone = 1.2 + (float) 0.6 * Math.random();
			fossils = 0.6 * Math.random();
			uranium = (metals / 2 + radiation / 1000) * Math.random();
			salt = 1.0 * Math.random();
			energy = (Math.random() + atmosphericPressure * Math.random() / 2 + waterLandRatio * Math.random() / 2) / 3 * 2;
			break;
		case 1:
			type = VENUSLIKE;
			image = "orange" + (int) (Math.random() * NUMORANGEPLANETS + 1) + ".jpg";
			color = Color.orange;
			size = (int) (Math.random() * 5) + RADIUSVENUS;
			radius = radius + (int) (Math.random() * RADIUSVENUS);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = -20.0 + Math.random() * 320; // -20 .. +300
			gravitation = 0.3 + 0.8 * Math.random();
			density = 0.5 + gravitation * 4.0 * Math.random();
			atmosphericPressure = Math.abs(gravitation - 0.3 + gravitation * Math.random());
			atmosphericComposition = 2.0 * Math.random();
			windSpeed = atmosphericPressure * (5.0 + Math.random() * 30);
			waterLandRatio = 0.6 + 0.6 * Math.random();
			fertility = 0.3 + waterLandRatio * Math.random() / 2 + atmosphericPressure * Math.random() / 2;
			// System.out.println("Fertility Venus-type: " + fertility);
			geoAge = 2.3 + (Math.random() * 5);
			hoursPerDay = 10.0 + 28.0 * Math.random();
			daysPerYear = 100 + Math.random() * 200 * gravitation + orbitNumber * 50.0 * Math.random();
			axialTilt = 0.1 + 1.9 * Math.random();
			radiation = 300.0 + 900.0 * Math.random();
			metals = 0.5 + 1.5 * Math.random();
			stone = 0.6 + (float) 0.9 * Math.random();
			fossils = 0.6 * Math.random();
			uranium = (metals / 2 + radiation / 1000) * Math.random();
			salt = 0.5 + 1.0 * Math.random();
			energy = (Math.random() + atmosphericPressure * Math.random() / 2 + waterLandRatio * Math.random() / 2) / 3 * 2;
			break;

		case 2:
			type = EARTHLIKE;
			float selectColor = (int) (Math.random() * 2);
			if (selectColor > 1) {
				image = "green" + (int) (Math.random() * NUMGREENPLANETS + 1) + ".jpg";
				color = Color.green;
			} else {
				image = "blue" + (int) (Math.random() * NUMBLUEPLANETS + 1) + ".jpg";
				color = Color.blue;
			}
			size = (int) (Math.random() * 5) + RADIUSEARTH;
			radius = radius + (int) (Math.random() * RADIUSEARTH);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			gravitation = 0.4 + 1.4 * Math.random();
			density = 2.0 + gravitation * 4.0 * Math.random();
			atmosphericPressure = Math.abs(gravitation - 0.5 + gravitation * Math.random());
			atmosphericComposition = 0.1 + 1.0 * Math.random() + 0.9 * Math.random();
			windSpeed = atmosphericPressure * (5.0 + Math.random() * 30);
			waterLandRatio = 2.0 * Math.random();
			fertility = 0.5 + (waterLandRatio * Math.random() / 2) + (0.5 * Math.random());
			geoAge = 2.3 + (Math.random() * 5);
			hoursPerDay = 12.0 + 24.0 * Math.random();
			daysPerYear = 100 + Math.random() * 350 * gravitation + orbitNumber * 10.0 * Math.random();

			// if (spaceOpera.getDifficultyLevel()==D_KINDERGARDEN) {
			avgTemp = Math.random() * 40;
			axialTilt = 0.5 + 1.0 * Math.random();
			radiation = 100.0 * Math.random() + 400.0 * Math.random();
			metals = 0.8 + 1.2 * Math.random();
			// } else if (spaceOpera.getDifficultyLevel()==D_EASY) {
			// avgTemp = -10.0 + Math.random() * 20 + Math.random() * 40;
			// axialTilt = 0.4 + 1.2*Math.random();
			// radiation = 300.0*Math.random() + 400.0*Math.random();
			// metals = 0.6 + 1.4 * Math.random();
			// } else {
			// avgTemp = -30.0 + Math.random() * 30 + Math.random() * 50;
			// axialTilt = 0.3 + 1.4*Math.random();
			// radiation = 400.0*Math.random() + 400.0*Math.random();
			// metals = 0.5 + 1.5 * Math.random();
			// }
			stone = 0.8 + (float) 1.2 * Math.random();
			fossils = 0.5 + 1.5 * Math.random();
			uranium = (metals / 2 + radiation / 600) * Math.random();
			salt = 0.7 + 0.6 * Math.random();
			energy = (Math.random() + atmosphericPressure * Math.random() / 2 + waterLandRatio * Math.random() / 2) / 3 * 2;

			break;
		case 3:
		case 4:
			type = MARSLIKE;
			image = "red" + (int) (Math.random() * NUMREDPLANETS + 1) + ".jpg";
			color = Color.red;
			size = (int) (Math.random() * 5) + RADIUSMARS;
			radius = radius + (int) (Math.random() * RADIUSMARS);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = -40.0 + Math.random() * 200; // -40 .. +160
			gravitation = 0.5 + 0.8 * Math.random();
			density = 1.5 + gravitation * 4.0 * Math.random();
			atmosphericPressure = Math.abs(gravitation - 0.5 + gravitation * Math.random());
			atmosphericComposition = 2.0 * Math.random();
			windSpeed = atmosphericPressure * (5.0 + Math.random() * 30);
			waterLandRatio = 0.3 + 0.6 * Math.random();
			fertility = 0.2 + waterLandRatio * Math.random() / 2 + atmosphericPressure * Math.random() / 2;
			geoAge = 2.3 + (Math.random() * 5);
			hoursPerDay = 16.0 + 30.0 * Math.random();
			daysPerYear = 150 + Math.random() * 250 * gravitation + orbitNumber * 30.0 * Math.random();
			axialTilt = 0.5 + 1.5 * Math.random();
			radiation = 200.0 + 800.0 * Math.random();
			metals = 0.2 + 1.8 * Math.random();
			stone = 0.8 + (float) 1.2 * Math.random();
			fossils = 0.2 + 1.6 * Math.random();
			uranium = (metals / 2 + radiation / 1000) * Math.random();
			salt = 0.5 + 1.0 * Math.random();
			energy = (Math.random() + atmosphericPressure * Math.random() / 2 + waterLandRatio * Math.random() / 2) / 3 * 2;
			break;
		case 5:
			type = ASTEROIDBELT;
			image = "aster" + (int) (Math.random() * NUMASTEROIDBELTS + 1) + ".jpg";
			color = Color.white;
			size = 9;
			radius = radius + (int) (Math.random() * RADIUSASTEROIDBELT / 2);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = -200.0f + Math.random() * 250; // -200 .. 50
			gravitation = 0.1f;
			density = 2.0 + 8.0 * Math.random() / gravitation;
			atmosphericPressure = 0.0f;
			atmosphericComposition = 0.0f;
			windSpeed = 0.0f;
			waterLandRatio = 1.0 + (float) 1.0 * Math.random();
			fertility = 0.0f;
			geoAge = (float) 2.3 + (Math.random() * 5);
			hoursPerDay = 0.0;
			daysPerYear = 180 * orbitNumber + 360.0 * Math.random();
			axialTilt = 0.0;
			radiation = 50.0 + 450.0 * Math.random();
			metals = 0.3 + (float) 1.7 * Math.random();
			stone = (float) 2.0 * Math.random();
			fossils = (float) 0.0;
			uranium = (metals / 2 + radiation / 500) * Math.random();
			salt = 0.8 + (float) 1.2 * Math.random();
			energy = (float) 0.5 * Math.random();
			break;
		case 6:
			type = GASGIANT;
			image = "pink" + (int) (Math.random() * NUMPINKPLANETS + 1) + ".jpg";
			color = Color.pink;
			size = (int) (Math.random() * 6) + RADIUSGASGIANT;
			radius = radius + (int) (Math.random() * RADIUSGASGIANT / 2);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = (float) -200.0 + Math.random() * 500; // -200 .. +300
			gravitation = (float) 3.0 + 5.0 * Math.random();
			density = 2.0 + 8.0 * Math.random() / gravitation;
			atmosphericPressure = (float) 3.0 + gravitation * Math.random();
			atmosphericComposition = 1.5 + 0.5 * Math.random();
			windSpeed = (float) atmosphericPressure * (5.0 + Math.random() * 50);
			waterLandRatio = (float) 0.3 * Math.random();
			fertility = (float) 0.0;
			geoAge = (float) 2.3 + (Math.random() * 5);
			hoursPerDay = 32 + 50.0 * Math.random();
			daysPerYear = 450 * gravitation * orbitNumber + 450.0 * Math.random();
			axialTilt = 2.0 * Math.random();
			radiation = 500.0 + 1000.0 * Math.random();
			metals = (float) 1.4 * Math.random();
			stone = (float) 1.0 * Math.random();
			fossils = (float) 0.0;
			uranium = (metals / 2 + radiation / 1500) * Math.random();
			salt = (float) 0.5 * Math.random();
			energy = (float) 2.0 * Math.random();
			break;
		case 7:
		default:
			type = TRANSSATURN;
			image = "white" + (int) (Math.random() * NUMWHITEPLANETS + 1) + ".jpg";
			color = Color.white;
			size = (int) (Math.random() * 4) + RADIUSTRANSSATURN;
			radius = radius + (int) (Math.random() * RADIUSTRANSSATURN / 3);
			degree = (int) (Math.random() * 360);
			orbit = new Orbit(radius, degree);
			avgTemp = (float) -200.0 + Math.random() * 200; // -200 .. 0
			gravitation = (float) 2.0 + 4.0 * Math.random();
			density = 2.0 + 6.0 * Math.random() / gravitation;
			atmosphericPressure = (float) 2.0 + gravitation * Math.random();
			atmosphericComposition = 1.0 + 1.0 * Math.random();
			windSpeed = (float) atmosphericPressure * (5.0 + Math.random() * 50);
			waterLandRatio = 1.0 + (float) 1.0 * Math.random();
			fertility = (float) 0.3 * waterLandRatio;
			geoAge = (float) 2.3 + (Math.random() * 5);
			hoursPerDay = 40.0 + 15.0 * Math.random() * gravitation;
			daysPerYear = 650 * gravitation * orbitNumber + 650.0 * Math.random();
			axialTilt = 2.0 * Math.random();
			radiation = 200.0 + 800.0 * Math.random();
			metals = 0.6 + (float) 1.0 * Math.random();
			stone = 0.6 + (float) 0.8 * Math.random();
			fossils = (float) 0.5;
			uranium = (metals / 2 + radiation / 1000) * Math.random();
			salt = 0.5 + (float) 0.5 * Math.random();
			energy = (float) 1.0 * Math.random();
			break;
		}
		initializeFloraFauna();
		// XXX: more extraordinary resources:
		// marble, pearls, gems, drugs, tourist attractions, ... (?)
		if (Math.random() > 0.9) {
			int rnd = (int) (5.0 * Math.random());
			switch (rnd) {
			case 0:
				special = "Marble";
				break;
			case 1:
				if (waterLandRatio > 0.5) {
					special = "Pearls";
				}
				break;
			case 2:
				special = "Gems";
				break;
			case 3:
				if (fauna > 0.5) {
					special = "Drugs";
				}
				break;
			case 4:
				special = "Tourist Attractions";
				break;
			default:
				special = "none";
			}
		} else {
			special = "none";
		}
		return;
	}

	/**
	 * Max population is determined by the following values: <br>
	 * fertility <br>
	 * avg. temperature <br>
	 * atmospheric pressure <br>
	 * atmospheric composition <br>
	 * radiation <br>
	 * XXX: other values that affect max population? (avgAirSpeed, water,
	 * seasons, ...)
	 * 
	 * XXX: terraforming should improve especially those values, plus maybe
	 * fertility
	 * 
	 * @param player
	 * @return maximum possible population count for player type
	 */
	public long getMaxPopulationCount(Player player) {
		long maxPop = 1000000;

		maxPop *= fertility;

		PreferredPlanetValues preferences = player.getPreferredPlanetValues();
		// temp
		if (preferences.isTemperatureIdeal(avgTemp)) {
			maxPop *= 2.0;
		}
		if (preferences.isTemperatureGood(avgTemp)) {
			maxPop *= 1.5;
		}
		if (preferences.isTemperatureAcceptable(avgTemp)) {
			maxPop *= 0.8;
		}
		if (preferences.isTemperatureExtreme(avgTemp)) {
			maxPop *= 0.6;
		}
		if (preferences.isTemperatureOutOfRange(avgTemp)) {
			maxPop *= 0.3;
		}

		// air pressure
		if (preferences.airPressureIsIdeal(atmosphericPressure)) {
			maxPop *= 1.5;
		}
		if (preferences.airPressureIsAcceptable(atmosphericPressure)) {
			maxPop *= 1.1;
		}
		if (preferences.airPressureIsExtreme(atmosphericPressure)) {
			maxPop *= 0.6;
		}
		if (preferences.airPressureOutOfRange(atmosphericPressure)) {
			maxPop *= 0.1;
		}

		// radiation
		if (preferences.radiationIsIdeal(radiation)) {
			maxPop *= 1.4;
		}
		if (preferences.radiationIsAcceptable(radiation)) {
			maxPop *= 1.2;
		}
		if (preferences.radiationIsExtreme(radiation)) {
			maxPop *= 0.8;
		}
		if (preferences.radiationIsOutOfRange(radiation)) {
			maxPop *= 0.5;
		}

		// atmospheric composition
		if (preferences.atmosphericCompositionIsIdeal(atmosphericComposition)) {
			maxPop *= 1.5;
		}
		if (preferences.atmosphericCompositionIsAcceptable(atmosphericComposition)) {
			maxPop *= 0.9;
		}
		if (preferences.atmosphericCompositionIsExtreme(atmosphericComposition)) {
			maxPop *= 0.5;
		}
		if (preferences.atmosphericCompositionIsOutOfRange(atmosphericComposition)) {
			maxPop *= 0.2;
		}

		// gravitation
		if (preferences.gravitationIsIdeal(gravitation)) {
			maxPop *= 1.5;
		}
		if (preferences.gravitationIsAcceptable(gravitation)) {
			maxPop *= 1.2;
		}
		if (preferences.gravitationIsExtreme(gravitation)) {
			maxPop *= 0.6;
		}
		if (preferences.gravitationIsOutOfRange(gravitation)) {
			maxPop *= 0.1;
		}

		// seasons
		if (preferences.seasonsAreIdeal(axialTilt)) {
			maxPop *= 1.5;
		}
		if (preferences.seasonsAreAcceptable(axialTilt)) {
			maxPop *= 1.2;
		}
		if (preferences.seasonsArePoor(axialTilt)) {
			maxPop *= 0.8;
		}
		if (preferences.seasonsAreExtreme(axialTilt)) {
			maxPop *= 0.6;
		}

		// waterLandRatio
		if (preferences.waterLandRatioIsIdeal(waterLandRatio)) {
			maxPop *= 1.5;
		}
		if (preferences.waterLandRatioIsAverage(waterLandRatio)) {
			maxPop *= 1.2;
		}
		if (preferences.waterLandRatioIsPoor(waterLandRatio)) {
			maxPop *= 0.8;
		}
		if (preferences.waterLandRatioIsExtreme(waterLandRatio)) {
			maxPop *= 0.5;
		}

		/* windSpeed */
		if (preferences.windSpeedIsIdeal(windSpeed)) {
			maxPop *= 1.3;
		}
		if (preferences.windSpeedIsAcceptable(windSpeed)) {
			maxPop *= 1.2;
		}
		if (preferences.windSpeedIsPoor(windSpeed)) {
			maxPop *= 0.9;
		}
		if (preferences.windSpeedIsExtreme(windSpeed)) {
			maxPop *= 0.5;
		}

		if (player instanceof CPlayer) {
			if (!((CPlayer) player).getPreferredPlanetType().equals(this.type)) {
				maxPop /= 2;
			}
		}
		// some rounding
		if (maxPop > 1000000) {
			maxPop += 5000;
			return (maxPop / 10000) * 10000;
		} else {
			maxPop += 50;
			return (maxPop / 100) * 100;
		}

	}

	public String getMiningValue() {
		float miningValue = (float) (stone + metals + fossils + salt + uranium + wood) / 6;
		String miningText = "";
		if (miningValue < 0.01) {
			miningText = NONE;
		} else if (miningValue < 0.4) {
			miningText = VERY_POOR;
		} else if (miningValue < 0.8) {
			miningText = POOR;
		} else if (miningValue < 1.2) {
			miningText = SOME;
		} else if (miningValue < 1.6) {
			miningText = RICH;
		} else {
			miningText = VERY_RICH;
		}
		return (miningText);
	}

	// calculate planet 'value': habitability, fertility, resources, ...?
	// this function is similar to 'getNecessaryColonyModule()' and
	// 'getMiningValue()'
	// XXX: combine with miningValue, current colony module technology, ... ?
	public float getValue(Player player) {
		float value = 0.0f;
		int count = 0;
		PreferredPlanetValues preferences = player.getPreferredPlanetValues();
		// 1
		if (preferences.isTemperatureIdeal(avgTemp) || preferences.isTemperatureGood(avgTemp)) {
			value += 2;
		}
		if (preferences.isTemperatureAcceptable(avgTemp)) {
			value += 1;
		}

		// 2
		if (preferences.gravitationIsIdeal(gravitation)) {
			value += 2;
		}
		if (preferences.gravitationIsAcceptable(gravitation)) {
			value += 1;
		}

		// 3
		if (preferences.waterLandRatioIsIdeal(waterLandRatio)) {
			value += 2;
		}
		if (preferences.waterLandRatioIsAverage(waterLandRatio)) {
			value += 1;
		}

		// 4
		if (preferences.airPressureIsIdeal(atmosphericPressure)) {
			value += 2;
		}
		if (preferences.airPressureIsAcceptable(atmosphericPressure)) {
			value += 1;
		}

		// 5
		if (preferences.atmosphericCompositionIsIdeal(atmosphericComposition)) {
			value += 2;
		}
		if (preferences.atmosphericCompositionIsAcceptable(atmosphericComposition)) {
			value += 1;
		}

		// 6
		if (preferences.windSpeedIsIdeal(windSpeed)) {
			value += 2;
		}
		if (preferences.windSpeedIsAcceptable(windSpeed)) {
			value += 1;
		}

		// 7
		if (fertilityIsHigh()) {
			value += 2;
		}
		if (fertilityIsLow()) {
			value += 1;
		}

		// 8
		if (preferences.seasonsAreIdeal(axialTilt)) {
			value += 2;
		}
		if (preferences.seasonsAreAcceptable(axialTilt)) {
			value += 1;
		}

		// 9
		if (preferences.radiationIsAcceptable(radiation)) {
			value += 1;
		}
		if (preferences.radiationIsIdeal(radiation)) {
			value += 2;
		}

		// 10,11
		value += 2 * metals;
		// 12,13
		value += 2 * stone;
		// 14,15
		value += 2 * fossils;
		// 16,17
		value += 2 * uranium;
		// 18,19
		value += 2 * salt;
		// 20,21
		value += 2 * energy;
		// 22
		if (!special.equals("none")) {
			value += 2;
		}

		value /= 22;

		// System.out.println("planet " + name + " has a value of " + value);

		return (value);
	}

	public int getNecessaryColonyModuleNr(Player player) {
		String necessary = getNeededColonyModuleName(player);
		int colonyModule = 99;

		if (necessary.equals(COLONY_MODULE)) {
			colonyModule = 1;
		}
		if (necessary.equals(COLONY_MODULE_II)) {
			colonyModule = 2;
		}
		if (necessary.equals(COLONY_MODULE_III)) {
			colonyModule = 3;
		}
		if (necessary.equals(COLONY_MODULE_IV)) {
			colonyModule = 4;
		}
		if (necessary.equals(COLONY_MODULE_V)) {
			colonyModule = 5;
		}
		return (colonyModule);
	}

	public String getNeededColonyModuleName(Player player) {
		// XXX: For Humans, as long as player-ranges not taken into account

		String colonyModule = "impossible";
		PreferredPlanetValues preferred = player.getPreferredPlanetValues();

		if (type.equals(EARTHLIKE)) {
			colonyModule = COLONY_MODULE;
			if (preferred.airPressureOutOfRange(atmosphericPressure)) {
				colonyModule = COLONY_MODULE_II;
			}
			if (preferred.isTemperatureExtreme(avgTemp) || preferred.isTemperatureOutOfRange(avgTemp)) {
				colonyModule = COLONY_MODULE_III;
			}
			if (preferred.radiationIsExtreme(radiation)) {
				colonyModule = COLONY_MODULE_IV;
			}
			if (preferred.atmosphericCompositionIsOutOfRange(atmosphericComposition)) {
				colonyModule = COLONY_MODULE_V;
			}
			if (preferred.gravitationIsOutOfRange(gravitation)) {
				// XXX: gravitational colony module
				colonyModule = "impossible";
			}
		}

		if (type.equals(MARSLIKE)) {
			colonyModule = COLONY_MODULE_II;
			if (player instanceof CPlayer) {
				if (type.equals(((CPlayer) player).getPreferredPlanetType())) {
					colonyModule = COLONY_MODULE;
				}
				if (preferred.airPressureOutOfRange(atmosphericPressure)) {
					colonyModule = COLONY_MODULE_III;
				}
			}
			if (preferred.isTemperatureExtreme(avgTemp) || preferred.isTemperatureOutOfRange(avgTemp)) {
				colonyModule = COLONY_MODULE_III;
			}
			if (preferred.radiationIsExtreme(radiation)) {
				colonyModule = COLONY_MODULE_IV;
			}
			if (preferred.atmosphericCompositionIsOutOfRange(atmosphericComposition)) {
				colonyModule = COLONY_MODULE_V;
			}
			if (preferred.gravitationIsOutOfRange(gravitation)) {
				// XXX: gravitational colony module
				colonyModule = "impossible";
			}
		}
		if (type.equals(VENUSLIKE)) {
			colonyModule = COLONY_MODULE_III;
			if (player instanceof CPlayer) {
				if (type.equals(((CPlayer) player).getPreferredPlanetType())) {
					colonyModule = COLONY_MODULE;
				}
				if (preferred.airPressureOutOfRange(atmosphericPressure)) {
					colonyModule = COLONY_MODULE_II;
				}
				if (preferred.isTemperatureExtreme(avgTemp) || preferred.isTemperatureOutOfRange(avgTemp)) {
					colonyModule = COLONY_MODULE_III;
				}
			}
			if (preferred.radiationIsExtreme(radiation)) {
				colonyModule = COLONY_MODULE_IV;
			}
			if (preferred.atmosphericCompositionIsOutOfRange(atmosphericComposition)) {
				colonyModule = COLONY_MODULE_V;
			}
			if (preferred.gravitationIsOutOfRange(gravitation)) {
				// XXX: gravitational colony module
				colonyModule = "impossible";
			}
		}
		if (type.equals(MERCURYLIKE)) {
			colonyModule = COLONY_MODULE_IV;
			if (player instanceof CPlayer) {
				if (type.equals(((CPlayer) player).getPreferredPlanetType())) {
					colonyModule = COLONY_MODULE;
				}
				if (preferred.airPressureOutOfRange(atmosphericPressure)) {
					colonyModule = COLONY_MODULE_II;
				}
				if (preferred.isTemperatureExtreme(avgTemp) || preferred.isTemperatureOutOfRange(avgTemp)) {
					colonyModule = COLONY_MODULE_III;
				}
			}
			if (preferred.atmosphericCompositionIsOutOfRange(atmosphericComposition)) {
				colonyModule = COLONY_MODULE_V;
			}
			if (preferred.gravitationIsOutOfRange(gravitation)) {
				// XXX: gravitational colony module
				colonyModule = "impossible";
			}
		}

		return (colonyModule);
	}

	public void improveAtmosphericComposition(Player player, double atmosphericCompositionCorrection) {
		if (player.getPreferredPlanetValues().lessThanIdealAtmosphericComposition(atmosphericComposition)) {
			atmosphericComposition += atmosphericCompositionCorrection;
		} else if (player.getPreferredPlanetValues().moreThanIdealAtmosphericComposition(atmosphericComposition)) {
			atmosphericComposition -= atmosphericCompositionCorrection;
		}
	}

	public void improveAtmosphericPressure(Player player, double atmosphericPressureCorrection) {
		if (player.getPreferredPlanetValues().lessThanIdealAtmosphericPressure(atmosphericPressure)) {
			atmosphericPressure += atmosphericPressureCorrection;
		} else if (player.getPreferredPlanetValues().moreThanIdealAtmosphericPressure(atmosphericPressure)) {
			atmosphericPressure -= atmosphericPressureCorrection;
		}
	}

	public void improveGravitation(Player player, double improvement) {
		// this.gravitation = gravitation;
		if (player.getPreferredPlanetValues().lessThanIdealGravitation(gravitation)) {
			gravitation += improvement;
		} else if (player.getPreferredPlanetValues().moreThanIdealGravitation(gravitation)) {
			gravitation -= improvement;
		}
	}

	public void improveTemperature(Player player, double improvement) {
		if (player.getPreferredPlanetValues().lessThanIdealTemperature(avgTemp)) {
			avgTemp += improvement;
		} else if (player.getPreferredPlanetValues().moreThanIdealTemperature(avgTemp)) {
			avgTemp -= improvement;
		}
	}

	public void improveWindSpeed(Player player, double improvement) {
		if (player.getPreferredPlanetValues().lessThanIdealWindSpeed(windSpeed)) {
			windSpeed += improvement;
		} else if (player.getPreferredPlanetValues().moreThanIdealWindSpeed(windSpeed)) {
			windSpeed -= improvement;
		}
	}

	public void improveSeasons(Player player, double improvement) {
		if (player.getPreferredPlanetValues().lessThanIdealSeasons(axialTilt)) {
			axialTilt += improvement;
		} else if (player.getPreferredPlanetValues().moreThanIdealSeasons(axialTilt)) {
			axialTilt -= improvement;
		}
	}

	public void improveWaterLandRatio(Player player, double improvement) {
		if (player.getPreferredPlanetValues().lessThanIdealWaterLandRatio(waterLandRatio)) {
			waterLandRatio += improvement;
		} else if (player.getPreferredPlanetValues().moreThanIdealWaterLandRatio(waterLandRatio)) {
			waterLandRatio -= improvement;
		}
	}

	public void improveRadiation(double improve) {
		try {
			Player player = colony.getPlayer();
			if (player.getPreferredPlanetValues().lessThanIdealRadiation(radiation)) {
				radiation += improve;
			} else {
				radiation -= improve;
				if (radiation < 0) {
					radiation = 0;
				}
			}
		} catch (Exception e) {
			System.out.println("Warning: colony or player not found. " + e);
		}

	}

	//click inside or close to a planet?
	public boolean inside(int ix, int iy) {
		if ((ix >= mx - 5) && (iy >= my - 5) && (ix <= (mx + size + 5)) && (iy <= (my + size + 5))) {
			return true;
		} else {
			return false;
		}
	}

	public void load() {
	}

	public Planet(int num, Fullscreen fs, SunSystem s) {
		this.fs = fs;
		sunSystem = s;
		orbitNumber = num;
		moons = new Vector();
	}

	public void reCalculateFloraFauna() {
		double increase = 0.1;
		if (avgTempOutOfRange()) {
			increase /= 5;
		}
		if (avgTempExtreme()) {
			increase /= 2;
		}
		if (atmPressureOutOfRange()) {
			increase /= 3;
		}
		if (fertilityIsLow()) {
			increase /= 2;
		}
		if (fertilityIsAverage()) {
			increase /= 3;
		}
		if (fertilityIsHigh()) {
			increase /= 4;
		}
		if (fertilityIsVeryHigh()) {
			increase /= 5;
		}
		flora += increase;
		if (flora > 2) {
			flora = 2;
		}
		fauna += increase / 2;
		if (fauna > 2) {
			fauna = 2;
		}
		wood += increase / 2;
		if (wood > 2) {
			wood = 2;
		}
	}

	public void removeColony() {
		colonized = false;
		colony.destroy();
		colony = null;
	}

	public void save() {
	}

	public String toString() {
		return (new String("Planet " + name + ", Type " + type + ", Orbit " + orbit + ",\nTemp " + (float) avgTemp
				+ ", Gravitation " + (float) gravitation + ", Air pressure " + (float) atmosphericPressure + ", Air speed "
				+ (float) windSpeed + ",\nWater " + (float) waterLandRatio + ", Fertility " + (float) getFertility() + ", Age "
				+ (float) geoAge + ", Hours per day " + (float) hoursPerDay + ", Days per year " + (float) daysPerYear
				+ ",\nMetals " + (float) metals + ", fossils " + (float) fossils + ", salt " + (float) salt + ", energy "
				+ (float) energy));
	}

	public void updatePlanetPositions() {
		int oldPosition = orbit.getPosition();
		double moveDegrees = 360 / (daysPerYear / 360);
		// System.out.println("daysPerYear: " + daysPerYear + " yields " +
		// moveDegrees + " degrees movement.");
		int newPosition = oldPosition + (int) moveDegrees;
		while (newPosition >= 360) {
			newPosition -= 360;
		}
		orbit.setPosition(newPosition);
	}

	public void setFertility(double f) {
		fertility = f;
		if (fertility > 2) {
			fertility = 2;
		}
		reCalculateFloraFauna();
	}

	public void setFlora(double f) {
		flora = f;
		if (flora > 2) {
			flora = 2;
		}
		wood = f;
		if (wood > 2) {
			wood = 2;
		}
	}

	public void setFauna(double f) {
		fauna = f;
		if (fauna > 2) {
			fauna = 2;
		}
	}

	/**
	 * Some player-independent ranges (others are to be found in the
	 * PreferredPlanetValues class)
	 * ==============================================
	 * ============================================
	 */

	/* fertility (more is better) (player-independent) */
	public boolean fertilityIsVeryLow() {
		return (fertility < 0.3);
	}

	public boolean fertilityIsLow() {
		return (fertility >= 0.3) && (fertility < 0.8);
	}

	public boolean fertilityIsAverage() {
		return (fertility >= 0.8) && (fertility < 1.2);
	}

	public boolean fertilityIsHigh() {
		return (fertility >= 1.2) && (fertility < 1.7);
	}

	public boolean fertilityIsVeryHigh() {
		return (fertility >= 1.7);
	}

	/* flora (more is better) (player-independent ??) */
	public boolean floraIsVeryLow() {
		return (flora < 0.3);
	}

	public boolean floraIsLow() {
		return (flora >= 0.3) && (flora < 0.8);
	}

	public boolean floraIsAverage() {
		return (flora >= 0.8) && (flora < 1.2);
	}

	public boolean floraIsHigh() {
		return (flora >= 1.2) && (flora < 1.7);
	}

	public boolean floraIsVeryHigh() {
		return (flora >= 1.7);
	}

	/* fauna (more is better) (player-independent??) */
	public boolean faunaIsVeryLow() {
		return (fauna < 0.3);
	}

	public boolean faunaIsLow() {
		return (fauna >= 0.3) && (fauna < 0.8);
	}

	public boolean faunaIsAverage() {
		return (fauna >= 0.8) && (fauna < 1.2);
	}

	public boolean faunaIsHigh() {
		return (fauna >= 1.2) && (fauna < 1.7);
	}

	public boolean faunaIsVeryHigh() {
		return (fauna >= 1.7);
	}

	/**
	 * local range methods, similar to methods in PreferredPlanetValues, but
	 * without the Player-Parameter as they do not depend on the player, being
	 * globally valid...
	 */
	private boolean avgTempExtreme() {
		return (avgTemp >= -10 && avgTemp < 0) || (avgTemp >= 40 && avgTemp < 50);
	}

	private boolean avgTempOutOfRange() {
		return avgTemp < -10 || avgTemp >= 50;
	}

	private boolean atmPressureOutOfRange() {
		return atmosphericPressure < 0.4 || atmosphericPressure >= 2.5;
	}

}
