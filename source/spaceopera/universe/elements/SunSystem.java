//The class SunSystem displays a SunSystem in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.elements;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import spaceopera.gui.window.SunSystemDisplay;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The SunSystem displays the number of planets and the orbitals of a sunsystem
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class SunSystem implements SOConstants {

	private Sun sun;
	private Vector planets; // List of all planets
	// XXX: public Vector comets, asteroids, asteroidBelts, trading Stations,
	// outposts, ...
	private int[] backgroundStarsX;
	private int[] backgroundStarsY;
	// private int[] backgroundStarsZ;
	private boolean generated = false;
	private Hashtable explored; // contains names of all players that have
								// explored this
	private SunSystemDisplay sunSystemDisplay;

	// private SpaceOpera spaceOpera;
	private Fullscreen fs;
	private int sunSystemNumber = 0;

	// Accessors
	public void addExploredByPlayer(String n) {
		explored.put(n, n);
	}

	public void addFlag(Player p) {
		sun.addFlag(p);
	}

	public void addSpaceCraft(SpaceCraft rs) {
		sun.addSpaceCraft(rs);
	}

	public int getBackgroundStarX(int i) {
		return (backgroundStarsX[i]);
	}

	public int getBackgroundStarY(int i) {
		return (backgroundStarsY[i]);
	}

	public Hashtable getExplored() {
		return (explored);
	}

	public Vector getPlanets() {
		return (planets);
	}

	public String getName() {
		return (sun.getName());
	}

	public int getNumber() {
		return (sunSystemNumber);
	}

	public Sun getSun() {
		return (sun);
	}

	public Vector getSpaceCraft() {
		return (sun.getSpaceCraft());
	}

	public SunSystem getCurrentSunSystem() {
		return (fs.getSunSystem());
	}

	public SunSystemDisplay getSunSystemDisplay() {
		return (sunSystemDisplay);
	}

	public int getX() {
		return (sun.getX());
	}

	public int getY() {
		return (sun.getY());
	}

	public boolean isExploredBy(String n) {
		return (explored.containsKey(n));
	}

	public boolean isGenerated() {
		return (generated);
	}

	public void removeSpaceCraft(SpaceCraft rs) {
		sun.getSpaceCraft().removeElement(rs);
	}

	public void setBackgroundStarsX(int i, int x) {
		backgroundStarsX[i] = x;
	}

	public void setBackgroundStarsY(int i, int y) {
		backgroundStarsY[i] = y;
	}

	public void setPlanets(Vector p) {
		planets = p;
	}

	public void setSun(Sun s) {
		sun = s;
	}

	public void setSunSelected(boolean b) {
		sun.setSelected(b);
	}

	// Methods
	public Planet getPlanet(String pName) {
		Planet ret = null;
		for (int i = 0; i < planets.size(); i++) {
			Planet p = (Planet) planets.elementAt(i);
			if (p.getName().equals(pName)) {
				ret = p;
			}
		}
		return ret;
	}

	/**
	 * Get information about all cargo available in this sunsystem in this
	 * player's colonies Todo: trade outposts, allied players,...
	 * 
	 * @param player
	 * @return string array containing all goods available in this sunsystem
	 */
	public String[] getAvailableCargo(Player player) {
		// XXX: collect available cargo from own colonies and from trading
		// outposts etc.
		String cargo[] = new String[1];
		Vector temp = new Vector();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			if (planet.isColonized()) {
				Colony colony = planet.getColony();
				if (colony.getPlayer().equals(player)) {
					for (int j = R_STONE; j < R_GARMENTS; j++) {
						if (colony.getStoredResources(j) > 100) {
							// TODO String resource =
							// spaceOpera.getResourceName(j);
							// if (!temp.contains(resource)) {
							// temp.addElement(resource);
							// }
						}
					}
				}
			}
		}
		cargo = (String[]) temp.toArray(cargo);
		Arrays.sort(cargo);
		return (cargo);
	}

	/**
	 * Take the desired cargo from the sunsystem (colony, trade stations, ...)
	 * and load it into a ship
	 * 
	 * @param player
	 * @param cargo
	 * @param capacity
	 * @return int number of tons loaded
	 */
	public double loadCargo(Player player, String cargo, int capacity) {
		double loaded = 0;
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			if (planet.isColonized()) {
				Colony colony = planet.getColony();
				if (colony.getPlayer().equals(player)) {
					for (int j = R_STONE; j < R_GARMENTS; j++) {
						// TODO String resource = spaceOpera.getResourceName(j);
						// if (resource.equals(cargo)) {
						// if (colony.getStoredResources(j) > capacity) {
						// loaded = capacity;
						// } else if (colony.getStoredResources(j) > 0) {
						// loaded = colony.getStoredResources(j);
						// }
						// colony.removeStoredResources(j, loaded);
						// // System.out.println("loaded " + loaded +
						// // " tons of " + cargo);
						// }
					}
				}
			}
		}
		return loaded; // tons of available cargo for this player
	}

	public void unLoadCargo(String target, String cargo, int weight) {
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			if (planet.isColonized()) {
				Colony colony = planet.getColony();
				if (colony.getName().equals(target)) {
					for (int j = R_STONE; j < R_GARMENTS; j++) {
						// TODO String resource = spaceOpera.getResourceName(j);
						// if (resource.equals(cargo)) {
						// colony.addStoredResources(j, weight);
						// // System.out.println("unloaded " + weight +
						// // " tons of " + cargo);
						// }
					}
				}
			}
		}
	}

	/**
	 * Generate a random sunsystem
	 * 
	 * @param number
	 *            of the sunsystem
	 */
	public void generate(int number) {
		sunSystemNumber = number;
		int numPlanets = (int) (Math.random() * NUMPLANETS);
		// to prevent too many sunsystems with no planets:
		if (numPlanets == 0) {
			numPlanets = (int) (Math.random() * NUMPLANETS);
		}
		int j = 0; // count possible orbits outward
		for (int i = 0; i < numPlanets; i++) {
			j = j + (int) (Math.random() * 2) + 1;
			Planet p = new Planet(j, fs, this);
			p.setName(sun.getName() + " " + (i + 1));
			p.generate();
			planets.addElement(p);
		}
		backgroundStarsX = new int[SSBACKGROUNDSTARS];
		backgroundStarsY = new int[SSBACKGROUNDSTARS];
		// backgroundStarsZ[i] = new int[NUMBACKGROUNDSTARS];
		for (int i = 0; i < SSBACKGROUNDSTARS; i++) {
			backgroundStarsX[i] = (int) (Math.random() * SUNSYSTEMFRAMEX);
			backgroundStarsY[i] = (int) (Math.random() * SUNSYSTEMFRAMEY);
		}
		generated = true;
	}

	public void load() {
	}

	public void save() {
	}

	public SunSystem(Sun s, Fullscreen fs) {
		// spaceOpera = so;
		this.fs = fs;
		sun = s;
		planets = new Vector();
		explored = new Hashtable();
		sunSystemDisplay = new SunSystemDisplay(this, fs);
	}

	public String toString() {
		String system = new String("Sunsystem:\nPlaneten: \n");
		for (int i = 0; i < planets.size(); i++) {
			system = system + ((Planet) planets.elementAt(i)).toString() + "\n";
		}
		return (system);
	}

	public void updatePlanetPositions() {
		for (int i = 0; i < planets.size(); i++) {
			Planet p = (Planet) planets.elementAt(i);
			p.updatePlanetPositions();
		}
	}

	public void update(SunSystem s, Planet p) {
		sunSystemDisplay.setSunSystem(s);
		sun = s.getSun();
		planets = (Vector) s.getPlanets().clone();
		explored = (Hashtable) s.getExplored().clone();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			planet.setSelected(false);
			if (planet == p) {
				planet.setSelected(true);
			}
		}
		for (int i = 0; i < SSBACKGROUNDSTARS; i++) {
			setBackgroundStarsX(i, s.getBackgroundStarX(i));
			setBackgroundStarsY(i, s.getBackgroundStarY(i));
		}
		//display();
		// Test
		// System.out.println("Display Sunsystem: " + getName());
	}
}