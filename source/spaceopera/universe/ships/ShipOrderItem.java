//The class ShipOrderItem is one ship order, defined by ShipOrders class, 
//used in Ship and MPlayer
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ships;

import spaceopera.universe.SOConstants;

/** one ship order
 *
 */
public class ShipOrderItem implements SOConstants {
	private String option = ""; // e.g. 'auto'
	private String command = ""; // e.g. 'colonize', 'goto', 'explore'
	private String cargo = "";
	private String target = ""; // e.g. Sunsystem name, Planet name, or 'best', 'next', 'closest'...
	private String fill = "";
	
	public ShipOrderItem (String opt, String cmd, String targ) {
		option = opt;
		command = cmd;
		target = targ;			
	}
	
	public ShipOrderItem (String cmd, String crg, String targ, String f) {
		command = cmd;
		cargo = crg;
		target = targ;	
		fill = f;
	}
	
	public String getOption() {return(option);}
	public String getCommand() {return(command);}
	public String getTarget() {return(target);}
	public String getCargo() {return(cargo);}
	public String getFill()  {return(fill);}
	

	public String toString() {
		//without html formatting, for now
		StringBuffer ret = new StringBuffer();

		if (option.length()>0) {
			ret.append(option);
			ret.append(" ");
		}
		ret.append(command);
		ret.append(" ");
		if (cargo.length()>0) {
			ret.append(cargo);
			ret.append(" to ");
		}
		ret.append(target);
		if (fill.length()>0) {
			ret.append(", fill");
		}
		return(ret.toString());
	}
	
}
