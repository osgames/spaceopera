//The class ShipCompontent defines components for ships in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ships;

import spaceopera.universe.Resources;
import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.BuildProject;

/**
 * a ship consists of different shipComponents like 'drive', 'hull', 'shields'
 * and so on.
 */
public class ShipComponent extends BuildProject {

	
	private int weight = 0;
	private int payload = 0;
	private double sublightSpeed = 0; //in tenths of lightseconds per turn/battleturn?
	private double warpSpeed = 0;
	private int range = 0;
	private int ammo = 0;
	private int force = 0;
	private int armor = 0;
	private int shielding = 0;
	private int fuel = 0;
	private int lifeCycles = 0;
	private boolean shipAttack = false;
	private boolean planetAttack = false;
	private int transportCapability = 0;
	private int hullSize = 1;
	/** @see Resources.java */
	private int hullSizeDepending = NOT_DEPENDING;
	/** @see SOConstants.java */
	private int subType = 0; // subtype is used for ai-shipdesign to select
								// shipcomponents
	private int techLevel = 0; // XXX synchronize this with Techlevelcost in
								// Resources->Technologies

	// XXX: depending on desired acceleration?

	// Accessors
	public int getBaseAmmo() {
		return (ammo);
	}

	public int getArmor() {
		return (armor);
	}

	public double getCost(int k) {
		return (getProductionCost(k));
	}

	public int getForce() {
		return (force);
	}

	public int getLifeCycles() {
		return (lifeCycles);
	}

	public int getPayload() {
		return (payload);
	}

	public int getRange() {
		return (range);
	}

	public int getShielding() {
		return (shielding);
	}

	public double getSublightSpeed() {
		return (sublightSpeed);
	}

	public double getWarpSpeed() {
		return (warpSpeed);
	}

	public int getWeight() {
		return (weight);
	}

	public boolean getShipAttacks() {
		return (shipAttack);
	}

	public boolean getPlanetAttacks() {
		return (planetAttack);
	}

	public int getTransportCapability() {
		return (transportCapability);
	}

	public void addArmor(int i) {
		armor += i;
	} // add armor to ship

	public void addFuel(int i) {
		fuel += i;
	} // add fuel to ship

	public void addShielding(int i) {
		shielding += i;
	} // add shielding to ship

	public void damageArmor(int i) {
		armor -= i;
	} // damage armor

	public void setBaseAmmo(int i) {
		ammo = i;
	} // ammunition capacity

	public void setForce(int i) {
		force = i;
	} // weapon strength

	public void setLifeCycles(int i) {
		lifeCycles = i;
	} // torpedo lifecycles

	public void setPayload(int i) {
		payload = i;
	} // payload in tons

	public void setRange(int i) {
		range = i;
	} // range in lightseconds

	public void setSublightSpeed(int i) {
		sublightSpeed = i;
	}// XXX: what unit is this, see Resources class

	public void setWarpSpeed(int i) {
		warpSpeed = i;
	} // speed in light years, 0 means sublight

	public void setShipAttack(boolean b) {
		shipAttack = b;
	}

	public void setPlanetAttack(boolean b) {
		planetAttack = b;
	}

	public void setTransportCapability(int tc) {
		transportCapability = tc;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	// Methods
	public ShipComponent(int id, String name, String description, String type, boolean init, boolean allow, int weight,
			String picture) {
		setId(id);
		setName(name);
		setDescription(description);
		setType(type);
		this.weight = weight;
		setPicture(picture);
		setAllowBuild(allow);
	}

	public int getHullSize() {
		return hullSize;
	}

	public void setHullSize(int hullSize) {
		this.hullSize = hullSize;
	}

	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public int getHullSizeDepending() {
		return hullSizeDepending;
	}

	public void setHullSizeDepending(int hullSizeDepending) {
		this.hullSizeDepending = hullSizeDepending;
	}

	// XXX: new functions
	// from parent: public void setScanner(double d) { }

}