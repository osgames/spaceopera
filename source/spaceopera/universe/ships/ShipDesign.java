//The class ShipDesign defines the components used for a ship
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ships;

import java.util.*;

import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;


/**
 * Each ship has a shipDesign that defines the shipComponents the ship exists
 * from. Different players may have different ship designs.
 */
@SuppressWarnings({"rawtypes"})
public class ShipDesign implements Cloneable, SOConstants {

	private String shipClass = "";
	private String description = "";
	private String hull = "";
	private int hullsize = 1; //for components that are hullsize-dependent, (drives, armor, tactical, fueltank/-scoop)
	// XXX: ships may have more than one drive unit per type ??
	private String sublightDrive = "";
	private String interstellarDrive = "";

	//electronics
	private String scanner = "";
	private String targetComputer = "";

	// weapons
	private String beamWeaponType = "";
	private int beamWeaponCount = 0;
	private String missileWeaponType = "";
	private int missileWeaponCount = 0;
	private String particleWeaponType = "";
	private int particleWeaponCount = 0;
	private String bombWeaponType = "";
	private int bombWeaponCount = 0;

	// special components
	private String oreSeeker = "";
	private String tactical = ""; 
	private String fuelTank = "";
	private String fuelScoop = "";
	private String mineDeployer = "";
	private String colonyModule = "";
	private String cargoModule = "";
	private int cargoModuleCount = 0;	
	
	// armor and electronic shielding
	private String armorType = "";
	private int armorCount = 0;
	private String shieldType = "";
	private int shieldCount = 0;

	private String image = "";
	// private int payload = 1;
	// private int maxPayload = 1;
	private boolean isShip = true;
	private int counter = 1;

	public String getDescription() {
		return (description);
	}

	public String getSublightDrive() {
		return (sublightDrive);
	}

	public String getInterstellarDrive() {
		return (interstellarDrive);
	}

	public String getScanner() {
		return scanner;
	}

	public String getTargetComputer() {
		return targetComputer;
	}

	public String getHull() {
		return (hull);
	}

	public String getImage() {
		return (image);
	}

	// public int getMaxPayload() {return(maxPayload);}
	public String getShipClass() {
		return (shipClass);
	}

	public String getShieldType() {
		return (shieldType);
	}

	public int getShieldCount() {
		return (shieldCount);
	}

	public int getArmorCount() {
		return armorCount;
	}

	public String getArmorType() {
		return armorType;
	}

	public String createShipName() {
		return (shipClass + " " + counter++);
	}

	public boolean hasInterstellarDrive() {
		return (!interstellarDrive.equals(""));
	}

	public boolean isShip() {
		return (isShip);
	}

	public void setHull(String h) {
		hull = h;
	}

	public void setImage(String i) {
		image = i;
	}

	public void setName(String n) {
		shipClass = n;
	}

	public void clear() {
		// remove all components and whatever
		shipClass = "";
		sublightDrive = "";
		interstellarDrive = "";
		hull = "";
		image = "";
		isShip = true;
		beamWeaponType = "";
		beamWeaponCount = 0;
		missileWeaponType = "";
		missileWeaponCount = 0;
		particleWeaponType = "";
		particleWeaponCount = 0;
		bombWeaponType = "";
		bombWeaponCount = 0;
		oreSeeker = "";
		tactical = ""; 
		fuelTank = "";
		fuelScoop = "";
		mineDeployer = "";
		colonyModule = "";
		cargoModule = "";
		cargoModuleCount = 0;	
		scanner = "";
		targetComputer = "";
		armorCount = 0;
		armorType = "";
		shieldCount = 0;
		shieldType = "";
	}

	public double getCost(Player player, int k) {
		double cost = 0;
		Hashtable components = player.getPossibleComponents();
		// hull
		ShipComponent sc = (ShipComponent) components.get(hull);
		if (sc != null) {
			hullsize = sc.getHullSize();
			cost += addCost(k, sc, 1);
		}
		// drive
		sc = (ShipComponent) components.get(sublightDrive);
		if (sc != null) {
			cost += addCost(k, sc, 1);
		}
		sc = (ShipComponent) components.get(interstellarDrive);
		if (sc != null) {
			cost += addCost(k, sc, 1);
		}
		// electronics
		if (!"".equals(scanner)) {
			sc = (ShipComponent) components.get(scanner);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		if (!"".equals(targetComputer)) {
			sc = (ShipComponent) components.get(targetComputer);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		// shielding
		if (!"".equals(shieldType)) {
			sc = (ShipComponent) components.get(shieldType);
			if (sc != null) {
				cost += addCost(k, sc, shieldCount);
			}
		}		
		// armor
		if (!"".equals(armorType)) {
			sc = (ShipComponent) components.get(armorType);
			if (sc != null) {
				cost += addCost(k, sc, armorCount);
			}
		}
		// weapons
		if (!"".equals(beamWeaponType)) {
			sc = (ShipComponent) components.get(beamWeaponType);
			if (sc != null) {
				cost += addCost(k, sc, beamWeaponCount);
			}
		}
		if (!"".equals(particleWeaponType)) {
			sc = (ShipComponent) components.get(particleWeaponType);
			if (sc != null) {
				cost += addCost(k, sc, particleWeaponCount);
			}
		}
		if (!"".equals(missileWeaponType)) {
			sc = (ShipComponent) components.get(missileWeaponType);
			if (sc != null) {
				cost += addCost(k, sc, missileWeaponCount);
			}
		}
		if (!"".equals(bombWeaponType)) {
			sc = (ShipComponent) components.get(bombWeaponType);
			if (sc != null) {
				cost += addCost(k, sc, bombWeaponCount);
			}
		}

		// specials
        //XXX: ?? fuel scoop and other components weight depending on total ship size/weight?
		if (!"".equals(oreSeeker)) {
			sc = (ShipComponent) components.get(oreSeeker);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		if (!"".equals(tactical)) {
			sc = (ShipComponent) components.get(tactical);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		if (!"".equals(fuelTank)) {
			sc = (ShipComponent) components.get(fuelTank);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		if (!"".equals(fuelScoop)) {
			sc = (ShipComponent) components.get(fuelScoop);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}		
		if (!"".equals(mineDeployer)) {
			sc = (ShipComponent) components.get(mineDeployer);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}
		if (!"".equals(colonyModule)) {
			sc = (ShipComponent) components.get(colonyModule);
			if (sc != null) {
				cost += addCost(k, sc, 1);
			}
		}		
		if (!"".equals(cargoModule)) {
			sc = (ShipComponent) components.get(cargoModule);
			if (sc != null) {
				cost += addCost(k, sc, cargoModuleCount);
			}
		}			
		return (cost);
	}

	// XXX: getSupportCost is similar to getCost --> refactor this
	public double getSupportCost(Player player, int k) {
		double supportCost = 0;
		Hashtable components = player.getPossibleComponents();
		// hull
		ShipComponent sc = (ShipComponent) components.get(hull);
		if (sc != null) {
			hullsize = sc.getHullSize();
			supportCost += addSupportCost(k, sc, 1);
		}
		// drive
		sc = (ShipComponent) components.get(sublightDrive);
		if (sc != null) {
			supportCost += addSupportCost(k, sc, 1);
		}
		sc = (ShipComponent) components.get(interstellarDrive);
		if (sc != null) {
			supportCost += addSupportCost(k, sc, 1);
		}
		// electronics
		if (!"".equals(scanner)) {
			sc = (ShipComponent) components.get(scanner);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		if (!"".equals(targetComputer)) {
			sc = (ShipComponent) components.get(targetComputer);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		// armor
		if (!"".equals(armorType)) {
			sc = (ShipComponent) components.get(armorType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, armorCount);
			}
		}
		// shielding
		if (!"".equals(shieldType)) {
			sc = (ShipComponent) components.get(shieldType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, shieldCount);
			}
		}
		// weapons
		if (!"".equals(beamWeaponType)) {
			sc = (ShipComponent) components.get(beamWeaponType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, beamWeaponCount);
			}
		}
		if (!"".equals(particleWeaponType)) {
			sc = (ShipComponent) components.get(particleWeaponType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, particleWeaponCount);
			}
		}
		if (!"".equals(missileWeaponType)) {
			sc = (ShipComponent) components.get(missileWeaponType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, missileWeaponCount);
			}
		}
		if (!"".equals(bombWeaponType)) {
			sc = (ShipComponent) components.get(bombWeaponType);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, bombWeaponCount);
			}
		}
		//special components
        //XXX: ?? fuel scoop and other components weight depending on total ship size/weight?
		if (!"".equals(oreSeeker)) {
			sc = (ShipComponent) components.get(oreSeeker);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		if (!"".equals(tactical)) {
			sc = (ShipComponent) components.get(tactical);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		if (!"".equals(fuelTank)) {
			sc = (ShipComponent) components.get(fuelTank);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		if (!"".equals(fuelScoop)) {
			sc = (ShipComponent) components.get(fuelScoop);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}		
		if (!"".equals(mineDeployer)) {
			sc = (ShipComponent) components.get(mineDeployer);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}
		if (!"".equals(colonyModule)) {
			sc = (ShipComponent) components.get(colonyModule);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, 1);
			}
		}		
		if (!"".equals(cargoModule)) {
			sc = (ShipComponent) components.get(cargoModule);
			if (sc != null) {
				supportCost += addSupportCost(k, sc, cargoModuleCount);
			}
		}
		return (supportCost);
	}

	public Object clone() {
		ShipDesign sd = null;
		try {
			sd = (ShipDesign) super.clone();
		} catch (Exception e) {
			System.out.println("ShipDesign clone failed. " + e);
		}
		sd.scanner = this.scanner;
		sd.targetComputer = this.targetComputer;
		sd.beamWeaponType = this.beamWeaponType;
		sd.beamWeaponCount = this.beamWeaponCount;
		sd.missileWeaponType = this.missileWeaponType;
		sd.missileWeaponCount = this.missileWeaponCount;
		sd.particleWeaponType = this.particleWeaponType;
		sd.particleWeaponCount = this.particleWeaponCount;
		sd.bombWeaponType = this.bombWeaponType;
		sd.bombWeaponCount = this.bombWeaponCount;
		sd.oreSeeker = this.oreSeeker;
		sd.tactical = this.tactical; 
		sd.fuelTank = this.fuelTank;
		sd.fuelScoop = this.fuelScoop;
		sd.mineDeployer = this.mineDeployer;
		sd.colonyModule = this.colonyModule;
		sd.cargoModule = this.cargoModule;
		sd.cargoModuleCount = this.cargoModuleCount;	
		sd.armorType = this.armorType;
		sd.armorCount = this.armorCount;
		sd.shieldType = this.shieldType;
		sd.shieldCount = this.shieldCount;
		return sd;
	}

	// base armor value of hull plus n times armorType
	public float getTotalArmor(Player player) {
		ShipComponent sc = (ShipComponent) player.getPossibleComponents().get(hull);
		float armor = sc.getArmor();
		if (!"".equals(armorType)) {
			sc = (ShipComponent) player.getPossibleComponents().get(armorType);
			armor += armorCount * sc.getArmor();
		}
		return (armor);
	}

	// XXX: shielding possible from different devices?
	public float getShieldStrength(Player player) {
		float shield = 0;
		if (!"".equals(shieldType)) {
			ShipComponent sc = (ShipComponent) player.getPossibleComponents().get(shieldType);
			shield += shieldCount * sc.getArmor();
		}
		return (shield);
	}

	public int getColonyModuleNumber() {
		int colonyModuleNumber = 0;
		if (colonyModule.equals(COLONY_MODULE)) {
			colonyModuleNumber = 1;
		}
		if (colonyModule.equals(COLONY_MODULE_II)) {
			colonyModuleNumber = 2;
		}
		if (colonyModule.equals(COLONY_MODULE_III)) {
			colonyModuleNumber = 3;
		}
		if (colonyModule.equals(COLONY_MODULE_IV)) {
			colonyModuleNumber = 4;
		}
		if (colonyModule.equals(COLONY_MODULE_V)) {
			colonyModuleNumber = 5;
		}
		return (colonyModuleNumber);
	}

//	 XXX: tons of colonists????
	public int getTransportCapacity(Player player) {
		if (!"".equals(cargoModule)) {
			ShipComponent shipComponent = (ShipComponent) player.getPossibleComponents().get(cargoModule);
			return cargoModuleCount * shipComponent.getTransportCapability();
		} else {
			return 0;
		}		
	}

	public boolean canLandOnPlanets() {
		if (hull.equals(SCOUT_HULL) || hull.equals(GUNSHIP_HULL) || hull.equals(ESCORT_HULL)) {
			return (true);
		} else {
			return (false);
		}
	}

	public void setShip(boolean b) {
		isShip = b;
		if (!isShip) {
			sublightDrive = "";
			interstellarDrive = "";
			oreSeeker = "";
			tactical = ""; 
			fuelTank = "";
			fuelScoop = "";
			mineDeployer = "";
			colonyModule = "";
			cargoModule = "";
			cargoModuleCount = 0;	

		}
	}

	public ShipDesign(String n) {
		shipClass = n;
	}

	public String toString() {
		StringBuffer sd = new StringBuffer("ShipClass = " + shipClass);
		sd.append("\n drives = " + sublightDrive + ", " + interstellarDrive);
		sd.append("\n weapons:");
		sd.append("\n beam weapons = " + beamWeaponType + "(" + beamWeaponCount + ")");
		sd.append("\n particle weapons = " + particleWeaponType + "(" + particleWeaponCount + ")");
		sd.append("\n missile weapons = " + missileWeaponType + "(" + missileWeaponCount + ")");
		sd.append("\n bomb weapons = " + bombWeaponType + "(" + bombWeaponCount + ")");
		sd.append("\n oreSeeker = " + oreSeeker);
		sd.append("\n tactical = " + tactical);
		sd.append("\n fuelTank = " + fuelTank);
		sd.append("\n fuelScoop = " + fuelScoop);
		sd.append("\n mineDeployer = " + mineDeployer);
		sd.append("\n cargoModule = " + cargoModule + "(" + cargoModuleCount + ")");
		sd.append("\n scanner = " + scanner);
		sd.append("\n target computer = " + targetComputer);
		sd.append("\n armor = " + armorType + "(" + armorCount + ")");
		sd.append("\n shielding = " + shieldType + "(" + shieldCount + ")");
		return sd.toString();
	}

	public int getMaxPayload(Player player) {
		ShipComponent shipComponent = (ShipComponent) player.getPossibleComponents().get(hull);
		return shipComponent.getPayload();
	}

	// XXX: if components like sublightdrive are installed in bigger versions
	// (e.g. 4 times)
	// to accomodate for bigger hull size, count the total weight (e.g. payload
	// += 4*weight)
	public int getPayload(Player player) {
		int payload = 0;
		ShipComponent sc = null;
		Hashtable components = player.getPossibleComponents();
		if (!"".equals(scanner)) {
			sc = (ShipComponent) components.get(scanner);
			payload += sc.getWeight();
		}
		if (!"".equals(targetComputer)) {
			sc = (ShipComponent) components.get(targetComputer);
			payload += sc.getWeight();
		}
		if (!"".equals(beamWeaponType)) {
			sc = (ShipComponent) components.get(beamWeaponType);
			payload += beamWeaponCount * sc.getWeight();
		}
		if (!"".equals(particleWeaponType)) {
			sc = (ShipComponent) components.get(particleWeaponType);
			payload += particleWeaponCount * sc.getWeight();
		}
		if (!"".equals(missileWeaponType)) {
			sc = (ShipComponent) components.get(missileWeaponType);
			payload += missileWeaponCount * sc.getWeight();
		}
		if (!"".equals(bombWeaponType)) {
			sc = (ShipComponent) components.get(bombWeaponType);
			payload += bombWeaponCount * sc.getWeight();
		}

        //XXX: ?? fuel scoop and other components weight depending on total ship size/weight?
		if (!"".equals(oreSeeker)) {
			sc = (ShipComponent) components.get(oreSeeker);
			payload += sc.getWeight();
		}
		if (!"".equals(tactical)) {
			sc = (ShipComponent) components.get(tactical);
			payload += sc.getWeight();
		}
		if (!"".equals(fuelTank)) {
			sc = (ShipComponent) components.get(fuelTank);
			payload += sc.getWeight();
		}
		if (!"".equals(fuelScoop)) {
			sc = (ShipComponent) components.get(fuelScoop);
			payload += sc.getWeight();
		}		
		if (!"".equals(mineDeployer)) {
			sc = (ShipComponent) components.get(mineDeployer);
			payload += sc.getWeight();
		}
		if (!"".equals(colonyModule)) {
			sc = (ShipComponent) components.get(colonyModule);
			payload += sc.getWeight();
		}		
		if (!"".equals(cargoModule)) {
			sc = (ShipComponent) components.get(cargoModule);
			payload += cargoModuleCount * sc.getWeight();			
		}			
		if (!"".equals(armorType)) {
			sc = (ShipComponent) components.get(armorType);
			payload += armorCount * sc.getWeight();
		}
		if (!"".equals(shieldType)) {
			sc = (ShipComponent) components.get(shieldType);
			payload += shieldCount * sc.getWeight();
		}
		return payload;
	}

	public int getBeamWeaponCount() {
		return beamWeaponCount;
	}

	public void setBeamWeaponCount(int beamWeaponCount) {
		this.beamWeaponCount = beamWeaponCount;
	}

	public String getBeamWeaponType() {
		return beamWeaponType;
	}

	public void setBeamWeaponType(String beamWeaponType) {
		this.beamWeaponType = beamWeaponType;
	}

	public int getBombWeaponCount() {
		return bombWeaponCount;
	}

	public void setBombWeaponCount(int bombWeaponCount) {
		this.bombWeaponCount = bombWeaponCount;
	}

	public String getBombWeaponType() {
		return bombWeaponType;
	}

	public void setBombWeaponType(String bombWeaponType) {
		this.bombWeaponType = bombWeaponType;
	}

	public int getMissileWeaponCount() {
		return missileWeaponCount;
	}

	public void setMissileWeaponCount(int missileWeaponCount) {
		this.missileWeaponCount = missileWeaponCount;
	}

	public String getMissileWeaponType() {
		return missileWeaponType;
	}

	public void setMissileWeaponType(String missileWeaponType) {
		this.missileWeaponType = missileWeaponType;
	}

	public int getParticleWeaponCount() {
		return particleWeaponCount;
	}

	public void setParticleWeaponCount(int particleWeaponCount) {
		this.particleWeaponCount = particleWeaponCount;
	}

	public String getParticleWeaponType() {
		return particleWeaponType;
	}

	public void setParticleWeaponType(String particleWeaponType) {
		this.particleWeaponType = particleWeaponType;
	}
	
	public String getOreSeeker() {
		return oreSeeker;
	}

	public String getTactical() {
		return tactical;
	}

	public String getFuelTank() {
		return fuelTank;
	}

	public String getFuelScoop() {
		return fuelScoop;
	}

	public String getMineDeployer() {
		return mineDeployer;
	}

	public String getCargoModule() {
		return cargoModule;
	}

	public int getCargoModuleCount() {
		return cargoModuleCount;
	}	

	// add components
	public void addDrive(Player player, String drive) {
		Hashtable components = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) components.get(drive);
		if (sc != null && sc.getSublightSpeed() > 0) {
			sublightDrive = drive;
		}
		sc = (ShipComponent) components.get(drive);
		if (sc != null && sc.getWarpSpeed() > 0) {
			interstellarDrive = drive;
		}
	}
	
	public void addElectronics(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		if (sc.getSubType() == SCANNER_TYPE) {
			setScanner(name);
		}
		if (sc.getSubType() == TARGET_COMPUTER_TYPE) {
			setTargetComputer(name);
		}
	}

	// subtype armor and shielding are both of type shielding in ShipYard
	public boolean addShieldingOrArmor(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		// either armor or shielding, but not both!
		if (sc.getSubType() == ARMOR_TYPE) {
			return addArmor(name);
		} else {
			return addShielding(name);
		}
	}

	// several component types handled under 'special'
	public boolean addSpecial(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		if (sc.getSubType() == ORE_SEEKER_TYPE) {
			oreSeeker = name;
			return true;
		}
		if (sc.getSubType() == TACTICAL_TYPE) {
			tactical = name;
			return true;
		}
		if (sc.getSubType() == FUEL_TANK_TYPE) {
			fuelTank = name;
			return true;
		}
		if (sc.getSubType() == FUEL_SCOOP_TYPE) {
			fuelScoop = name;
			return true;
		}
		if (sc.getSubType() == MINE_DEPLOYER_TYPE) {
			mineDeployer = name;
			return true;
		}	
		if (sc.getSubType() == COLONY_MODULE_TYPE) {
			colonyModule = name;
			return true;
		}	
		if (sc.getSubType() == CARGO_TYPE) {
			if (cargoModule.equals("") || name.equals(cargoModule)) {
				cargoModule = name;
				cargoModuleCount++;
				return true;
			}
		}		
		return false;
	}	
	
	public boolean addWeapon(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		// either armor or shielding, but not both!
		if (sc.getSubType() == BEAM_WEAPON_TYPE) {
			return addBeamWeapon(name);
		} else if (sc.getSubType() == PARTICLE_WEAPON_TYPE) {
			return addParticleWeapon(name);
		} else if (sc.getSubType() == MISSILE_WEAPON_TYPE) {
			return addMissileWeapon(name);
		} else if (sc.getSubType() == BOMB_WEAPON_TYPE) {
			return addBombWeapon(name);
		}
		return false;
	}

	public boolean addArmor(String s) {
		if (armorType.equals("") || s.equals(armorType)) {
			armorType = s;
			armorCount++;
			return true;
		} else {
			return false;
		}
	}

	private void setTargetComputer(String name) {
		targetComputer = name;
	}

	private void setScanner(String name) {
		scanner = name;
	}	
	
	public boolean addShielding(String s) {
		if (shieldType.equals("") || s.equals(shieldType)) {
			shieldType = s;
			shieldCount++;
			return true;
		} else {
			return false;
		}
	}

	private boolean addBombWeapon(String name) {
		if (bombWeaponType.equals("") || name.equals(bombWeaponType)) {
			bombWeaponType = name;
			bombWeaponCount++;
			return true;
		} else {
			return false;
		}
	}

	private boolean addMissileWeapon(String name) {
		if (missileWeaponType.equals("") || name.equals(missileWeaponType)) {
			missileWeaponType = name;
			missileWeaponCount++;
			return true;
		} else {
			return false;
		}
	}

	private boolean addParticleWeapon(String name) {
		if (particleWeaponType.equals("") || name.equals(particleWeaponType)) {
			particleWeaponType = name;
			particleWeaponCount++;
			return true;
		} else {
			return false;
		}
	}

	private boolean addBeamWeapon(String name) {
		if (beamWeaponType.equals("") || name.equals(beamWeaponType)) {
			beamWeaponType = name;
			beamWeaponCount++;
			return true;
		} else {
			return false;
		}
	}

	// remove components
	// subtype armor and shielding are both of type shielding in ShipYard
	public boolean removeShieldingOrArmor(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		// either armor or shielding, but not both!
		if (sc.getSubType() == ARMOR_TYPE) {
			return removeArmor(name);
		} else {
			return removeShielding(name);
		}
	}

	public boolean removeWeapon(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		if (sc.getSubType() == BEAM_WEAPON_TYPE) {
			return removeBeamWeapon(name);
		} else if (sc.getSubType() == PARTICLE_WEAPON_TYPE) {
			return removeParticleWeapon(name);
		} else if (sc.getSubType() == MISSILE_WEAPON_TYPE) {
			return removeMissileWeapon(name);
		} else if (sc.getSubType() == BOMB_WEAPON_TYPE) {
			return removeBombWeapon(name);
		}
		return false;
	}

	public boolean removeElectronics(Player player, String name) {
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(name);
		if (sc.getSubType() == SCANNER_TYPE) {
			return removeScanner(name);
		} else if (sc.getSubType() == TARGET_COMPUTER_TYPE) {
			return removeTargetComputer(name);
		}
		return false;
	}

	public boolean removeSpecial(Player player, String name) {
		//Hashtable shipComponents = player.getPossibleComponents();
		//ShipComponent sc = (ShipComponent) shipComponents.get(name);
		if (oreSeeker.equals(name)) {
			oreSeeker = "";
			return true;
		}
		if (tactical.equals(name)) {
			tactical = "";
			return true;
		}
		if (fuelTank.equals(name)) {
			fuelTank = "";
			return true;
		}
		if (fuelScoop.equals(name)) {
			fuelScoop = "";
			return true;
		}
		if (mineDeployer.equals(name)) {
			mineDeployer = "";
			return true;
		}	
		if (colonyModule.equals(name)) {
			colonyModule = "";
			return true;
		}	
		if (cargoModule.equals(name)) {
			cargoModule = name;
			cargoModuleCount--;
			if (cargoModuleCount < 0) {
				cargoModuleCount = 0;
			}
			return true;
		}		
		return false;
	}	
	
	private boolean removeBombWeapon(String name) {
		if (bombWeaponType.equals(name)) {
			bombWeaponCount--;
			if (bombWeaponCount <= 0) {
				bombWeaponCount = 0;
				bombWeaponType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	private boolean removeMissileWeapon(String name) {
		if (missileWeaponType.equals(name)) {
			missileWeaponCount--;
			if (missileWeaponCount <= 0) {
				missileWeaponCount = 0;
				missileWeaponType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	private boolean removeParticleWeapon(String name) {
		if (particleWeaponType.equals(name)) {
			particleWeaponCount--;
			if (particleWeaponCount <= 0) {
				particleWeaponCount = 0;
				particleWeaponType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	private boolean removeBeamWeapon(String name) {
		if (beamWeaponType.equals(name)) {
			beamWeaponCount--;
			if (beamWeaponCount <= 0) {
				beamWeaponCount = 0;
				beamWeaponType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean removeDrive(String d) {
		if (sublightDrive.equals(d)) {
			sublightDrive = "";
			return true;
		}
		if (interstellarDrive.equals(d)) {
			interstellarDrive = "";
			return true;
		}
		return false;
	}

	private boolean removeTargetComputer(String name) {
		if (targetComputer.equals(name)) {
			targetComputer = "";
			return true;
		}
		return false;
	}

	private boolean removeScanner(String name) {
		if (scanner.equals(name)) {
			scanner = "";
			return true;
		}
		return false;
	}
	
	public void removeInterstellarDrive(String d) {
		if (interstellarDrive.equals(d)) {
			interstellarDrive = "";
		}
	}

	public boolean removeArmor(String s) {
		if (armorType.equals(s)) {
			armorCount--;
			if (armorCount <= 0) {
				armorCount = 0;
				armorType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean removeShielding(String s) {
		if (shieldType.equals(s)) {
			shieldCount--;
			if (shieldCount <= 0) {
				shieldCount = 0;
				shieldType = "";
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the colonyModule.
	 */
	public String getColonyModule() {
		return colonyModule;
	}

	private double addCost(int k, ShipComponent sc, int count) {
		double cost = 0;
		if (sc.getHullSizeDepending()==SOME_DEPENDING) {
			double factor = Math.log(hullsize);
			if (factor < 1) {factor=1;}
		    cost = count*sc.getCost(k)*factor;
		} else if (sc.getHullSizeDepending()==FULLY_DEPENDING) {
		    cost = count*sc.getCost(k)*hullsize;
		} else {
			cost = count*sc.getCost(k);
		}
		return cost;
	}

	private double addSupportCost(int k, ShipComponent sc, int count) {
		double cost = 0;
		if (sc.getHullSizeDepending()==SOME_DEPENDING) {
			double factor = Math.log(hullsize);
			if (factor < 1) {factor=1;}
		    cost = count*sc.getSupportCost(k)*factor;
		} else if (sc.getHullSizeDepending()==FULLY_DEPENDING) {
		    cost = count*sc.getSupportCost(k)*hullsize;
		} else {
			cost = count*sc.getSupportCost(k);
		}
		return cost;
	}

	public int getHullsize() {
		return hullsize;
	}	
	
}
