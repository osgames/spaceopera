//The class Ship contains the controlling logic for ship handling
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe.ships;

import java.awt.Graphics;
import java.util.Hashtable;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.RangeMarker;
import spaceopera.gui.objects.ShipDisplay;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;

/**
 * a ship is built on a colony, belongs to a player, may be moved each turn, is
 * drawn on the universe class
 */
@SuppressWarnings({"rawtypes"})
public class Ship extends SpaceCraft implements SOConstants {

	private float speed = 0;
	private int battleSpeed = 0;
	private boolean isTraveling = false;
	private int turns = 0;

	private int currentX;
	private int currentY;
	private int targetX;
	private int targetY;
	private int startX;
	private int startY;
	private double offsetX;
	private double offsetY;
	private double deltaX;
	private double deltaY;
	private double educationLevel = 0.0;
	private ShipDisplay shipDisplay = null;

	// Accessors
	public void addOffsetX(double x) {
		offsetX += x;
	}

	public void addOffsetY(double y) {
		offsetY += y;
	}

	public void decTurns() {
		turns--;
	}

	public int getBattleSpeed() {
		return (battleSpeed);
	}

	public double getDeltaX() {
		return (deltaX);
	}

	public double getDeltaY() {
		return (deltaY);
	}

	public double getEducationLevel() {
		return (educationLevel);
	}

	public boolean isTraveling() {
		return (isTraveling);
	}

	public String getNameOfTargetSun() {
		if (targetSunSystem != null) {
			return (targetSunSystem.getName());
		} else {
			return ("");
		}
	}

	public double getOffsetX() {
		return (offsetX);
	}

	public double getOffsetY() {
		return (offsetY);
	}

	public ShipOrderItem getCurrentOrder() {
		return (currentOrder);
	}

	public float getSpeed() {
		return (speed);
	}

	public int getStartX() {
		return (startX);
	}

	public int getStartY() {
		return (startY);
	}

	public int getTargetX() {
		return (targetX);
	}

	public int getTargetY() {
		return (targetY);
	}

	public int getTurns() {
		return (turns);
	}

	public int getX() {
		return (currentX);
	}

	public int getY() {
		return (currentY);
	}

	// Methods
	public boolean canColonize(Planet planet) {
		boolean canColonize = false;
		int available = getColonyModuleNumber();
		int necessary = planet.getNecessaryColonyModuleNr(getPlayer());
		if (available >= necessary) {
			canColonize = true;
		}
		return (canColonize);
	}

	public void completeOrder() {
		fromSunSystem = targetSunSystem;
		targetSunSystem.addSpaceCraft(this);
		player.removeFromShipsTo(targetSunSystem.getSun().getNumber());
		shipDisplay.setTargetSelected(false);
		isTraveling = false;
		if (!targetSunSystem.isExploredBy(player.getName())) {
			targetSunSystem.addExploredByPlayer(player.getName());
			Vector planets = targetSunSystem.getPlanets();
			for (int i = 0; i < planets.size(); i++) {
				Planet p = (Planet) planets.elementAt(i);
				if (player instanceof CPlayer) {
					if (!p.isColonized() && ((CPlayer) player).getPreferredPlanetType().equals(p.getType())) {
						// Test
						// System.out.println("added planet " + p);
						player.addPlanet(p);
					}
				} else {
					player.addPlanet(p);
					// XXX: show event only for VERY SPECIAL planets!
					// XXX: show event only once per game / 10 turns / ???
					if ((Math.random() * 20) > 18) {
						// TODO if ((p.getType().equals(MERCURYLIKE)) ||
						// (p.getType().equals(VENUSLIKE))) {
						// universe.queueEvent("LavaWorld");
						// }
						// if (p.getType().equals(EARTHLIKE)) {
						// if (p.isColonized()) {
						// universe.queueEvent("HomeWorldLost");
						// } else {
						// universe.queueEvent("HomeWorld");
						// }
						// }
						// if (p.getType().equals(ASTEROIDBELT)) {
						// if (Math.random() * 10 > 4) {
						// universe.queueEvent("AsteroidBelt1");
						// } else {
						// universe.queueEvent("AsteroidBelt2");
						// }
						// }
					}
				}
			}
		}
		if (player instanceof MPlayer) {
			targetSunSystem.setSunSelected(true);
			if (targetSunSystem.getCurrentSunSystem() != targetSunSystem) {
				targetSunSystem.update(targetSunSystem, null);
			}
		}

		if (currentOrder.getCommand().equals(TRANSPORT)) {
			targetSunSystem.unLoadCargo(currentOrder.getTarget(), currentOrder.getCargo(), loadFactor);
			loadFactor = 0;
		}

		shipDisplay.setSelected(false);
		offsetX = 0.0;
		offsetY = 0.0;
		turns = 0;

		currentX = targetSunSystem.getX();
		currentY = targetSunSystem.getY();
		shipDisplay.setDrawX(currentX + 12);
		shipDisplay.setDrawY(currentY - 8);
		startX = currentX;
		startY = currentY;
		if (!currentOrder.getOption().equals("auto")) {
			currentOrder = new ShipOrderItem("", "none", "");
		}
	}

	public void setNextOrder(String sun, String planet, String order) {
		ShipOrderItem nextOrder;
		if (planet == null || "".equals(planet)) {
			// XXX depending on planet being null here is error-prone!
			nextOrder = new ShipOrderItem("", order, sun);
		} else {
			nextOrder = new ShipOrderItem("", order, planet);
		}
		if (currentOrder.getCommand().equals(NONE)) {
			setCurrentOrder(nextOrder);
		} else {
			addNextOrder(nextOrder);
		}
	}

	public void setCurrentOrder(ShipOrderItem soi) {
		currentOrder = soi;

		fromSunSystem = targetSunSystem;
		targetSunSystem = universe.getSunSystem(currentOrder.getTarget());
		planet = targetSunSystem.getPlanet(currentOrder.getTarget());

		if ((fromSunSystem != null) && (fromSunSystem.equals(targetSunSystem))) {
			// ship is already there
			shipDisplay.setTargetSelected(false);
			isTraveling = false;
			player.removeFromShipsTo(targetSunSystem.getNumber());
		} else {
			isTraveling = true;
			shipDisplay.setTargetSelected(true);
			setTarget(targetSunSystem.getX() + 5, targetSunSystem.getY() + 5);
			player.addToShipsTo(targetSunSystem.getNumber());
			int distance = (int) Math.sqrt((double) ((currentX - targetX) * (currentX - targetX) + (currentY - targetY)
					* (currentY - targetY)));
			if (speed < 0.01) {
				speed = 0.01f;
			}
			double time = distance / speed;
			turns = (int) time + 1;
			deltaX = (targetX - currentX) / time;
			deltaY = (targetY - currentY) / time;
			startX = currentX;
			startY = currentY;
		}
		try {
			fromSunSystem.removeSpaceCraft(this);
		} catch (Exception e) {
			// XXX Nullpointer exception happens all the time for CPlayer
		}

		double cargoLoaded = 0;
		if (currentOrder.getCommand().equals(TRANSPORT)) {
			// add cargo to ship, remove cargo from sunsystem
			// (colony/station/...)
			cargoLoaded = fromSunSystem.loadCargo(this.getPlayer(), currentOrder.getCargo(), this.getTransportCapacity());
			// XXX: when adding cargo, increase loadFactor, when removing
			// cargo, decrease loadFactor
			loadFactor += cargoLoaded;
			cargo = soi.getCargo();
		}
	}

	public void setPosition(int x, int y) {
		currentX = x;
		currentY = y;
		shipDisplay.setDrawX(x);
		shipDisplay.setDrawY(y);
	}

	public void setTarget(int x, int y) {
		targetX = x;
		targetY = y;
	}

	public Ship(Player player, ShipDesign shipDesign, Colony colony, Universe universe) {
		super(player, shipDesign, colony, universe);
		String drive = shipDesign.getInterstellarDrive();
		Hashtable possibleComponents = player.getPossibleComponents();
		currentOrder = new ShipOrderItem("", "none", "");
		try {
			ShipComponent sc = (ShipComponent) possibleComponents.get(drive);
			speed = ((int) sc.getWarpSpeed()) * universe.getPixelPerLightYear();
		} catch (Exception e) {
			// ignore this, some SpaceCraft have no drive!
		}

		battleSpeed = 1; // XXX:+
		// (shipDesign.getMaxPayload()/shipDesign.getPayload());

		if (colony != null) {
			educationLevel = colony.getEducationLevel(); // take from colony
		} else {
			// start of game:
			double difficulty = (double) universe.getDifficultyLevel();
			difficulty /= 5;
			educationLevel = 10.0 - difficulty;
		}
		shipDisplay = new ShipDisplay(this, player);
	}

	// add 040210 SR - for Mini-Info
	// private ShipMiniInfo x;
	public void showMiniInfo(SpaceOpera so) {
		// ShipMiniInfo x = new ShipMiniInfo();
		// x.showInfo(s);
		// TODO so.showShipInfo(this);
	}

	public void cleanup() {
		shipDisplay.cleanup();
	}

	public void clear(Graphics g) {
		shipDisplay.clear(g);
	}

	public void display(Graphics g, SpaceOpera so) {
		shipDisplay.display(g, so);
	}

	public RangeMarker getRM() {
		return shipDisplay.getRM();
	}

	public boolean isSelected() {
		return shipDisplay.isSelected();
	}

	public void setSelected(boolean s) {
		shipDisplay.setSelected(s);
	}

	public boolean targetIsSelected() {
		return shipDisplay.targetIsSelected();
	}

	public void setScanned(boolean b) {
		shipDisplay.setScanned(b);
	}

	public void paintDragLine(Graphics g, SpaceOpera so) {
		shipDisplay.paintDragLine(g, so);
	}

	public boolean isClicked(int x, int y) {
		return shipDisplay.isClicked(x, y);
	}

	public void moveDragLine(Graphics g, SpaceOpera so, int x, int y) {
		shipDisplay.moveDragLine(g, so, x, y);

	}

}