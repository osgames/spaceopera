//The class Technology is used to manage technologies in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;
import spaceopera.universe.ships.ShipComponent;

/**
 * Class Technology describes a technology that can be researched by a player.
 * Some technologies are already known at the begin of a game. When a technology
 * is fully researched, sometimes one or two other technologies become
 * researchable and possibly a building, a product or a shipcomponent can become
 * buildable for this player.
 */
@SuppressWarnings({ "rawtypes" })
public class Technology implements Cloneable {

	private boolean allowStudy;
	private int allowsBuilding;
	private int obsoletesBuilding;
	private int allowsShipComponent;
	private int obsoletesShipComponent;
	private int allowsProduct;
	private String cls;
	private int childTech1;
	private int childTech2;
	private double complete;
	private boolean currentState;
	private String description;
	private int id;
	private boolean initialState;
	private boolean isStudied;
	private String name;
	private String picture;
	private double researchCost;
	private double researchCostPaid;

	// Accessors
	public int getId() {
		return (id);
	}

	public String getName() {
		return (name);
	}

	public String getCls() {
		return (cls);
	}

	public boolean getInitialState() {
		return (initialState);
	}

	public boolean getAllowStudy() {
		return (allowStudy);
	}

	public boolean getCurrentState() {
		return (currentState);
	}

	public String getComplete() {
		String percent = new Double(complete).toString();
		int pos = percent.indexOf(".");
		if (pos + 3 < percent.length()) {
			percent = percent.substring(0, pos + 3);
		}
		return (percent);
	}

	public String getDescription() {
		return (description);
	}

	public double getResearchCost() {
		return (researchCost);
	}

	public void setResearchCost(double cost) {
		researchCost = cost;
	}

	public double getResearchCostPaid() {
		return (researchCostPaid);
	}

	public int getChildTech1() {
		return (childTech1);
	}

	public int getChildTech2() {
		return (childTech2);
	}

	public int getAllowsBuilding() {
		return (allowsBuilding);
	}

	public int getObsoletesBuilding() {
		return (obsoletesBuilding);
	}

	public int getAllowsProduct() {
		return (allowsProduct);
	}

	public int getAllowsComponent() {
		return (allowsShipComponent);
	}

	public int getObsoletesShipComponent() {
		return (obsoletesShipComponent);
	}

	public void setDescription(String d) {
		description = d;
	}

	// public void setResearchCostPaid(double f) {researchCostPaid=f;}

	// public void setComplete(double c) {complete=c;}
	public boolean allowStudy() {
		return (allowStudy);
	}

	public boolean isStudied() {
		return (isStudied);
	}

	// Methods
	public void addResearchCostPaid(double f, Player player) {
		researchCostPaid += f;
		complete = (100 / researchCost * researchCostPaid);
		if (complete >= 100) {
			complete = 100;
			completeTech(player);
		}
	}

	public Object clone() {
		Technology object = null;
		try {
			object = (Technology) super.clone();
		} catch (Exception e) {
			System.out.println("Technology clone failed. " + e);
		}
		return object;
	}

	private void completeTech(Player player) {
		// System.out.println("playername: " + player.getName() + " tech: " +
		// this.getName());
		List<Technology> possibleTechnologies = null;
		List<Building> possibleBuildings = null;
		List<Colony> colonies = null;
		Vector possibleProducts = null;
		isStudied = true;
		// child technologies can now be studied
		possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = possibleTechnologies.get(i);
			if ((tech.id == childTech1) || (tech.id == childTech2)) {
				if (player instanceof MPlayer) {
					System.out.println("Research: Technology " + tech.name + " can be studied now.");
				}
				tech.allowStudy = true;
			}
		}
		// researched buildings can now be built
		possibleBuildings = player.getPossibleBuildings();
		for (int i = 0; i < possibleBuildings.size(); i++) {
			Building e = (Building) possibleBuildings.get(i);
			if (e.getId() == allowsBuilding) {
				if (player instanceof MPlayer) {
					System.out.println("Research: Building " + e.getName() + " can be built now.");
				}
				e.setAllowBuild(true);
			}
			// obsoletes building
			if (e.getId() == obsoletesBuilding) {
				if (player instanceof MPlayer) {
					System.out.println(name + " obsoletes " + e.getName());
					e.setAllowBuild(false);
				}
			}
		}

		// researched products can now be built
		possibleProducts = player.getPossibleProducts();
		for (int i = 0; i < possibleProducts.size(); i++) {
			Product p = (Product) possibleProducts.elementAt(i);
			if (p.getId() == allowsProduct) {
				if (player instanceof MPlayer) {
					System.out.println("Research: Product " + p.getName() + " can be built now.");
				}
				p.setAllowBuild(true);
			}

			// XXX: obsoletes product??
		}
		colonies = player.getColonies();
		for (int i = 0; i < colonies.size(); i++) {
			Colony col = colonies.get(i);
			possibleBuildings = col.getPossibleBuildings();
			for (int j = 0; j < possibleBuildings.size(); j++) {
				Building e = (Building) possibleBuildings.get(j);
				if (e.getId() == allowsBuilding) {
					e.setAllowBuild(true);
				}
			}
			possibleProducts = col.getPossibleProducts();
			for (int j = 0; j < possibleProducts.size(); j++) {
				Product p = (Product) possibleProducts.elementAt(j);
				if (p.getId() == allowsProduct) {
					p.setAllowBuild(true);
				}
			}
		}
		// researched shipcomponents can now be used for shipdesigns
		Enumeration possibleComponents = player.getPossibleComponents().elements();
		while (possibleComponents.hasMoreElements()) {
			ShipComponent shipComp = (ShipComponent) possibleComponents.nextElement();
			if (shipComp.getId() == allowsShipComponent) {
				shipComp.setAllowBuild(true);
				if (player instanceof MPlayer) {
					System.out.println("Research: Ship component " + shipComp.getName() + " can be used now.");
				} else {
					((CPlayer) player).newShipsCanBeBuilt(shipComp);
				}

			}
			// XXX: obsoletes component (e.g. warp 2 drive obsoletes warp 1
			// drive?)
			if (shipComp.getId() == obsoletesShipComponent) {
				if (player instanceof MPlayer) {
					System.out.println(name + " obsoletes " + shipComp.getName());
					shipComp.setAllowBuild(false);
				}
			}
		}

		// XXX: personalities have different research priorities and sequences!
		// 1) the simplest one is implemented below
		// 2) more advanced would be a research sequence (e.g.
		// 1-3-7-24-19-20-2-8-5-...)
		// 3) another strategy would be to research all techs from level 1
		// first, then all from level 2, ...
		possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = possibleTechnologies.get(i);
			if (tech.getCls().equals(cls)) {
				if ((tech.allowStudy) && (!tech.isStudied)) {
					if (cls.equals("Physics")) {
						player.setCurrentPhysics(tech.getName());
					} else if (cls.equals("Biology")) {
						player.setCurrentBiology(tech.getName());
					} else if (cls.equals("Mathematics")) {
						player.setCurrentMathematics(tech.getName());
					} else if (cls.equals("Social")) {
						player.setCurrentSocial(tech.getName());
					} else if (cls.equals("Economy")) {
						player.setCurrentEconomy(tech.getName());
					} else if (cls.equals("Military")) {
						player.setCurrentMilitary(tech.getName());
					}
					break;
				}
			}
		}
	}

	public void obsoletesBuilding(int o) {
		obsoletesBuilding = o;
	}

	public void obsoletesShipComponent(int o) {
		obsoletesShipComponent = o;
	}

	public Technology(int id, String name, String cls, String description, boolean init, boolean allow, int childtech1,
			int childtech2, int building, int prod, int comp, double cost, String pict) {
		this.id = id;
		this.name = name;
		this.cls = cls;
		this.initialState = init;
		this.isStudied = init;
		this.allowStudy = allow;
		this.description = description;
		this.researchCost = cost;
		this.childTech1 = childtech1;
		this.childTech2 = childtech2;
		this.allowsBuilding = building;
		this.allowsProduct = prod;
		this.allowsShipComponent = comp;
		this.picture = pict;
	}
}