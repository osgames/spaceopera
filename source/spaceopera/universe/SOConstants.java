//The class SOConstants bundles all (most?) constants in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

package spaceopera.universe;

public interface SOConstants {
  String VERSIONSTRING="0.6.001"; // Version.Release.Modification (Modification = "daily" build)
  int COLONYIMAGES=140;           // # colony images
  //XXX: rename and regroup planet images to reflect mars-like, earth-like logic
  int NUMGREYPLANETS=13;          // # of grey planet images (mercurylike)
  int NUMGREENPLANETS=17;         // # of green planet images (earthlike)
  int NUMBLUEPLANETS=13;          // # of blue planet images (earthlike II)
  int NUMREDPLANETS=6;            // # of red planet images (marslike)
  int NUMORANGEPLANETS=10;        // # of orange planet images (venuslike)
  int NUMWHITEPLANETS=10;         // # of white planet images (gas giants)
  int NUMPINKPLANETS=20;          // # of pink planet images (gas giants II)
  int NUMASTEROIDBELTS=3;         // # of asteroid belt images
  int MINSTARDIST=40;             // min distance in pixel between stars
  int MAXSTARDIST=110;             // max distance in pixel between stars
  int SSBACKGROUNDSTARS=100;      // number of backgroundstars Sunsystem
  int FIGHTBACKGROUNDSTARS=200;   // number of backgroundstars FightDetail
  int NUMPLANETS=7;               // max. number of planets per sunsystem
  int STARSIZE=8;                 // displayed starsize in pixel
  int SUNSYSTEMFRAMEX=320;        // Framesize Sunsystem
  int SUNSYSTEMFRAMEY=260;        // Framesize Sunsystem
  int SUNSYSTEMLEFT=8;            // Frameposition left
  int SUNSYSTEMTOP=35;            // Frameposition top
  int ULEFT=5;                    // Frameposition Universe left
  int UTOP=35;                    // Frameposition Universe top
  int PLANETTOP=288;              // Frameposition top
  int TASKBAR_HEIGHT = 26;        // Windows taskbar height 
    
  
  //new initial vars - add by S.R 
  int ALLWINDOWSIZEX=800;			// global WindowSize 
  int ALLWINDOWSIZEY=600;			// global WindowSize
  boolean ANTIALIAS=true;			// global Antialias
  
  // fix all to 1 or 2 sizes in next gui-version
  // make understandig code easier
  int SMALLWINDOWSIZEX=420;
  int SMALLWINDOWSIZEY=240;
  int ANIMWINDOWSIZEX=360;
  int ANIMWINDOWSIZEY=460;
  int SOUNDSETTINGSSIZEX=420;
  int SOUNDSETTINGSSIZEY=360;
  int ANIMSIZEX=280;
  int ANIMSIZEY=140;
  int MESSAGEBOXSIZEX=400;
  int MESSAGEBOXSIZEY=200;
    
  int COLONYRESOURCESIZEX=		ALLWINDOWSIZEX;  // can always change to standard
  int COLONYRESOURCESIZEY=		ALLWINDOWSIZEY;  //
  int WINDOWSIZEX=				ALLWINDOWSIZEX;  // only for tests or specials 
  int WINDOWSIZEY=				ALLWINDOWSIZEY;  // not removed
  int PLANETLISTWINDOWSIZEX=	ALLWINDOWSIZEX;
  int PLANETLISTWINDOWSIZEY=	ALLWINDOWSIZEY;  
  int SHIPYARD_SIZEX=			ALLWINDOWSIZEX;	
  int SHIPYARD_SIZEY=			ALLWINDOWSIZEY;	

  int FIGHTFRAMEX=640;            // Framesize Sunsystem FightDetail x
  int FIGHTFRAMEY=480;            // Framesize Sunsystem FightDetail y
  int SUN_TYPES=6;              // number of different sun types (red, white,...)
  int ORBITAL_DISTANCE=9;         // distance between planetary orbits
  int RADIUSEARTH=7;              // radius offsets
  int RADIUSMARS=6;
  int RADIUSVENUS=5;
  int RADIUSMERCURY=4;
  int RADIUSASTEROIDBELT=5;
  int RADIUSGASGIANT=10;
  int RADIUSTRANSSATURN=7;
  int NUMCOLONISTS=10000;         // Number of colonists in Colonyship
  int STARTYEAR=2400;             // starting year
  //int MANYEARDAYS=300;            // 300 working days = Standard 'Man year' = one game turn
  int MOVEPIXELS=10;              // ship/torpedo moves n*number of pixels per battle-turn
  int MAXBATTLESPEED=20;          // maximum battlespeed for Synch-Loop
  
  //display resource density on planets
  String NONE="none";           
  String VERY_POOR="very poor";
  String POOR="poor";
  String SOME="some";
  String RICH="rich";
  String VERY_RICH="very rich";
  
  //Buttons
  String CLOSE = "Close";
  String YES = "Yes";
  String NO = "No";
  String CANCEL = "Cancel";
  String GO = "Go!";
  String USE = "Use";
  
  //demand:
  ////String NONE="none";
  double D_VERY_LOW=0.3;  
  double D_LOW=0.6; 	  
  double D_AVERAGE=0.9;    
  double D_HIGH=1.3;      
  double D_VERY_HIGH=1.7;
  double D_EXTREMELY_HIGH=3;
  String VERY_LOW="very low";
  String LOW="low";
  String AVERAGE="average";
  String HIGH="high";
  String VERY_HIGH="very high";
  String EXTREMELY_HIGH="extremely high";
  int MINDISTSQUARED = 8100;       // minimal distance between players is 90 Pixel
   
  //Shipyard
  String DRIVES="Drives";
  String ELECTRONICS="Electronics";
  String ARMOR="Armor";
  String SHIELDING="Shielding";  
  String BASIC_SHIELDING="Basic Shielding Unit"; 
  String IMPROVED_SHIELDING="Improved Shielding Unit"; 
  String ADVANCED_SHIELDING="Advanced Shielding Unit";
  String PLANETARY_SHIELDING="Planetary Shielding";
  String ADVANCED_PLANETARY_SHIELDING="Advanced Planetary Shielding";
  String SPECIAL="Special";
  String WEAPONS="Weapons";
  String SATELLITE="Satellite";
  
  //hull size depending shipcomponents
  int NOT_DEPENDING = 0; //hull size does not affect weight and cost
  int SOME_DEPENDING = 1; //log of hull size multiplies weight and cost
  int FULLY_DEPENDING = 2; //hull size multiplies weight and cost
  
  //AI shipdesign
  int SUBLIGHT_DRIVE_TYPE = 1;
  int FTL_DRIVE_TYPE = 2;
  int BEAM_WEAPON_TYPE = 3;
  int MISSILE_WEAPON_TYPE = 4;
  int PARTICLE_WEAPON_TYPE = 5;
  int BOMB_WEAPON_TYPE = 6;
  int ORE_SEEKER_TYPE = 7;
  int TACTICAL_TYPE = 8;
  int FUEL_TANK_TYPE = 9;
  int FUEL_SCOOP_TYPE = 10;
  int CARGO_TYPE = 11;
  int MINE_DEPLOYER_TYPE = 12;
  int TARGET_COMPUTER_TYPE = 13;
  int SCANNER_TYPE = 14;
  int ARMOR_TYPE = 15;
  int SHIELDING_TYPE = 16;
  int COLONY_MODULE_TYPE = 17;
  int AI_SHIP_TEMPLATE_ARRAY_SIZE = 18;
  //XXX: when adding new design area types, also extend the templates in AiShipBuilder!  (e.g. area weapons)
  
  //Planets
  String MERCURYLIKE = "Mercurylike";
  String EARTHLIKE = "Earthlike";
  String MARSLIKE = "Marslike";
  String VENUSLIKE = "Venuslike";
  String GASGIANT = "Gasgiant";
  String ASTEROIDBELT = "Asteroidbelt";
  String TRANSSATURN = "TransSaturn";
  
  // Resource numbering
  int STANDARD_DAY = 8;
  int R_MIN = 1;
  int R_WORKUNITS = 1;
  int R_WORKUNITS_CONSTRUCTION = 2;
  int R_WORKUNITS_OPERATION = 3;
  // services
  
  int R_WORKUNITS_MILITARY = 4;
  int R_WORKUNITS_EDUCATION = 5;
  int R_WORKUNITS_ADMINISTRATION = 6;
  int R_END_WORKUNITS = 7;
  int R_ELECTRICITY = 8;
  int R_RAWMATERIAL = 9;
  int R_STONE = 10;
  int R_WOOD = 11;
  int R_COAL = 12;
  int R_OIL = 13;
  int R_IRON = 14;
  int R_COPPER = 15;
  int R_ALUMINUM = 16;
  int R_SILICIUM = 17;
  int R_SILVER = 18;
  int R_GOLD = 19;
  int R_URANIUM = 20;
  int R_FOOD = 21;
  int R_MEAT = 22;
  int R_FISH = 23;
  int R_RICE = 24;
  int R_CROP = 25;
  int R_DAIRY = 26;
  int R_VEGETABLES = 27;
  int R_END_FOOD = 28;
  int R_FRESHWATER = 28;
  int R_WINE = 29;
  int R_SALT = 30;
  int R_PROCESSED = 31;
  int R_GOODS = 32;
  int R_STEEL = 33;
  int R_ALLOY = 34;
  int R_TOOLS = 35;
  int R_MACHINES = 36;
  int R_ELECTRONICS = 37;
  int R_GLASS = 38;
  int R_CHEMICALS = 39;
  int R_DRUGS = 40;
  int R_PLASTIC = 41;
  int R_GARMENTS = 42;
  int R_WASTE = 43;
  int R_MAX = 44;
  // Difficulty levels:
  int D_BASE_DIFFICULTY=91;
  int D_KINDERGARDEN = 1; // BASE + 1*1 = 92
  int D_EASY = 2 ;        // BASE + 2*2 = 95
  int D_MEDIUM = 3;       // BASE + 3*3 = 100
  int D_HARD = 4;         // BASE + 4*4 = 107
  int D_IMPOSSIBLE = 5;   // BASE + 5*5 = 116
  // Technology/Component 'classes'
  String BIOLOGY = "Biology";
  String ECONOMY = "Economy";
  String MILITARY = "Military";
  String PHYSICS = "Physics";
  String SOCIAL = "Social";
  String MATHEMATICS = "Mathematics";
  // Buildings / technologies
  String BANK = "Bank";
  String BARRACKS="Barracks";  
  String CHURCH="Church";
  String COLONY_MANAGER="Colony Manager";
  String ADVANCED_COLONY_MANAGER="Advanced Colony Manager";
  String COAL_MINE = "Coal Mine";  
  String FACTORY = "Factory";
  String FARM = "Farm";
  String HABITAT = "Habitat";
  String LIBRARY="Library";
  String ORBITAL_SHIPYARD="Orbital Shipyard";
  String STRIP_MINING_UNIT = "Strip Mining Unit";
  String POWER_PLANT = "Power Plant";
  String SAW_MILL = "Saw Mill";
  String SCHOOL="School";
  String SHIP="Ship";
  String SCOUT_SHIP="Scout Ship";
  String COLONY_SHIP="Colony Ship";
  String DEFENDER="Defender";
  String SMALL_TRANSPORT = "Small Transport";
  String ESCORT="Escort";
  String FRIGATE="Frigate";
  String SOLAR_POWER_PLANT = "Solar Power Plant";
  String BATTLE_SCHOOL="Battle School";
  String TEMPLE="Temple";
  String UNIVERSITY="University";
  String WATER_PURIFICATOR="Water Purificator";
  String WEAPONS_FACTORY = "Weapons Factory";
  String TERRAFORM_FERTILITY = "Terraform (Fertility)";
  String TERRAFORM_TEMPERATURE = "Terraform (Temperature)";
  String TERRAFORM_RADIATION = "Terraform (Radiation)";
  String TERRAFORM_ATMOSPHERIC_PRESSURE = "Terraform (atm. pressure)";
  String TERRAFORM_ATMOSPHERIC_COMPOSITION = "Terraform (atm. composition)";
  String TERRAFORM_GRAVITATION = "Terraform (gravitation)";
  String TERRAFORM_WATER_LAND_RATIO= "Terraform (water/land)";
  String TERRAFORM_WEATHER_CONTROL  = "Terraform (weather)";
  String TERRAFORM_FLORA = "Terraform (flora)";
  String TERRAFORM_FAUNA = "Terraform (fauna)";
  
  // Ship Components
  String SCOUT_HULL = "Scout Hull";
  String GUNSHIP_HULL = "Gunship Hull";
  String ESCORT_HULL = "Escort Hull";
  String SMALL_TRANSPORT_HULL = "Small Transport Hull";
  String FRIGATE_HULL = "Frigate Hull";
  String COLONY_HULL = "Colony Hull";
  String SUBLIGHT_DRIVE = "Sublight drive";
  String LASER = "Laser";
  String BUNDLED_LASER = "Bundled laser";
  String LASER_CANNON = "Laser cannon";
  String TORPEDO = "Torpedo";
  String LONG_RANGE_TORPEDO = "Long Range Torpedo";
  String HIGH_SPEED_TORPEDO = "High Speed Torpedo";
  String STARBURST_TORPEDO = "Starburst Torpedo";  
  String DEFENSE_BATTERY = "Defense Battery";
  String DEFENSE_BATTERY_2 = "Defense Battery 2";
  String DEFENSE_BATTERY_3 = "Defense Battery 3";  
  String PELLET_GUN = "High Speed Projectile";
  String RAIL_GUN = "Rail Gun";
  String FUEL_SCOOP = "Fuel Scoop";
  String ARMOR_CLASS_I = "Armour Class I";
  String COLONY_MODULE = "Colony Module";
  String COLONY_MODULE_II = "Colony Module II";
  String COLONY_MODULE_III = "Colony Module III";
  String COLONY_MODULE_IV = "Colony Module IV";
  String COLONY_MODULE_V = "Colony Module V";
    
  //research cost
  double TECHLEVEL_0 =        1000;
  double TECHLEVEL_1 =       10000;
  double TECHLEVEL_2 =      100000;
  double TECHLEVEL_3 =     1000000;
  double TECHLEVEL_4 =    10000000;
  double TECHLEVEL_5 =   100000000;
  double TECHLEVEL_6 =  1000000000;
  double TECHLEVEL_7 = 10000000000.0;
  
  // ship orders
  int GOTO_TAB = 0;
  int SCOUT_TAB = 1;
  int COLONIZE_TAB = 2;
  int TRANSPORT_TAB = 3;
  int TRADE_TAB = 4;
  int ATTACK_TAB = 5;
  int DEFEND_TAB = 6;
  String NONE_AVAILABLE = "none available";
  String EXPLORE = "Explore";
  String COLONIZE = "Colonize";
  String TRANSPORT = "Transport";
  String GOTO = "Goto";
  String FIRST = "first";
  String BEST = "best";
  String CLOSEST = "closest";
  String NEXT = "next";
  String AREA = "area";
  String AUTO = "auto";
  String WHITE = "white";
  String YELLOW = "yellow";
  String RED = "red";
  String BLUE = "blue";
  String GREEN = "green";
  
  // player relations
  float NEUTRAL = 0;
  float GOOD = 1;
  float BAD = -1;
  
  // player attitude
  String AGGRESSIVE_ATTITUDE = "aggressive";
  String NEUTRAL_ATTITUDE = "neutral";
  String DEFENSIVE_ATTITUDE = "defensive";
  
  //Education Level Matrix
  //Level 0 is some stone age Education Level
  //Level 10 is around contemporary EL  
  //Level 20 is utopian EL
  //the first column is the uneducated percentage of the population
  //the second col. is the percentage with a primary education and so on
  int[][] educationLevelMatrix = {
  		{ 100 ,  0,  0,  0 } , 
		{  99 ,  1,  0,  0 } , 
		{  97 ,  2,  1,  0 } , 
		{  92 ,  6,  2,  0 } , 
		{  84 , 12,  3,  1 } , 
		{  75 , 18,  5,  2 } , 
		{  59 , 31,  7,  3 } , 
		{  38 , 47, 11,  4 } , 
		{  26 , 54, 14,  6 } , 
		{  19 , 53, 20,  8 } , 
		{  15 , 46, 28, 11 } , 
		{  12 , 36, 38, 14 } , 
		{  10 , 29, 44, 17 } , 
		{   8 , 24, 46, 22 } , 
		{   6 , 21, 45, 28 } , 
		{   5 , 18, 42, 35 } , 
		{   4 , 16, 37, 43 } , 
		{   3 , 14, 34, 49 } , 
		{   2 , 12, 32, 54 } , 
		{   1 , 11, 31, 57 } , 
		{   1 , 10, 30, 59 }
  };
  
}


