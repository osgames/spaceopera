//The class Universe contains sunsystems, nebulae, spaceships.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.components.SOEvent;
import spaceopera.gui.window.UniverseDisplay;
import spaceopera.test.Fullscreen;
import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ai.PlayerRelation;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.elements.SunSystem;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Universe class displays all the sunsystems, all known ships and some
 * background stars
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class Universe implements Runnable, SOConstants {

	private UniverseDisplay universeDisplay;
	// TODO: static universeSize for now
	private int universeSize = 900;
	private List<Sun> suns;
	private int galaxySize = 0;
	// private Sun dummySun;
	private Player currentPlayer;
	private Vector players;
	private int difficultyLevel; // Game settings
	private int numberOfOpponents; // Game settings
	private boolean randomPlayers;
	private boolean replaceEliminated;
	private int numStars = 1;
	private Vector buildings; // all possible buildings
	private Vector events; // all possible SpaceOpera events
	private Vector products; // all possible products
	private Vector components; // all possible components
	private Vector technologies; // all possible technologies
	private Vector ships = null;// all ships in Universe
	// public Vector otherObjects; // Nebulae, wandering Planets, Quasars ...
	// (?)
	private int turnNr; // turn - number (year/month/day?)
	// private String type; // sphere, spiral, ...
	private int pixelPerLightYear = 10;
	private int[] bgStarsX; // coordinates of background-stars
	private int[] bgStarsY;
	private Color[] bgStarsColor;
	// public int[] bgStarsZ;
	private SpaceCraft currentSpaceCraft = null;

	private boolean moveShip = false;
	// private SpaceOpera spaceOpera;
	private Fullscreen fs;
	private String[] starNames;
	private boolean[] starNamesUsed;
	private String[] playerNames;
	private Color[] playerColors;
	private String[] resourceName;
	private String[] resourceUnit;
	private int[][] educationLevelMatrix;
	private int maxSunsystemSpread = 10;
	private int sunSystemOffset;

	public int getMaxSunsystemSpread() {
		return maxSunsystemSpread;
	}

	// Accessors
	public void addBuilding(Building e) {
		buildings.addElement(e);
	}

	public void addComponent(ShipComponent k) {
		components.addElement(k);
	}

	public void addEvent(SOEvent e) {
		events.addElement(e);
	}

	// public void addFights(FightDetailVisible f) {fights.addElement(f);}
	public void addProduct(Product p) {
		products.addElement(p);
	}

	public void addSpaceCraft(SpaceCraft ship) {
		ships.addElement(ship);
	}

	public void addTechnology(Technology t) {
		technologies.addElement(t);
	}

	public void cleanup() {
		universeDisplay.cleanup();
	}

	public void clearScanArea() {
		universeDisplay.clearScanArea();
	}

	public void display() {
		universeDisplay.repaint();
	}

	public Color getBgStarColor(int i) {
		return (bgStarsColor[i]);
	}

	public int getBgStarX(int i) {
		return (bgStarsX[i]);
	}

	public int getBgStarY(int i) {
		return (bgStarsY[i]);
	}

	public Vector getBuildings() {
		return (buildings);
	}

	public int getDifficultyLevel() {
		return (difficultyLevel);
	}

	// public Vector getFights() {return(fights);}
	public SpaceCraft getCurrentSpaceCraft() {
		return (currentSpaceCraft);
	}

	public int getGalaxySize() {
		return galaxySize;
	}

	public Graphics getGraphics() {
		return (universeDisplay.getGraphics());
	}

	public int getPixelPerLightYear() {
		return (pixelPerLightYear);
	}

	public Player getPlayer() {
		return (currentPlayer);
	}

	public String getPlayerName() {
		return (currentPlayer.getName());
	}

	public Vector getPlayers() {
		return (players);
	}

	public Vector getProducts() {
		return (products);
	}

	public boolean getRandomPlayers() {
		return (randomPlayers);
	}

	public boolean getReplaceEliminated() {
		return (replaceEliminated);
	}

	public String getResourceName(int i) {
		return (resourceName[i]);
	}

	public String getResourceUnit(int i) {
		return (resourceUnit[i]);
	}

	public Vector getSOComponents() {
		return (components);
	}

	public Vector getShips() {
		return (ships);
	}

	public Sun getSun() {
		return (getSelectedSun());
	}

	public List<Sun> getSuns() {
		return (suns);
	}

	public String[] getSunNames() {
		String names[] = new String[suns.size()];
		for (int i = 0; i < suns.size(); i++) {
			names[i] = ((Sun) suns.get(i)).getName();
		}
		if (names.length == 0) {
			names = new String[] { NONE_AVAILABLE };
		}
		java.util.Arrays.sort(names);
		return (names);
	}

	public String[] getUnexploredSunNames(String playerName) {
		String names[] = new String[suns.size()];
		int j = 0;
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			// if a ship is already on its way to this sun, it might be explored
			// in the next turn ...
			if (!sun.isExploredBy(playerName)) {
				names[j] = sun.getName();
				j++;
			}
		}
		String sortedNames[] = new String[j];
		for (int i = 0; i < j; i++) {
			sortedNames[i] = names[i];
		}
		if (sortedNames.length == 0) {
			sortedNames = new String[] { "none available" };
		}
		java.util.Arrays.sort(sortedNames);
		return (sortedNames);
	}

	public UniverseDisplay getUniverseDisplay() {
		return (universeDisplay);
	}

	public int getUniverseSize() {
		return universeSize;
	}

	public Vector getTechnologies() {
		return (technologies);
	}

	public int getTurnNr() {
		return (turnNr);
	}

	public void incTurnNr() {
		turnNr++;
	}

	public void paintScanArea(Player p, Graphics2D g) {
		universeDisplay.paintScanArea(p, g, false);
	}

	public void removeSpaceCraft(SpaceCraft ship) {
		ships.removeElement(ship);
	}

	public void setDifficultyLevel(int i) {
		difficultyLevel = i;
	}

	public void setGalaxySize(int i) {
		galaxySize = i;
	}

	public void setNumberOfOpponents(int i) {
		numberOfOpponents = i;
	}

	public void setRandomPlayers(boolean b) {
		randomPlayers = b;
	}

	public void setReplaceEliminated(boolean b) {
		replaceEliminated = b;
	}

	public void setPlayer(Player player) {
		currentPlayer = player;
	}

	public void setPlayerColony(Colony c) {
		currentPlayer.setCurrentColony(c);
	}

	public void setPlayerColors(Color[] c) {
		playerColors = c;
	}

	public void setPlayerName(String s) {
		currentPlayer.setName(s);
	}

	public void setPlayerNames(String[] n) {
		playerNames = n;
	}

	public void setResourceNames(String[] n) {
		resourceName = n;
	}

	public void setResourceUnits(String[] n) {
		resourceUnit = n;
	}

	public void setEducationLevelMatrix(int[][] e) {
		educationLevelMatrix = e;
	}

	public void setCurrentSpaceCraft(SpaceCraft s) {
		currentSpaceCraft = s;
	}

	public void setStarNames(String[] n, boolean[] u) {
		starNames = n;
		starNamesUsed = u;
	}

	public String getBuildingName(int b) {
		String name = "Building Nr. " + b;
		for (int i = 0; i < buildings.size(); i++) {
			Building building = (Building) buildings.elementAt(i);
			if (building.getId() == b) {
				name = building.getName();
				i = buildings.size();
			}
		}
		return (name);
	}

	public String getComponentName(int c) {
		String name = "Component Nr. " + c;
		for (int i = 0; i < components.size(); i++) {
			ShipComponent comp = (ShipComponent) components.elementAt(i);
			if (comp.getId() == c) {
				name = comp.getName();
				i = components.size();
			}
		}
		return (name);
	}

	public String getProductName(int p) {
		String name = "Product Nr. " + p;
		for (int i = 0; i < products.size(); i++) {
			Product prod = (Product) products.elementAt(i);
			if (prod.getId() == p) {
				name = prod.getName();
				i = products.size();
			}
		}
		return (name);
	}

	private Sun getSelectedSun() {
		Sun selected = null;
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			if (sun.getSelected()) {
				selected = sun;
			}
		}
		if (selected == null) {
			selected = (Sun) suns.get(0);
		}
		return selected;
	}

	public SunSystem getSunSystem(String target) {
		SunSystem found = null;
		for (int i = 0; i < suns.size(); i++) {
			SunSystem tempSunSystem = ((Sun) suns.get(i)).getSunSystem();
			if (tempSunSystem.getName().equals(target)) {
				found = tempSunSystem;
				break;
			}
			Vector planets = tempSunSystem.getPlanets();
			for (int j = 0; j < planets.size(); j++) {
				Planet tempPlanet = (Planet) planets.elementAt(j);
				if (tempPlanet.getName().equals(target)) {
					found = tempPlanet.getSunSystem();
					break;
				}
			}
		}
		return found;
	}

	public String getTechnologyName(int t) {
		String name = "Technology Nr. " + t;
		for (int i = 0; i < technologies.size(); i++) {
			Technology tech = (Technology) technologies.elementAt(i);
			if (tech.getId() == t) {
				name = tech.getName();
				i = technologies.size();
			}
		}
		return (name);
	}

	// Methods
	private void checkFirstContact(Player playerA, Player playerB) {
		// if playerA knows playerB, playerB also knows playerA (?)
		boolean firstContact = false;

		if (!playerA.knowsPlayer(playerB.getName())) {
			playerA.meetsPlayer(playerB.getName());
			playerB.meetsPlayer(playerA.getName());
			firstContact = true;
		}

		// TODO if (firstContact) {
		// if (playerA instanceof MPlayer) {
		// // queue event 'firstcontact for playerA (meet playerB)
		// queueEvent(playerB.getName());
		// }
		// if (playerB instanceof MPlayer) {
		// // queue event 'firstcontact for playerB (meet playerA)
		// queueEvent(playerA.getName());
		// }
		// }
	}

	public void clear() {
		suns.clear();
		ships.removeAllElements();
		players.removeAllElements();
		currentSpaceCraft = null;
		// stop();
		universeDisplay.clear();
	}

	public void deleteShip(SunSystem sunSystem, SpaceCraft ship) {
		Graphics g = this.getGraphics();
		ship.clear(g);
		removeSpaceCraft(ship);
		sunSystem.removeSpaceCraft(ship);
		for (int i = 0; i < players.size(); i++) {
			Player player = (Player) players.elementAt(i);
			if (player.hasShip(ship)) {
				player.removeSpaceCraft(ship);
			}
		}
		ship = null;
	}

	public void executeBattles() {
		Vector ships = null;
		Vector planets = null;
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			Player firstPlayer = null;
			boolean showFight = false;
			boolean fight = false;
			// ship to ship fight
			ships = sun.getSpaceCraft();
			for (int j = 0; j < ships.size(); j++) {
				SpaceCraft spaceCraft = (SpaceCraft) ships.elementAt(j);
				Player shipPlayer = spaceCraft.getPlayer();
				if (firstPlayer == null) {
					firstPlayer = shipPlayer;
				}
				if (!firstPlayer.getName().equals(shipPlayer.getName())) {
					checkFirstContact(firstPlayer, shipPlayer);
					if ((firstPlayer.getPlayerRelation(shipPlayer) < NEUTRAL)
							|| (shipPlayer.getPlayerRelation(shipPlayer) < NEUTRAL)) {
						if (spaceCraft.getShipAttacks()) {
							if (spaceCraft.isShip()) {
								fight = true;
								// TODO if ((firstPlayer instanceof MPlayer) ||
								// (shipPlayer instanceof MPlayer)
								// || (spaceOpera.getShowBattles())) {
								// showFight = true;
								// }
							}
						}
					}
				}
			}
			// ship to colony fight
			planets = sun.getPlanets();
			for (int j = 0; j < planets.size(); j++) {
				Planet planet = (Planet) planets.elementAt(j);
				if (planet.isColonized()) {
					for (int k = 0; k < ships.size(); k++) {
						SpaceCraft spaceCraft = (SpaceCraft) ships.elementAt(k);
						Player shipPlayer = spaceCraft.getPlayer();
						Colony colony = planet.getColony();
						Player colonyPlayer = colony.getPlayer();
						if (!shipPlayer.getName().equals(colonyPlayer.getName())) {
							checkFirstContact(shipPlayer, colonyPlayer);
							if (spaceCraft.isShip() && spaceCraft.getPlanetAttacks()) {
								if (colonyPlayer.getPlayerRelation(shipPlayer) < NEUTRAL) {
									fight = true;
									// TODO if ((colonyPlayer instanceof
									// MPlayer) || (shipPlayer instanceof
									// MPlayer)
									// || (spaceOpera.getShowBattles())) {
									// showFight = true;
									// }
								}
							}
						}
					}
				}
			}
			if (fight) {
				// XXX: if several fights pop up, only the last is shown, all
				// others are executed automatically 8-(
				//TODO spaceOpera.executeFight(sun, showFight);
			}
		}
	}

	public void generate() {
		turnNr = 1;
		for (int i = 0; i < starNames.length; i++) {
			starNamesUsed[i] = false;
		}
		for (int i = 0; i < numStars; i++) {
			// select a unused starname
			String starName = "";
			boolean nameUsed = true;
			int nameNumber = 0;
			int breakOff = 0;
			while (nameUsed) {
				if (numStars < 50) {
					breakOff++;
				}
				breakOff++;
				nameNumber = (int) (Math.random() * starNames.length);
				if (!starNamesUsed[nameNumber]) {
					starNamesUsed[nameNumber] = true;
					nameUsed = false;
					starName = starNames[nameNumber];
				}
				// if all names are used, create a random name
				// XXX: group the syllables into language-groups (arabic,
				// nordic, chinese, ...)
				// or else lots of names like 'Skara gamal' or such will be
				// created
				if ((nameUsed) && ((breakOff > starNames.length) || (i % 8 == 1))) {
					nameUsed = false;
					int nameLength = 2 + (int) (Math.random() * 6);
					String[] selChars1 = { "Al", "An", "Ar", "Ard", "As", "Aun", "Aum", "Baal", "Bali", "Bei", "Bel", "B'", "Ben",
							"Brae", "Bor", "Bas", "Bash", "Bran", "Bar", "Bryn", "Byd", "Cux", "Cor", "Caer", "Coch", "Cym", "De",
							"Dao", "Dai", "Dun", "Di", "Eh", "El", "Er", "Ea", "Ead", "Fa", "Fan", "Fal", "Far", "Fe", "G'", "Ger",
							"Glen", "Gie", "Ging", "Gir", "H'", "Ha", "Hag", "Hwyr", "Il", "Jas", "Keogh", "Kial", "Kyr", "La",
							"Llyn", "Mac", "Mas", "Mor", "Mos", "Me", "Na", "Nagh", "Nin", "Ny", "Od", "Og", "Oor", "Pi", "Pro",
							"Rai", "Re", "Ruch", "Ruk", "Skara", "Sa", "Sad", "S'", "Sig", "Ssah", "Ssl", "Sib", "Sir", "Su", "Sym",
							"Tar", "Tap", "Tha", "T'", "Thor", "Tyr", "Unuk", "Ur", "Vor", "Wu", "Xu", "Ya", "Yi", "Yn", "Ynys",
							"Yr", "Zu" };
					String[] selChars2 = { "", "", "", "", "", "", "", " ", " ", " ", " ", " ", " ", " ", "-", "-", "'" };
					String[] selChars3 = { "ach", "al", "ben", "bar", "bran", "bia", "boeia", "chor", "ch", "crux", "da", "dal",
							"den", "dui", "el", "er", "es", "el", "ech", "end", "fak", "feh", "gafr", "gam", "gamal", "gwir",
							"gwyn", "ham", "hem", "il", "is", "ja", "jo", "kan", "kal", "la", "le", "li", "lil", "lik", "lynn",
							"man", "men", "main", "mal", "merik", "mest", "mir", "no", "nan", "ni", "och", "ogh", "pas", "pem",
							"qu", "re", "rah", "rash", "sa", "shem", "suud", "shain", "tair", "te", "tead", "ul", "un", "ur", "us",
							"von", "vor", "wan", "xin", "yao", "za", "zam", "zar", "Zu", "�a", "�i", "�n", "s�", "r�", "t�", "�y",
							"g�z", "�le", "n�", "d�", "f�", "b�", "n�" };

					int selChar = (int) (Math.random() * selChars1.length);
					starName = starName + selChars1[selChar];
					selChar = (int) (Math.random() * selChars2.length);
					starName = starName + selChars2[selChar];
					selChar = (int) (Math.random() * selChars3.length);
					starName = starName + selChars3[selChar];
					if (nameLength > 2) {
						selChar = (int) (Math.random() * selChars3.length);
						starName = starName + selChars3[selChar];
					}
					if (nameLength > 4) {
						selChar = (int) (Math.random() * selChars1.length);
						starName = starName + " " + selChars1[selChar];
					}
					if (nameLength > 6) {
						selChar = (int) (Math.random() * selChars2.length);
						starName = starName + selChars2[selChar];
						selChar = (int) (Math.random() * selChars3.length);
						starName = starName + selChars3[selChar];
					}
					// System.out.println("name: " + starName);
				}
			}
			Sun newSun = new Sun(starName, fs);
			newSun.generate(suns, i, this);
			suns.add(newSun);
		}
		bgStarsX = new int[universeSize];
		bgStarsY = new int[universeSize];
		bgStarsColor = new Color[universeSize];
		for (int i = 0; i < universeSize; i++) {
			int frameSize = universeSize;
			// if (frameSize < 500) {
			// frameSize = 500;
			// }
			bgStarsX[i] = (int) (Math.random() * frameSize);
			bgStarsY[i] = (int) (Math.random() * frameSize);
			int colr = (int) (Math.random() * 11);
			switch (colr) {
			case 0:
				bgStarsColor[i] = new Color(205, 205, 205);
				break;
			case 1:
				bgStarsColor[i] = new Color(216, 216, 216);
				break;
			case 2:
				bgStarsColor[i] = new Color(180, 180, 180);
				break;
			case 3:
				bgStarsColor[i] = new Color(216, 216, 0);
				break;
			case 5:
				bgStarsColor[i] = new Color(180, 180, 0);
				break;
			case 6:
				bgStarsColor[i] = new Color(200, 0, 0);
				break;
			case 7:
				bgStarsColor[i] = new Color(50, 150, 150);
				break;
			case 8:
				bgStarsColor[i] = new Color(10, 10, 180);
				break;
			case 9:
				bgStarsColor[i] = new Color(0, 0, 150);
				break;
			case 10:
				bgStarsColor[i] = new Color(0, 150, 150);
				break;
			default:
				bgStarsColor[i] = Color.WHITE;
				break;
			}

		}
		initPlayers();
	}

	public SOEvent getEvent(String name) {
		SOEvent se = null;
		for (int i = 0; i < events.size(); i++) {
			SOEvent t = (SOEvent) events.elementAt(i);
			if (t.getName().equals(name)) {
				se = t;
				break;
			}
		}
		return se;
	}

	public void load() {
	}

	public void moveShips() {
		Graphics g = this.getGraphics();
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft spaceCraft = (SpaceCraft) ships.elementAt(i);
			if (spaceCraft.isShip()) {
				Ship ship = (Ship) spaceCraft;
				if (ship.getTurns() > 1) {
					ship.decTurns();
					ship.clear(g);
					ship.addOffsetX(ship.getDeltaX());
					ship.addOffsetY(ship.getDeltaY());
					ship.setPosition(ship.getStartX() + (int) ship.getOffsetX(), ship.getStartY() + (int) ship.getOffsetY());
					// TODO ship.paintDragLine(g, fs);
					// ship.display(g, fs);
				} else {
					if (ship.isTraveling()) {
						// ship arrived
						for (int j = 0; j < suns.size(); j++) {
							Sun s = (Sun) suns.get(j);
							s.setSelected(false);
						}
						ship.clear(g);
						ship.completeOrder();
					}
				}
			}
		}
		return;
	}

	// TODO public void queueEvent(String eventName) {
	// spaceOpera.queueEvent(eventName);
	// }

	public void initPlayers() {
		Vector playerships = null;
		Player mPlayer = null;
		currentPlayer = new MPlayer(this, "Humans", Color.red);
		mPlayer = currentPlayer;
		players.addElement(currentPlayer);
		addPlayerShipsToUniverse();
		boolean[] playerUsed = new boolean[playerNames.length];
		for (int j = 0; j < playerNames.length; j++) {
			playerUsed[j] = false;
		}
		for (int j = 0; j < numberOfOpponents; j++) {
			int selectPlayer = (int) (Math.random() * playerNames.length);
			while (playerUsed[selectPlayer] == true) {
				selectPlayer = (int) (Math.random() * playerNames.length);
			}
			playerUsed[selectPlayer] = true;
			currentPlayer = new CPlayer(this, playerNames[selectPlayer], playerColors[selectPlayer]);
			players.addElement(currentPlayer);
			addPlayerShipsToUniverse();
		}
		currentPlayer = mPlayer;
		//XXX only relations between mplayer and others, not between others and others?
		for (int j = 0; j <= numberOfOpponents; j++) {
			Player jPlayer = (Player) players.elementAt(j);
			for (int k = 0; k <= numberOfOpponents; k++) {
				if (j != k) {
					Player kPlayer = (Player) players.elementAt(k);
					jPlayer.addRelation(new PlayerRelation(kPlayer));
				}
			}
		}
	}

	/**
	 * 
	 */
	private void addPlayerShipsToUniverse() {
		List<SpaceCraft> playerships = currentPlayer.getShips();
		for (int i = 0; i < playerships.size(); i++) {
			SpaceCraft ship = playerships.get(i);
			addSpaceCraft(ship);
		}
	}

	public void run() {
		// dummy method
	}

	public void save() {
		// XXX: implement save()
	}

	public void selectSun(Sun sun) {
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			if (s.equals(sun)) {
				s.setSelected(true);
			} else {
				s.setSelected(false);
			}
		}
	}

	public void setUniverseSize() {
		pixelPerLightYear = 12;
		if (galaxySize == 0) {
			maxSunsystemSpread = 80;
			sunSystemOffset = 400;
			numStars = 1;
		}
		if (galaxySize == 1) {
			maxSunsystemSpread = 250;
			sunSystemOffset = 350;
			numStars = 4 + (int) (Math.random() * 5) + 1;
		}
		if (galaxySize == 2) {
			maxSunsystemSpread = 390;
			sunSystemOffset = 250;
			numStars = 9 + (int) (Math.random() * 15) + 1;
		}
		if (galaxySize == 3) {
			maxSunsystemSpread = 550;
			sunSystemOffset = 180;
			numStars = 24 + (int) (Math.random() * 25) + 1;
		}
		if (galaxySize == 4) {
			maxSunsystemSpread = 700;
			sunSystemOffset = 100;
			numStars = 49 + (int) (Math.random() * 30) + 1;
		}
		if (galaxySize == 5) {
			maxSunsystemSpread = 860;
			sunSystemOffset = 20;
			numStars = 79 + (int) (Math.random() * 40) + 1;
		}
		universeDisplay.setPreferredSize(new Dimension(universeSize, universeSize));
	}

	public String toString() {
		String universe = new String("Universe:\n");
		for (int i = 0; i < suns.size(); i++) {
			universe = universe + ((Sun) suns.get(i)).toString() + "\n";
		}
		return (universe);
	}

	// public Universe(SpaceOpera so) {
	// spaceOpera = so;
	public Universe(Fullscreen fs) {
		// spaceOpera = so;
		suns = new Vector();
		// colonies = new Vector();
		players = new Vector();
		ships = new Vector();
		buildings = new Vector();
		events = new Vector();
		products = new Vector();
		components = new Vector();
		technologies = new Vector();
		universeDisplay = new UniverseDisplay(this, fs);
		Resources res = new Resources(this);
		initPlayers();
	}

	public void updatePlanetPositions() {
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.getSunSystem().updatePlanetPositions();
		}
	}

	public int getSunsystemOffset() {
		return sunSystemOffset;
	}


	public Fullscreen getFs() {
		return fs;
	}

	public void setFs(Fullscreen fs) {
		this.fs = fs;
	}

	
}