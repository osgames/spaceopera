//The class Resources defines resources like buildings and technologies in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.universe;

import java.awt.*;

import spaceopera.gui.components.SOEvent;
import spaceopera.universe.colony.Building;
import spaceopera.universe.ships.ShipComponent;

/** The Resources class defines all SpaceOpera resources
 *  (Buildings, technologies, shipComponents, products)
 * 
 */
public class Resources implements SOConstants {
  private Universe universe;
  
  public Resources(Universe u) {
	universe=u;
	defNames();	
	defBuildings();
	defComponents();
	defEvents();
	defProducts();
	defTechnologies();
  }
  // Buildings are things that belong to a colony, that add to the colonies output, that
  // cost the colony time, money and resources
  // 
  // Warning: building names must not be longer than 30 chars, because else the 
  // add/remove buttons do not work properly in @see ColonyDetail.initProductionQueue
  private void defBuildings() {
		int id=0,product=0,shipComp=0;		
		String name="",descr="",picture="",cls="";
		boolean unique=false,build=false;
		Building building= null;
		// Buildings
		  id=1;name=FACTORY;descr="This factory produces steel and goods and increases colony production by up to 100 units.";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_factory";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_WOOD,5);
		  building.setProductionCost(R_IRON,1);
		  building.setSupportCost(R_WORKUNITS_OPERATION,10);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,50);
		  building.setSupportCost(R_STONE,2);
		  building.setSupportCost(R_WOOD,0.5);
		  building.setSupportCost(R_OIL,1);
		  building.setSupportCost(R_IRON,2);
		  building.setSupportCost(R_COPPER,0.2);
		  building.setSupportCost(R_ALUMINUM,0.5);
		  building.setResourceProduction(R_WORKUNITS, 100);
		  building.setResourceProduction(R_STEEL, 1);
		  building.setResourceProduction(R_GLASS, 1);
		  building.setResourceProduction(R_GOODS, 10);
		  building.setResourceProduction(R_ELECTRONICS, 0.1);
		  building.setResourceProduction(R_WASTE, 1);
		  universe.addBuilding(building);
		  
		  id=2;name=STRIP_MINING_UNIT;descr="The 'Strip mining unit' produces up to 20 tons of ore per turn.";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(5);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,5);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_WOOD,5);
		  building.setSupportCost(R_IRON,2);
		  building.setResourceProduction(R_STONE, 10.0);
		  building.setResourceProduction(R_IRON, 17.0);
		  building.setResourceProduction(R_COPPER, 1.0);
		  building.setResourceProduction(R_ALUMINUM, 1.0);
		  building.setResourceProduction(R_GOLD, 0.1);
		  building.setResourceProduction(R_SILVER, 0.7);
		  building.setResourceProduction(R_URANIUM, 0.2);
		  building.setResourceProduction(R_WASTE, 1);
		  universe.addBuilding(building);		  
		  
		  id=3;name="Ore Mine II";descr="The 'Ore mine II' produces up to 40 tons of ore per turn.";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,5);
		  building.setProductionCost(R_IRON,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,100);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,15);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_WOOD,5);
		  building.setSupportCost(R_IRON,3);
		  building.setResourceProduction(R_STONE, 20.0);
		  building.setResourceProduction(R_IRON, 36.0);
		  building.setResourceProduction(R_COPPER, 1.0);
		  building.setResourceProduction(R_ALUMINUM, 1.1);
		  building.setResourceProduction(R_GOLD, 0.2);
		  building.setResourceProduction(R_SILVER, 0.9);
		  building.setResourceProduction(R_URANIUM, 0.3);
		  building.setResourceProduction(R_WASTE, 2);
		  universe.addBuilding(building);		  
		  
		  id=4;name=ORBITAL_SHIPYARD;descr="With an orbital shipyard, bigger ships than scouts can be built.";cls="Ships";
		  product=2;shipComp=0;unique=true;build=true;picture="b_starbase_simple_f";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);		  
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,30000);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_ALUMINUM,20);
		  building.setProductionCost(R_SILICIUM,2);
		  building.setProductionCost(R_GOLD,0.5);
		  building.setProductionCost(R_URANIUM,0.5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,300);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,30);
		  building.setSupportCost(R_ELECTRICITY,0);
		  building.setSupportCost(R_IRON,3);
		  building.setSupportCost(R_GOODS,20);
		  building.setSupportCost(R_URANIUM,0.01);
		  building.setResourceProduction(R_WASTE, 2);
		  building.addDefense(5.0); //XXX: differentiate defense types?
		  universe.addBuilding(building);
		  
		  id=5;name=BANK;descr="A bank increases colony trade.";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_bank";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_WOOD,1);
		  building.setProductionCost(R_GLASS,1);
		  building.setResourceProduction(R_GOODS, 10);
		  building.setResourceProduction(R_WASTE, 1);
		  building.addTrade(5.0); //XXX: what is trade?
		  universe.addBuilding(building);
		  
		  id=6;name=BARRACKS;descr="Barrack is the basic military unit for a colony.";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=true;picture="b_barracks";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(7);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_WOOD,50);
		  building.setProductionCost(R_GLASS,5);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_MILITARY,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_WOOD,1);
		  building.setResourceProduction(R_WASTE, 1);
		  building.addDefense(5.0);
		  universe.addBuilding(building);
		  
		  id=7;name=BATTLE_SCHOOL;descr="The battle school produces ground troops and improves space fight strategies.";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_battle_school";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);		  
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,30000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,20);
		  building.setProductionCost(R_GLASS,5);
		  building.setProductionCost(R_IRON,10);
		  building.setSupportCost(R_WORKUNITS_MILITARY,300);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,3);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setResourceProduction(R_WASTE, 1);
		  building.addDefense(10);
		  building.multiplyResearch(1.01);
		  universe.addBuilding(building);
		  
		  id=8;name=LIBRARY;descr="The library increases the research production of a colony by 5%.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=true;picture="b_library";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,30);
		  building.setSupportCost(R_WORKUNITS_OPERATION,25);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,5);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setSupportCost(R_STONE,1);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.multiplyResearch(1.01);
		  universe.addBuilding(building);
		  
		  id=9;name=SCHOOL;descr="The school increases the research production of a colony by 10%.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=true;picture="b_school";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,5);
		  building.setProductionCost(R_WOOD,20);
		  building.setProductionCost(R_GLASS,5);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,300);
		  building.setSupportCost(R_WORKUNITS_OPERATION,25);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,5);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,3);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_WOOD,10);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.multiplyResearch(1.01);
		  universe.addBuilding(building);
		  
		  id=10;name=UNIVERSITY;descr="The university increases the research production of a colony by 20%. The university technology must be researched first.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=false;picture="b_university";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,20000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_WOOD,50);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_IRON,20);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,600);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,15);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,6);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_STONE,5);
		  building.setSupportCost(R_WOOD,20);
		  building.setResourceProduction(R_WASTE, 1);
		  building.setResourceProduction(R_ELECTRONICS, 1);
		  building.multiplyResearch(1.1);
		  universe.addBuilding(building);
		  
		  id=11;name=CHURCH;descr="A church improves the happiness of colony population by 20%.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=false;picture="b_church";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_WOOD,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,10);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,10);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,1);
		  building.setSupportCost(R_STONE,10);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.multiplyHappiness(1.1);
		  universe.addBuilding(building);
		  
		  id=12;name=PLANETARY_SHIELDING;descr="A planetary shield protects colonists and buildings from attacks with beam weapons. Bombs and rockets are not deflected.";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet_shield1";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_SILICIUM,10);
		  building.setProductionCost(R_COPPER,20);
		  building.setSupportCost(R_WORKUNITS_MILITARY,10);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_COPPER,1);
		  building.setSupportCost(R_SILICIUM,1);
		  building.setResourceProduction(R_WASTE, 2);
		  building.addDefense(5); //XXX: shield defense?
		  universe.addBuilding(building);
		  	  	  
		  id=13;name="Ore Mine III";descr="The 'Ore mine III' produces up to 100 tons of ore per turn..";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(7);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,9000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,50);
		  building.setProductionCost(R_IRON,50);
		  building.setSupportCost(R_WORKUNITS_OPERATION,150);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,20);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_WOOD,20);
		  building.setSupportCost(R_IRON,10);
		  building.setResourceProduction(R_STONE, 30.0);
		  building.setResourceProduction(R_IRON, 87.5);
		  building.setResourceProduction(R_COPPER, 3.5);
		  building.setResourceProduction(R_ALUMINUM, 2.7);
		  building.setResourceProduction(R_GOLD, 1.2);
		  building.setResourceProduction(R_SILVER, 3.8);
		  building.setResourceProduction(R_URANIUM, 1.3);
		  building.setResourceProduction(R_WASTE, 3);
		  universe.addBuilding(building);
		  
		  id=14;name="Ore Mine IV";descr="The 'Ore mine IV' produces up to 200 tons of ore per turn.";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(7);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,50);
		  building.setProductionCost(R_IRON,50);
		  building.setSupportCost(R_WORKUNITS_OPERATION,250);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,25);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_WOOD,50);
		  building.setSupportCost(R_IRON,20);
		  building.setResourceProduction(R_STONE, 40.0);
		  building.setResourceProduction(R_IRON, 171);
		  building.setResourceProduction(R_COPPER, 8);
		  building.setResourceProduction(R_ALUMINUM, 7);
		  building.setResourceProduction(R_GOLD, 2.2);
		  building.setResourceProduction(R_SILVER, 7.8);
		  building.setResourceProduction(R_URANIUM, 4);
		  building.setResourceProduction(R_WASTE, 4);
		  universe.addBuilding(building);
		  
		  id=15;name="Colonist Bureau";descr="The colonist bureau reduces the impact of the feared 'colonist disease', which is an extreme kind of homesickness.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,10000);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_IRON,1);
		  building.setProductionCost(R_GLASS,1);
		  building.setSupportCost(R_WORKUNITS_OPERATION,5);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,25);
		  building.setSupportCost(R_ELECTRICITY,2);
		  building.setSupportCost(R_GOODS,2);
		  building.setResourceProduction(R_WASTE, 0.2);
		  building.addHappiness(0.1);
		  universe.addBuilding(building);
		  
		  id=16;name="Colonist Disease Treatment";descr="The treatment for colonists disease eliminates the feared 'colonist disease'.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_WOOD,20);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_OPERATION,25);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,25);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setSupportCost(R_GOODS,5);
		  building.setResourceProduction(R_WASTE, 1);
		  building.addHappiness(0.5);
		  universe.addBuilding(building);
		  
		  id=17;name="Psycho Institute";descr="The psycho-institute increases happiness of all colonists by a factor of 25%.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,30000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GOODS,10);
		  building.setProductionCost(R_IRON,40);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setSupportCost(R_GOODS,10);
		  building.setResourceProduction(R_WASTE, 2);
		  building.multiplyHappiness(1.25);
		  universe.addBuilding(building);
		  
		  id=18;name="Translation Office";descr="The translation office enables communication with aliens.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GOODS,50);
		  building.setProductionCost(R_COPPER,5);
		  building.setProductionCost(R_SILICIUM,10);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,50);
		  building.setSupportCost(R_ELECTRICITY,5);
		  building.setSupportCost(R_SILICIUM,1);
		  building.setSupportCost(R_COPPER,1);
		  building.setResourceProduction(R_WASTE, 2);
		  building.multiplyTrade(1.2);
		  universe.addBuilding(building);

		  id=19;name="Tools Factory";descr="The tools factory produces tools and increases productivity of a colony by 5%";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_tools_factory";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,10000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,200);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,100);
		  building.setSupportCost(R_IRON,20);
		  building.setSupportCost(R_COPPER,1);
		  building.setResourceProduction(R_GOODS, 2);
		  building.setResourceProduction(R_STEEL, 2);
		  building.setResourceProduction(R_TOOLS, 10);
		  building.setResourceProduction(R_ELECTRONICS, 1);
		  building.setResourceProduction(R_WASTE, 5);
		  universe.addBuilding(building);
		    
		  id=20;name="Machine Factory";descr="The machine factory produces machines and increases productivity of a colony by 10%";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_tools_factory";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_IRON,50);
		  building.setProductionCost(R_GOODS,50);
		  building.setSupportCost(R_WORKUNITS_OPERATION,300);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,150);
		  building.setSupportCost(R_WOOD,5);
		  building.setSupportCost(R_IRON,50);
		  building.setSupportCost(R_COPPER,2);
		  building.setSupportCost(R_ALUMINUM,2);
		  building.setSupportCost(R_STEEL,1);
		  building.setSupportCost(R_TOOLS,1);
		  building.setSupportCost(R_CHEMICALS,5);
		  building.setResourceProduction(R_GOODS, 20);
		  building.setResourceProduction(R_ALLOY, 5);
		  building.setResourceProduction(R_MACHINES, 10);
		  building.setResourceProduction(R_ELECTRONICS, 1);
		  building.setResourceProduction(R_WASTE, 10);
		  universe.addBuilding(building);
		  
		  id=21;name="Electronics Factory";descr="The electronics factory produces electronics and machines and increases productivity of a colony by 20%";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_tools_factory";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_STONE,100);
		  building.setProductionCost(R_IRON,100);
		  building.setProductionCost(R_GOODS,100);
		  building.setSupportCost(R_WORKUNITS_OPERATION,300);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,150);
		  building.setSupportCost(R_WOOD,5);
		  building.setSupportCost(R_IRON,100);
		  building.setSupportCost(R_COPPER,5);
		  building.setSupportCost(R_ALUMINUM,5);
		  building.setSupportCost(R_STEEL,5);
		  building.setSupportCost(R_TOOLS,10);
		  building.setSupportCost(R_CHEMICALS,5);
		  building.setResourceProduction(R_GOODS, 25);
		  building.setResourceProduction(R_ELECTRONICS, 50);
		  building.setResourceProduction(R_MACHINES, 10);
		  building.setResourceProduction(R_WASTE, 15);
		  universe.addBuilding(building);
		  
		  id=22;name="Robot Factory";descr="The robot factory produces electronics and machines and increases productivity of a colony by 50%";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_tools_factory";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,25000);
		  building.setProductionCost(R_STONE,200);
		  building.setProductionCost(R_IRON,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setSupportCost(R_WORKUNITS_OPERATION,500);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,200);
		  building.setSupportCost(R_WOOD,15);
		  building.setSupportCost(R_IRON,200);
		  building.setSupportCost(R_COPPER,25);
		  building.setSupportCost(R_ALUMINUM,20);
		  building.setSupportCost(R_STEEL,25);
		  building.setSupportCost(R_TOOLS,20);
		  building.setSupportCost(R_CHEMICALS,10);
		  building.setResourceProduction(R_GOODS, 120);
		  building.setResourceProduction(R_ELECTRONICS, 50);
		  building.setResourceProduction(R_MACHINES, 50);
		  building.setResourceProduction(R_WASTE, 30);
		  universe.addBuilding(building);
		  
		  id=23;name="Planetary Scanner I";descr="The planetary scanner I has a range of about 3 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=true;picture="b_planet-scanner1";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,2);
		  building.setProductionCost(R_IRON,2);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_MILITARY,20);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_IRON,1);
		  building.setSupportCost(R_STONE,1);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.setScanner(3.0);
		  universe.addBuilding(building);		  
		  
		  id=24;name="Cloning Factory";descr="A cloning factory increases population growth by about 1000 births per year.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_genetics_centre";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(11);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,25000);
		  building.setProductionCost(R_STONE,200);
		  building.setProductionCost(R_IRON,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,1000);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,200);
		  building.setSupportCost(R_STONE,10);
		  building.setSupportCost(R_WOOD,10);
		  building.setSupportCost(R_TOOLS,1);
		  building.setSupportCost(R_GOODS,5);
		  building.setSupportCost(R_CHEMICALS,5);
		  building.setSupportCost(R_OIL,5);
		  building.setResourceProduction(R_DRUGS, 5);
		  building.setResourceProduction(R_WASTE, 10);
		  building.addPopulation(1000);
		  universe.addBuilding(building);
		  
		  id=25;name="Genetics Centre";descr="A centre for genetics prolonges the live cycle by a factor of up to 20%";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(12);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,36000);
		  building.setProductionCost(R_STONE,200);
		  building.setProductionCost(R_IRON,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_EDUCATION,250);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,50);
		  building.setSupportCost(R_STONE,10);
		  building.setSupportCost(R_WOOD,10);
		  building.setSupportCost(R_ELECTRONICS,10);
		  building.setSupportCost(R_CHEMICALS,25);
		  building.setResourceProduction(R_WASTE, 10);
		  building.addPopulation(10000);
		  universe.addBuilding(building);
		  		  
		  //XXX: tune cost, sequence and build=true/false of terraform projects 
		  //XXX: the cheaper terraform(fertility) project can improve up to fertility 1.0 (?)
		  id=26;name=TERRAFORM_FERTILITY;descr="Terraform (Fertility) improves the planet basic fertility and also affects flora, fauna and the 'wood' resource.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setSupportCost(R_WORKUNITS_OPERATION,10);
		  building.setFertilityIncrease(0.1); // fertility is between 0 and 2
		  building.setUseOnlyOnce(true);
		  universe.addBuilding(building);
		  
		  id=27;name=TERRAFORM_TEMPERATURE;descr="Terraform (Temperature) improves the habitability of a planet by improving (either increasing or lowering) the temperature";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setTemperatureImprovement(1); //degrees C
		  universe.addBuilding(building);
		  
		  id=28;name=TERRAFORM_RADIATION;descr="Terraform (Radiation) improves the habitability of a planet by reducing the radioactivity";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setRadiationReduction(100); //millirem
		  universe.addBuilding(building);
		  
		  id=29;name=TERRAFORM_ATMOSPHERIC_PRESSURE;descr="Terraform (Atmospheric pressure) improves the habitability of a planet by increasing or reducing the air pressure towards a more ideal range.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setAtmosphericPressureCorrection(0.1);
		  universe.addBuilding(building);
		  	  
		  id=30;name="Planetary Scanner II";descr="The planetary scanner II has a range of about 5 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scanner2";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,4);
		  building.setProductionCost(R_IRON,4);
		  building.setProductionCost(R_COPPER,2);
		  building.setProductionCost(R_SILICIUM,2);
		  building.setProductionCost(R_GOODS,10);
		  building.setSupportCost(R_WORKUNITS_MILITARY,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_IRON,0.1);
		  building.setSupportCost(R_COPPER,0.1);
		  building.setSupportCost(R_SILICIUM,0.1);
		  building.setResourceProduction(R_WASTE, 1);
		  building.setScanner(5.0);
		  universe.addBuilding(building);
		  
		  id=31;name="Planetary Scanner III";descr="The planetary scanner III has a range of about 8 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scanner3";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,12500);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_IRON,5);
		  building.setProductionCost(R_COPPER,5);
		  building.setProductionCost(R_SILICIUM,5);
		  building.setProductionCost(R_GOODS,50);
		  building.setSupportCost(R_WORKUNITS_MILITARY,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_IRON,0.5);
		  building.setSupportCost(R_COPPER,0.5);
		  building.setSupportCost(R_SILICIUM,0.5);
		  building.setResourceProduction(R_WASTE, 1);
		  building.setScanner(8.0);
		  universe.addBuilding(building);
		  
		  id=32;name="Planetary Scanner IV";descr="The planetary scanner IV has a range of about 12 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scanner4";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(12);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,20000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_COPPER,5);
		  building.setProductionCost(R_SILICIUM,5);
		  building.setProductionCost(R_GOODS,100);
		  building.setSupportCost(R_WORKUNITS_MILITARY,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,25);
		  building.setSupportCost(R_IRON,1);
		  building.setSupportCost(R_COPPER,0.5);
		  building.setSupportCost(R_SILICIUM,0.5);
		  building.setResourceProduction(R_WASTE, 2);
		  building.setScanner(12.0);
		  universe.addBuilding(building);
		  
		  id=33;name="Planetary Scanner V";descr="The planetary scanner V has a range of about 16 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scanner5";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(14);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,25000);
		  building.setProductionCost(R_STONE,100);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_COPPER,10);
		  building.setProductionCost(R_SILICIUM,10);
		  building.setProductionCost(R_GOODS,200);
		  building.setSupportCost(R_WORKUNITS_MILITARY,80);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,30);
		  building.setSupportCost(R_IRON,1);
		  building.setSupportCost(R_COPPER,0.5);
		  building.setSupportCost(R_SILICIUM,0.5);
		  building.setResourceProduction(R_WASTE, 2);
		  building.setScanner(16.0);
		  universe.addBuilding(building);
		  
		  id=34;name="Planetary Scanner VI";descr="The planetary scanner VI has a range of about 25 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scanner6";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(16);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,35000);
		  building.setProductionCost(R_STONE,200);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_COPPER,20);
		  building.setProductionCost(R_SILICIUM,20);
		  building.setProductionCost(R_GOODS,500);
		  building.setSupportCost(R_WORKUNITS_MILITARY,100);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,50);
		  building.setSupportCost(R_IRON,1);
		  building.setSupportCost(R_COPPER,1);
		  building.setSupportCost(R_SILICIUM,1);
		  building.setResourceProduction(R_WASTE, 3);
		  building.setScanner(25.0);
		  universe.addBuilding(building);
		  
		  id=35;name="Planetary Ultra Scanner";descr="The planetary ultra scanner has a range of about 50 LJ";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet-scannerUS";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(18);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,50000);
		  building.setProductionCost(R_STONE,250);
		  building.setProductionCost(R_IRON,25);
		  building.setProductionCost(R_COPPER,25);
		  building.setProductionCost(R_SILICIUM,25);
		  building.setProductionCost(R_GOODS,1000);
		  building.setSupportCost(R_WORKUNITS_MILITARY,200);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,100);
		  building.setSupportCost(R_IRON,1);
		  building.setSupportCost(R_COPPER,1);
		  building.setSupportCost(R_SILICIUM,1);
		  building.setResourceProduction(R_WASTE, 3);
		  building.setScanner(50.0);
		  universe.addBuilding(building);
		  
		  id=36;name="Trade Union House";descr="The trade union allows for more efficient trading and better tax income.";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_genetics_centre";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,200);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_GOODS,50);
		  building.setProductionCost(R_GLASS,10);
		  building.setSupportCost(R_WORKUNITS_OPERATION,100);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_GOODS,10);
		  building.setSupportCost(R_WINE,10);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.multiplyTrade(1.05);
		  universe.addBuilding(building);
		  
		  id=37;name="Traders Association Guildhouse";descr="The Traders Association increases trade revenue immensely.";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_STONE,250);
		  building.setProductionCost(R_IRON,50);
		  building.setProductionCost(R_GOODS,150);
		  building.setProductionCost(R_GLASS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,200);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_GOODS,10);
		  building.setSupportCost(R_WINE,10);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.multiplyTrade(1.2);
		  universe.addBuilding(building);
		  
		  id=38;name=DEFENSE_BATTERY;descr="The defense battery I fires up to two rockets per turn, with a range of 3 lightseconds.";cls=MILITARY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_planet_missile_defense_1";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_MILITARY,20);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_IRON,10);
		  building.setSupportCost(R_GOODS,10);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setResourceProduction(R_WASTE, 1);
		  building.addDefense(5);
		  universe.addBuilding(building);

		  id=66;name=DEFENSE_BATTERY_2;descr="The defense battery II fires up to four rockets per turn, with a range of 4 lightseconds .";cls=MILITARY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_planet_missile_defense_2";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,4000);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_IRON,12);
		  building.setProductionCost(R_GOODS,6);
		  building.setSupportCost(R_WORKUNITS_MILITARY,25);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,15);
		  building.setSupportCost(R_IRON,15);
		  building.setSupportCost(R_GOODS,15);
		  building.setSupportCost(R_CHEMICALS,2);
		  building.setResourceProduction(R_WASTE, 2);
		  building.addDefense(10);
		  universe.addBuilding(building);
		  
		  id=67;name=DEFENSE_BATTERY_3;descr="The defense battery III fires up to eight rockets per turn, with a range of 5 lightseconds.";cls=MILITARY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_planet_missile_defense_3";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,5000);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_IRON,15);
		  building.setProductionCost(R_GOODS,10);
		  building.setSupportCost(R_WORKUNITS_MILITARY,30);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,2);
		  building.setSupportCost(R_ELECTRICITY,20);
		  building.setSupportCost(R_IRON,20);
		  building.setSupportCost(R_GOODS,20);
		  building.setSupportCost(R_CHEMICALS,3);
		  building.setResourceProduction(R_WASTE, 3);
		  building.addDefense(15);
		  universe.addBuilding(building);
		  
		  
		  id=39;name="Bomb Shelter";descr="The bomb shelter protects part of the population from orbital bombing. One shelter per 10000 colonists is needed. It does not protect buildings.";cls=MILITARY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_planet_bombshelter";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(4);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_MILITARY,10);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,1);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_FOOD,1);
		  building.setSupportCost(R_GOODS,1);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setResourceProduction(R_WASTE, 0.1);
		  building.addDefense(100);
		  universe.addBuilding(building);
		  
		  id=40;name=POWER_PLANT;descr="The coal power plant produces 1000 kW and uses coal.";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_COPPER,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,20);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_COAL,5);
		  building.setSupportCost(R_COPPER,0.1);
		  building.setResourceProduction(R_ELECTRICITY, 1000);
		  building.setResourceProduction(R_WASTE, 5);
		  universe.addBuilding(building);		  
		  
		  id=41;name="Habitat";descr="One habitat unit provides shelter for up to 100000 colonists.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_habitat";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,5);
		  building.setProductionCost(R_WOOD,5);
		  building.setProductionCost(R_IRON,2);
		  building.setProductionCost(R_GOODS,1);
		  building.setProductionCost(R_GLASS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,1000);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,100);
		  building.setSupportCost(R_ELECTRICITY,2000);
		  building.setSupportCost(R_FOOD,500);
		  building.setSupportCost(R_FRESHWATER,200);
		  building.setSupportCost(R_WINE,50);
		  building.setSupportCost(R_SALT,1);
		  building.setSupportCost(R_GARMENTS,2);		  
		  building.setSupportCost(R_GOODS,100);
		  building.setSupportCost(R_GLASS,10);
		  building.setResourceProduction(R_WASTE, 200);
		  building.addHousing(100000);
		  universe.addBuilding(building);
		  
		  id=42;name="Habitat II";descr="One habitat unit provides shelter for up to 100000 colonists on planets without breathable atmosphere.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,9000);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_STEEL,10);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,1200);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,120);
		  building.setSupportCost(R_ELECTRICITY,2000);
		  building.setSupportCost(R_FOOD,500);
		  building.setSupportCost(R_FRESHWATER,200);
		  building.setSupportCost(R_WINE,50);
		  building.setSupportCost(R_SALT,1);
		  building.setSupportCost(R_GARMENTS,3);
		  building.setSupportCost(R_GOODS,100);
		  building.setSupportCost(R_GLASS,15);
		  building.setResourceProduction(R_WASTE, 200);
		  building.addHousing(100000);
		  universe.addBuilding(building);
		  
		  id=43;name="Habitat III";descr="One habitat unit provides shelter for up to 100000 colonists on (radiated) earth- and marslike planets.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,12000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_STEEL,20);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,2000);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,200);
		  building.setSupportCost(R_ELECTRICITY,2000);
		  building.setSupportCost(R_FOOD,500);
		  building.setSupportCost(R_FRESHWATER,200);
		  building.setSupportCost(R_WINE,50);
		  building.setSupportCost(R_SALT,1);
		  building.setSupportCost(R_GARMENTS,4);
		  building.setSupportCost(R_GOODS,100);
		  building.setSupportCost(R_GLASS,15);
		  building.setResourceProduction(R_WASTE, 200);
		  building.addHousing(100000);
		  universe.addBuilding(building);
		  
		  id=44;name="Habitat IV";descr="One habitat unit provides shelter for up to 100000 colonists on hostile venus-, earth- and marslike planets (radiated, poisonous).";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(12);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,12000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_STEEL,20);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,2000);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,200);
		  building.setSupportCost(R_ELECTRICITY,2000);
		  building.setSupportCost(R_FOOD,500);
		  building.setSupportCost(R_FRESHWATER,200);
		  building.setSupportCost(R_WINE,50);
		  building.setSupportCost(R_SALT,1);
		  building.setSupportCost(R_GARMENTS,4);
		  building.setSupportCost(R_GOODS,100);
		  building.setSupportCost(R_GLASS,15);
		  building.setResourceProduction(R_WASTE, 200);
		  building.addHousing(100000);
		  universe.addBuilding(building);
		  
		  id=45;name="Outpost Habitat";descr="An outpost habitat unit provides shelter for up to 10000 colonists.";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=false;picture="b_starbase_simple_f";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_ALUMINUM,10);
		  building.setProductionCost(R_GLASS,5);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,500);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,25);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,5);
		  building.setSupportCost(R_ELECTRICITY,500);
		  building.setSupportCost(R_FOOD,50);
		  building.setSupportCost(R_FRESHWATER,20);
		  building.setSupportCost(R_WINE,5);
		  building.setSupportCost(R_SALT,0.2);
		  building.setSupportCost(R_GARMENTS,2);
		  building.setSupportCost(R_GOODS,20);
		  building.setSupportCost(R_GLASS,3);
		  building.setResourceProduction(R_WASTE, 10);
		  building.addHousing(10000);
		  universe.addBuilding(building);
		  	  
		  id=46;name=SOLAR_POWER_PLANT;descr="The solar powerplant produces 100 kW.";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GLASS,20);
		  building.setProductionCost(R_SILICIUM,5);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,20);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_SILICIUM,1);
		  building.setResourceProduction(R_ELECTRICITY, 100);
		  universe.addBuilding(building);
		  	  
		  id=47;name="Hydroponics Farm";descr="The hydroponics farm produces food.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_b_hydrofarm";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_GLASS,20);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,200);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_GOODS,50);
		  building.setSupportCost(R_WASTE,10);
		  building.setSupportCost(R_CHEMICALS,20);
		  building.setResourceProduction(R_WOOD, 10);
		  building.setResourceProduction(R_RICE, 100);
		  building.setResourceProduction(R_CROP, 100);
		  building.setResourceProduction(R_VEGETABLES, 200);
		  building.setResourceProduction(R_WINE, 1);
		  universe.addBuilding(building);
		  
		  id=48;name="Water Recycler";descr="The water recycling unit cleans up used water.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,2);
		  building.setSupportCost(R_GOODS,1);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setResourceProduction(R_FRESHWATER, 100);
		  building.setResourceProduction(R_WASTE, 1);
		  universe.addBuilding(building);		  
		  
		  id=49;name="Chemical Factory";descr="The chemical factory produces chemical resources productivity of a colony by 10%";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,10000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_IRON,50);
		  building.setProductionCost(R_GOODS,50);
		  building.setSupportCost(R_WORKUNITS_OPERATION,250);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,150);
		  building.setSupportCost(R_OIL,15);
		  building.setSupportCost(R_COAL,2);
		  building.setSupportCost(R_MACHINES,4);
		  building.setSupportCost(R_STEEL,4);
		  building.setSupportCost(R_TOOLS,1);		  
		  building.setResourceProduction(R_PLASTIC, 1);
		  building.setResourceProduction(R_CHEMICALS, 10);
		  building.setResourceProduction(R_MACHINES, 2);
		  building.setResourceProduction(R_WASTE, 10);
		  universe.addBuilding(building);		  
		  
		  id=50;name=FARM;descr="The Farm unit produces food.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_farm";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(5);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_GOODS,10);
		  building.setSupportCost(R_WORKUNITS_OPERATION,500);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_GOODS,3);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setSupportCost(R_FRESHWATER,100);
		  building.setResourceProduction(R_WOOD, 100);
		  building.setResourceProduction(R_MEAT, 200);
		  building.setResourceProduction(R_FISH, 100);
		  building.setResourceProduction(R_CROP, 100);
		  building.setResourceProduction(R_VEGETABLES, 200);
		  building.setResourceProduction(R_DAIRY, 100);
		  building.setResourceProduction(R_WINE, 100);
		  universe.addBuilding(building);
		  
		  id=51;name=COAL_MINE;descr="The 'Coal mine' produces up to 20 tons of coal and some chemicals per turn.";cls=PHYSICS;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(5);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,5);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,10);
		  building.setSupportCost(R_ELECTRICITY,10);
		  building.setSupportCost(R_GOODS,1);
		  building.setSupportCost(R_IRON,2);
		  building.setResourceProduction(R_STONE, 5.0);
		  building.setResourceProduction(R_COAL, 15);
		  building.setResourceProduction(R_OIL, 3);
		  building.setResourceProduction(R_CHEMICALS, 2);
		  building.setResourceProduction(R_WASTE, 1);
		  universe.addBuilding(building);
		  
		  id=52;name=COLONY_MANAGER;descr="The colony manager plans the basic colony requirements like buildings, food and power supply.";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=true;picture="b_colony_manager";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_GLASS,20);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,25);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,250);
		  building.setSupportCost(R_GOODS,5);
		  building.setSupportCost(R_WINE,1);
		  building.setResourceProduction(R_WASTE, 5);
		  universe.addBuilding(building);		   
		  
		  id=53;name=SAW_MILL;descr="The saw mill produces wood, tools and some goods.";cls=ECONOMY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_WOOD,5);
		  building.setProductionCost(R_IRON,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,50);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_IRON,2);
		  building.setResourceProduction(R_WOOD, 12);
		  building.setResourceProduction(R_TOOLS, 2);
		  building.setResourceProduction(R_GOODS, 2);
		  universe.addBuilding(building);
		  
		  id=54;name=WATER_PURIFICATOR;descr="The water purificator provides drinking water.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(6);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_STONE,10);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,2);
		  building.setSupportCost(R_GOODS,1);
		  building.setSupportCost(R_CHEMICALS,1);
		  building.setResourceProduction(R_FRESHWATER, 100);
		  building.setResourceProduction(R_WASTE, 1);
		  universe.addBuilding(building);
		  
		  id=55;name=TEMPLE;descr="The temple increases population happiness.";cls=SOCIAL;
		  product=0;shipComp=0;unique=true;build=true;picture="b_temple";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(5);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_IRON,1);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GOODS,2);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,2);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_GOODS,1);
		  building.multiplyHappiness(1.05);
		  universe.addBuilding(building);

		  id=56;name="Habitat V";descr="One habitat unit provides shelter for up to 100000 colonists on hostile mercury-, venus-, earth- and marslike planets (radiated, poisonous).";cls=BIOLOGY;
		  product=0;shipComp=0;unique=false;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(12);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,12000);
		  building.setProductionCost(R_STONE,50);
		  building.setProductionCost(R_GLASS,10);
		  building.setProductionCost(R_STEEL,20);
		  building.setProductionCost(R_GOODS,5);
		  building.setSupportCost(R_WORKUNITS_OPERATION,2000);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,50);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,200);
		  building.setSupportCost(R_ELECTRICITY,2000);
		  building.setSupportCost(R_FOOD,500);
		  building.setSupportCost(R_FRESHWATER,200);
		  building.setSupportCost(R_WINE,50);
		  building.setSupportCost(R_SALT,1);
		  building.setSupportCost(R_GARMENTS,5);
		  building.setSupportCost(R_GOODS,100);
		  building.setSupportCost(R_GLASS,15);
		  building.setResourceProduction(R_WASTE, 200);
		  building.addHousing(100000);
		  universe.addBuilding(building);		  
		  
		  id=57;name=ADVANCED_COLONY_MANAGER;descr="The advanced colony manager improves efficiency and automatically plans colony requirements like supply and defense.";cls=ECONOMY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		  building.setProductionCost(R_IRON,20);
		  building.setProductionCost(R_GLASS,20);
		  building.setProductionCost(R_GOODS,20);
		  building.setSupportCost(R_WORKUNITS_OPERATION,25);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,250);
		  building.setSupportCost(R_GOODS,5);
		  building.setSupportCost(R_WINE,1);
		  building.setResourceProduction(R_WASTE, 5);
		  universe.addBuilding(building);
		  
		  id=58;name=WEAPONS_FACTORY;descr="The weapons factory produces handguns and rifles, ammunition and torpedos.";cls=MILITARY;
		  product=0;shipComp=0;unique=false;build=true;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(9);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,30000);
		  building.setProductionCost(R_IRON,10);
		  building.setProductionCost(R_WOOD,10);
		  building.setProductionCost(R_STONE,20);
		  building.setProductionCost(R_GOODS,2);
		  building.setSupportCost(R_WORKUNITS_ADMINISTRATION,1);  
		  building.setSupportCost(R_WORKUNITS_OPERATION,10);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,1);
		  building.setSupportCost(R_ELECTRICITY,50);
		  building.setSupportCost(R_STONE,1);
		  building.setSupportCost(R_WOOD,1);
		  building.setSupportCost(R_STEEL,1);
		  building.setSupportCost(R_ELECTRONICS,0.2); 		  
		  building.setSupportCost(R_CHEMICALS,2);
		  building.setSupportCost(R_COPPER,0.2);
		  building.setSupportCost(R_ALUMINUM,0.5);
		  building.setResourceProduction(R_WASTE, 1);		  
		  universe.addBuilding(building);

		  id=59;name=TERRAFORM_WEATHER_CONTROL;descr="Terraform (Weather control) improves the habitability of a planet by introducing weather control.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setWeatherCorrection(0.1);
		  universe.addBuilding(building);		  

		  id=60;name=TERRAFORM_ATMOSPHERIC_COMPOSITION;descr="Terraform (Atmospheric composition) improves the habitability of a planet by improving the atmospheric composition towards a more ideal range.";cls=PHYSICS;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setAtmosphericCompositionImprovement(0.1);
		  universe.addBuilding(building);	
		  
		  id=61;name=TERRAFORM_GRAVITATION;descr="Terraform (Gravitation) improves the habitability of a planet by installing gravitation controls for the planetary surface.";cls=PHYSICS;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setGravitationImprovement(0.1);
		  universe.addBuilding(building);	

		  id=62;name=TERRAFORM_WATER_LAND_RATIO;descr="Terraform (Water/Land Ratio) improves the habitability of a planet by adding or removing lots of water, rocks and soil or by melting or freezing lots of ice/water. This can be achieved by (or conflicting with) terraform (temperature).";cls=PHYSICS;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setWaterLandRatioImprovement(0.1);
		  universe.addBuilding(building);			  

		  id=63;name=TERRAFORM_FLORA;descr="Terraform (Flora) introduces plants of earth origin in the planets biosphere.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setFloraImprovement(0.1);
		  universe.addBuilding(building);			  

		  id=64;name=TERRAFORM_FAUNA;descr="Terraform (Fauna) introduces animals of earth origin into the planets biosphere.";cls=BIOLOGY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_building_todo";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(10);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		  building.setProductionCost(R_ELECTRICITY,200);
		  building.setProductionCost(R_GOODS,200);
		  building.setProductionCost(R_TOOLS,10);
		  building.setProductionCost(R_CHEMICALS,50);
		  building.setFaunaImprovement(0.1);
		  universe.addBuilding(building);	
		  
		  id=65;name=ADVANCED_PLANETARY_SHIELDING;descr="A planetary shield protects colonists and buildings from attacks with beam weapons. Bombs and rockets are not deflected.";cls=MILITARY;
		  product=0;shipComp=0;unique=true;build=false;picture="b_planet_shield2";
		  building= new Building(id,name,cls,descr,product,shipComp,unique,build,picture);
		  building.setEducationLevel(8);
		  building.setProductionCost(R_WORKUNITS_CONSTRUCTION,6000);
		  building.setProductionCost(R_STONE,40);
		  building.setProductionCost(R_IRON,40);
		  building.setProductionCost(R_SILICIUM,20);
		  building.setProductionCost(R_COPPER,40);
		  building.setSupportCost(R_WORKUNITS_MILITARY,20);
		  building.setSupportCost(R_WORKUNITS_CONSTRUCTION,2);
		  building.setSupportCost(R_ELECTRICITY,40);
		  building.setSupportCost(R_COPPER,2);
		  building.setSupportCost(R_SILICIUM,2);
		  building.setResourceProduction(R_WASTE, 5);
		  building.addDefense(10); //XXX: shield defense?
		  universe.addBuilding(building);		  
		  
		  //next building: id=68;

		  
		  
		  
		  
		  
// XXX: More buildings, changes to buildings logic:
//**************************************************
//
// Social:
// Departement of Information (spy information)
// Holiday centre             (money, happiness)
// Dream castle               (money and happiness)
// 36 hour week               (warning does not match ColonyDetail settings)
// 4 week holidays            (happiness, increased productivity)
// Temple                     (happiness)
// Church                     (happiness, stability)
// Cathedral                  (happiness, stability, tourism)
// Monastery                  (happiness, education)
// Priest School              (education)
// Meditation Centre          (happiness, productivity)
// Disney World Building, Park, Country, Planet
// West ..... Mall
// Embassy, Advanced Embassy
// Foreign Relations Office
// Departement of Peace
// Departement of Covert Action
// Student Exchange program
//
// Physics:
// Gas Power Plant            (power)
// Oil Power Plant            (power)
// Uranium Power Plant        (power)
// Tidal force Power Plant    (power)
// hydro-electric Power Plant (power)
// Fusion Power Plant         (power)
// oil platform               (oil)
// oil refinery               (oil, goods, chemicals)
// glass factory              (glass, goods)
//
// Economy:
// Storage                    (allows storage of resources up to 1000(?) units)
// Tax Collection House       (money)
// Orbital Resource Market    (money, resource trading)
// Stock Exchange             (money)
// Waste disposal (landfill)  (waste removal)
// Waste reprocessing unit    (waste removal)
// Orion Trade Market         (money, resource trading, tourism)
// Free Traders Association   (resource trading, spying)
//
// Biology:
// Terraforming Buildings (for each tech, different intensity)
// Habitat for radiated planets
// Habitat for toxic planets
// Habitat for planets without atmosphere
  }

  // Components
  private void defComponents() {
		String name="",descr="",type="",picture="";
		boolean unique=false,build=false;
		int id=0, weight=0;
		ShipComponent shipComponent= null;
  // XXX: for each ship or shipcomponent, use up the support cost in each turn!
  // XXX: Warning: the human fleet slowly decays due to lack of support material!
  // *****************************************************************************
		
		/**
		 * Hulls 
		 * Hullsize: (used to multiply parts size/weight/resources for bigger ships,
		 *            e.g. sublightdrives, armor, fuel tanks or -scoops, 
		 *   1=scout, 
		 *   2=gunship, 
		 *   4=escort, 
		 *   10=destroyer, 
		 *   25=frigate, 
		 *   50=cruiser, carrier, colonyship, 
		 *   100=battleship 
		 *   200=dreadnought
		 *   500=superdreadnought
		 *   250=large carrier
		 *   500=mauler
		 *   1000=battle star
		 *   2000=death star
		 */
		id=9;name=SCOUT_HULL;descr ="This small ship can land and start from every planet. It has a max. payload of 50 tons.";
		type="Hull";unique=false;build=true;weight=10;picture="s_scoutship";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2500);
		shipComponent.setProductionCost(R_IRON,7);
		shipComponent.setProductionCost(R_ALUMINUM,2);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1);  //crew 1
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);  //ground support 
		shipComponent.setSupportCost(R_WORKUNITS_EDUCATION,0.1);  //training, tactical school, ...
		shipComponent.setPayload(60);
		shipComponent.setHullSize(1);
		shipComponent.addArmor(1);		
		universe.addComponent(shipComponent);
		
		id=22;name=ESCORT_HULL;descr ="Ships with this hull cannot land on planets. Payload is up to 500 tons.";
		type="Hull";unique=false;build=true;weight=100;picture="s_escort";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,5000);
		shipComponent.setProductionCost(R_IRON,60);
		shipComponent.setProductionCost(R_STEEL,10);
		shipComponent.setProductionCost(R_ALUMINUM,10);
		shipComponent.setProductionCost(R_GLASS,10);
		shipComponent.setProductionCost(R_GOODS,10);
		shipComponent.setSupportCost(R_WORKUNITS_MILITARY,10);  //crew
		shipComponent.setSupportCost(R_WORKUNITS_CONSTRUCTION,0.3);  //ground support personnel
		shipComponent.setSupportCost(R_WORKUNITS_EDUCATION,0.1);  //training, tactical school, ...
		shipComponent.setPayload(500);
		shipComponent.setHullSize(4);
		shipComponent.addArmor(10);
		universe.addComponent(shipComponent);	
		
		id=21;name=SMALL_TRANSPORT_HULL;descr ="Ships with this hull cannot land on planets. Payload is up to 500 tons.";
		type="Hull";unique=false;build=false;weight=25;picture="s_transport";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,20);
		shipComponent.setProductionCost(R_ALUMINUM,3);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,3); //crew
		shipComponent.setSupportCost(R_WORKUNITS_CONSTRUCTION,0.3);  //ground support personnel	
		shipComponent.setSupportCost(R_WORKUNITS_EDUCATION,0.1);  //training, tactical school, ...
		shipComponent.setPayload(500);
		shipComponent.setHullSize(4);
		shipComponent.addArmor(1);
		universe.addComponent(shipComponent);		
		
		id=55;name=FRIGATE_HULL;descr ="Ships with this hull cannot land on planets. Payload is up to 5000 tons.";
		type="Hull";unique=false;build=false;weight=1000;picture="s_frigate";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		shipComponent.setProductionCost(R_IRON,600);
		shipComponent.setProductionCost(R_STEEL,200);
		shipComponent.setProductionCost(R_ALUMINUM,100);
		shipComponent.setProductionCost(R_GLASS,50);
		shipComponent.setProductionCost(R_GOODS,50);
		shipComponent.setSupportCost(R_WORKUNITS_MILITARY,10);	//crew
		shipComponent.setSupportCost(R_WORKUNITS_CONSTRUCTION,0.5);  //ground support personnel
		shipComponent.setSupportCost(R_WORKUNITS_EDUCATION,1);  //training, tactical school, ...
		shipComponent.setPayload(5000);
		shipComponent.setHullSize(25);
		shipComponent.addArmor(10);
		universe.addComponent(shipComponent);			
		
		id=10;name=COLONY_HULL;descr ="This ship can land on planets (once). It has a max. payload of 20000 tons. It is usually used for colonizing planets.";
		type="Hull";unique=false;build=true;weight=1000;picture="s_colonyship";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,10000);
		shipComponent.setProductionCost(R_IRON,800);
		shipComponent.setProductionCost(R_STEEL,100);
		shipComponent.setProductionCost(R_ALUMINUM,80);
		shipComponent.setProductionCost(R_GLASS,10);
		shipComponent.setProductionCost(R_GOODS,10);
		shipComponent.setSupportCost(R_WORKUNITS_MILITARY,10); //crew
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,10); //crew
		shipComponent.setPayload(20000);
		shipComponent.setHullSize(50);
		shipComponent.addArmor(1);
		universe.addComponent(shipComponent);	
			
		//XXX: when adding new hulls, also add templates and shipdesign logic to AiShipBuilder
		
		/**
		 * Drives
		 */
		id=11;name=SUBLIGHT_DRIVE;descr ="The sublight drive is used for in-system travel only. No interspace-capability. Mostly used for intra-system transports, fighters and such.";
		type=DRIVES;unique=false;build=true;weight=8;picture="d_sublight_drive";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,6);
		shipComponent.setProductionCost(R_COPPER,2);
		shipComponent.setProductionCost(R_URANIUM,0.5);
		shipComponent.setProductionCost(R_GOODS,1.5);
		shipComponent.setSupportCost(R_IRON,0.1); //repairs, spare parts
		shipComponent.setSupportCost(R_URANIUM,0.01);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.1);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
        shipComponent.setSublightSpeed(1); //XXX: what unit is this? (speed, acceleration, acceleration per tons, ...???)
                                           // tenths of a lightsecond per battleturn (see torpedo)
        shipComponent.setSubType(SUBLIGHT_DRIVE_TYPE);
		universe.addComponent(shipComponent);
		
		id=1;name="Warp 1 drive";descr ="Warp 1 drive moves a ship with warp 1 speed.";
		type=DRIVES;unique=false;build=true;weight=15;picture="d_warp_drive1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,10);
		shipComponent.setProductionCost(R_COPPER,1);
		shipComponent.setProductionCost(R_ALUMINUM,5);
		shipComponent.setProductionCost(R_URANIUM,0.2);
		shipComponent.setProductionCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);		
		shipComponent.setSupportCost(R_IRON,0.1);
		shipComponent.setSupportCost(R_ALUMINUM,0.01);
		shipComponent.setSupportCost(R_URANIUM,0.01);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setWarpSpeed(1);
		shipComponent.setSubType(FTL_DRIVE_TYPE);
		universe.addComponent(shipComponent);
		
		id=2;name="Warp 2 drive";descr ="Warp 2 drive moves a ship with warp 2 speed.";
		type=DRIVES;unique=false;build=false;weight=20;picture="d_warp_drive2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1200);
		shipComponent.setProductionCost(R_IRON,15);
		shipComponent.setProductionCost(R_COPPER,2);
		shipComponent.setProductionCost(R_ALUMINUM,6);
		shipComponent.setProductionCost(R_URANIUM,0.3);
		shipComponent.setProductionCost(R_GOODS,5);
		shipComponent.setSupportCost(R_IRON,0.1);
		shipComponent.setSupportCost(R_ALUMINUM,0.01);
		shipComponent.setSupportCost(R_URANIUM,0.01);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3); 
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setWarpSpeed(2);
		shipComponent.setSubType(FTL_DRIVE_TYPE);
		universe.addComponent(shipComponent);
		
		id=3;name="Warp 3 drive";descr ="Warp 3 drive moves a ship with warp 3 speed.";
		type=DRIVES;unique=false;build=false;weight=25;picture="d_warp_drive3";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,23);
		shipComponent.setProductionCost(R_COPPER,3);
		shipComponent.setProductionCost(R_ALUMINUM,7);
		shipComponent.setProductionCost(R_URANIUM,0.4);
		shipComponent.setProductionCost(R_GOODS,5);
		shipComponent.setSupportCost(R_IRON,0.1);
		shipComponent.setSupportCost(R_ALUMINUM,0.01);
		shipComponent.setSupportCost(R_URANIUM,0.01);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.4);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setWarpSpeed(3);		
		shipComponent.setSubType(FTL_DRIVE_TYPE);
		universe.addComponent(shipComponent);
		
		id=4;name="Warp 4 drive";descr ="Warp 4 drive moves a ship with warp 4 speed.";
		type=DRIVES;unique=false;build=false;weight=30;picture="d_warp_drive4";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(4);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,23);
		shipComponent.setProductionCost(R_COPPER,3);
		shipComponent.setProductionCost(R_ALUMINUM,7);
		shipComponent.setProductionCost(R_URANIUM,0.5);
		shipComponent.setProductionCost(R_GOODS,5);
		shipComponent.setSupportCost(R_IRON,0.1);
		shipComponent.setSupportCost(R_ALUMINUM,0.01);
		shipComponent.setSupportCost(R_URANIUM,0.01);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setWarpSpeed(4);
		shipComponent.setSubType(FTL_DRIVE_TYPE);
		universe.addComponent(shipComponent);
		
		
		/**
		 * Weapons
		 */
		//Beam weapons
		id=6;name=LASER;descr ="The laser is the simplest weapon. It's not very strong and has a range of only a few light-seconds.";
		type=WEAPONS;unique=false;build=true;weight=6;picture="w_laser1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,6);
		shipComponent.setProductionCost(R_COPPER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setRange(1);
		shipComponent.setForce(1);
		shipComponent.setShipAttack(true);
		shipComponent.setSubType(BEAM_WEAPON_TYPE);
		universe.addComponent(shipComponent);	
		
		id=7;name=LASER_CANNON;descr ="The laser cannon is a bit stronger than the basic laser and also has a better range.";
		type=WEAPONS;unique=false;build=false;weight=12;picture="w_laser-c";
		shipComponent.setTechLevel(1);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,14);
		shipComponent.setProductionCost(R_COPPER,3);
		shipComponent.setProductionCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setRange(2);
		shipComponent.setForce(2);
		shipComponent.setShipAttack(true);
		shipComponent.setSubType(BEAM_WEAPON_TYPE);
		universe.addComponent(shipComponent);
		
		id=46;name=BUNDLED_LASER;descr ="The bundled laser combines the power of three lasers onto one target.";
		type=WEAPONS;unique=false;build=false;weight=15;picture="w_bundledlaser";
		shipComponent.setTechLevel(1);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,14);
		shipComponent.setProductionCost(R_COPPER,3);
		shipComponent.setProductionCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setRange(1);
		shipComponent.setForce(3);
		shipComponent.setShipAttack(true);
		shipComponent.setSubType(BEAM_WEAPON_TYPE);
		universe.addComponent(shipComponent);
		
		//missiles
		id=8;name=TORPEDO;descr ="The torpedo has a range of up to 200 light-seconds. One set contains 4 torpedoes.";
		type=WEAPONS;unique=false;build=true;weight=50;picture="w_torpedo1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,14);
		shipComponent.setProductionCost(R_GOODS,36);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setSublightSpeed(1); //lightseconds per battleturn
		shipComponent.setForce(10);
		shipComponent.setLifeCycles(3);
		shipComponent.setBaseAmmo(4);
		shipComponent.setShipAttack(true);
		shipComponent.setPlanetAttack(true);
		shipComponent.setSubType(MISSILE_WEAPON_TYPE);
		universe.addComponent(shipComponent);

		//XXX: shipcomponent for torpedo 2 and rocket w_rak1-9 images
		
		id=50;name=LONG_RANGE_TORPEDO;descr ="The long range torpedo has a range of about 500 light-seconds. One set contains 4 torpedoes.";
		type=WEAPONS;unique=false;build=false;weight=80;picture="w_torpedo_lr"; 
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,24);
		shipComponent.setProductionCost(R_GOODS,56);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setSublightSpeed(1); //lightseconds per battleturn
		shipComponent.setForce(10);
		shipComponent.setLifeCycles(6);
		shipComponent.setBaseAmmo(4);
		shipComponent.setShipAttack(true);
		shipComponent.setPlanetAttack(true);
		shipComponent.setSubType(MISSILE_WEAPON_TYPE);
		universe.addComponent(shipComponent);
		
		id=51;name=HIGH_SPEED_TORPEDO;descr ="The high speed torpedo is almost twice as fast as the basic torpedo and a slightly better range. One set contains 4 torpedoes.";
		type=WEAPONS;unique=false;build=false;weight=80;picture="w_torpedo_hs"; 
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,24);
		shipComponent.setProductionCost(R_GOODS,56);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setSublightSpeed(2); //lightseconds per battleturn
		shipComponent.setForce(10);
		shipComponent.setLifeCycles(2);
		shipComponent.setBaseAmmo(4);
		shipComponent.setShipAttack(true);
		shipComponent.setPlanetAttack(true);
		shipComponent.setSubType(MISSILE_WEAPON_TYPE);
		universe.addComponent(shipComponent);		
		
		id=52;name=STARBURST_TORPEDO;descr ="The starburst torpedo combines higher speed and longer range with more force. One set contains 4 torpedoes.";
		type=WEAPONS;unique=false;build=false;weight=150;picture="w_torpedo_sb"; 
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,44);
		shipComponent.setProductionCost(R_GOODS,106);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setSublightSpeed(2); //lightseconds per battleturn
		shipComponent.setForce(12);
		shipComponent.setLifeCycles(3);
		shipComponent.setBaseAmmo(4);
		shipComponent.setShipAttack(true);
		shipComponent.setPlanetAttack(true);
		shipComponent.setSubType(MISSILE_WEAPON_TYPE);
		universe.addComponent(shipComponent);	

		//particle weapons
		id=47;name=PELLET_GUN;descr ="The Pellet Gun fires small steel pellets at very high speed (about 0.5c).";
		type=WEAPONS;unique=false;build=false;weight=30;picture="w_pellet_gun"; 
		shipComponent.setTechLevel(2);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,13);
		shipComponent.setProductionCost(R_COPPER,4);
		shipComponent.setProductionCost(R_STEEL,10);
		shipComponent.setProductionCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setSupportCost(R_STEEL,1);
		shipComponent.setRange(2);
		shipComponent.setForce(2);
		shipComponent.setShipAttack(true);
		shipComponent.setSubType(PARTICLE_WEAPON_TYPE);
		universe.addComponent(shipComponent);

		id=48;name=RAIL_GUN;descr ="The Railgun fires streams of small steel pellets at very high speed (about 0.6c).";
		type=WEAPONS;unique=false;build=false;weight=35;picture="w_rail_gun"; 
		shipComponent.setTechLevel(3);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2500);
		shipComponent.setProductionCost(R_IRON,14);
		shipComponent.setProductionCost(R_STEEL,13);
		shipComponent.setProductionCost(R_GOODS,8);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setSupportCost(R_STEEL,3);
		shipComponent.setRange(2);
		shipComponent.setForce(3);
		shipComponent.setShipAttack(true);
		shipComponent.setSubType(PARTICLE_WEAPON_TYPE);
		universe.addComponent(shipComponent);		
		
		//bombs
		id=19;name="Biological bomb";descr ="This bomb destroys up to 20% of the population of a colony. One set contains 4 bombs.";
		type=WEAPONS;unique=false;build=false;weight=5;picture="w_bomb-bio";		
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setProductionCost(R_CHEMICALS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1);
		shipComponent.setBaseAmmo(4);
		shipComponent.setForce(20);
		shipComponent.setSubType(BOMB_WEAPON_TYPE);
		universe.addComponent(shipComponent);
		
		id=20;name="Atomic bomb";descr ="This bomb destroys up to 10% of the population of a colony. One set contains 2 bombs.";
		type=WEAPONS;unique=false;build=true;weight=5;picture="w_bomb-nuke";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,500);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_URANIUM,0.5);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setBaseAmmo(2);
		shipComponent.setForce(10);
		shipComponent.setPlanetAttack(true);
		shipComponent.setSubType(BOMB_WEAPON_TYPE);
		universe.addComponent(shipComponent);			
		
		/**
		 * Special components
		 */	
		id=12;name="Ore seeker I";descr ="Ships with an ore seeker unit can mine for ores on uninhabitable planets or in asteroid belts.";
		type=SPECIAL;unique=false;build=false;weight=25;picture="s_ore-seeker1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,5);
		shipComponent.setProductionCost(R_COPPER,11);
		shipComponent.setProductionCost(R_ALUMINUM,2);
		shipComponent.setProductionCost(R_GOODS,6);
		shipComponent.setSupportCost(R_IRON,0.5);
		shipComponent.setSupportCost(R_ALUMINUM,0.5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setSubType(ORE_SEEKER_TYPE);
		universe.addComponent(shipComponent);
		
		id=13;name="Fighting stabilisator";descr ="The fighting stabilisator makes a ship faster in battle.";
		type=SPECIAL;unique=false;build=false;weight=5;picture="s_fighting_stabi1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_ALUMINUM,2);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setProductionCost(R_ELECTRONICS,0.1);
		shipComponent.setSupportCost(R_GOODS,0.5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setSubType(TACTICAL_TYPE);
		universe.addComponent(shipComponent);
		
		id=29;name="Battle Stabilisator";descr ="The Battle Stabilisator improves ship reaction speed and stability in fights.";
		type=ELECTRONICS;unique=false;build=false;weight=5;picture="s_battle-stabi";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1200);
		shipComponent.setProductionCost(R_IRON,3);
		shipComponent.setProductionCost(R_COPPER,1);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setProductionCost(R_ELECTRONICS,0.1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.setWarpSpeed(1);
		shipComponent.setSubType(TACTICAL_TYPE);
		universe.addComponent(shipComponent);

		//XXX: define fuel units and usage
		id=32;name="Fuel Tank I";descr ="The fuel tank stores up to 100 megazorks of energy.";
		type=SPECIAL;unique=false;build=true;weight=5;picture="s_fueltank1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,200);
		shipComponent.setProductionCost(R_COPPER,4);
		shipComponent.setProductionCost(R_SILVER,0.2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.1);
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.addFuel(100);
		shipComponent.setSubType(FUEL_TANK_TYPE);
		universe.addComponent(shipComponent);

		//XXX: implement fuel scoop with fuel tanks and ship range
		id=49;name=FUEL_SCOOP;descr ="The fuel scoop allows unlimited distance with warp 1 speed.";
		type=SPECIAL;unique=false;build=true;weight=1;picture="s_fuel_scoop"; 
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_COPPER,1);
		shipComponent.setProductionCost(R_STEEL,0.4);
		shipComponent.setProductionCost(R_ELECTRONICS,0.01);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);	
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.setSubType(FUEL_SCOOP_TYPE);
		universe.addComponent(shipComponent);
		

		//XXX: weight is 10 only when full !?
		//XXX: the number '10' occurs in too many different places here!
		id=53;name="Cargo Container (10 tons)";descr ="This cargo container can be used to transport up to 10 tons of goods.";
		type=SPECIAL;unique=false;build=true;weight=10;picture="s_cargo_module_10";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,100);
		shipComponent.setProductionCost(R_IRON,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);	
		shipComponent.setTransportCapability(10);
		shipComponent.setSubType(CARGO_TYPE);
		universe.addComponent(shipComponent);	

		id=54;name="Cargo Container (100 tons)";descr ="This cargo container can be used to transport up to 100 tons of goods.";
		type=SPECIAL;unique=false;build=true;weight=100;picture="s_cargo_module_100";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,500);
		shipComponent.setProductionCost(R_IRON,5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);	
		shipComponent.setTransportCapability(100);
		shipComponent.setSubType(CARGO_TYPE);
		universe.addComponent(shipComponent);			

		id=23;name="Mine deployer";descr ="Equipped with a mine deployer unit, a ship can deploy up to 50 mines around a planet in one turn.";
		type=SPECIAL;unique=false;build=false;weight=25;picture="s_mine-deployer_1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1200);
		shipComponent.setProductionCost(R_IRON,10);
		shipComponent.setProductionCost(R_ALUMINUM,5);
		shipComponent.setProductionCost(R_GOODS,5);
		shipComponent.setSupportCost(R_GOODS,2);
		shipComponent.setSupportCost(R_CHEMICALS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1.2);
		shipComponent.setBaseAmmo(50);
		shipComponent.addArmor(10);
		shipComponent.setSubType(MINE_DEPLOYER_TYPE);
		universe.addComponent(shipComponent);				
		
		id=40;name="Mine deployer II";descr ="Equipped with a mine deployer II unit, a ship can deploy up to 100 mines around a planet in one turn.";
		type=SPECIAL;unique=false;build=false;weight=80;picture="s_mine-deployer_2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,24);
		shipComponent.setProductionCost(R_ALUMINUM,16);
		shipComponent.setProductionCost(R_GOODS,30);
		shipComponent.setSupportCost(R_GOODS,5);
		shipComponent.setSupportCost(R_CHEMICALS,5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,2.5);
		shipComponent.setBaseAmmo(100);
		shipComponent.addArmor(20);
		shipComponent.setSubType(MINE_DEPLOYER_TYPE);
		universe.addComponent(shipComponent);
		
		
		/**
		 * Electronics
		 */		
		id=14;name="Target computer I";descr ="The target computer I improves the targetting of a ship.";
		type=ELECTRONICS;unique=false;build=false;weight=1;picture="s_tc-1";
		shipComponent.setTechLevel(1);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,0.2);
		shipComponent.setProductionCost(R_SILVER,0.2);
		shipComponent.setProductionCost(R_GOLD,0.1);
		shipComponent.setSupportCost(R_ELECTRONICS,0.5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setSubType(TARGET_COMPUTER_TYPE);
		universe.addComponent(shipComponent);
		
		id=15;name="Target computer II";descr ="The target computer II improves the targetting of a ship.";
		type=ELECTRONICS;unique=false;build=false;weight=2;picture="s_tc-2";
		shipComponent.setTechLevel(2);
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,0.4);
		shipComponent.setProductionCost(R_SILVER,0.4);
		shipComponent.setProductionCost(R_GOLD,0.2);
		shipComponent.setSupportCost(R_ELECTRONICS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setSubType(TARGET_COMPUTER_TYPE);
		universe.addComponent(shipComponent);
		
		id=17;name="Scanner I";descr ="The scanner I has a range of 1 LJ.";
		type=ELECTRONICS;unique=false;build=true;weight=2;picture="s_scanner1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,500);
		shipComponent.setProductionCost(R_IRON,1);
		shipComponent.setProductionCost(R_SILVER,0.1);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setRange(1); 
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
		
		id=18;name="Scanner II";descr ="The scanner II has a range of  2 LJ.";
		type=ELECTRONICS;unique=false;build=false;weight=3;picture="s_scanner2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,600);
		shipComponent.setProductionCost(R_IRON,1);
		shipComponent.setProductionCost(R_SILVER,0.1);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setRange(2); 
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
	
		id=24;name="Scanner III";descr ="The scanner III has a range of  5 LJ.";
		type=ELECTRONICS;unique=false;build=false;weight=5;picture="s_scanner3";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,800);
		shipComponent.setProductionCost(R_IRON,1);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setRange(5); 
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
		
		id=25;name="Scanner 4";descr ="The scanner IV has a range of  10 LJ.";
		type=ELECTRONICS;unique=false;build=false;weight=5;picture="s_scanner4";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1);
		shipComponent.setRange(10);
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
		
		id=26;name="Scanner 5";descr ="The scanner V has a range of  25 LJ.";
		type=ELECTRONICS;unique=false;build=false;weight=6;picture="s_scanner5";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(4);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1200);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,2);
		shipComponent.setRange(25);
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
		
		id=27;name="Scanner 6";descr ="The scanner VI has a range of 50 LJ.";
		type=ELECTRONICS;unique=false;build=false;weight=8;picture="s_scanner6";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(5);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,5);
		shipComponent.setRange(50);
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);
		
		id=28;name="Ultra Scanner";descr ="The  ultra scanner has an unlimited range.";
		type=ELECTRONICS;unique=false;build=false;weight=10;picture="s_scannerUS";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(6);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2500);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,10);
		shipComponent.setRange(100);
		shipComponent.setSubType(SCANNER_TYPE);
		universe.addComponent(shipComponent);

		/**
		 * satellites
		 */		
		id=33;name="Defense satellite I";descr ="The defense satellite I - hull with a payload of 20 tons can be equipped with eg. two laser guns.";
		type=SATELLITE;unique=false;build=false;weight=10;picture="s_defense-sat1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000);
		shipComponent.setProductionCost(R_IRON,7);
		shipComponent.setProductionCost(R_ALUMINUM,3);
		shipComponent.setProductionCost(R_GOODS,2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.3);
		shipComponent.setPayload(50);
		shipComponent.addArmor(10);
		universe.addComponent(shipComponent);
		
		id=34;name="Defense satellite II";descr ="The defense satellite II - hull with a payload of 40 tons can be equipped with eg. five laser guns.";
		type=SATELLITE;unique=false;build=false;weight=25;picture="s_defense-sat2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,12);
		shipComponent.setProductionCost(R_STEEL,8);
		shipComponent.setProductionCost(R_ALUMINUM,4);
		shipComponent.setProductionCost(R_SILVER,1);
		shipComponent.setProductionCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setPayload(100);
		shipComponent.addArmor(25);
		universe.addComponent(shipComponent);
		
		id=35;name="Defense satellite III";descr ="The defense satellite III - hull with a payload of 100 tons can be equipped with eg. ten laser guns.";
		type=SATELLITE;unique=false;build=false;weight=50;picture="s_defense-sat3";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		shipComponent.setProductionCost(R_IRON,21);
		shipComponent.setProductionCost(R_STEEL,18);
		shipComponent.setProductionCost(R_ALUMINUM,14);
		shipComponent.setProductionCost(R_SILVER,1);
		shipComponent.setProductionCost(R_GOODS,4);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1);
		shipComponent.setPayload(200);
		shipComponent.addArmor(200);
		universe.addComponent(shipComponent);
		
		id=36;name="Defense satellite IV";descr ="The defense satellite IV - hull with a payload of 200 tons can be equipped with eg.  20 laser guns.";
		type=SATELLITE;unique=false;build=false;weight=100;picture="s_defense-sat4";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(4);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,4000);
		shipComponent.setProductionCost(R_IRON,46);
		shipComponent.setProductionCost(R_STEEL,38);
		shipComponent.setProductionCost(R_ALUMINUM,14);
		shipComponent.setProductionCost(R_SILVER,2);
		shipComponent.setProductionCost(R_GOODS,5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,1.5);
		shipComponent.setPayload(500);
		shipComponent.addArmor(250);
		universe.addComponent(shipComponent);
			
		/**
		 * Armor
		 */
		id=31;name=ARMOR_CLASS_I;descr ="The simple armor improves the protection from laser fire for ships and stations.";
		type=SHIELDING;unique=false;build=true;weight=8;picture="s_armour_c1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,100);
		shipComponent.setProductionCost(R_IRON,8);
		shipComponent.setProductionCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.1);
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.addArmor(5);
		shipComponent.setSubType(ARMOR_TYPE);
		universe.addComponent(shipComponent);		
		
		id=37;name="Armour Class II";descr ="The class II armour improves the protection from laser fire for ships and stations.";
		type=SHIELDING;unique=false;build=false;weight=12;picture="s_armour_c2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,300);
		shipComponent.setProductionCost(R_IRON,12);
		shipComponent.setProductionCost(R_ALUMINUM,1);
		shipComponent.setProductionCost(R_SILVER,0.5);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.addArmor(10);
		shipComponent.setSubType(ARMOR_TYPE);
		universe.addComponent(shipComponent);
		
		id=38;name="Armour Class III";descr ="The class III armour improves the protection from laser fire for ships and stations.";
		type=SHIELDING;unique=false;build=false;weight=15;picture="s_armour_c3";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,800);
		shipComponent.setProductionCost(R_IRON,16);
		shipComponent.setProductionCost(R_ALUMINUM,1);
		shipComponent.setProductionCost(R_SILVER,0.8);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.4);
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.addArmor(25);
		shipComponent.setSubType(ARMOR_TYPE);
		universe.addComponent(shipComponent);
		
		id=39;name="Armour Class IV";descr ="The class IV armour improves the protection from laser fire for ships and stations.";
		type=SHIELDING;unique=false;build=false;weight=20;picture="s_armour_c4";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,1500);
		shipComponent.setProductionCost(R_IRON,19);
		shipComponent.setProductionCost(R_ALUMINUM,1.2);
		shipComponent.setProductionCost(R_SILVER,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);	
		shipComponent.setHullSizeDepending(FULLY_DEPENDING);
		shipComponent.addArmor(50);
		shipComponent.setSubType(ARMOR_TYPE);
		universe.addComponent(shipComponent);

		id=16;name="Electronic Shield I";descr ="The shield I protects ships from lasers up to class 5 and reduces the effects of stronger lasers.";
		type=SHIELDING;unique=false;build=false;weight=3;picture="s_shield-1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,2000);
		shipComponent.setProductionCost(R_IRON,2);
		shipComponent.setProductionCost(R_COPPER,0.2);
		shipComponent.setProductionCost(R_SILVER,0.1);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.2);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.addShielding(2);
		shipComponent.setSubType(SHIELDING_TYPE);
		universe.addComponent(shipComponent);			
		
		//XXX: protection against different attack types
		id=30;name="Electronic Shielding II";descr ="The shielding type II protects from simple laser beams.  It also has a slight chance to deflect meteorites or projectiles.";
		type=SHIELDING;unique=false;build=false;weight=4;picture="s_shield-2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		shipComponent.setProductionCost(R_IRON,3);
		shipComponent.setProductionCost(R_COPPER,0.5);
		shipComponent.setProductionCost(R_SILVER,0.2);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.addShielding(5);
		shipComponent.setSubType(SHIELDING_TYPE);
		universe.addComponent(shipComponent);

		id=56;name="Electronic Shielding III";descr ="The shielding type III protects from most beam weapons.  It also has a slight chance to deflect meteorites or projectiles.";
		type=SHIELDING;unique=false;build=false;weight=5;picture="s_shield-2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,5000);
		shipComponent.setProductionCost(R_IRON,4);
		shipComponent.setProductionCost(R_COPPER,1);
		shipComponent.setProductionCost(R_SILVER,0.5);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.addShielding(10);
		shipComponent.setSubType(SHIELDING_TYPE);
		universe.addComponent(shipComponent);
		
		id=57;name="Electronic Shielding IV";descr ="The shielding type IV protects from most beam weapons and from some particle weapons.  It also has a slight chance to deflect meteorites or projectiles.";
		type=SHIELDING;unique=false;build=false;weight=8;picture="s_shield-2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,3000);
		shipComponent.setProductionCost(R_IRON,5);
		shipComponent.setProductionCost(R_COPPER,2);
		shipComponent.setProductionCost(R_SILVER,1);
		shipComponent.setProductionCost(R_GOODS,1);
		shipComponent.setSupportCost(R_GOODS,1);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,0.5);
		shipComponent.setHullSizeDepending(SOME_DEPENDING);
		shipComponent.addShielding(15);
		shipComponent.setSubType(SHIELDING_TYPE);
		universe.addComponent(shipComponent);
		
		/**
		 * Colony modules
		 */
		
		id=5;name=COLONY_MODULE;descr ="With a colony module, earthlike planets can be colonized.";
		type=SPECIAL;unique=false;build=true;weight=600;picture="s_colony_mod1";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(0);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,10000);
		shipComponent.setProductionCost(R_IRON,100);
		shipComponent.setProductionCost(R_ALUMINUM,100);
		shipComponent.setProductionCost(R_GOODS,200);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setSupportCost(R_IRON,1);
		shipComponent.setSupportCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_GOODS,2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,12);
		shipComponent.setSubType(COLONY_MODULE_TYPE);
		universe.addComponent(shipComponent);	
		
		id=41;name=COLONY_MODULE_II;descr ="With a colony module II, earth- and marslike planets can be colonized.";
		type=SPECIAL;unique=false;build=false;weight=800;picture="s_colony_mod2";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);		
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,15000);
		shipComponent.setProductionCost(R_IRON,150);
		shipComponent.setProductionCost(R_ALUMINUM,150);
		shipComponent.setProductionCost(R_GOODS,300);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setSupportCost(R_IRON,2);
		shipComponent.setSupportCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,15);
		shipComponent.setSubType(COLONY_MODULE_TYPE);
		universe.addComponent(shipComponent);
		
		id=42;name=COLONY_MODULE_III;descr ="With a colony module III, venus-, earth- and marslike planets can be colonized.";
		type=SPECIAL;unique=false;build=false;weight=1000;picture="s_colony_modR";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(2);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,25000);
		shipComponent.setProductionCost(R_IRON,200);
		shipComponent.setProductionCost(R_ALUMINUM,200);
		shipComponent.setProductionCost(R_GOODS,400);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setSupportCost(R_IRON,3);
		shipComponent.setSupportCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,20);
		shipComponent.setSubType(COLONY_MODULE_TYPE);
		universe.addComponent(shipComponent);
		
		id=43;name=COLONY_MODULE_IV;descr ="With a colony module IV, mercury-, venus-, earth- and marslike planets can be colonized.";
		type=SPECIAL;unique=false;build=false;weight=1000;picture="s_colony_modM";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(3);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,30000);
		shipComponent.setProductionCost(R_IRON,200);
		shipComponent.setProductionCost(R_ALUMINUM,200);
		shipComponent.setProductionCost(R_GOODS,400);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setSupportCost(R_IRON,3);
		shipComponent.setSupportCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,20);
		shipComponent.setSubType(COLONY_MODULE_TYPE);
		universe.addComponent(shipComponent);
		
		id=44;name=COLONY_MODULE_V;descr ="With a colony module V, (radiated or poisonous) mercury-, venus-, earth- and marslike planets can be colonized.";
		type=SPECIAL;unique=false;build=false;weight=1200;picture="s_colony_modV";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(4);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,40000);
		shipComponent.setProductionCost(R_IRON,200);
		shipComponent.setProductionCost(R_ALUMINUM,200);
		shipComponent.setProductionCost(R_GOODS,500);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setProductionCost(R_GLASS,100);
		shipComponent.setSupportCost(R_IRON,5);
		shipComponent.setSupportCost(R_ALUMINUM,2);
		shipComponent.setSupportCost(R_GOODS,3);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,20);
		shipComponent.setSubType(COLONY_MODULE_TYPE);
		universe.addComponent(shipComponent);

		//XXX: implement outpost logic in game, also for computerplayers: subtype(?) and AiShipBuilder
		id=45;name="Outpost module";descr ="With an outpost module, sunsystems without planet can be 'colonized'.";
		type=SPECIAL;unique=false;build=false;weight=1500;picture="s_outpost";
		shipComponent= new ShipComponent(id,name,descr,type,unique,build,weight,picture);
		shipComponent.setTechLevel(1);
		shipComponent.setProductionCost(R_WORKUNITS_CONSTRUCTION,25000);
		shipComponent.setProductionCost(R_IRON,200);
		shipComponent.setProductionCost(R_ALUMINUM,200);
		shipComponent.setProductionCost(R_GOODS,600);
		shipComponent.setProductionCost(R_TOOLS,200);
		shipComponent.setProductionCost(R_GLASS,300);
		shipComponent.setSupportCost(R_IRON,3);
		shipComponent.setSupportCost(R_ALUMINUM,1);
		shipComponent.setSupportCost(R_GOODS,2);
		shipComponent.setSupportCost(R_WORKUNITS_OPERATION,25);
		universe.addComponent(shipComponent);
		
		//next shipcomponent number is 58
		
		
// XXX: More shipComponents, changes to logic:
//*********************************************
//
// Military
// ========
// special weapons:
// -------------
// Tractor beams
// Stealth generator
// Stealth unit
// Stealth field
// Stealth disruptor field
// Warp inhibitor field
// Spy beam
// ECM unit
// Teleport inhibitor field
//
// mines and torpedos:
// ------------------
// Mine deployers
// Torpedo Mine
// Sub space disruptor (bomb)
// ECM unit
// Sub Space Torpedo
// Gravity Blast Torpedo
//
// matter projection:
// ------------------ 
// Mass projector
// plasma projector
// Black Hole Projectile
// Antimatter Projectile
		
// 
// energy projection:
// ------------------
// Proton blaster
// Sub space beam
// Tesla ray projector
// Sun beam

//		
// bombs
// -----		
// Stronger bombs and torpedos
// Terraform bomb
// Nemp bomb
// Time freeze bomb
// Antimatter bomb
// Magnetic storm bomb
// Mindstorm bomb
// Teleport bomb


// Mathematics:
// ============
// Antimatter theory
// Warp bombs
// Gravity theory
// Gravitational bombs
// improved force fields
// Target computer III & IV
// stealth scanner
//
// Biology:
// Water recycling unit
// Hydroponics unit
// Air recycling unit
  }

  // Events
  private void defEvents() {
	int id=0;
	String name="",title="",descr="",picture="",animation="";
	SOEvent soEvent = null;
    // welcome event activated at game start
	id=1;
	name="Welcome";
	title="Welcome to the SpaceOpera Universe!";
	descr="In the year 2399, the spaceship 'Maria' was hurtled into an " +
	      "uncharted galaxy by a drive malfunction.\n\nSeveral thousand " +
	      "colonists aboard were suddenly cut off from support by their " +
	      "homeplanet.\n\nCould they find a suitable planet to colonize? " +
	      "Will they survive? Will they eventually meet aliens? Will they " +
	      "ever find a way to communicate or travel back to earth? ";
	picture="resources/images/alien-tunnel.jpg";
    animation="";
	//animation="resources/animations/lava.avi"; // should be some swirling colors, followed by a black background with some stars.
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

    // all first contact events are activated when the human player
    // meets a race for the FIRST time. This can be when
    // - two ships 'meet' in same system
    // - enemy ship enters system with colony
    // - enemy ship enters scanner area
    // XXX get better names for those events,
    //       eg. "MeetSquiggles" or "FirstContactToSquiggles"
	
	// XXX only the id and the name are unique, refactor event 2-21
	// XXX first contact event should be before battle event
	// XXX after battle, the descr. text is no longer fitting
    id=2;
	name="Squiggles";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
	      "Squiggles because they look so squiggly. \n" +
	      "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/squiggles.jpg";
	animation="resources/animations/squiggle1.avi";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=3;
	name="Froggies";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Froggies because they look a bit froglike. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg"; // XXX: some jpg
	animation=""; // XXX: some avi
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=4;
	name="Punks";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Punks because, well, they are. At least you thinks so. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=5;
	name="Bears";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Bears because they're big and hairy. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=6;
	name="Krauts";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Krauts because they sound like Krauts. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=7;
	name="Mandschas";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Mandschas because that's what they always seem to say. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

 	id=8;
 	name="Piggies";
 	title="First Contact!";
 	descr="You have made first contact with an alien race. You call them  " +
		  "Piggies because that's what they look like. \n" +
	   	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
 	animation="";
 	soEvent = new SOEvent(id,name,title,descr,picture,animation);
 	universe.addEvent(soEvent);

  	id=9;
  	name="Pylons";
  	title="First Contact!";
  	descr="You have made first contact with an alien race. You call them  " +
		  "Pylons because that's what they look like. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
  	picture="resources/images/alien1.jpg";
  	animation="";
  	soEvent = new SOEvent(id,name,title,descr,picture,animation);
  	universe.addEvent(soEvent);

   	id=10;
   	name="Gods";
   	title="First Contact!";
   	descr="You have made first contact with an alien race. You call them  " +
		  "Gods because that's what seemed the right thing to do. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
    picture="resources/images/alien1.jpg";
    animation="";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);

	id=11;
	name="Stoners";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Stoners because look like they're made of stone. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=12;
	name="Mussels";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Mussels because that's what they look like. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=13;
	name="Dragons";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
	  	  "Dragons because they really are. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

 	id=14;
 	name="Little green men";
 	title="First Contact!";
 	descr="You have made first contact with an alien race. You call them  " +
	      "Little green men because they really are. \n" +
	      "That's probably not what they call themselves. You'll have to find out. ";
 	picture="resources/images/alien1.jpg";
 	animation="resources/animations/aliens.avi";
 	soEvent = new SOEvent(id,name,title,descr,picture,animation);
 	universe.addEvent(soEvent);

	id=15;
	name="Ugly Ducklings";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Ugly Ducklings because they're really ugly. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);


	id=16;
	name="Paintballs";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
	  	  "Paintballs because seem to throw paintballs all the time. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=17;
	name="Furballs";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Furballs because they're round and hairy. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=18;
	name="Fire-Beings";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Fire-Beings because they look like flames and heat. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=19;
	name="Crystal things";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Crystal things because they look like crystals on wheels. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=20;
	name="Whirlies";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
		  "Whirlies because they whirl around a lot. \n" +
	  	  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

	id=21;
	name="Zwilniks";
	title="First Contact!";
	descr="You have made first contact with an alien race. You call them  " +
	  	  "Zwilniks because you don't like them very much. \n" +
		  "That's probably not what they call themselves. You'll have to find out. ";
	picture="resources/images/alien1.jpg";
	animation="";
	soEvent = new SOEvent(id,name,title,descr,picture,animation);
	universe.addEvent(soEvent);

    id=22;
    name="LavaWorld"; // looks like hell
    title="Discovery!";
    descr="You have discovered a very strange planet. Seems not to be " +
          "colonizable.";
    picture="resources/images/lava.jpg";
    animation="resources/animations/lava.avi";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);

    id=23;
    name="HomeWorld";  // looks like home
    title="Discovery!";
    descr="You have discovered a beautiful new planet. Looks " +
          "just like home, ready to colonize.";
    picture="resources/images/homeworld.jpg";
    animation="resources/animations/homeworld.avi";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);

    id=24;
    name="HomeWorldLost";   // looks like paradise, unfortunately already colonized!
    title="Discovery!";
    descr="You have discovered a beautiful new planet. Looks " +
          "just like home. \n\n" +
          "Unfortunately this planet is already colonized.";
    picture="";
    animation="resources/animations/homeworld.avi";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);

    id=25;
    name="AsteroidBelt1";
    title="Discovery!";
    descr="You have discovered a nice asteroid belt. " +
          "It's probably very rich with minerals. \n\n" +
          "Go ahead and build asteroid mining units!.";
    picture="";
    animation="resources/animations/aster1.avi";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);

    id=26;
    name="AsteroidBelt2";
    title="Discovery!";
    descr="You have discovered a nice asteroid belt.u " +
          "It's probably very rich with minerals. \n\n" +
          "Go ahead and build asteroid mining units!.";
    picture="";
    animation="resources/animations/aster2.avi";
    soEvent = new SOEvent(id,name,title,descr,picture,animation);
    universe.addEvent(soEvent);
  }


  // Names
  private void defNames() {
	  String[] starNames = new String[] {
	  "Alcatraz", "Aldebaran", "Alderamin", "Amagalandaranbalander",
	  "Ammon", "Andromeda", "Anima", "Ant", "Antares", "Anubis",
	  "Asimov", "Armageddon", "Armagnac", "Bai Hu", "Barbara", "Bear", "Bell", "Beteigeuze", "Bin Laden",
	  "Bilbo", "Bingo", "Brian Boru", "Borg", "Buzz Lightyear", "Caesar", "Camp David", "Canopus",
	  "Capella", "Cassiopeia", "Castle of Aaaargh", "Celeborn", "Central", "Cet Gra Xrx", "Charon",
	  "Che Guevara", "Chiron", "Chocolat", "Choron", "Circle", "Circus", "Coercri", "Cosmos",
	  "Costigan", "Cragganmore", "Cronos", "Dagobert", "Dawn", "De Gaulle", "Delta",
	  "Dick", "Dinner", "Doggarnogugliagug", "DocSmith", "D�ner Kebap", "Doom", "Dune",
	  "Donald", "Donaldson", "Don't stop here", "Dragon", "Duck", "EAV",
	  "Edda", "Eros", "Ethan", "Fangorn", "Ffallhilaliga", "Fella",
	  "Fidel", "Filius", "Flavour", "Foo", "Freya", "Frodo", "Fry",
	  "Galactica", "Gamgangur'itz", "Gandalf", "Gates", "Gehenna", "Glamour", "Gan Baria Houk", "George Washington", 
	  "Glen Morangie", "Glenda", "Gold", "Gondolin", "Gondor", "Guam", "Gu-kle-rackatz", "Guild", "Hack", "Hame", "Handy",
	  "Haven", "Heaven", "Heinlein", "Hell", "Heretic", "Heron", "Hi'ackabb-reck",
	  "Hohoho", "Home", "Honk", "Hou Gong", "Hougoumont", "Hubble", "Hydra", "Hy Ty Taviadyn",
	  "Imperia", "iX", "Jabberwocky", "Jamba", "Jassa Javeera Yaina", "Java", "Kalevala", "Kanopus", "Kebab",
	  "Kemosabe", "Kent", "Khanat", "Khazad-Dum", "Khor", "Kluirgknat", "Krishna", "Kubrick",
	  "Lagavulin", "Lam og badan bum", "Le Gaullois", "Lem", "Lemming", "Les Enfers", "Lexington", "LightYear",
	  "Linux", "Loch Ness", "Lonabar", "Lyra", "Mac", "MacAllan", "Mammon", "Mame",
	  "Mao", "Marx", "Master", "Mentor", "Merlin", "MIB", "Micky", "Midi", "Mime", "Mine",
	  "Mint", "Mintaka", "Mirsalupsi", "Mira", "Mirzam", "Money", "Monk", "Monocerotis",
	  "Monty", "Moron", "Musk", "Mythos", "Naos", "Nachos", "Naxos",
	  "Neve", "New Wales", "Newton", "Nihao", "Nirvana Beta", "Number of the Beast", "Oban", "Obelix",
      "Occitaine", "Om", "Onassis", "Ontario", "Orion", "Ouagadougou", "Palindrome", "Panic", "Para", "Paradise", "Parallel", "Parma",
	  "Pathos", "Pax Americana", "Pepper", "Perseus", "Phlossten Paradise", "Phone", "Pi", "Pine",
	  "Pilatus", "Pilsner", "Poi Og Ranadar", "Polaris", "Pong", "Python", "Quake", "Qing Long", "Rambo",
      "Regulus", "Rhea", "Rhumba", "Rockefeller", "Roi Noir", "Saddam Hussein", "Sagitta", "Salsaparilla", "Sambigliong", "Sanskrit",
	  "Scotia", "Seth", "Si Fu", "Silver", "Sire", "Sirius", "Smiley", "So Slossonan",
	  "Solaris", "Spica", "Spring Tide", "Sss'ch ss ch'chs", "Star", "Starlet", "Sting", "Stuyvesant", "Sugus",
	  "Summer", "Super Constellation", "Tacos", "Talgo", "Talisker", "Tangens", "Tantra", "Taurus", "Tazenda", "Teegeeack",
	  "Tex", "Texas", "TGV", "Tian Chu", "Tian Huang Da Di", "Tin", "Tintagel", "Thor", "Thoth", "Trantor",
	  "Troja", "Trust", "Tux", "Umayra Harx", "Umbriel", "Up The Ante", "Varga", "Vercing�torix", "Virgo", "Voronoi", "Waterloo", "Waypoint", "Wega",
      "Why Me?", "Werewolf", "Wine", "Winnie Poo","Wink", "Winter", "Wombat", "Wu Di Nei Zuo", "X", "Xiao Tianxing", "Xuan Wu",
	  "Xylian Dorayx A", "Xyzzy", "Yahoo", "Yapyapyadda", "Yard", "Yast", "Yepsiyeleyen", "Yes", "Yi He Yuan",
	  "Yoho", "York", "Yr Cymru", "Yup", "Zaffaraya", "Zapf DingBat", "Zeus", "Zhu Que", "Zork",
	  "Zubenelgenubi", "Zurg", "ZZZ", "Z9" };
//System.out.println("Starnames count: " + starNames.length);
	  boolean[] starNamesUsed = new boolean[starNames.length];
	  universe.setStarNames(starNames,starNamesUsed);

	  //XXX: combine player names, colors and maybe a description with the CPlayer class!
	  String[] playerNames = new String[] {
	  "Squiggles", "Froggies", "Punks", "Bears", "Krauts",
	  "Mandschas", "Piggies", "Pylons", "Gods", "Stoners",
	  "Mussels", "Dragons", "Little green men", "Ugly Ducklings", "Paintballs",
	  "Furballs", "Fire-Beings", "Crystal things", "Whirlies", "Zwilniks"};
	  universe.setPlayerNames(playerNames);

	  Color[] playerColors = new Color[] {
	  Color.orange, Color.pink, Color.green, Color.magenta, Color.blue,
	  Color.yellow.brighter(), Color.white, Color.cyan, Color.gray, Color.lightGray,
	  Color.orange.darker(),Color.pink.darker(), Color.green.darker(), Color.magenta.darker(),
	  Color.blue.darker(),Color.yellow.darker(),Color.cyan.darker(),
	  Color.getHSBColor(255,200,200),Color.getHSBColor(220,255,200), Color.getHSBColor(220,200,255)};
	  //Color.getHSBColor(255,238,238),Color.getHSBColor(255,128,128),Color.getHSBColor(128,128,255)};
	  universe.setPlayerColors(playerColors);
	  
	  String[] resourceNames = new String[] {
      "", "total work units", "Production", "Support",
      "Military","Education", "Administration",
	  "", "Electricity", "Raw material", "Stone",
      "Wood", "Coal", "Oil", "Iron", "Copper",
      "Aluminum", "Silicium", "Silver", "Gold", "Uranium",
      "Food", "Meat", "Fish", "Rice", "Crop",
      "Dairyprod", "Vegetables", "Freshwater", "Wine", "Salt",
      "Processed materials", "Goods", "Steel", "Alloy",
      "Tools", "Machines", ELECTRONICS, "Glass", "Chemicals",
      "Drugs", "Plastic", "Garments", "Waste"};	  
	  universe.setResourceNames(resourceNames);
	  String[] resourceUnits = new String[] {
	  "", "units", "units","units","units","units","units",
	  "", "kW", "tons", "tons","tons", "tons", "tons", "tons", "tons",
	  "tons", "tons", "tons", "tons", "tons","tons", "tons", "tons", "tons", "tons",
	  "tons", "tons", "tons", "tons", "tons","tons", "tons", "tons", "tons",
	  "tons", "tons", "tons", "tons", "tons","tons", "tons", "tons", "tons"};	  
	  universe.setResourceUnits(resourceUnits);
  }


  // Products
  //Products are things, that are not fixed to a colony, they are created and gone
  //XXX: currently, ships and satellites figure as 'products' in the Colony and ColonyDetails class
  private void defProducts() {
  	 //XXX: products ??
  	 /**
	 String name="",cls="",descr="",picture="";
	 boolean unique=false,build=false;
	 int id=0;
	 */
  	 //XXX: trade items
  	 /**
	 Product product= null;
	 id=5;name="Trade items";descr ="Trade items for interstellar trade.";
		  unique=false;build=true;picture="product";cls="Economy";
	 product= new Product(id,name,descr,unique,build,picture,cls);
	 product.setProductionCost(R_WORKUNITS_CONSTRUCTION,10);
	 product.setProductionCost(R_IRON,2);
	 product.setProductionCost(R_WOOD,2);
	 product.setProductionCost(R_STONE,1);
	 product.setProductionCost(R_TOOLS,1);
	 product.setResourceProduction(R_GOODS, 5);
	 product.setResourceProduction(R_WASTE, 1);
	 universe.addProduct(product);
	 */
	 //XXX: trader ship??
	 /** 
	 id=9;name="Trader ship";descr ="The Tradership buys and sells goods automatically. It responds to supply and demand and transports goods from one planet to another.";
		  unique=false;build=false;picture="s_transport";cls="Ship";
	 product= new Product(id,name,descr,unique,build,picture,cls);
	 product.setProductionCost(R_WORKUNITS_CONSTRUCTION,1000000);
	 product.setProductionCost(R_IRON,50);
	 product.setProductionCost(R_COPPER,10);
	 product.setProductionCost(R_ALUMINUM,10);
	 product.setProductionCost(R_GOODS,11);
	 universe.addProduct(product);
	 */
  }

  // Technologies
  //XXX: which buildings, components or products obsolete something?
  
	//XXX: hull for images ship1, ship2, small-bomber, small-fighter, fuel-trans, TRS-A-n
	//(possibly rename and reclassify some of them )
	//XXX: shipcomponent for w_laser2 + 3 images
	//XXX: shipcomponent for images proton blaster, sp3f laser, gauss cannon	  
  
  
  private void defTechnologies() {
		int id=0,childtech1=0,childtech2=0,building=0,prod=0,shipComp=0;
		double cost = 0.0;
		String name="",cls="",descr="",picture="";
		boolean known=false,study=false;
		Technology technology=null;

		//Hulls
		id=7;name="Defense satellite";descr="We should design a small defensive satellite to protect our colony against enemy attacks.";
		known=false;study=true;childtech1=54;childtech2=0;building=0;prod=0;shipComp=33;cls=MILITARY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=54;name="Defense satellite 2";descr="We should design a better defensive satellite to protect our colony against enemy attacks.";
		known=false;study=false;childtech1=60;childtech2=0;building=0;prod=0;shipComp=34;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(33);
		universe.addTechnology(technology);		
		id=60;name="Defense satellite 3";descr="We should design an improved defensive satellite to protect our colony against enemy attacks.";
		known=false;study=false;childtech1=67;childtech2=0;building=0;prod=0;shipComp=35;cls=MILITARY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(34);
		universe.addTechnology(technology);		
		id=67;name="Defense satellite 4";descr="We should design a bigger defensive satellite to protect our colony against enemy attacks.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=36;cls=MILITARY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(35);
		universe.addTechnology(technology);		
		id=37;name="Scout hull";descr="We should design a small ship hull for exploration or fighter tasks.";
		known=true;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=9;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=18;name="Escort hull";descr="We should design a small ship suited for armed escorts, small transports or fast couriers.";
		known=true;study=false;childtech1=98;childtech2=0;building=0;prod=0;shipComp=22;cls=PHYSICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=46;name="Colonyship hull";descr="We should design a ship hull big enough to contain a colony module.";
		known=true;study=false;childtech1=53;childtech2=0;building=0;prod=0;shipComp=10;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	
		id=53;name="Transportship hull";descr="We should design a ship hull for medium sized transporters.";
		known=false;study=true;childtech1=0;childtech2=0;building=0;prod=0;shipComp=21;cls=PHYSICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=98;name="Frigate hull";descr="We should design a large ship hull with enough shielding, usable as military ship.";
		known=false;study=true;childtech1=0;childtech2=0;building=0;prod=0;shipComp=55;cls=PHYSICS;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		
		
		//Drives
		id=32;name=SUBLIGHT_DRIVE;descr="We should do some research to find a working sublight-drive.";
		known=true;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=11;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=1;name="Warp 1 drive";descr="The warp technology promises faster-than-light drives. We should invest in this area.";
		known=true;study=false;childtech1=2;childtech2=0;building=0;prod=0;shipComp=1;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);		
		universe.addTechnology(technology);
		id=2;name="Warp 2 drive";descr="We should try to improve the existing warp drive.";
		known=false;study=true;childtech1=3;childtech2=0;building=0;prod=0;shipComp=2;cls=PHYSICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//technology.obsoletesShipComponent(1); //XXX: MAY obsolete...
		universe.addTechnology(technology);
		id=3;name="Warp 3 drive";descr="We have already built warp 1 & 2 drives. Maybe we can build a warp 3 drive?";
		known=false;study=false;childtech1=4;childtech2=0;building=0;prod=0;shipComp=3;cls=PHYSICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//technology.obsoletesShipComponent(2); //XXX: MAY obsolete...
		universe.addTechnology(technology);	
		id=4;name="Warp 4 drive";descr="More research in the warp technology area promises a warp 4 drive.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=4;cls=PHYSICS;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//technology.obsoletesShipComponent(3); //XXX: MAY obsolete...
		universe.addTechnology(technology);
		
		//Weapons
		id=5;name=LASER;descr="Laser technology uses a highly focussed lightbeam that can be used as a weapon.";
		known=true;study=false;childtech1=6;childtech2=7;building=0;prod=0;shipComp=6;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=6;name=LASER_CANNON;descr="This theory of mine promises an improved laser weapon.";
		known=false;study=false;childtech1=83;childtech2=0;building=0;prod=0;shipComp=7;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	
		id=83;name=BUNDLED_LASER;descr="If we bundle some lasers and use a common focussing lens, we could virtually 'reach out and touch' our enemies!";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=46;cls=MILITARY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);					
		id=52;name="Atomic bomb";descr="With what we know about fusion, we could build fusion bombs for orbital bombardment.";
		known=true;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=20;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=14;name="Biological Bomb";descr="A bomb with biological agents should be much nastier than fusion bombs.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=19;cls=BIOLOGY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	
		id=45;name=TORPEDO;descr="Beam weapons are ok, but with torpedoes, we could attack enemies without coming into beam range!";
		known=true;study=false;childtech1=89;childtech2=0;building=0;prod=0;shipComp=8;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=89;name="Long Range Torpedo";descr="We should research a torpedo with a better range performance.";
		known=false;study=true;childtech1=90;childtech2=0;building=0;prod=0;shipComp=50;cls=MILITARY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=90;name="High Speed Torpedo";descr="We should research a faster torpedo.";
		known=false;study=false;childtech1=91;childtech2=0;building=0;prod=0;shipComp=51;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=91;name="Starburst Torpedo";descr="We should design a faster torpedo with higher range.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=52;cls=MILITARY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		id=86;name="Pellet gun";descr="If we could accelerate little pellets to very high speeds, this would give us a very nasty weapon.";
		known=false;study=false;childtech1=87;childtech2=0;building=0;prod=0;shipComp=47;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=87;name="Pellet rail gun";descr="The Pellet Gun is alright, but I still think we should throw more pellets at our enemies. Throw 'em in clusters, I say.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=48;cls=MILITARY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		

		//Special equipment
		id=17;name="Mine deployer";descr="We should try to build a mine deployer ship component.";
		known=false;study=true;childtech1=71;childtech2=0;building=0;prod=0;shipComp=23;cls=MILITARY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		id=19;name="Ore seeker I";descr="We should research some kind of automated ore mining device. Added to small ships, we could mine asteroid belts, moons or uninhabited planets. ";
		known=false;study=true;childtech1=0;childtech2=0;building=0;prod=0;shipComp=12;cls=ECONOMY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=22;name="Target computer I";descr="We should research a targetting computer for armed ships.";
		known=false;study=true;childtech1=23;childtech2=86;building=0;prod=0;shipComp=14;cls=MATHEMATICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=23;name="Target computer II";descr="Whe should try to enhance the capabilities of our targetting computers.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=15;cls=MATHEMATICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete...  technology.obsoletesShipComponent(14);
		universe.addTechnology(technology);		
		id=24;name="Scanner 1";descr="We should develop some kind of scanner technology.";
		known=true;study=false;childtech1=25;childtech2=0;building=23;prod=0;shipComp=17;cls=MATHEMATICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=25;name="Scanner 2";descr="We could really use better scanners.";
		known=false;study=true;childtech1=41;childtech2=0;building=30;prod=0;shipComp=18;cls=MATHEMATICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(23);		
		id=30;name="Fighting stabilisator";descr="This study could lead to a better battle performance of our ships.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=13;cls=SOCIAL;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=41;name="Scanner 3";descr="We should improve our scanning abilities.";
		known=false;study=false;childtech1=42;childtech2=0;building=31;prod=0;shipComp=24;cls=MATHEMATICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(30);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(18);
		universe.addTechnology(technology);		
		id=42;name="Scanner 4";descr="This study shows that we could our scanners even more efficient.";
		known=false;study=false;childtech1=57;childtech2=0;building=32;prod=0;shipComp=25;cls=MATHEMATICS;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(31);				
		//XXX: MAY obsolete... technology.obsoletesShipComponent(24);
		universe.addTechnology(technology);			
		id=57;name="Scanner 5";descr="Our ship scanners have been hampered by enemies using stealth technology. Maybe we can improve our scanner technology?.";
		known=false;study=false;childtech1=58;childtech2=0;building=33;prod=0;shipComp=26;cls=MATHEMATICS;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(32);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(25);
		universe.addTechnology(technology);
		id=58;name="Scanner 6";descr="Better scanner tech would help our ships in battle and in deep space.";
		known=false;study=false;childtech1=59;childtech2=0;building=34;prod=0;shipComp=27;cls=MATHEMATICS;cost=TECHLEVEL_5;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(33);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(26);
		universe.addTechnology(technology);
		id=59;name="Ultra Scanner";descr="This could be the final breakthrough in scanner tech.";
		known=false;study=false;childtech1=0;childtech2=0;building=35;prod=0;shipComp=28;cls=MATHEMATICS;cost=TECHLEVEL_6;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(34);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(27);
		universe.addTechnology(technology);	
		id=44;name=COLONY_MODULE;descr="We should design a ship component to transport colonists and setup a new colony on earthlike planets.";
		known=true;study=false;childtech1=72;childtech2=0;building=0;prod=0;shipComp=5;cls=BIOLOGY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
	
		id=65;name=ARMOR_CLASS_I;descr="Our ships are very vulnerable. Maybe we could design some kind of armor?";
		known=true;study=false;childtech1=68;childtech2=0;building=0;prod=0;shipComp=31;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=66;name="Fuel cell I";descr="To improve ship speed and range, we should research fuel storage technology.";
		known=true;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=32;cls=PHYSICS;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);

		id=68;name="Armour Class II";descr="A study of new composite materials could lead to enhanced armor for ships and satellites.";
		known=false;study=true;childtech1=69;childtech2=0;building=0;prod=0;shipComp=37;cls=PHYSICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete...	technology.obsoletesShipComponent(31);
		universe.addTechnology(technology);				
		id=69;name="Armour Class III";descr="Our composite tech specialists speaks of a better performance against laser and micro-meteorites.";
		known=false;study=false;childtech1=70;childtech2=0;building=0;prod=0;shipComp=38;cls=PHYSICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(37);
		universe.addTechnology(technology);		
		id=70;name="Armour Class IV";descr="This research papers shows a possibility to build better and lighter armor. But it's not cheap.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=39;cls=PHYSICS;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(38);
		universe.addTechnology(technology);
		id=71;name="Mine deployer II";descr="We should design a more efficient mine deployer ship component.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=40;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		//XXX: MAY obsolete... technology.obsoletesShipComponent(23);
		universe.addTechnology(technology);
		id=72;name=COLONY_MODULE_II;descr="We should design a ship component to colonize the not quite earth-like planets.";
		known=false;study=true;childtech1=73;childtech2=76;building=42;prod=0;shipComp=41;cls=BIOLOGY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesShipComponent(5); //always use the best colony module available
		universe.addTechnology(technology);		
		id=73;name=COLONY_MODULE_III;descr="There are a lot of planets that we cannot colonize with our existing technology. A better colony module would help here.";
		known=false;study=false;childtech1=74;childtech2=0;building=43;prod=0;shipComp=42;cls=BIOLOGY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(42); 
		technology.obsoletesShipComponent(41); //always use the best colony module available
		universe.addTechnology(technology);		
		id=74;name=COLONY_MODULE_IV;descr="This paper shows that we could colonize most planets with an improved colony module. Should we try to design this?";
		known=false;study=false;childtech1=75;childtech2=0;building=44;prod=0;shipComp=43;cls=BIOLOGY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(43);
		technology.obsoletesShipComponent(42);//always use the best colony module available		 
		universe.addTechnology(technology);
		id=75;name=COLONY_MODULE_V;descr="We could establish small colonies even on the most demanding planets. With terraforming, these planets can be improved later.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=44;cls=BIOLOGY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(44);  //XXX: test
		technology.obsoletesShipComponent(43); //always use the best colony module available
		universe.addTechnology(technology);


		id=61;name="Trade ship";descr="A small independent trading ship would be very beneficial to our economy.";
		known=false;study=false;childtech1=62;childtech2=0;building=0;prod=9;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		


		id=88;name="Fuel scoop";descr="A fuel scoop device would enable our ships to travel without refueling stops.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=49;cls=PHYSICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		
		//Buildings
		id=8;name="Ore mine I";descr="Research ore mining technologies.";
		known=true;study=false;childtech1=9;childtech2=19;building=2;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=9;name="Ore mine II";descr="Let's try to improve the basic ore mining unit.";
		known=false;study=true;childtech1=33;childtech2=0;building=3;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(2);
		universe.addTechnology(technology);
		
		//XXX: tune and re-sequence the terraform projects
		id=10;name=TERRAFORM_FERTILITY;descr="We should research some terraforming technologies. The most efficient would probably be to improve the fertility of a planet.";
		known=false;study=true;childtech1=95;childtech2=77;building=26;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=95;name=TERRAFORM_FLORA;descr="With a little research, we could create a terraforming project to seed a planets biosphere with terrestric plants. ";
		known=false;study=false;childtech1=96;childtech2=0;building=63;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=96;name=TERRAFORM_FAUNA;descr="Research for a terraforming project to introduce terrestric animals into a planets biosphere. ";
		known=false;study=false;childtech1=0;childtech2=0;building=64;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		
		
		id=11;name=TERRAFORM_TEMPERATURE;descr="Research weather and atmospheric terraforming.";
		known=false;study=true;childtech1=13;childtech2=0;building=27;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=13;name=TERRAFORM_ATMOSPHERIC_PRESSURE;descr="Advanced research for weather and atmospheric terraforming.";
		known=false;study=false;childtech1=92;childtech2=94;building=29;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=92;name=TERRAFORM_ATMOSPHERIC_COMPOSITION;descr="This study promises more advances in weather and atmospheric terraforming";
		known=false;study=false;childtech1=97;childtech2=0;building=60;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=97;name=TERRAFORM_WEATHER_CONTROL;descr="With some research we might develop a weather control technology.";
		known=false;study=false;childtech1=0;childtech2=0;building=59;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_5;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=94;name=TERRAFORM_WATER_LAND_RATIO;descr="This research paper promises some improvement in the planetary water/land distribution. Should we do more research, Sir?";
		known=false;study=false;childtech1=12;childtech2=0;building=62;prod=0;shipComp=0;cls=PHYSICS;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=12;name=TERRAFORM_RADIATION;descr="With some research we could design a terraforming device that removes excessive planetary radiation.";
		known=false;study=false;childtech1=93;childtech2=0;building=28;prod=0;shipComp=0;cls=PHYSICS;cost=TECHLEVEL_5;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=93;name=TERRAFORM_GRAVITATION;descr="This interesting paper postulates a way to reduce gravity on high-G colonies. If the basic facts are sound, this could be very promising.";
		known=false;study=false;childtech1=0;childtech2=0;building=61;prod=0;shipComp=0;cls=PHYSICS;cost=TECHLEVEL_6;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	

		id=15;name="Cloning basics";descr="We don't know much about cloning, but it sure is interesting.";
		known=false;study=false;childtech1=16;childtech2=0;building=24;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=16;name="Genetics clinic";descr="This research paper promises advances in genetic medicin. Should we follow this lead, Sir?.";
		known=false;study=false;childtech1=14;childtech2=0;building=25;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			

		id=20;name="Tools factory";descr="We should design some kind of factory to help us produce basic tools.";
		known=false;study=true;childtech1=21;childtech2=0;building=19;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=21;name="Machine factory";descr="A machine factory would improve planetary output.";
		known=false;study=false;childtech1=55;childtech2=0;building=20;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		

		//XXX: MAY obsolete... technology.obsoletesShipComponent(17);
		universe.addTechnology(technology);				
		id=26;name="University";descr="The university improves the research output of a colony.";
		known=false;study=true;childtech1=0;childtech2=0;building=10;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=27;name="Alien Language Centre";descr="This study could enable basic communication with alien beings.";
		known=false;study=false;childtech1=0;childtech2=0;building=18;prod=0;shipComp=0;cls=MATHEMATICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=28;name="Colonist Advisory Bureau";descr="We need to create an institution to support colonists on new and established colonies.";
		known=false;study=true;childtech1=29;childtech2=0;building=15;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=29;name="Cure for Colonist's disease";descr="The feared 'colonist disease' leads to suicides and high productivity losses. This research could lead to a treatment.";
		known=false;study=false;childtech1=31;childtech2=0;building=16;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);

		id=31;name="Psycho-Institute";descr="This paper shows some interesting properties of the human brain. If it lives up to its promises, we should see a big improvement in research and productivity.";
		known=false;study=false;childtech1=30;childtech2=0;building=17;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	

		id=33;name="Ore mine III";descr="With some research, we could build advanced ore mining units.";
		known=false;study=false;childtech1=34;childtech2=0;building=13;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(3);
		universe.addTechnology(technology);
		id=34;name="Ore mine IV";descr="This research paper promises a better performance in our ore mining units.";
		known=false;study=false;childtech1=0;childtech2=0;building=14;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(13);
		universe.addTechnology(technology);
		id=35;name="Orbital shipyard";descr="We should design an orbital shipyard to build bigger ships. Such a platform could also improve trade.";
		known=true;study=false;childtech1=17;childtech2=0;building=4;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=36;name=BARRACKS;descr="The barracks is the basic military unit.";
		known=true;study=false;childtech1=79;childtech2=0;building=8;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		

		id=38;name=SCHOOL;descr="We should design an institution to provide the basic education for our colonists.";
		known=true;study=false;childtech1=26;childtech2=0;building=9;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=39;name="Battle school";descr="We should create some institution to educate officers, pilots and advanced soldiers.";
		known=false;study=false;childtech1=78;childtech2=0;building=7;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=40;name=FACTORY;descr="We should create a simple factory unit to improve production on our colonies.";
		known=true;study=false;childtech1=0;childtech2=0;building=1;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			

		id=43;name="Genetical Analysis";descr="We should analyze the genetical structure of our population. \n\nThis could be the first step for advanced medical technologies.";
		known=false;study=true;childtech1=15;childtech2=0;building=0;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);

		id=47;name="Banking";descr="This research is the foundation for banking, trade, money, exchange.";
		known=false;study=true;childtech1=61;childtech2=0;building=5;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=48;name=LIBRARY;descr="A library is the foundation for a good education system.";
		known=true;study=false;childtech1=0;childtech2=0;building=8;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=49;name="Religion";descr="Religion is an important part of every culture.";
		known=true;study=false;childtech1=0;childtech2=0;building=11;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
	
		id=55;name="Electronics factory";descr="We should design a factory for electronics products to increase our high-tech output.";
		known=false;study=false;childtech1=56;childtech2=0;building=21;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=56;name="Robot factory";descr="With the technology advances so far, we could design an automated factory.";
		known=false;study=false;childtech1=0;childtech2=0;building=22;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=62;name="Trade union";descr="This study shows the possible benefits of some kind of trade union. Should we research more, Sir?.";
		known=false;study=false;childtech1=63;childtech2=0;building=0;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		id=63;name="Traders Association";descr="This study shows the possible benefits of some kind of traders association. Do you want us to research this, Sir?";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=0;cls=ECONOMY;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=64;name="Linguistics Study";descr="Studies in language and behaviour show a possibility of communication with aliens.";
		known=false;study=true;childtech1=27;childtech2=0;building=0;prod=0;shipComp=0;cls=MATHEMATICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=76;name="Outpost";descr="Even in sun-systems without planets me might want to build some kind of outpost base.";
		known=false;study=false;childtech1=0;childtech2=0;building=45;prod=0;shipComp=45;cls=BIOLOGY;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=77;name="Chemical plant";descr="We should design a factory to increase our production of chemical products.";
		known=false;study=false;childtech1=0;childtech2=0;building=49;prod=0;shipComp=0;cls=BIOLOGY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		id=78;name="Strategy school";descr="We should create an institution to train ship and fleet commanders.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	
		id=79;name="Military school";descr="We should create an institution for the basic eduction of our officers.";
		known=false;study=true;childtech1=39;childtech2=0;building=0;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=80;name="Colony Management";descr="Should we research this proposed instiution for colony management?";
		known=true;study=false;childtech1=81;childtech2=0;building=52;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=81;name="Management AI";descr="This research proposal shows interesting possibilities in the Artificial Intelligence area.";
		known=false;study=true;childtech1=82;childtech2=0;building=0;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=82;name="Advanced Colony Management";descr="We should do some research to improve our colony management.";
		known=false;study=false;childtech1=0;childtech2=0;building=57;prod=0;shipComp=0;cls=SOCIAL;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=84;name=WEAPONS_FACTORY;descr="We should design a weapons factory to produce handguns and rifles, ammunition and torpedos.";
		known=true;study=false;childtech1=0;childtech2=0;building=58;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);
		id=85;name=DEFENSE_BATTERY;descr="We should design some basic ground defense battery.";
		known=true;study=false;childtech1=100;childtech2=0;building=38;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);	

		id=100;name=DEFENSE_BATTERY_2;descr="We recommend to research an improved ground defense unit.";
		known=false;study=false;childtech1=101;childtech2=0;building=66;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(38);
		universe.addTechnology(technology);		
		id=101;name=DEFENSE_BATTERY_3;descr="We should improve our ground defense units.";
		known=false;study=false;childtech1=0;childtech2=0;building=67;prod=0;shipComp=0;cls=MILITARY;cost=TECHLEVEL_0;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		technology.obsoletesBuilding(66);
		universe.addTechnology(technology);		
		
		id=50;name="Force fields";descr="We should research force field technology. This is the base technology for shielding.";
		known=false;study=true;childtech1=51;childtech2=88;building=0;prod=0;shipComp=0;cls=MATHEMATICS;cost=TECHLEVEL_1;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);				
		id=51;name=SHIELDING;descr="With more studies in the force field area, we may be able to build some shielding for our ships.";
		known=false;study=false;childtech1=102;childtech2=0;building=0;prod=0;shipComp=16;cls=MATHEMATICS;cost=TECHLEVEL_2;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=102;name=BASIC_SHIELDING;descr="With more studies in the force field area, we may be able to improve the shielding units for our ships.";
		known=false;study=false;childtech1=103;childtech2=0;building=0;prod=0;shipComp=30;cls=MATHEMATICS;cost=TECHLEVEL_3;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=103;name=PLANETARY_SHIELDING;descr="Some researchers think that even planets could be protected with an artificial force field.";		
		known=false;study=false;childtech1=104;childtech2=0;building=12;prod=0;shipComp=0;cls=MATHEMATICS;cost=TECHLEVEL_4;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);			
		id=104;name=IMPROVED_SHIELDING;descr="With more studies in the force field area, we may be able to improve the shielding units for our ships.";
		known=false;study=false;childtech1=99;childtech2=0;building=0;prod=0;shipComp=56;cls=MATHEMATICS;cost=TECHLEVEL_5;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);					
		id=99;name=ADVANCED_PLANETARY_SHIELDING;descr="With more research we should be able to build better artificial force field to protect our planets.";		
		known=false;study=false;childtech1=105;childtech2=0;building=65;prod=0;shipComp=0;cls=MATHEMATICS;cost=TECHLEVEL_6;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		id=105;name=ADVANCED_SHIELDING;descr="With more studies in the force field area, we may be able to further improve the shielding units for our ships.";
		known=false;study=false;childtech1=0;childtech2=0;building=0;prod=0;shipComp=57;cls=MATHEMATICS;cost=TECHLEVEL_7;picture="tech";
		technology= new Technology(id,name,cls,descr,known,study,childtech1,childtech2,building,prod,shipComp,cost,picture);
		universe.addTechnology(technology);		
		//next tech id = 106
		

//XXX: put the technology-descriptions into sequence, add follow-up technologies where useful, maybe add 'dummy' technologies

//XXX: randomize the known technologies at game start (depending on difficulty level?)
		
// XXX: More technologies, changes to technologies:
//**************************************************
// Social
// Project Open Mind
// Departement of Information (spy information)
// Holiday centre             (money, happiness)
// Dream control              (-> Dream castle)
// Pilgrims (Buildings, Paths, Monuments, Wonders)
// Social Security
// Welfare Plan
// 401 K
//
// Biology:
// Terraforming Tech: (Radiation, Climate, Weather, Air, Water, fertility, flora, fauna, temperature, toxic)
// Terraforming Radiation (-100 mRem)
// Terraforming Climate (- 0.2 units)
// Terraforming improve fertility
// Terraforming add fertility
// Terraforming add flora
// Terraforming add fauna
// Terraforming:rise/lower temperature
// Terraforming de-tox
// Bio Coating of surfaces
// Bio Solar Cells
// Water cleaning, Freshwater production
// Hydroponics plant, factory, tower
// Terraform robots
// Terraform platform
//
// Physics:
// Fusion
// Fuel cells II, III, IV
// Tractor beams             (-> Military component: tractor beams)
// Mass driver
// Particle beam
// Warp laser
// Pulse laser
// Phased laser
// Focussed multiple laser (2,3,5)
// Ship/Station for outpost in systems without planets
// time freeze container (transport of spoilable goods, e.g. colonists) ???
// new research tree: miniaturization projects (random miniaturization) ?
// gravitation bombs/torpedos
//
// Economy
// Resource Market     (-> Orbital Resource Market Building)
// Foreign Trade agreement
// Orion Trade Market        (-> Orion Trade Market Building)
// Pollution control
// Pollution Agreement
// Pollution elimination
// Trade routes
// Free Traders Association  (-> Free Traders Association Building)
// Departement of Mutual Benefit
// Resource exchange program
// Mathematics:
// Advanced communication
// Unlimited energy formula
// Matter conversion (for drives and weapons)
// Energy bundling (for more efficient lasers)
  }
}