package spaceopera.test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import spaceopera.gui.components.ImageCanvas;
import spaceopera.gui.controls.SoButton;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.PlanetDetail;
import spaceopera.gui.window.ShipMiniInfo;
import spaceopera.gui.window.ShipOrders;
import spaceopera.gui.window.helper.UiManager;
import spaceopera.gui.window.list.ListPanel;
import spaceopera.gui.window.settings.NewGameSettings;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.elements.SunSystem;
import spaceopera.universe.ships.SpaceCraft;

/**
 * the 'Fullscreen' class is the main window and container for<br>
 * -universe<br>
 * -sunsystem<br>
 * -orbital view<br>
 * -...<br>
 * <br>
 * 
 * TODO refactor 'server' stuff
 * 
 */
@SuppressWarnings({ "rawtypes" })
public class Fullscreen extends JFrame implements SOConstants {

	private static final long serialVersionUID = 1L;
	private SunSystem sunSystem; // TOP LEFT
	private Universe universe; // CENTER

	// left side
	private JPanel leftPanel;
	private JPanel leftTopTitle;
	private ImageCanvas titleImage;
	private JPanel sunsystemControl;
	private JPanel leftTopSunsystem;
	private JPanel planetControl;
	private JPanel leftCenterTopPlanet;
	private ImageCanvas planetImage;
	private JPanel colonyControl;
	private JPanel leftCenterBottomColony;
	private ImageCanvas colonyImage;
	private JPanel shipControl;
	private JPanel leftBottomShip;
	private JPanel leftBottomFiller;
	// center
	private JPanel centerPanel;
	// TODO private JPanel menuPanel;
	private JPanel mapPanel;
	private JPanel nextButtonPanel;
	// right side
	private JPanel rightPanel;
	private ListPanel rightTopListPanel;
	private JPanel rightCenterDetail;
	private JPanel rightBottomNews;

	private JLabel shipLabel;
	private JLabel colonyLabel;
	private JLabel planetLabel;
	private JLabel systemLabel;

	// TODO popup windows for now
	private ShipOrders shipOrders;
	private ColonyDetail colonyDetail;

	@SuppressWarnings("deprecation")
	public Fullscreen() {

		UiManager.set();

		universe = new Universe(this);
		universe.generate();

		Toolkit tk = Toolkit.getDefaultToolkit();
		int xSize = ((int) tk.getScreenSize().getWidth());
		int ySize = ((int) tk.getScreenSize().getHeight());

		// Left (West) Panel: Sunsystem, Orbit(Planet), Colony(Surface), Ship
		// X: 320
		leftPanel = new JPanel();
		leftPanel.setBackground(Color.gray);
		BoxLayout leftLayout = new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS);
		leftPanel.setLayout(leftLayout);

		leftTopTitle = new JPanel();
		leftTopTitle.setLayout(new BoxLayout(leftTopTitle, BoxLayout.PAGE_AXIS));
		leftTopTitle.setBackground(Color.gray);
		leftTopTitle.setPreferredSize(new Dimension(320, 70));
		titleImage = new ImageCanvas(this, "resources/images/so_title.jpg");
		titleImage.setBounds(0, 0, 320, 70);
		titleImage.init();
		titleImage.display();
		leftTopTitle.add(titleImage);

		sunsystemControl = getSunSystemControl();
		leftTopSunsystem = new JPanel();
		leftTopSunsystem.setLayout(new GridLayout(1, 1, 0, 0));
		leftTopSunsystem.setBackground(Color.black);
		leftTopSunsystem.setPreferredSize(new Dimension(SUNSYSTEMFRAMEX, 260));
		Sun s = new Sun("Empty", this);
		SunSystem ss = new SunSystem(s, this);
		ss.generate(0);
		leftTopSunsystem.add(ss.getSunSystemDisplay());
		leftTopSunsystem.revalidate();

		planetControl = getPlanetControl();
		leftCenterTopPlanet = new JPanel();
		leftCenterTopPlanet.setLayout(new BoxLayout(leftCenterTopPlanet, BoxLayout.PAGE_AXIS));
		leftCenterTopPlanet.setBackground(Color.black);
		leftCenterTopPlanet.setPreferredSize(new Dimension(320, 240));
		planetImage = new ImageCanvas(this, "resources/images/empty.jpg");
		leftCenterTopPlanet.add(planetImage);
		planetImage.setBounds(0, 0, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
		planetImage.init();
		planetImage.display();

		colonyControl = getColonyControl();
		leftCenterBottomColony = new JPanel();
		leftCenterBottomColony.setLayout(new BoxLayout(leftCenterBottomColony, BoxLayout.PAGE_AXIS));
		leftCenterBottomColony.setBackground(Color.blue);
		leftCenterBottomColony.setPreferredSize(new Dimension(320, 240));
		colonyImage = new ImageCanvas(this, "resources/images/empty.jpg");
		colonyImage.setBounds(0, 0, 320, 240);
		colonyImage.init();
		colonyImage.display();
		leftCenterBottomColony.add(colonyImage);

		shipControl = getShipControl();
		leftBottomShip = new JPanel();
		leftBottomShip.setBackground(Color.black);
		leftBottomShip.setPreferredSize(new Dimension(320, 150));

		leftBottomFiller = new JPanel();
		leftBottomFiller.setBackground(Color.black);
		leftBottomFiller.setPreferredSize(new Dimension(320, 20));

		leftPanel.add(leftTopTitle);
		// leftPanel.add(Box.createVerticalGlue());
		leftPanel.add(sunsystemControl);
		leftPanel.add(leftTopSunsystem);
		leftPanel.add(Box.createVerticalGlue());
		leftPanel.add(planetControl);
		leftPanel.add(leftCenterTopPlanet);
		leftPanel.add(Box.createVerticalGlue());
		leftPanel.add(colonyControl);
		leftPanel.add(leftCenterBottomColony);
		leftPanel.add(Box.createVerticalGlue());
		leftPanel.add(shipControl);
		leftPanel.add(leftBottomShip);
		leftPanel.add(Box.createVerticalGlue());
		leftPanel.add(leftBottomFiller);

		// Center Panel Map and others
		// X: 900
		centerPanel = new JPanel();
		BoxLayout centerLayout = new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS);
		centerPanel.setLayout(centerLayout);

		mapPanel = new JPanel();
		mapPanel.setBackground(Color.black);
		mapPanel.setPreferredSize(new Dimension(900, 900));

		// TODO brauchts da noch ein JScrollPane?
		// JScrollPane jsp = new JScrollPane(universe.getUniverseDisplay(),
		// JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
		// JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//
		// // jsp.setViewportView(universe.getUniverseDisplay());
		// jsp.getVerticalScrollBar().setUnitIncrement(10);
		// jsp.getHorizontalScrollBar().setUnitIncrement(10);
		mapPanel.add(universe.getUniverseDisplay());

		nextButtonPanel = new JPanel();
		nextButtonPanel.setBackground(Color.DARK_GRAY);
		nextButtonPanel.setPreferredSize(new Dimension(900, 180));

		centerPanel.add(mapPanel);
		centerPanel.add(nextButtonPanel);

		// Right (East) Panel: Colony Detail, Research, News/Log
		rightPanel = new JPanel();
		rightPanel.setBackground(Color.green);
		BoxLayout rightLayout = new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS);
		rightPanel.setLayout(rightLayout);

		rightTopListPanel = new ListPanel(this);
		rightTopListPanel.setBackground(Color.black);
		rightTopListPanel.setPreferredSize(new Dimension(460, 350));
		rightTopListPanel.setLayout(new GridLayout(1, 1, 0, 0));

		rightCenterDetail = new JPanel();
		rightCenterDetail.setBackground(Color.gray);
		rightCenterDetail.setPreferredSize(new Dimension(460, 400));
		rightCenterDetail.setLayout(new GridLayout(1, 1, 0, 0));

		rightBottomNews = new JPanel();
		rightBottomNews.setBackground(Color.blue);
		rightBottomNews.setPreferredSize(new Dimension(460, 330));

		rightPanel.add(rightTopListPanel);
		rightPanel.add(rightCenterDetail);
		rightPanel.add(Box.createVerticalGlue());
		rightPanel.add(rightBottomNews);
		rightPanel.add(Box.createVerticalGlue());

		getContentPane().add("West", leftPanel);
		getContentPane().add("Center", centerPanel);
		getContentPane().add("East", rightPanel);

		setSize(xSize, ySize);
		setUndecorated(true);
		pack();
		show();

		@SuppressWarnings("unused")
		NewGameSettings newGameSettings = new NewGameSettings(this);

	}

	public static void main(String[] args) {
		// System.getProperties().setProperty("awt.useSystemAAFontSettings",
		// "lcd");
		// System.getProperties().setProperty("awt.useSystemAAFontSettings",
		// "off");
		// System.setProperty("swing.aatext", "false");

		Fullscreen fs = new Fullscreen();

	}

	public void initiateNewGame() {
		Sun sun = null;
		SunSystem s = null;
		List suns = null;
		universe.setFs(this);
		universe.setUniverseSize();
		universe.clear();
		universe.generate();
		suns = universe.getSuns();
		sun = (Sun) suns.get(0);
		sun.setSelected(true);
		s = sun.getSunSystem();

		// TODO for Test purposes only:
		// ???
		// for (int k = 0; k < suns.size(); k++) {
		// Sun x = (Sun) universe.getSuns().get(k);
		// SunSystem ss = x.getSunSystem();
		// ss.addExploredByPlayer("Humans");
		// for (int i = 0; i < ss.getSun().getPlanets().size(); i++) {
		// Planet planet = (Planet) ss.getSun().getPlanets().elementAt(i);
		// planet.addExploredByPlayer("Humans");
		// if (!planet.isColonized() && EARTHLIKE.equals(planet.getType())) {
		// planet.colonize(universe.getPlayer());
		// }
		// universe.getPlayer().addPlanet(planet);
		// }
		// }
		// end of TODO

		// TODO universe size depending on screen resolution
		universe.getUniverseDisplay().setSize(900, 900);
		universe.display();
		if (universe.getPlayer().getColonies().size() > 0) {
			Colony c = universe.getPlayer().getColonies().get(0);
			c.getPlanet().setSelected(true);
			updateSunSystem(c.getSunSystem(), null);
			displayPlanetImage(c.getPlanet());
			displayColonyImage(c);
		} else {
			updateSunSystem(s, null);
		}
		if (universe.getPlayer().getShips().size() > 0) {
			displayNextShip();
		}
		refreshOpenWindows();
	}

	private void refreshOpenWindows() {

		refreshColonyTable();
		refreshPlanetTable();
		refreshShipTable();
		refreshSystemTable();
		refreshShipDesignTable();
		refreshTechnologyTable();

		// TODO refreshOpenWindows
		// if (colonyDetail != null) {
		// colonyDetail.refresh();
		// }
		// if (colonyResources != null) {
		// colonyResources.refresh();
		// }
		// // if (planetDetail!=null) {planetDetail.refresh();}
		// if (scienceDetail != null) {
		// scienceDetail.refresh();
		// }
		// if (shipYard != null) {
		// shipYard.refresh();
		// }
		// if (shipOrders != null) {
		// shipOrders.refresh();
		// }
		// if (shipList != null) {
		// shipList.refresh();
		// }
		// if (sunSystemList != null) {
		// sunSystemList.refresh();
		// }
		// if (colonyList != null) {
		// colonyList.refresh();
		// }
		// if (planetList != null) {
		// planetList.refresh();
		// }
		// if (shipMiniInfo != null) {
		// shipMiniInfo.refresh();
		// }
	}

	public void refreshShipTable() {
		rightTopListPanel.refreshShipTable();
		rightTopListPanel.revalidate();
	}

	public void refreshPlanetTable() {
		rightTopListPanel.refreshPlanetTable();
	}

	public void refreshColonyTable() {
		rightTopListPanel.refreshColonyTable();
	}

	public void refreshSystemTable() {
		rightTopListPanel.refreshSystemTable();
	}

	public void refreshShipDesignTable() {
		rightTopListPanel.refreshShipDesignTable();
		rightTopListPanel.revalidate();
	}

	public void refreshTechnologyTable() {
		rightTopListPanel.refreshTechnologyTable();
		rightTopListPanel.revalidate();
	}

	public void refreshShipInfo(SpaceCraft s) {
		shipLabel.setText("Ship: " + s.getShipName());
		leftBottomShip.removeAll();
		ShipMiniInfo ship = new ShipMiniInfo(s, this);
		ship.setShipLayout(leftBottomShip);
		ship.init();
		leftBottomShip.revalidate();
		refreshShipTable();
	}

	public void updateSunSystem(SunSystem s, Planet p) {
		systemLabel.setText("System: " + s.getName());
		leftTopSunsystem.removeAll();
		leftTopSunsystem.add(s.getSunSystemDisplay());
		leftTopSunsystem.revalidate();
		repaint();
	}

	public boolean allowColonyDetail() {
		// TODO Auto-generated method stub
		return false;
	}

	public void refreshPlanetLabel(Planet currentPlanet) {
		if (currentPlanet != null) {
			planetLabel.setText("Planet: " + currentPlanet.getName());
		}
	}

	public void refreshColonyLabel(Colony currentColony) {
		if (currentColony != null) {
			colonyLabel.setText("Colony: " + currentColony.getName());
		}
	}

	public void displayPlanetImage(Planet currentPlanet) {

		// List suns = universe.getSuns();
		// TODO not here???
		// for (int i = 0; i < suns.size(); i++) {
		// Sun s = (Sun) suns.get(i);
		// s.setSelected(false);
		// }

		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.get(i);
			planet.setSelected(false);
			planet.setSunSelected(false);
		}

		if (currentPlanet != null) {
			planetLabel.setText("Planet: " + currentPlanet.getName());
			currentPlanet.setSelected(true);
			currentPlanet.setSunSelected(true);
			player.setCurrentPlanet(currentPlanet);
			updateSunSystem(currentPlanet.getSunSystem(), currentPlanet);
			planetImage.setImage(this, "resources/images/bodies/" + currentPlanet.getImage());
			displayPlanetDetail();
		} else {
			planetLabel.setText("Planet: none");
			planetImage.setImage(this, "resources/images/empty.jpg");
		}
		planetImage.setBounds(0, 0, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
		planetImage.init();
		planetImage.display();

	}

	public void displayColonyImage(Colony colony) {

		if (colony != null) {
			colonyLabel.setText("Colony: " + colony.getName());
			universe.selectSun(colony.getSun());
			Player player = universe.getPlayer();
			colony.setPlanetSelected(true);
			colony.setSunSelected(true);
			player.setCurrentColony(colony);
			updateSunSystem(colony.getSunSystem(), colony.getPlanet());
			colonyImage.setImage(this, "resources/images/colonies/" + colony.getImage());
			colonyImage.setBounds(0, 0, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
			colonyImage.init();
			colonyImage.display();
			// TODO colony detail stuff
			// if (colonyDetail != null) {
			// colonyDetail.dispose();
			// colonyDetail = null;
			// }
			displayColonyDetail(colony);
		} else {
			// no colony
			colonyLabel.setText("Colony: none");
			colonyImage.setImage(this, "resources/images/empty.jpg");
			colonyImage.setBounds(0, 0, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
			colonyImage.init();
			colonyImage.display();
		}
	}

	public Player getCurrentPlayer() {
		return (universe.getPlayer());
	}

	public String getCurrentPlayerName() {
		return (universe.getPlayerName());
	}

	public SunSystem getSunSystem() {
		return sunSystem;
	}

	public Universe getUniverse() {
		return universe;
	}

	public void setUniverse(Universe universe) {
		this.universe = universe;
	}

	// new game settings
	public void setGalaxySize(int i) {
		universe.setGalaxySize(i);
	}

	public void setDifficultyLevel(int i) {
		universe.setDifficultyLevel(i);
	}

	public void setNumberOfOpponents(int i) {
		universe.setNumberOfOpponents(i);
	}

	public void setRandomPlayers(boolean b) {
		universe.setRandomPlayers(b);
	}

	public void setReplaceEliminated(boolean b) {
		universe.setReplaceEliminated(b);
	}

	public void setPlayerName(String s) {
		universe.setPlayerName(s);
	}

	// TODO move to controller class
	private JPanel getSunSystemControl() {
		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);
		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.LINE_AXIS));
		control.setBackground(Color.BLACK);

		JPanel controlLeft = new JPanel();
		controlLeft.setLayout(left);
		controlLeft.setForeground(Color.YELLOW);
		controlLeft.setBackground(Color.BLACK);
		systemLabel = new JLabel("System: none");
		systemLabel.setForeground(Color.yellow);
		systemLabel.setBackground(Color.BLACK);
		systemLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlLeft.add(systemLabel);
		SoButton previous = new SoButton("Previous");
		previous.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPreviousSystem();
			}
		});
		SoButton next = new SoButton("Next");
		next.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayNextSystem();
			}
		});

		JPanel controlRight = new JPanel();
		controlRight.setLayout(right);
		controlRight.setForeground(Color.YELLOW);
		controlRight.setBackground(Color.BLACK);

		// controlRight.add(new SoButton(""));
		controlRight.add(Box.createHorizontalGlue());
		controlRight.add(previous);
		controlRight.add(next);
		control.add(controlLeft);
		control.add(Box.createHorizontalGlue());
		control.add(controlRight);
		return control;
	}

	// TODO move to controller class
	private JPanel getPlanetControl() {
		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);

		SoButton planetDetail = new SoButton("Planet Detail");
		planetDetail.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPlanetDetail();
			}
		});
		SoButton previous = new SoButton("Previous");
		previous.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPreviousPlanet();
			}
		});
		SoButton next = new SoButton("Next");
		next.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayNextPlanet();
			}
		});

		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.LINE_AXIS));
		control.setBackground(Color.BLACK);

		JPanel controlLeft = new JPanel();
		controlLeft.setLayout(left);
		controlLeft.setForeground(Color.YELLOW);
		controlLeft.setBackground(Color.BLACK);
		planetLabel = new JLabel("Planet: none");
		planetLabel.setForeground(Color.yellow);
		planetLabel.setBackground(Color.BLACK);
		planetLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlLeft.add(planetLabel);

		JPanel controlRight = new JPanel();
		controlRight.setLayout(right);
		controlRight.setForeground(Color.YELLOW);
		controlRight.setBackground(Color.BLACK);

		controlRight.add(planetDetail);
		controlRight.add(Box.createHorizontalGlue());
		controlRight.add(previous);
		controlRight.add(next);
		control.add(controlLeft);
		control.add(Box.createHorizontalGlue());
		control.add(controlRight);
		return control;
	}

	// TODO move to controller class
	private JPanel getColonyControl() {
		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);

		SoButton colonyDetail = new SoButton("Colony Detail");
		colonyDetail.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO what is the current colony?
				displayColonyDetail(universe.getPlayer().getCurrentColony());
			}
		});

		SoButton previous = new SoButton("Previous");
		previous.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPreviousColony();
			}
		});
		SoButton next = new SoButton("Next");
		next.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayNextColony();
			}
		});

		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.LINE_AXIS));
		control.setBackground(Color.black);

		JPanel controlLeft = new JPanel();
		controlLeft.setLayout(left);
		controlLeft.setForeground(Color.YELLOW);
		controlLeft.setBackground(Color.BLACK);
		colonyLabel = new JLabel("Colony: none");
		colonyLabel.setForeground(Color.yellow);
		colonyLabel.setBackground(Color.BLACK);
		colonyLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlLeft.add(colonyLabel);

		JPanel controlRight = new JPanel();
		controlRight.setLayout(right);
		controlRight.setForeground(Color.YELLOW);
		controlRight.setBackground(Color.BLACK);
		controlRight.add(colonyDetail);
		controlRight.add(Box.createHorizontalGlue());
		controlRight.add(previous);
		controlRight.add(next);

		control.add(controlLeft);
		control.add(Box.createHorizontalGlue());
		control.add(controlRight);
		return control;
	}

	// TODO move to controller class
	private JPanel getShipControl() {
		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);

		SoButton shipOrders = new SoButton("Ship Orders");
		shipOrders.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayShipOrders();
			}
		});
		SoButton previous = new SoButton("Previous");
		previous.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPreviousShip();
				displayShipOrders();
			}
		});
		SoButton next = new SoButton("Next");
		next.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayNextShip();
				displayShipOrders();
			}
		});

		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.LINE_AXIS));
		control.setBackground(Color.BLACK);

		JPanel controlLeft = new JPanel();
		controlLeft.setLayout(left);
		controlLeft.setForeground(Color.YELLOW);
		controlLeft.setBackground(Color.black);
		controlLeft.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		shipLabel = new JLabel("Ship: none");
		shipLabel.setForeground(Color.yellow);
		shipLabel.setBackground(Color.BLACK);
		shipLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlLeft.add(shipLabel);

		JPanel controlRight = new JPanel();
		controlRight.setLayout(right);
		controlRight.setForeground(Color.YELLOW);
		controlRight.setBackground(Color.BLACK);
		controlRight.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlRight.add(shipOrders);
		controlRight.add(Box.createHorizontalGlue());
		controlRight.add(previous);
		controlRight.add(next);
		control.add(controlLeft);
		control.add(Box.createHorizontalGlue());
		control.add(controlRight);
		return control;
	}

	// TODO refactor move to controller class
	public void displayNextSystem() {
		Sun sun = null;
		Graphics g = universe.getGraphics();
		List suns = universe.getSuns();
		boolean selectedOne = false;
		for (int i = 0; i < suns.size(); i++) {
			sun = (Sun) suns.get(i);
			if (sun.getSelected()) {
				selectedOne = true;
				sun.setSelected(false);
				if (i < suns.size() - 1) {
					// the next one
					sun = (Sun) suns.get(i + 1);
					sun.setSelected(true);
					break;
				} else {
					// the first one
					sun = (Sun) suns.get(0);
					sun.setSelected(true);
					break;
				}
			}
		}
		if (!selectedOne) {
			// no sun
			sun = (Sun) suns.get(0);
			sun.setSelected(true);
		}
		updateSunSystem(sun.getSunSystem(), null);
		if (sun.isExploredBy(universe.getPlayerName()) && sun.getSunSystem().getPlanets().size() > 0) {
			Planet planet = (Planet) sun.getSunSystem().getPlanets().elementAt(0);
			displayPlanetImage(planet);
			universe.getPlayer().setCurrentPlanet(planet);
			hasColony(planet);
		} else {
			displayPlanetImage(null);
			displayColonyImage(null);
		}
		sun.display(g);
		// universe.display();
		universe.getUniverseDisplay().revalidate();
	}

	public void displayPreviousSystem() {
		Sun sun = null;
		Graphics g = universe.getGraphics();
		List suns = universe.getSuns();
		boolean selectedOne = false;
		for (int i = suns.size() - 1; i >= 0; i--) {
			sun = (Sun) suns.get(i);
			if (sun.getSelected()) {
				sun.setSelected(false);
				selectedOne = true;
				if (i > 0) {
					// the previous one
					sun = (Sun) suns.get(i - 1);
					universe.selectSun(sun);
					break;
				} else {
					// the last one
					sun = (Sun) suns.get(suns.size() - 1);
					sun.setSelected(true);
					universe.selectSun(sun);
					break;
				}
			}
		}
		if (!selectedOne) {
			// no sun
			sun = (Sun) suns.get(0);
			sun.setSelected(true);
		}
		updateSunSystem(sun.getSunSystem(), null);
		if (sun.isExploredBy(universe.getPlayerName()) && sun.getSunSystem().getPlanets().size() > 0) {
			Planet planet = (Planet) sun.getSunSystem().getPlanets().elementAt(0);
			displayPlanetImage(planet);
			universe.getPlayer().setCurrentPlanet(planet);
			hasColony(planet);
		} else {
			displayPlanetImage(null);
			displayColonyImage(null);
		}
		sun.display(g);
		// universe.display();
		universe.getUniverseDisplay().revalidate();
	}

	private void hasColony(Planet planet) {
		Colony colony = planet.getColony();
		if (colony != null && colony.getPlayerName().equals(universe.getPlayerName())) {
			displayColonyImage(colony);
		} else {
			displayColonyImage(null);
		}
	}

	// TODO Test logic after exploration
	private void displayPreviousPlanet() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.setSelected(false);
		}
		// display next planet
		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		Planet curPlanet = player.getCurrentPlanet();
		Planet planet = null;
		boolean none = true;
		if (planets != null) {
			for (int i = 0; i < planets.size(); i++) {
				planet = (Planet) planets.get(i);
				planet.setSelected(false);
				planet.setSunSelected(false);
				if (planet == curPlanet) {
					none = false;
					// take previous
					if (i > 0) {
						planet = (Planet) planets.get(i - 1);
					} else {
						planet = (Planet) planets.get(planets.size() - 1);
					}
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					displayPlanetImage(planet);
					hasColony(planet);
					break;
				}
			}
			if (none) {
				// no Current Planet yet
				if (planets.size() > 0) {
					planet = (Planet) planets.get(0);
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					displayPlanetImage(planet);
					hasColony(planet);
				}
			}
			displayPlanetDetail();
		}
	}

	// TODO Test logic after exploration
	private void displayNextPlanet() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.setSelected(false);
		}
		// display next planet
		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		Planet curPlanet = player.getCurrentPlanet();
		Planet planet = null;
		boolean none = true;
		if (planets != null) {
			for (int i = 0; i < planets.size(); i++) {
				planet = (Planet) planets.get(i);
				planet.setSelected(false);
				planet.setSunSelected(false);
				if (planet.equals(curPlanet)) {
					if (i < planets.size() - 1) {
						none = false;
						planet = (Planet) planets.get(i + 1);
						planet.setSelected(true);
						planet.setSunSelected(true);
						player.setCurrentPlanet(planet);
						// System.out.println("Sunsystem: " +
						// planet.getSunSystem().getName());
						updateSunSystem(planet.getSunSystem(), planet);
						displayPlanetImage(planet);
						hasColony(planet);
						break;
					}
				}
			}
			if (none) {
				if (planets.size() > 0) {
					planet = (Planet) planets.get(0);
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					displayPlanetImage(planet);
					hasColony(planet);
				}
			}
			displayPlanetDetail();
		}
	}

	private void displayPreviousColony() {
		// clear all marks first
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.setSelected(false);
		}
		// display previous colony
		Player player = universe.getPlayer();
		Colony currentColony = player.getCurrentColony();
		Colony colony = null;
		boolean none = true;
		List<Colony> colonies = player.getColonies();
		for (int i = colonies.size() - 1; i >= 0; i--) {
			colony = colonies.get(i);
			colony.setPlanetSelected(false);
			colony.setSunSelected(false);
			if (colony == currentColony) {
				none = false;
				if (i > 0) {
					// take the previous one
					colony = player.getColonies().get(i - 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					displayPlanetImage(colony.getPlanet());
					displayColonyImage(colony);
					break;
				} else {
					// take the last one
					colony = colonies.get(player.getColonies().size() - 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					displayPlanetImage(colony.getPlanet());
					displayColonyImage(colony);
					break;
				}
			}
			displayColonyDetail(colony);
		}
		if (none) {
			// no Current Colony yet
			if (colonies.size() > 0) {
				colony = colonies.get(0);
				colony.setPlanetSelected(true);
				colony.setSunSelected(true);
				player.setCurrentColony(colony);
				updateSunSystem(colony.getSunSystem(), colony.getPlanet());
				displayPlanetImage(colony.getPlanet());
				displayColonyImage(colony);
			}
			displayColonyDetail(colony);
		}
	}

	private void displayNextColony() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.setSelected(false);
		}
		Player player = universe.getPlayer();
		Colony currentColony = player.getCurrentColony();
		Colony colony = null;
		boolean none = true;
		List<Colony> colonies = player.getColonies();
		for (int i = 0; i < colonies.size(); i++) {
			colony = colonies.get(i);
			colony.setPlanetSelected(false);
			colony.setSunSelected(false);
			if (colony == currentColony) {
				none = false;
				if (i < colonies.size() - 1) {
					// take next
					colony = colonies.get(i + 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					displayPlanetImage(colony.getPlanet());
					displayColonyImage(colony);
					break;
				} else {
					// take the first
					colony = colonies.get(0);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					displayPlanetImage(colony.getPlanet());
					displayColonyImage(colony);
					break;
				}
			}
			displayColonyDetail(colony);
		}
		if (none) {
			// no Current Colony yet
			if (colonies.size() > 0) {
				colony = colonies.get(0);
				colony.setPlanetSelected(true);
				colony.setSunSelected(true);
				player.setCurrentColony(colony);
				updateSunSystem(colony.getSunSystem(), colony.getPlanet());
				displayPlanetImage(colony.getPlanet());
				displayColonyImage(colony);
			}
			displayColonyDetail(colony);
		}
	}

	private void displayPreviousShip() {
		SpaceCraft ship = null;
		Player player = universe.getPlayer();
		boolean ok = false;
		List<SpaceCraft> playerships = player.getShips();
		for (int i = playerships.size() - 1; i >= 0; i--) {
			ship = playerships.get(i);
			if (ship.isSelected()) {
				ship.setSelected(false);
				universe.setCurrentSpaceCraft(ship);
				refreshShipInfo(ship);
				if (i > 0) {
					ship = playerships.get(i - 1);
					ship.setSelected(true);
					ok = true;
					break;
				} else {
					ship = playerships.get(playerships.size() - 1);
					ship.setSelected(true);
					ok = true;
					break;
				}
			}
		}
		if (ok) {
			universe.setCurrentSpaceCraft(ship);
			refreshShipInfo(ship);
		} else if (playerships.size() > 0) {
			ship = playerships.get(0);
			ship.setSelected(true);
			universe.setCurrentSpaceCraft(ship);
			refreshShipInfo(ship);
		}
	}

	private void displayNextShip() {
		SpaceCraft ship = null;
		Player player = universe.getPlayer();
		boolean ok = false;
		List<SpaceCraft> playerships = player.getShips();
		for (int i = 0; i < playerships.size(); i++) {
			ship = playerships.get(i);
			if (ship.isSelected()) {
				ship.setSelected(false);
				universe.setCurrentSpaceCraft(ship);
				refreshShipInfo(ship);
				if (i < playerships.size() - 1) {
					ship = playerships.get(i + 1);
					ship.setSelected(true);
					ok = true;
					break;
				} else {
					ship = playerships.get(0);
					ship.setSelected(true);
					ok = true;
					break;
				}
			}
		}
		if (ok) {
			universe.setCurrentSpaceCraft(ship);
			refreshShipInfo(ship);
		} else if (playerships.size() > 0) {
			ship = playerships.get(0);
			ship.setSelected(true);
			universe.setCurrentSpaceCraft(ship);
			refreshShipInfo(ship);
		}
	}

	private void displayPlanetDetail() {
		PlanetDetail planetDetail = new PlanetDetail(universe.getPlayer().getCurrentPlanet(), this);
		rightCenterDetail.removeAll();
		rightCenterDetail.add(planetDetail);
		rightCenterDetail.revalidate();
	}

	public void displayShipOrders() {
		ShipOrders shipOrders = new ShipOrders(this);
		rightCenterDetail.removeAll();
		rightCenterDetail.add(shipOrders);
		rightCenterDetail.revalidate();
	}

	public void displayColonyDetail(Colony colony) {
		ColonyDetail colonyDetail = new ColonyDetail(colony, this);
		rightCenterDetail.removeAll();
		rightCenterDetail.add(colonyDetail);
		rightCenterDetail.revalidate();
	}

}
