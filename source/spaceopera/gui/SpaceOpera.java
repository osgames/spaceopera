//The class SpaceOpera is the central class and contains the main() method.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui;

import java.awt.Graphics;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.components.SOEvent;
import spaceopera.gui.helper.PlayMusic;
import spaceopera.gui.helper.SOConfig;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.ColonyResources;
import spaceopera.gui.window.FightDetail;
import spaceopera.gui.window.FightDetailVisible;
import spaceopera.gui.window.PlanetDetail;
import spaceopera.gui.window.ScienceDetail;
import spaceopera.gui.window.ShipMiniInfo;
import spaceopera.gui.window.ShipOrders;
import spaceopera.gui.window.ShipYard;
import spaceopera.gui.window.SpaceOperaDisplay;
import spaceopera.gui.window.SunSystemDisplay;
import spaceopera.gui.window.helper.CheatWindow;
import spaceopera.gui.window.helper.HelpWindow;
import spaceopera.gui.window.helper.PlayAnim;
import spaceopera.gui.window.helper.PlayEffect;
import spaceopera.gui.window.helper.ShowGPL;
import spaceopera.gui.window.helper.ShowLicense;
import spaceopera.gui.window.list.ColonyList;
import spaceopera.gui.window.list.PlanetList;
import spaceopera.gui.window.list.ShipList;
import spaceopera.gui.window.list.SunSystemList;
import spaceopera.gui.window.settings.AnimSettings;
import spaceopera.gui.window.settings.EffectSettings;
import spaceopera.gui.window.settings.MusicSettings;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Technology;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.CPlayer;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.elements.SunSystem;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/**
 * Class SpaceOpera is the main class. It contains the Universe, the Sunsystem display the planet display, Menus and the main()
 * Method.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
@Deprecated
public class SpaceOpera implements SOConstants {

	private Universe universe; // Universe: display stars and background
	private SunSystem sunSystem;
	// private Sun s;
	private SpaceOperaDisplay spaceOperaDisplay;
	private ColonyDetail colonyDetail;
	private ColonyResources colonyResources;
	private PlanetDetail planetDetail;
	private ScienceDetail scienceDetail;
	private ShipYard shipYard;
	private ShipOrders shipOrders;
	private ShipList shipList;
	private PlanetList planetList;
	private HelpWindow helpWindow;
	private ShowGPL showGPL;
	private CheatWindow cheatWindow;
	private MusicSettings musicSettings;
	private FightDetailVisible fightDetailVisible;
	private boolean musicOn;
	private PlayMusic playMusic;
	private boolean musicFromDir;
	private String musicDir;
	private Vector musicList;
	private AnimSettings animSettings;
	private boolean animationsOn;
	private EffectSettings effectSettings;
	private boolean effectsOn;
	private PlayEffect playEffect;
	private ShowLicense showLicense;
	private SunSystemList sunSystemList;
	private ColonyList colonyList;
	private SOConfig soConfig;
	private Vector queuedEvents;
	private Vector fights; // list of all FightDetail instances
	private boolean showShipInfo = true;
	private ShipMiniInfo shipMiniInfo;

	// add by SR-040224 for more GameSettings
	// private MenuSettingsOther mnuSetOther;

	private boolean showColonies = false; // some cheats
	private boolean showShips = false;
	private boolean showDirections = false;
	private boolean showScanArea = false;
	private boolean allowColonyDetail = false;
	private boolean scoutAllSuns = false;
	private boolean showBattles = false;

	// Accessors:
	public void addBattle(FightDetailVisible f) {
		fights.addElement(f);
	}

	public void addShipToUniverse(SpaceCraft s) {
		universe.addSpaceCraft(s);
	}

	public void cleanupUniverse() {
		universe.cleanup();
	}

	public void deleteShipFromUniverse(SunSystem sunSystem, SpaceCraft ship) {
		universe.deleteShip(sunSystem, ship);
	}

	public void displayUniverse() {
		universe.display();
	}

	// public void disposeColonyDetail() {
	// colonyDetail.dispose();
	// colonyDetail = null;
	// }

	public boolean allowColonyDetail() {
		return (allowColonyDetail);
	}

	public boolean getAnimations() {
		return animationsOn;
	}

	public String getBuildingName(int building) {
		return (universe.getBuildingName(building));
	}

	public ColonyDetail getColonyDetail() {
		return (colonyDetail);
	}

	public String getComponentName(int comp) {
		return (universe.getComponentName(comp));
	}

	public Player getCurrentPlayer() {
		return (universe.getPlayer());
	}

	public String getCurrentPlayerName() {
		return (universe.getPlayerName());
	}

	public SpaceCraft getCurrentShip() {
		return (universe.getCurrentSpaceCraft());
	}

	public int getDifficultyLevel() {
		return (universe.getDifficultyLevel());
	}

	public boolean getEffects() {
		return effectsOn;
	}

	public String getProductName(int prod) {
		return (universe.getProductName(prod));
	}

	public String getResourceName(int i) {
		return (universe.getResourceName(i));
	}

	public String getResourceUnit(int i) {
		return (universe.getResourceUnit(i));
	}

	public boolean getShowBattles() {
		return (showBattles);
	}

	public boolean getScoutAllSuns() {
		return (scoutAllSuns);
	}

	public boolean getShowColonies() {
		return (showColonies);
	}

	public boolean getShowDirections() {
		return (showDirections);
	}

	public boolean getShowShips() {
		return (showShips);
	}

	public boolean getShowScanArea() {
		return (showScanArea);
	}

	public SpaceOperaDisplay getDisplay() {
		return (spaceOperaDisplay);
	}

	public boolean getMusicState() {
		return (musicOn);
	}

	public String getMusicDirectory() {
		return (musicDir);
	}

	public boolean getMusicFromDir() {
		return (musicFromDir);
	}

	public Vector getMusicList() {
		return (musicList);
	}

	public List getSuns() {
		return (universe.getSuns());
	}

	public String[] getSunNames() {
		return (universe.getSunNames());
	}

	public String[] getUnexploredSunNames(String p) {
		return (universe.getUnexploredSunNames(p));
	}

	public SunSystem getSunSystem() {
		return (sunSystem);
	}

	public SunSystemDisplay getSunSystemDisplay() {
		// return (sunSystem.getSunSystemDisplay());
		return null;
	}

	public String getTechnologyName(int tech) {
		return (universe.getTechnologyName(tech));
	}

	public int getTurnNr() {
		return (universe.getTurnNr());
	}

	public Universe getUniverse() {
		return (universe);
	}

	public void selectSunInUniverse(Sun sun) {
		universe.selectSun(sun);
	}

	public void allowColonyDetail(boolean b) {
		allowColonyDetail = b;
	}

	public void setAnimConfig(boolean b) {
		animationsOn = b;
	}

	public void storeAnimConfig(boolean b) {
		setAnimConfig(b);
		soConfig.setAnimConfig(b);
	}

	public void setEffectConfig(boolean b) {
		effectsOn = b;
	}

	public void storeEffectConfig(boolean b) {
		setEffectConfig(b);
		soConfig.setEffectsConfig(b);
	}

	public void setColonyDetail(ColonyDetail c) {
		colonyDetail = c;
	}

	public void setCoordinates(String s) {
		spaceOperaDisplay.setCoordinates(s);
	}

	public void setDifficultyLevel(int i) {
		universe.setDifficultyLevel(i);
	}

	public void setGalaxySize(int i) {
		universe.setGalaxySize(i);
	}

	public void setMusicConfig(boolean on, boolean dir, String sD, Vector sL) {
		musicOn = on;
		musicFromDir = dir;
		musicDir = sD;
		musicList = sL;
	}

	public void storeMusicConfig(boolean on, boolean dir, String sD, Vector sL) {
		setMusicConfig(on, dir, sD, sL);
		soConfig.setMusicConfig(on, dir, sD, sL);
	}

	public void setNumberOfOpponents(int i) {
		universe.setNumberOfOpponents(i);
	}

	public void setPlayerColony(Colony c) {
		universe.setPlayerColony(c);
	}

	public void setPlayerName(String s) {
		universe.setPlayerName(s);
	}

	public void setRandomPlayers(boolean b) {
		universe.setRandomPlayers(b);
	}

	public void setReplaceEliminated(boolean b) {
		universe.setReplaceEliminated(b);
	}

	public void setShipOrders() {
		showShipOrders();
	};

	public void setShowBattles(boolean b) {
		showBattles = b;
	}

	public void setScoutAllSuns(boolean b) {
		scoutAllSuns = b;
	}

	public void setShowColonies(boolean b) {
		showColonies = b;
	}

	public void setShowDirections(boolean b) {
		showDirections = b;
	}

	public void setShowScanArea(boolean b) {
		showScanArea = b;
	}

	public void setShowShips(boolean b) {
		showShips = b;
	}

	private void advanceColoniesOfAllPlayers() {
		Player player = null;
		Vector players = null;
		players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			player = (Player) players.elementAt(i);
			List<Colony> colonies = player.getColonies();
			for (int j = 0; j < colonies.size(); j++) {
				Colony colony = colonies.get(j);
				colony.nextTurn();

				// population died off
				if (colony.getPopulationCount() == 0) {
					Planet planet = colony.getPlanet();
					planet.removeColony();
					colony.getSun().removeFlag(player.getName());
					player.removeColony(colony);
					System.out.println("Colony " + colony.getName() + " of player " + player.getName() + " perished.");
				}

			}
		}
	}

	private void calculateTechnologyAdvances() {
		Technology tech = null;
		Player player = null;
		Vector players = null;
		players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			player = (Player) players.elementAt(i);
			double production = player.getResearchProduction();
			if (player instanceof MPlayer) {
				production = production / (D_BASE_DIFFICULTY + getDifficultyLevel() * getDifficultyLevel()) * 100;
				// test
				// System.out.println("Research production: " + production);
			}
			double dPhysics = production / 100.0 * player.getPhysicsPercent();
			double dBiology = production / 100.0 * player.getBiologyPercent();
			double dMathematics = production / 100.0 * player.getMathematicsPercent();
			double dSocial = production / 100.0 * player.getSocialPercent();
			double dEconomy = production / 100.0 * player.getEconomyPercent();
			double dMilitary = production / 100.0 * player.getMilitaryPercent();

			// test
			// System.out.println("player " + player.getName() + ", physics
			// research: " + dPhysics);
			// System.out.println("player " + player.getName() + ", biology
			// research: " + dBiology);
			// System.out.println("player " + player.getName() + ", mathematics
			// research: " + dMathematics);
			// System.out.println("player " + player.getName() + ", social
			// research: " + dSocial);
			// System.out.println("player " + player.getName() + ", economy
			// research: " + dEconomy);
			// System.out.println("player " + player.getName() + ", military
			// research: " + dMilitary);

			List<Technology> possibleTechnologies = player.getPossibleTechnologies();
			for (int j = 0; j < possibleTechnologies.size(); j++) {
				tech = possibleTechnologies.get(j);
				double cost = tech.getResearchCost();
				double costPaid = tech.getResearchCostPaid();
				double remains = cost - costPaid;
				// System.out.println(player.getName() + ": " + tech.getName() +
				// " == " + player.getCurrentPhysics() + "?");
				if (tech.getName().equals(player.getCurrentPhysics())) {
					if (remains > dPhysics) {
						tech.addResearchCostPaid(dPhysics, player);
					} else {
						dPhysics -= (remains);
						player.addTreasury(dPhysics);
						dPhysics = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				} else if (tech.getName().equals(player.getCurrentBiology())) {
					if (remains > dBiology) {
						tech.addResearchCostPaid(dBiology, player);
					} else {
						dBiology -= (remains);
						player.addTreasury(dBiology);
						dBiology = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				} else if (tech.getName().equals(player.getCurrentMathematics())) {
					if (remains > dMathematics) {
						tech.addResearchCostPaid(dMathematics, player);
					} else {
						dMathematics -= (remains);
						player.addTreasury(dMathematics);
						dMathematics = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				} else if (tech.getName().equals(player.getCurrentSocial())) {
					if (remains > dSocial) {
						tech.addResearchCostPaid(dSocial, player);
					} else {
						dSocial -= (remains);
						player.addTreasury(dSocial);
						dSocial = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				} else if (tech.getName().equals(player.getCurrentEconomy())) {
					if (remains > dEconomy) {
						tech.addResearchCostPaid(dEconomy, player);
					} else {
						dEconomy -= (remains);
						player.addTreasury(dEconomy);
						dEconomy = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				} else if (tech.getName().equals(player.getCurrentMilitary())) {
					if (remains > dMilitary) {
						tech.addResearchCostPaid(dMilitary, player);
					} else {
						dMilitary -= (remains);
						player.addTreasury(dMilitary);
						dMilitary = 0.0;
						tech.addResearchCostPaid(cost, player);
					}
				}
			}
			player.setResearchProduction(0.0);
		}
	}

	// XXX: currently only for the one human player possible
	private void calculateScanResult() {
		int humanPlayerRGB = 0;
		// System.out.print("Scanning: ...");
		spaceOperaDisplay.createBufferedImage();

		Vector players = universe.getPlayers();
		Player player = null;
		int scanRange = 0;

		for (int i = 0; i < players.size(); i++) {
			// System.out.print(".");
			player = (Player) players.elementAt(i);
			if (player instanceof MPlayer) {
				spaceOperaDisplay.paintHumanScanArea(player);
			}
		}

		// for each enemy ship: check if scanned
		// for each enemy colony: check if scanned
		for (int i = 0; i < players.size(); i++) {
			// System.out.print(".");
			player = (Player) players.elementAt(i);
			if (player instanceof CPlayer) {
				// for each colony: add scanned area
				List<Colony> colonies = player.getColonies();
				for (int j = 0; j < colonies.size(); j++) {
					Colony colony = colonies.get(j);
					colony.setScanned(spaceOperaDisplay.isColonyScanned(colony));
					// XXX: if necessary here, why not for ships below, and
					// maybe once would be enough?
					universe.cleanup();
				}
				// for each ship: add scanned area
				List<SpaceCraft> ships = player.getShips();
				for (int j = 0; j < ships.size(); j++) {
					Ship ship = (Ship) ships.get(j);
					ship.setScanned(spaceOperaDisplay.isShipScanned(ship));
					// XXX: see above
				}

			}
		}
		// Scanning done
		// System.out.println("... done.");
	}

	public void display() {
		spaceOperaDisplay.repaint();
	}

	public void displayColony(Colony colony) {

		if (colony != null) {
			universe.selectSun(colony.getSun());
			Player player = universe.getPlayer();
			colony.setPlanetSelected(true);
			colony.setSunSelected(true);
			player.setCurrentColony(colony);
			updateSunSystem(colony.getSunSystem(), colony.getPlanet());
			changePlanetImage(colony.getPlanet());
			universe.display();
			// if (colonyDetail != null) {
			// colonyDetail.dispose();
			// colonyDetail = null;
			// }
			// TODO colonyDetail = new ColonyDetail(colony, this);
		}
	}

	public void displayColonyResources() {
		Player player = universe.getPlayer();
		Colony colony = player.getCurrentColony();
		if (colonyResources != null) {
			colonyResources.dispose();
		}
		colonyResources = new ColonyResources(colony, this);
	}

	public void displayNextColony() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.setSelected(false);
		}
		Player player = universe.getPlayer();
		Colony currentColony = player.getCurrentColony();
		Colony colony = null;
		boolean none = true;
		List<Colony> colonies = player.getColonies();
		for (int i = 0; i < colonies.size(); i++) {
			colony = colonies.get(i);
			colony.setPlanetSelected(false);
			colony.setSunSelected(false);
			if (colony == currentColony) {
				none = false;
				if (i < colonies.size() - 1) {
					// take next
					colony = colonies.get(i + 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					changePlanetImage(colony.getPlanet());
					break;
				} else {
					// take the first
					colony = colonies.get(0);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					changePlanetImage(colony.getPlanet());
					break;
				}
			}
		}
		if (none) {
			// no Current Colony yet
			if (colonies.size() > 0) {
				colony = colonies.get(0);
				colony.setPlanetSelected(true);
				colony.setSunSelected(true);
				player.setCurrentColony(colony);
				updateSunSystem(colony.getSunSystem(), colony.getPlanet());
				changePlanetImage(colony.getPlanet());
			}
		}
		universe.display();
		// if (colonyDetail != null) {
		// colonyDetail.closeColonyDetail();
		// colonyDetail = null;
		// }
		// Test
		// System.out.println("Free memory: " +
		// Runtime.getRuntime().freeMemory());
		if (colony != null) {
			// TODO colonyDetail = new ColonyDetail(colony, this);
		}
	}

	public void displayNextPlanet() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.setSelected(false);
		}
		// display next planet
		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		Planet curPlanet = player.getCurrentPlanet();
		Planet planet = null;
		boolean none = true;
		if (planets != null) {
			for (int i = 0; i < planets.size(); i++) {
				planet = (Planet) planets.get(i);
				planet.setSelected(false);
				planet.setSunSelected(false);
				if (planet == curPlanet) {
					if (i < planets.size() - 1) {
						none = false;
						planet = (Planet) planets.get(i + 1);
						planet.setSelected(true);
						planet.setSunSelected(true);
						player.setCurrentPlanet(planet);
						updateSunSystem(planet.getSunSystem(), planet);
						changePlanetImage(planet);
						break;
					}
				}
			}
			if (none) {
				if (planets.size() > 0) {
					planet = (Planet) planets.get(0);
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					changePlanetImage(planet);
				}
			}
			universe.display();
			if (planet != null) {
				// TODO planetDetail = new PlanetDetail(planet, this);
			}
		}
	}

	public void displayNextStar() {
		Sun sun = null;
		Graphics g = universe.getGraphics();
		List suns = universe.getSuns();
		boolean selectedOne = false;
		for (int i = 0; i < suns.size(); i++) {
			sun = (Sun) suns.get(i);
			if (sun.getSelected()) {
				selectedOne = true;
				sun.setSelected(false);
				if (i < suns.size() - 1) {
					// the next one
					sun = (Sun) suns.get(i + 1);
					sun.setSelected(true);
					break;
				} else {
					// the first one
					sun = (Sun) suns.get(0);
					sun.setSelected(true);
					break;
				}
			}
		}
		if (!selectedOne) {
			// no sun
			sun = (Sun) suns.get(0);
			sun.setSelected(true);
		}
		updateSunSystem(sun.getSunSystem(), null);
		changePlanetImage(null);
		sun.display(g);
		universe.display();
	}

	public void displayPlanet(Planet displayPlanet) {

		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.setSelected(false);
		}

		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.get(i);
			planet.setSelected(false);
			planet.setSunSelected(false);
		}

		displayPlanet.setSelected(true);
		displayPlanet.setSunSelected(true);
		player.setCurrentPlanet(displayPlanet);
		updateSunSystem(displayPlanet.getSunSystem(), displayPlanet);
		changePlanetImage(displayPlanet);
		universe.display();

		if (displayPlanet != null) {
			// TODO planetDetail = new PlanetDetail(displayPlanet, this);
		}
	}

	public void displayPreviousColony() {
		// clear all marks first
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.setSelected(false);
		}
		// display previous colony
		Player player = universe.getPlayer();
		Colony currentColony = player.getCurrentColony();
		Colony colony = null;
		boolean none = true;
		List<Colony> colonies = player.getColonies();
		for (int i = colonies.size() - 1; i >= 0; i--) {
			colony = colonies.get(i);
			colony.setPlanetSelected(false);
			colony.setSunSelected(false);
			if (colony == currentColony) {
				none = false;
				if (i > 0) {
					// take the previous one
					colony = player.getColonies().get(i - 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					changePlanetImage(colony.getPlanet());
					break;
				} else {
					// take the last one
					colony = colonies.get(player.getColonies().size() - 1);
					colony.setPlanetSelected(true);
					colony.setSunSelected(true);
					player.setCurrentColony(colony);
					updateSunSystem(colony.getSunSystem(), colony.getPlanet());
					changePlanetImage(colony.getPlanet());
					break;
				}
			}
		}
		if (none) {
			// no Current Colony yet
			if (colonies.size() > 0) {
				colony = colonies.get(0);
				colony.setPlanetSelected(true);
				colony.setSunSelected(true);
				player.setCurrentColony(colony);
				updateSunSystem(colony.getSunSystem(), colony.getPlanet());
				changePlanetImage(colony.getPlanet());
			}
		}
		universe.display();
		// if (colonyDetail != null) {
		// colonyDetail.closeColonyDetail();
		// colonyDetail = null;
		// }
		if (colony != null) {
			// TODO colonyDetail = new ColonyDetail(colony, this);
		}
	}

	public void displayPreviousPlanet() {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun s = (Sun) suns.get(i);
			s.setSelected(false);
		}
		// display next planet
		Player player = universe.getPlayer();
		List<Planet> planets = player.getPlanetList();
		Planet curPlanet = player.getCurrentPlanet();
		Planet planet = null;
		boolean none = true;
		if (planets != null) {
			for (int i = 0; i < planets.size(); i++) {
				planet = (Planet) planets.get(i);
				planet.setSelected(false);
				planet.setSunSelected(false);
				if (planet == curPlanet) {
					none = false;
					// take previous
					if (i > 0) {
						planet = (Planet) planets.get(i - 1);
					} else {
						planet = (Planet) planets.get(planets.size() - 1);
					}
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					changePlanetImage(planet);
					break;
				}
			}
			if (none) {
				// no Current Planet yet
				if (planets.size() > 0) {
					planet = (Planet) planets.get(0);
					planet.setSelected(true);
					planet.setSunSelected(true);
					player.setCurrentPlanet(planet);
					updateSunSystem(planet.getSunSystem(), planet);
					changePlanetImage(planet);
				}
			}
			// refresh Universe
			universe.display();
			if (planet != null) {
				// TODO planetDetail = new PlanetDetail(planet, this);
			}
		}
	}

	public void displayPreviousStar() {
		Sun sun = null;
		Graphics g = universe.getGraphics();
		List suns = universe.getSuns();
		boolean selectedOne = false;
		for (int i = suns.size() - 1; i >= 0; i--) {
			sun = (Sun) suns.get(i);
			if (sun.getSelected()) {
				sun.setSelected(false);
				selectedOne = true;
				if (i > 0) {
					// the previous one
					sun = (Sun) suns.get(i - 1);
					sun.setSelected(true);
					break;
				} else {
					// the last one
					sun = (Sun) suns.get(suns.size() - 1);
					sun.setSelected(true);
					break;
				}
			}
		}
		if (!selectedOne) {
			// no sun
			sun = (Sun) suns.get(0);
			sun.setSelected(true);
		}
		updateSunSystem(sun.getSunSystem(), null);
		changePlanetImage(null);
		sun.display(g);
		universe.display();
	}

	public void disposeOpenWindows() {
		// not necessary for dialogs
		if (colonyResources != null) {
			colonyResources.dispose();
		}

		if (scienceDetail != null) {
			scienceDetail.dispose();
		}
		if (shipYard != null) {
			shipYard.dispose();
		}

		if (fightDetailVisible != null) {
			fightDetailVisible.dispose();
		}

		try {
			playMusic.setMusicOn(false);
			playMusic.stop();
			playMusic = null;
		} catch (Exception e) {
			// ignore repeated stops
		}
	}

	public void executeFight(Sun sun, boolean visible) {
		// TEST
		// System.out.println("Battle in System " + sun.getName());
		if (visible) {
			fightDetailVisible = new FightDetailVisible(sun, this);
		} else {
			FightDetail fightDetail = new FightDetail(sun, this);
			fightDetail.executeAllSteps(null);
		}
	}

	private void incrementTurnNumber() {
		universe.incTurnNr();
		// XXX: in messages window
		System.out.println("Turn #: " + universe.getTurnNr());
		spaceOperaDisplay.setCurrentYear("   Year: " + (2400 + universe.getTurnNr()));
		// XXX refactor this
		double treasury = universe.getPlayer().getTreasury();
		String sTreasury = String.valueOf(treasury);
		int pos = sTreasury.indexOf(".");
		if (pos + 3 < sTreasury.length()) {
			sTreasury = sTreasury.substring(0, pos + 3);
		}
		spaceOperaDisplay.setTreasury("   Total Treasury: " + sTreasury);
	}

	public void initiateNewGame() {
		Sun sun = null;
		SunSystem s = null;
		List suns = null;
		universe.setUniverseSize();
		universe.clear();
		universe.generate();
		suns = universe.getSuns();
		sun = (Sun) suns.get(0);
		sun.setSelected(true);
		s = sun.getSunSystem();
		universe.display();
		updateSunSystem(s, null);
		// refreshOpenWindows();
	}

	public void load() {
		// XXX: load a previously stored game or a tutorial game
	}

	public static void main(String arg[]) {
		SpaceOpera so = new SpaceOpera();
	}

	public void nextTurn() {
		if (effectsOn) {
			playEffect.play("resources/effects/drum1.wav");
		}
		// clean up old turn data
		universe.clearScanArea();
		saveOpenWindows();
		// next turn
		incrementTurnNumber();
		otherPlayersMakeTheirTurns();
		setShipOrdersForAllPlayers();
		universe.moveShips();
		universe.updatePlanetPositions();
		getSunSystemDisplay().repaint();
		advanceColoniesOfAllPlayers();
		calculateTechnologyAdvances();
		universe.executeBattles();
		calculateScanResult();
		reactivateHumanPlayer();
		// refreshOpenWindows();
		universe.display();
		showEvents();
		spaceOperaDisplay.show();
	}

	private void otherPlayersMakeTheirTurns() {
		Player player = null;
		Vector players = null;
		players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			player = (Player) players.elementAt(i);
			universe.setPlayer(player);
			if (player instanceof CPlayer) {
				((CPlayer) player).colonizePlanetNow();
				((CPlayer) player).setColonyOrders();
				player.setResearch();
			}
		}
	}

	private void setShipOrdersForAllPlayers() {
		Player player = null;
		Vector players = null;
		players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			player = (Player) players.elementAt(i);
			universe.setPlayer(player);
			player.setShipOrders();
		}
	}

	public void changePlanetImage(Planet planet) {
		spaceOperaDisplay.changePlanetImage(planet);
	}

	public void print() {
		System.out.print(universe.toString());
	}

	public void quit() {
		disposeOpenWindows();
		System.exit(0);
	}

	private void reactivateHumanPlayer() {
		Player player = null;
		Vector players = null;
		players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			player = (Player) players.elementAt(i);
			if (player instanceof MPlayer) {
				universe.setPlayer(player);
			}
		}
	}

	public void save() {
		// XXX: save a game
	}

	private void saveOpenWindows() {
		if (colonyDetail != null) {
			colonyDetail.saveColony();
		}
		if (scienceDetail != null) {
			scienceDetail.saveSettings();
		}
		// XXX: planetDetail, shipYard, ...
	}

	public void selectNextShip() {
		Graphics g = universe.getGraphics();
		SpaceCraft ship = null;
		Player player = universe.getPlayer();
		boolean ok = false;
		List<SpaceCraft> playerships = player.getShips();
		for (int i = 0; i < playerships.size(); i++) {
			ship = playerships.get(i);
			if (ship.isSelected()) {
				ship.setSelected(false);
				universe.setCurrentSpaceCraft(ship);
				ship.display(g, this);
				if (i < playerships.size() - 1) {
					ship = playerships.get(i + 1);
					ship.setSelected(true);
					ok = true;
					break;
				} else {
					ship = playerships.get(0);
					ship.setSelected(true);
					ok = true;
					break;
				}
			}
		}
		if (ok) {
			universe.setCurrentSpaceCraft(ship);
			ship.display(g, this);
		} else if (playerships.size() > 0) {
			ship = playerships.get(0);
			ship.setSelected(true);
			universe.setCurrentSpaceCraft(ship);
			ship.display(g, this);
		}
	}

	public void selectPreviousShip() {
		Graphics g = universe.getGraphics();
		SpaceCraft ship = null;
		Player player = universe.getPlayer();
		boolean ok = false;
		List<SpaceCraft> playerships = player.getShips();
		for (int i = playerships.size() - 1; i >= 0; i--) {
			ship = playerships.get(i);
			if (ship.isSelected()) {
				ship.setSelected(false);
				universe.setCurrentSpaceCraft(ship);
				ship.display(g, this);
				if (i > 0) {
					ship = playerships.get(i - 1);
					ship.setSelected(true);
					ok = true;
					break;
				} else {
					ship = playerships.get(playerships.size() - 1);
					ship.setSelected(true);
					ok = true;
					break;
				}
			}
		}
		if (ok) {
			universe.setCurrentSpaceCraft(ship);
			ship.display(g, this);
		} else if (playerships.size() > 0) {
			ship = playerships.get(0);
			ship.setSelected(true);
			universe.setCurrentSpaceCraft(ship);
			ship.display(g, this);
		}
	}

	public void selectShip(SpaceCraft ship) {
		if (ship != null) {
			Graphics g = universe.getGraphics();
			SpaceCraft spaceCraft = null;
			Player player = universe.getPlayer();
			List<SpaceCraft> playerships = player.getShips();
			for (int i = 0; i < playerships.size(); i++) {
				spaceCraft = playerships.get(i);
				if (spaceCraft.isSelected()) {
					spaceCraft.setSelected(false);
					universe.setCurrentSpaceCraft(spaceCraft);
				}
			}
			ship.setSelected(true);
			universe.setCurrentSpaceCraft(ship);
			ship.display(g, this);
		}
	}

	public void queueEvent(String eventName) {
		SOEvent se = universe.getEvent(eventName);
		queuedEvents.addElement(se);
	}

	// public void showShipInfo(SpaceCraft s) {
	// if (showShipInfo) {
	// if (shipMiniInfo != null) {
	// shipMiniInfo.dispose();
	// }
	// shipMiniInfo = new ShipMiniInfo(s, this);
	// shipMiniInfo.refresh();
	// }
	// }

	// called from toolbar
	// public void showShipInfo() {
	// showShipInfo = true;
	// if (shipMiniInfo != null) {
	// shipMiniInfo.dispose();
	// }
	// shipMiniInfo = new ShipMiniInfo(getCurrentShip(), this);
	// shipMiniInfo.refresh();
	// }

	private void showEvents() {
		// are there any queued events to show in this turn ?
		for (int i = 0; i < queuedEvents.size(); i++) {
			SOEvent se = (SOEvent) queuedEvents.elementAt(i);

			try {
				PlayAnim pa = new PlayAnim(se, this);
				while (pa.isShowing()) {
					try {
						// System.out.println("sleeping " );
						Thread.sleep(100);
					} catch (Exception e) {/* ignore this error */
					}
				}
			} catch (Exception e) {
				System.out.println("Cannot display event: " + e);
			}
		}
		queuedEvents.removeAllElements();
	}

	public SpaceOpera() {
		queuedEvents = new Vector();
		fights = new Vector();
		colonyDetail = null;
		// universe = new Universe(this);
		// universe.generate();
		// sunSystem = universe.getSun().getSunSystem();
		SpaceOperaDisplay.setDefaultLookAndFeelDecorated(true);
		spaceOperaDisplay = new SpaceOperaDisplay(universe, this);
		// MessageBox mb = new MessageBox(this, "Space Opera: Copyright Notice",
		// "Space Opera Version " + VERSIONSTRING + "\n" +
		// "Copyright (C) 1996-2002, Lorenz Beyeler\n"+
		// "Space Opera comes with ABSOLUTELY NO WARRANTY;\n" +
		// "This is free software, and you are welcome " +
		// "to redistribute it under certain conditions; " +
		// "For more details see 'Help->GPL Information' and " +
		// "'Help->License information'.");
		// try { Thread.sleep(1500); } catch(Exception e) {/* always ignore */}
		// mb.dispose();

		readConfiguration();
		playIntroAnimation();
		// Play some sound
		playMusic = new PlayMusic(this);
		playMusic.setMusicOn(musicOn);

		// Start the effects player
		playEffect = new PlayEffect(this);
		// will be used like:
		// if (effectsOn) {
		// pe.play("ting.wav"); // or something like that 8-)
		// }

		// NewGameSettings newGameSettings = null;
		// newGameSettings = new NewGameSettings(this);

	}

	// public void toggleShipInfo(SpaceCraft s) {
	// showShipInfo = !showShipInfo;
	// if (!showShipInfo) {
	// if (shipMiniInfo != null) {
	// shipMiniInfo.dispose();
	// }
	// } else {
	// showShipInfo(s);
	// }
	// }

	// public void closeShipInfo() {
	// if (shipMiniInfo != null) {
	// showShipInfo = false;
	// shipMiniInfo.dispose();
	// }
	// }

	public void updateSunSystem(SunSystem s, Planet p) {
		if (sunSystem == null) {
			sunSystem = s;
		}
		sunSystem.update(s, p);
	}

	private void readConfiguration() {
		animationsOn = false;
		musicOn = false;
		effectsOn = false;
		musicFromDir = true;
		musicDir = "music";
		musicList = null;

		try {
			soConfig = new SOConfig(this);
			soConfig.readConfig("resources/config/anim.xml");
			soConfig.readConfig("resources/config/music.xml");
			soConfig.readConfig("resources/config/effects.xml");
		} catch (Throwable t) {
			System.out.println("The 'config' directory may not be setup correctly. " + t);
		}
	}

	private void playIntroAnimation() {
		// XXX: display intro anyway (if animation off), but only with image
		// if (animationsOn) {
		SOEvent soEvent = universe.getEvent("Welcome");
		try {
			PlayAnim pa = new PlayAnim(soEvent, this);
			while (pa.isShowing()) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {/* always ignore */
				}
			}
		} catch (Throwable t) {
			System.out.println("Cannot display intro: " + t);
		}
		// }
	}

	public void testEvent() {
		SOEvent se = universe.getEvent("AsteroidBelt1");

		try {
			PlayAnim pa = new PlayAnim(se, this);
			while (pa.isShowing()) {
				try {
					// System.out.println("sleeping " );
					Thread.sleep(100);
				} catch (Exception e) {/* ignore this error */
				}
			}
		} catch (Exception e) {
			System.out.println("Cannot display event: " + e);
		}
	}

	public void showShipOrders() {
		// if (shipOrders != null) {
		// shipOrders.close();
		// shipOrders = null;
		// }
		// // shipOrders = new ShipOrders(this);
	}

	public void showAnimSettings() {
		if (animSettings != null) {
			animSettings.dispose();
			animSettings = null;
		}
		animSettings = new AnimSettings(this);
	}

	public void showCheatWindow() {
		if (cheatWindow != null) {
			cheatWindow.dispose();
			cheatWindow = null;
		}
		cheatWindow = new CheatWindow(this);
	}

	public void showEffectSettings() {
		if (effectSettings != null) {
			effectSettings.dispose();
			effectSettings = null;
		}
		effectSettings = new EffectSettings(this);
	}

	public void showColonyList() {
		if (colonyList != null) {
			colonyList.dispose();
			colonyList = null;
		}
		colonyList = new ColonyList(this);
	}

	public void showGPL() {
		if (showGPL != null) {
			showGPL.dispose();
			showGPL = null;
		}
		showGPL = new ShowGPL();
	}

	public void showHelp() {
		if (helpWindow != null) {
			helpWindow.dispose();
			helpWindow = null;
		}
		helpWindow = new HelpWindow();
	}

	public void showLicense() {
		if (showLicense != null) {
			showLicense.dispose();
			showLicense = null;
		}
		showLicense = new ShowLicense();
	}

	public void showMusicSettings() {
		if (musicSettings != null) {
			musicSettings.dispose();
			musicSettings = null;
		}
		musicSettings = new MusicSettings(this);
		// start or stop background music:
		try {
			playMusic.setMusicOn(musicOn);
		} catch (Exception e) {
			// ignore this
		}
	}

	public void showPlanetList() {
		if (planetList != null) {
			planetList.dispose();
			planetList = null;
		}
		planetList = new PlanetList(this);
	}

	public void showScienceDetail() {
		if (scienceDetail != null) {
			scienceDetail.dispose();
			scienceDetail = null;
		}
		scienceDetail = new ScienceDetail(this);
	}

	public void showShipList() {
		if (shipList != null) {
			shipList.dispose();
			shipList = null;
		}
		shipList = new ShipList(this);
	}

	public void showShipYard() {
		if (shipYard != null) {
			shipYard.dispose();
			shipYard = null;
		}
		shipYard = new ShipYard(this);
	}

	public void showSunSystemList() {
		if (sunSystemList != null) {
			sunSystemList.dispose();
			sunSystemList = null;
		}
		sunSystemList = new SunSystemList(this);
	}

}
