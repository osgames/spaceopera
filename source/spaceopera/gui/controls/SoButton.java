package spaceopera.gui.controls;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JButton;

public class SoButton extends JButton {

	public SoButton(String label) {
		this.setText(label);
		this.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 10));
		this.setBackground(Color.DARK_GRAY);
		this.setForeground(Color.CYAN);
		this.setMargin(new Insets(0,0,0,0));
		//this.setFont(font );
	}

}
