//The class ShipDisplay displays a ship in the universe view
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects;

import java.awt.Color;
import java.awt.Graphics;

import spaceopera.gui.SpaceOpera;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ships.Satellite;

/**
 * Displays satellites in the universe view
 */
public class SatelliteDisplay extends SpaceCraftDisplay {
	
	private Satellite satellite = null;
	
	public SatelliteDisplay(Satellite s, Player p) {
		super (p);
		satellite = s;
	}
	
	public void cleanup(Graphics g) {
		drawShip(g, Color.black);
	}
	
	public void clear(Graphics g) {
		// if ((player instanceof MPlayer) || (so.getShowShips())) {
		// or ship in scannerrange of MPlayer
		// or spying from, union with MPlayer...
		drawShip(g, Color.black);
		// }
	}
	
	public void display(Graphics g, SpaceOpera so) {
		if (player instanceof MPlayer || so.getShowShips() || isScanned) {
			// or spying from, union with CPlayer...
			drawShip(g, color);
			if (isSelected) {
				selectShip(g, Color.white);
			} else {
				selectShip(g, Color.black);
			}
		}
	}
	
	public void drawShip(Graphics g, Color c) {
		g.setColor(c);
		g.fillOval(drawX - 3, drawY - 3, 7, 7);
		g.drawLine(drawX - 5, drawY, drawX + 5, drawY);
		g.drawLine(drawX, drawY - 5, drawX, drawY + 5);
	}
	
	public void selectShip(Graphics g, Color c) {
		g.setColor(c);
		g.drawOval(drawX - 6, drawY - 6, 14, 14);
	}
}
