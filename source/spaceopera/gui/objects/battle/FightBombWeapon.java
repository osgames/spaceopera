//The class FightBombWeapon is used for displaying Bombs and Explosions in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

import spaceopera.gui.objects.weapons.Weapon;


/**
 * this is the interface class that is used in FightDetail.checkDistanceAndFire
 * to connect to a ships bomb-style (e.g. laser) <br>
 */
public class FightBombWeapon extends FightObject {

	protected int fromX = 0;
	protected int fromY = 0;
	protected int toX = 0;
	protected int toY = 0;
	protected Weapon weapon = null;
	protected Graphics graphics = null;

	public FightBombWeapon(Weapon w, Graphics g, Object o, int fx, int fy, int tx, int ty) {
		weapon = w;
		graphics = g;
		fromX = fx;
		fromY = fy;
		toX = tx;
		toY = ty;
		orderTarget = o;
	}

	public void clear(Graphics g) {
	}

	public void display(Graphics g) {
	}

	public void explode(Graphics g, int step) {
	}

	public void fire() {
		// System.out.println("exploding bomb at " + fromX + ", " + fromY+ ", "
		// + toX + ", " + toY);
		weapon.fire(graphics, fromX, fromY, toX, toY);
	}

	public void fire2() {
		weapon.fire2(graphics, fromX, fromY, toX, toY);
	}

	public Weapon getWeapon() {
		return (weapon);
	}

	public int getWeaponForce() {
		return (weapon.getForce());
	}

	public void unFire() {
		weapon.unFire(graphics, fromX, fromY, toX, toY);
	}

	public int getTargetComputer() {
		return 0;
	}
}