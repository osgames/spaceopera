//The class FightTorpedoWeapon is used for displaying Torpedos.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

import spaceopera.gui.objects.weapons.Torpedo;
import spaceopera.gui.objects.weapons.Weapon;


/**
 * this is the interface class that is used in FightDetail.checkDistanceAndFire
 * to connect to a ships torpedo-style weapons XXX: Probably rename this class
 * to something like 'torpedo-like-weapons-interface-class'?
 */
public class FightTorpedoWeapon extends FightObject {

	private int oldX = 0;
	private int oldY = 0;
	private int lifeCycles = 0;
	private Weapon weapon = null;
	private FightObject fightObject = null; // Ship, station or planet that
											// fired this torpedo

	public String playerName = "";

	public void useLifeCycle() {
		lifeCycles--;
	}

	public int getLifeCycles() {
		return (lifeCycles);
	}

	public Weapon getWeapon() {
		return (weapon);
	}

	public int getWeaponBattleSpeed() {
		return (weapon.getBattleSpeed());
	}

	public int getWeaponForce() {
		return (weapon.getForce());
	}

	// Constructor
	public FightTorpedoWeapon(Object target, FightObject fo, Weapon w, int fx, int fy) {
		super();
		orderTarget = target;
		weapon = w;
		lifeCycles = weapon.getLifeCycles();
		fightObject = fo;
		x = fx;
		y = fy;
	}

	// Methods
	public void clear(Graphics g) {
		weapon.clear(g, x, y);
	}

	public void display(Graphics g) {
		((Torpedo) weapon).display(g, x, y);
	}

	public void explode(Graphics g, int step) {
		weapon.explode(g, step, x, y);
	}

	public FightObject getFightObject() {
		return fightObject;
	}

	public int getTargetComputer() {
		return 0;
	}

}