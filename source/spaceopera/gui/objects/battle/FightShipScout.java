//The class FightShipScout is used to display ScoutShips in a Fight in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

import spaceopera.universe.ships.Ship;


/** The class FightShipScout is used to display a ship with hull-size 'scout' in the FightDetail view.
 * XXX: rename to FightShipSmall (-small, -medium, -big, -large, ...?)
 */
public class FightShipScout extends FightShip {

  public void clear(Graphics g) {
    g.setColor(Color.black);
    g.drawLine(x-2,y,  x-2,y+4);
    g.drawLine(x-1,y-3,x-1,y+4);
    g.drawLine(x,y-5,  x,  y+4);
    g.drawLine(x+1,y-3,x+1,y+4);
    g.drawLine(x+2,y,  x+2,y+4);
    if (isSelected) {
        g.drawOval(x-6,y-6,12,12);
    }
    return;
  }

  public void display(Graphics g) {
    g.setColor(ship.getColor());
    g.drawLine(x-2,y,  x-2,y+4);
    g.drawLine(x-1,y-3,x-1,y+4);
    g.drawLine(x,y-5,  x,  y+4);
    g.drawLine(x+1,y-3,x+1,y+4);
    g.drawLine(x+2,y,  x+2,y+4);
    if (isSelected) {
        g.setColor(Color.white);
        g.drawOval(x-6,y-6,12,12);
    } else {
        g.setColor(Color.black);
        g.drawOval(x-6,y-6,12,12);
    }
    return;
  }

  public void explode(Graphics g, int step) { }
  
  public FightShipScout (Ship s, int x, int y) {
     super(s,x,y);
  }

}