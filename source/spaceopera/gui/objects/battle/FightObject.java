//The class FightObject is the parent class for objects in a fight in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

/** Parent abstract class for all objects displayed in the FightDetail view 
 *  (Ships, Stations, Lasers...)
 */
public abstract class FightObject {
    protected String order = ""; // "attack", "defend", "wait", "flee"
    protected Object orderTarget = null;
    protected int x=0;
    protected int y=0;
    protected int oldx=0;
    protected int oldy=0;

    public int  getX()       {return(x);}
    public int  getY()       {return(y);}
    public void setX(int x)  {this.x=x;}
    public void setY(int y)  {this.y=y;}

    public FightObject() {   }
    public abstract void display(Graphics g);
    public abstract void clear(Graphics g);
    public abstract void explode(Graphics g, int step);
    
    //XXX: some child classes (FightBeamWeapon, FightBombWeapon, FightExplosion...) don't need this method. Refactor class hierarchy? 
    public abstract int  getTargetComputer();
	/**
	 * @return Returns the order.
	 */
	public String getOrder() {
		return order;
	}
	/**
	 * @param order The order to set.
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	/**
	 * @return Returns the orderTarget.
	 */
	public Object getOrderTarget() {
		return orderTarget;
	}
	/**
	 * @param orderTarget The orderTarget to set.
	 */
	public void setOrderTarget(Object orderTarget) {
		this.orderTarget = orderTarget;
	}
	/**
	 * @return Returns the oldx.
	 */
	public int getOldx() {
		return oldx;
	}
	/**
	 * @param oldx The oldx to set.
	 */
	public void setOldx(int oldx) {
		this.oldx = oldx;
	}
	/**
	 * @return Returns the oldy.
	 */
	public int getOldy() {
		return oldy;
	}
	/**
	 * @param oldy The oldy to set.
	 */
	public void setOldy(int oldy) {
		this.oldy = oldy;
	}
}