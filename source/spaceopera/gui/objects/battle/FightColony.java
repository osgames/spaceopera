//The class FightColony is used for colonies in a fight in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.util.*;
import java.awt.*;

import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;

/**
 * The class FightColony is used as interface to colonies in the FightDetail
 * view.
 */
@SuppressWarnings({"rawtypes"})
public class FightColony extends FightObject {

	protected Colony colony = null;
	protected boolean isSelected = false;

	public Vector getWeapons() {
		return (colony.getWeapons());
	}

	public int getX() {
		return (x);
	}

	public int getY() {
		return (y);
	}

	public Player getPlayer() {
		return (colony.getPlayer());
	}

	public void clear(Graphics g) {
	}

	public void display(Graphics g) {
	}

	public void explode(Graphics g, int step) {
	}

	public FightColony(Colony c, int x, int y) {
		super();
		colony = c;
		this.x = x;
		this.y = y;
	}

	public boolean inside(int ix, int iy) {
		return ((ix >= x - 5) && (iy >= y - 5) && (ix <= (x + 5)) && (iy <= (y + 5)));
	}

	public void setOrder() {
	}

	public void setPosition(int x, int y) {
	}

	//XXX: target computers for colonies?
	public int getTargetComputer() {
		return 1;
	}

}