//The class FightBeamWeapon s used for displaying beamweapons in a fight
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

import spaceopera.gui.objects.weapons.Weapon;

/**
 * this is the interface class that is used in FightDetail.checkDistanceAndFire
 * to connect to a ships beam-weapons (e.g. laser) XXX: Probably rename this
 * class to something like 'beam-weapon-interface-class' ?
 */
public class FightBeamWeapon extends FightObject {

	protected int fromX = 0;
	protected int fromY = 0;
	protected int toX = 0;
	protected int toY = 0;
	protected Weapon weapon = null;
	protected Graphics graphics = null;

	public FightBeamWeapon(Weapon w, Graphics g, int fx, int fy, int tx, int ty) {
		weapon = w;
		graphics = g;
		fromX = fx;
		fromY = fy;
		toX = tx;
		toY = ty;
	}

	public void clear(Graphics g) {
	}

	public void display(Graphics g) {
	}

	public void explode(Graphics g, int step) {
	}

	public void fire() {
		weapon.fire(graphics, fromX, fromY, toX, toY);
	}

	public void fire2() {
	}

	public void unFire() {
		weapon.unFire(graphics, fromX, fromY, toX, toY);
	}

	public int getTargetComputer() {
		return 0;
	}

}