//The class FightExplosion is used for displaying Explosions in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.*;

/** The class FightExplosion is used to display weapon impacts in the FightDetail view.
 */
public class FightExplosion extends FightObject {

  protected int myX=0;
  protected int myY=0;
  protected Graphics graphics=null;

  public void clear(Graphics g) {  }
  public void display(Graphics g) {  }
  public void explode(Graphics g, int step) {  }


  public FightExplosion(Graphics g, int x, int y) {
     graphics=g;
     myX = x;
     myY = y;
  }


  public void fire() {
        graphics.setColor(Color.yellow);
        graphics.fillOval(myX-2,myY-2,4,4);
        graphics.setColor(Color.green);
        graphics.drawOval(myX-3,myY-3,6,6);
  }


  public void unFire() {
        graphics.setColor(Color.black);
        graphics.fillOval(myX-3,myY-3,6,6);
  }

  public int getTargetComputer() {
	return 0;
  }

}