//The class FightShip is used for using and displaying ships in the
//FightDetail and FightDetailVisible classes of the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.battle;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

import spaceopera.gui.objects.weapons.Weapon;
import spaceopera.universe.ai.Player;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The class FightShip is used to display ships in the FightDetail view.
 */
@SuppressWarnings({"rawtypes"})
public abstract class FightShip extends FightObject {

	protected SpaceCraft ship = null;
	protected boolean isSelected = false;

	public int getBattleSpeed() {
		return (ship.getBattleSpeed());
	}

	public boolean getPlanetAttacks() {
		return (ship.getPlanetAttacks());
	}

	public Player getPlayer() {
		return (ship.getPlayer());
	}

	public String getPlayerName() {
		return (ship.getPlayerName());
	}

	public String getShipType() {
		return (ship.getShipName());
	}

	public boolean getShipAttacks() {
		return (ship.getShipAttacks());
	}

	public Vector getShipWeapons() {
		return (ship.getWeapons());
	}

	public int getTargetComputer() {
		return (ship.getTargetComputer());
	}

	public void doDamage(float damage, Weapon weapon) {
		ship.doDamage(damage, weapon);
	}

	public boolean isDestroyed() {
		return (ship.isDestroyed());
	}

	// called by FightDetail.executeStep()
	public void checkOrders(FightShip fs, Vector fightingShips, Sun s) {
		getPlayer().checkOrders(fs, fightingShips, s);
	}

	public void completeOrder() {
	}

	public void explode(Graphics g) {
		// XXX: export this code (and change it) for the different ship types?
		g.setColor(Color.yellow);
		g.fillOval(x - 5, y - 5, 10, 10);
		try {
			Thread.sleep(120);
		} catch (Exception e) {/* always ignore */
		}
		g.setColor(Color.red);
		g.fillOval(x - 9, y - 9, 18, 18);
		g.setColor(Color.blue);
		g.fillOval(x - 4, y - 4, 8, 8);
		try {
			Thread.sleep(120);
		} catch (Exception e) {/* always ignore */
		}
		g.setColor(Color.yellow);
		g.fillOval(x - 10, y - 10, 20, 20);
		try {
			Thread.sleep(160);
		} catch (Exception e) {/* always ignore */
		}
		g.setColor(Color.black);
		g.fillRect(x - 10, y - 10, 20, 20);
	}

	public FightShip(SpaceCraft s, int x, int y) {
		super();
		ship = s;
		this.x = x;
		this.y = y;
	}

	public boolean inside(int ix, int iy) {
		return ((ix >= x - 5) && (iy >= y - 5) && (ix <= (x + 5)) && (iy <= (y + 5)));
	}

	public void setOrder() {
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
		return;
	}

	/**
	 * @return Returns the isSelected.
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * @param isSelected
	 *            The isSelected to set.
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * @return Returns the ship.
	 */
	public SpaceCraft getShip() {
		return ship;
	}

	/**
	 * @param ship
	 *            The ship to set.
	 */
	public void setShip(SpaceCraft ship) {
		this.ship = ship;
	}
}