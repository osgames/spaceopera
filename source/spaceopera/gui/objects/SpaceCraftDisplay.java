/*
 * Created on May 1, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package spaceopera.gui.objects;

import java.awt.Color;
import java.awt.Graphics;

import spaceopera.gui.SpaceOpera;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;

/**
 * Parent class for displaying ships and such in the universe display
 */
public class SpaceCraftDisplay implements SOConstants {
	
	protected Player player;
	protected Color color;
	protected boolean isSelected = false;
	protected boolean isScanned = false;
	protected int drawX;
	protected int drawY;
	
	public SpaceCraftDisplay(Player p) {
		player = p;
		color = player.getColor();
	}

	public void cleanup(){}

	public void clear(Graphics g){}

	public boolean isClicked(int x, int y) {
		if ((x >= drawX - 5) && (y >= drawY - 5) && (x <= (drawX + 5)) && (y <= (drawY + 5))) {
			return true;
		} else {
			return false;
		}
	}
	
	public void display(SpaceOpera so){
		System.out.println("scd display");
	}

	public void drawShip(){}
	
	public void setPosition(int x, int y){}
	
	public void setScanned(boolean b) {
		isScanned = b;
	}

	public boolean isScanned() {
		return (isScanned);
	}

	/**
	 * @return Returns the drawX.
	 */
	public int getDrawX() {
		return drawX;
	}

	/**
	 * @param drawX The drawX to set.
	 */
	public void setDrawX(int drawX) {
		this.drawX = drawX;
	}

	/**
	 * @return Returns the drawY.
	 */
	public int getDrawY() {
		return drawY;
	}

	/**
	 * @param drawY The drawY to set.
	 */
	public void setDrawY(int drawY) {
		this.drawY = drawY;
	}
	
	public boolean isSelected() {
		return (isSelected);
	}
	
	public void setSelected(boolean s) {
		isSelected = s;
	}
	
}
