//The class ShipDisplay displays a ship in the universe view
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.RangeMarker;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ships.Ship;

/**
 * Display ships in the universe
 */
public class ShipDisplay extends SpaceCraftDisplay implements SOConstants {

	private RangeMarker rangeMarker = null;
	private Ship ship = null;
	
	public ShipDisplay(Ship s, Player p) {
		super (p);
		ship = s;
		rangeMarker = new RangeMarker(0, 0, 0, 0);
	}
	
	public void display(Graphics g, SpaceOpera so) {
			
		if (player instanceof MPlayer || so.getShowShips() || isScanned) {
			// XXX: or spying from, union with CPlayer...								
			if (isSelected) {
				// draw a white circle in front of ship, 
				drawShip(g, color);
				selectShip(g, Color.white);
				if ((ship.isTraveling()) && (rangeMarker != null)) {
					rangeMarker.display(g);
				}
			} else {
				//or a black circle behind ship?
				selectShip(g, Color.black);
				drawShip(g, color);
				if (ship.isTraveling()) {
					if ((player instanceof MPlayer) || (so.getShowDirections())) {
						// XXX: isScanned for directions
						rangeMarker.display(g);
					}
				} else if (rangeMarker != null) {
					rangeMarker.clear(g);
				}
			}
		}
	}
	
	public void drawShip(Graphics g, Color c) {		
		g.setColor(c);
		g.drawLine(drawX - 3, drawY - 2, drawX - 3, drawY + 5);
		g.drawLine(drawX - 2, drawY, drawX - 2, drawY + 5);
		g.drawLine(drawX - 1, drawY - 5, drawX - 1, drawY + 5);
		g.drawLine(drawX, drawY - 8, drawX, drawY + 5);
		g.drawLine(drawX + 1, drawY - 5, drawX + 1, drawY + 5);
		g.drawLine(drawX + 2, drawY, drawX + 2, drawY + 5);
		g.drawLine(drawX + 3, drawY - 2, drawX + 3, drawY + 5);
		if (!c.equals(Color.BLACK)) {
			//Black color when cleaning up
			g.setColor(Color.YELLOW);
		}
		g.drawLine(drawX - 2, drawY + 8, drawX - 2, drawY + 11);
		g.drawLine(drawX - 1, drawY + 6, drawX - 1, drawY + 15);
		g.drawLine(drawX, drawY + 6, drawX, drawY + 20);
		g.drawLine(drawX + 1, drawY + 6, drawX + 1, drawY + 15);
		g.drawLine(drawX + 2, drawY + 8, drawX + 2, drawY + 11);
	}

	
	public void cleanup(Graphics g) {
		drawShip(g, Color.black);
		selectShip(g, Color.black);
		if (rangeMarker != null) {
			rangeMarker.clear(g);
		}
	}
	
	public void clear(Graphics g, SpaceOpera so) {
		if (player instanceof MPlayer || so.getShowShips() || isScanned) {
			// XXX: or spying from, union with MPlayer...

			// >>>Antialias-patch add by SR-040224
//			if (ANTIALIAS == true) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//			}
			// <<<

			drawShip(g, Color.black);
			selectShip(g, Color.black);
			if (rangeMarker != null) {
				rangeMarker.clear(g);
			}
		}
	}
	
	
	public void moveDragLine(Graphics g, SpaceOpera so, int x, int y) {
		if (((player instanceof MPlayer) || (so.getShowDirections()))) {
			// or ship in scannerrange of MPlayer
			// or spying from, union with MPlayer...
			rangeMarker.clear(g);
			rangeMarker = new RangeMarker(drawX, drawY, x, y);
			rangeMarker.display(g);
		}
	}

	public void paintDragLine(Graphics g, SpaceOpera so) {
		if ((player instanceof MPlayer) || (so.getShowDirections())) {
			// or ship in scannerrange of MPlayer
			// or spying from, union with MPlayer...
			rangeMarker.clear(g);
			rangeMarker = new RangeMarker(drawX, drawY, ship.getTargetX(), ship.getTargetY());
			rangeMarker.setTargetSelected(true);
			rangeMarker.display(g);
		}
	}

	
	private void selectShip(Graphics g, Color c) {
		g.setColor(c);
		g.drawOval(drawX - 6, drawY - 6, 14, 14);
	}

	public RangeMarker getRM() {
		return (rangeMarker);
	}

	public boolean targetIsSelected() {
		return (rangeMarker.isTargetSelected());
	}

	//set the target mode (affects the color of the rm)
	public void setTargetSelected(boolean b) {
		rangeMarker.setTargetSelected(b);	
	}
	
}
