//The class Laser is a component for ships.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.weapons;

import java.awt.*;



/**
 * The laser is a weapon built into a SpaceCraft
 */
public class Laser extends BeamWeapon {

	public Laser(String name, int range, int force) {
		super(name, range, force);
	}

	public void clear(Graphics g, int fx, int fy) {
	}

	public void display(Graphics g, int x, int y) {
	}

	public void explode(Graphics g, int step, int x, int y) {
	}

	public void fire(Graphics g, int fx, int fy, int tx, int ty) {
		g.setColor(Color.yellow);
		g.drawLine(fx, fy, tx, ty);
	}

	public void fire2(Graphics g, int fx, int fy, int tx, int ty) {
	}

	public void unFire(Graphics g, int fx, int fy, int tx, int ty) {
		g.setColor(Color.black);
		g.drawLine(fx, fy, tx, ty);
	}
}