//The class LongRangeTorpedo is used for fights between spaceships
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.weapons;

import java.awt.*;

import spaceopera.universe.SOConstants;

/** The torpedo is a weapon built into a ship or a satellite, can also be fired from planets
 */
public class HighSpeedTorpedo extends Torpedo implements SOConstants {

   public HighSpeedTorpedo(String name, int ammo, int battleSpeed, int force, int lifeCycles) {
      super(name,ammo,battleSpeed,force,lifeCycles);
   }

   
   // Each weapon fires differently!
   public void clear(Graphics g, int x, int y) {
    int[] xArr = new int [3];
    int[] yArr = new int [3];
    int n = 3;
    g.setColor(Color.black);
    xArr[0] = x - 2;
    xArr[1] = x;
    xArr[2] = x + 2;
    yArr[0] = y - 2;
    yArr[1] = y + 2;
    yArr[2] = y - 2;
    g.fillPolygon(xArr, yArr, n);   	
   }
   
   public void display(Graphics g, int x, int y) {
	   int[] xArr = new int [3];
	   int[] yArr = new int [3];
	   int n = 3;
	   g.setColor(Color.white);
	   xArr[0] = x - 2;
	   xArr[1] = x;
	   xArr[2] = x + 2;
	   yArr[0] = y - 2;
	   yArr[1] = y + 2;
	   yArr[2] = y - 2;
	   g.fillPolygon(xArr, yArr, n);
   }
   
   public void explode(Graphics g, int step, int x, int y) {
   	 if (step == 1) {
        g.setColor(Color.blue);
        g.fillOval(x-5,y-5, 10, 10);
   	 } else if (step == 2) {
   	    g.setColor(Color.yellow);
   	    g.fillRect(x-5,y-5, 10, 10);
   	 } else if (step == 3) {
   	    g.setColor(Color.black);
   	    g.fillRect(x-5,y-5, 10, 10);      		
   	 }
   		
   }
   
   public void fire(Graphics g, int fx, int fy, int tx, int ty) {
      // Torpedo doesn't fire, it explodes...
   }
   public void fire2(Graphics g, int fx, int fy, int tx, int ty) {
      // Torpedo doesn't fire, it explodes...
   }
   public void unFire(Graphics g, int fx, int fy, int tx, int ty) {
      // Torpedo doesn't fire, it explodes...
   }

}

