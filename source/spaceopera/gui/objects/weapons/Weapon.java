//The class Weapon is a component for ships and satellites, some can also be planet based.
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.weapons;

import java.awt.*;

/** The Weapon class is the parent class for Laser, Bomb, Torpedo and others
 */
public abstract class Weapon {
   protected String name = "";
   protected int rangeInPixel = 0;
   protected int ammo = 1;           // Number of torpedos in store (0 .. n)
   									 // for beam weapons and such this is always 1 
   protected int baseAmmo = 1;		 // restock up to baseAmmo
   protected int force = 0;          // strength of explosion on impact / approach
   protected int battleSpeed = 0;     // speed per 'battle-turn'  (n*MovePixels)
   protected int lifeCycles=0;        // number of 'battle-turns' for torpedo to 'live' (5 .. n)

   public Weapon(String n) {
      name = n;
   }
   
   abstract public void clear(Graphics g, int x, int y);
   abstract public void display(Graphics g, int x, int y);
   abstract public void explode(Graphics g, int step, int x, int y);   
   abstract public void fire(Graphics g, int fx, int fy, int tx, int ty);
   abstract public void fire2(Graphics g, int fx, int fy, int tx, int ty);
   abstract public void unFire(Graphics g, int fx, int fy, int tx, int ty);

   public int getBattleSpeed()		  {return(battleSpeed);}
   public int getLifeCycles()		  {return(lifeCycles);}
   public void addLifeCycles(int lc)  {lifeCycles+=lc;}
   public int getForce()     		  {return(force);}
   public int getRange()     		  {return(rangeInPixel);}
   public String getName()    		  {return(name);}
   public int getAmmo()        		  {return(ammo);}
   public int getBaseAmmo()        		  {return(baseAmmo);}   
   public void useAmmo(int i)         {ammo-=i;}
   public void restockAmmo(int i)     {ammo+=i;}
}