//The class Bomb is a component for ships in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.weapons;

import java.awt.*;

import spaceopera.universe.SOConstants;

public class Bomb extends Weapon implements SOConstants{

   public Bomb(String n, int a, int f) {
      super(n);
      rangeInPixel = MOVEPIXELS;
      ammo = a;
      force = f;
   }

   public void clear(Graphics g, int fx, int fy) { }
   
   public void display(Graphics g, int x, int y) { }
   
   public void explode(Graphics g, int step, int x, int y) { 
   }
   
   public void fire(Graphics g, int fx, int fy, int tx, int ty) {
   	//System.out.println("Bomb.fire() at " + tx + ", " + ty);
          g.setColor(Color.red);
          g.fillOval(tx-2,ty-2,4,4);
          g.setColor(Color.yellow);
          g.drawOval(tx-3,ty-3,6,6);
          
          // test
          //g.setColor(Color.red);
          //g.fillOval(100,100,4,4);
          //g.setColor(Color.yellow);
          //g.drawOval(100,100,6,6);          
   }

   public void fire2(Graphics g, int fx, int fy, int tx, int ty) {
   	//System.out.println("Bomb.fire() at " + tx + ", " + ty);
          g.setColor(Color.white);
          g.fillOval(tx-4,ty-4,8,8);
          g.setColor(Color.yellow);
          g.drawOval(tx-5,ty-5,10,10);
   }

   public void unFire (Graphics g, int fx, int fy, int tx, int ty) {
    g.setColor(Color.black);
    g.drawOval(tx-5,ty-5,10,10);   	
   }
}

