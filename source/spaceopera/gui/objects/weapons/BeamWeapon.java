//The class BeamWeapon is used to group the ShipComponent-Weapons of this type
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.objects.weapons;

import spaceopera.universe.SOConstants;

//import java.awt.*;

/** The beamweapon groups Laser, LaserCannon and other beamweapons
 */
public abstract class BeamWeapon extends Weapon implements SOConstants{

   public BeamWeapon(String n, int r, int f) {
      super(n);
      //Movepixels is a theoretical lightsecond, add another one to compensate for movement logic
      rangeInPixel = r*MOVEPIXELS+MOVEPIXELS;
      force = f;
   }

   //Methods
   //XXX: pull up/down the beamweapon-specific methods?
}

