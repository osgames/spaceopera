//The class PlayAnim is used to play animations in specific parts and situations in the game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

package spaceopera.gui.window.helper;

import javax.media.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.ImageCanvas;
import spaceopera.gui.components.SODialog;
import spaceopera.gui.components.SOEvent;
import spaceopera.universe.SOConstants;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

/**
 * The PlayAnim class is used to play animations in AVI files or such.
 */
public class PlayAnim extends SODialog implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private javax.media.Player player;
	private ImageCanvas imageCanvas1;
	private Panel center, centerTop, centerBottom, topRight;
	private Panel south;
	private File file;
	private SOEvent soEvent;
	private TextArea taEventText;
	private Button quit;
	private boolean noAnim;

	public PlayAnim(SOEvent se, SpaceOpera so) {
		super(so.getDisplay());
		soEvent = se;
		setTitle(soEvent.getTitle());
		this.setModal(true);
		setLayout(new BorderLayout());
		setBackground(Color.black);
		setForeground(Color.lightGray);
		doStaticLayout();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (player != null) {
					player.stop();
				}
				dispose();
			}
		});
		String fileName = soEvent.getAnimation();
		if (fileName.equals("") || so.getAnimations() == false) {
			noAnim = true;
		}
		if (noAnim) {
			// Show picture
			fileName = soEvent.getPicture();
			loadPicture(fileName);
		} else {
			// Show animation
			loadAnimation(fileName);
		}

		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		setSize(ANIMWINDOWSIZEX, ANIMWINDOWSIZEY);
		setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);

		if (noAnim) {
			imageCanvas1.init();
			imageCanvas1.display();
		}
		show();
	}

	private void loadAnimation(String fileName) {
		try {
			file = new File(fileName);
			// file = new File("animations/lava.avi");
			URL url = file.toURL();
			final Container contentPane = new Panel();
			contentPane.setLayout(new BorderLayout());
			if (player != null) {
				player.stop();
			}
			try {
				player = Manager.createPlayer(url);

				System.out.println("Player: " + player.toString());

				ControllerListener listener = new ControllerAdapter() {
					public void realizeComplete(RealizeCompleteEvent event) {
						Component vc = player.getVisualComponent();
						vc.setSize(320, 240);
						if (vc != null) {
							contentPane.add(vc, BorderLayout.CENTER);
						}
						topRight.add("Center", contentPane);
						pack();
					}
				};
				player.addControllerListener(listener);
				player.start();
			} catch (Exception e) {
				System.out.println(e);
			}
		} catch (Exception e) {
			System.err.println("Could not load animation file: " + fileName + ", " + e);
		}

	}

	private void loadPicture(String fileName) {
		try {
			// System.out.println("Displaying file: 1" + fileName);
			imageCanvas1 = new ImageCanvas(topRight, fileName);
			imageCanvas1.setBounds(0, 0, 320, 240);
			topRight.add("Center", imageCanvas1);
		} catch (Exception e) {
			System.err.println("Could not load picture: " + fileName + ", " + e);
		}
	}

	// not tested with null param in constructor
	// public static void main(String args[]) {
	// PlayAnim p = new PlayAnim(null,null);
	// }

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("quit")) {
			if (player != null) {
				player.stop();
			}
			this.dispose();
		}
	}

	private void doStaticLayout() {
		// Center
		center = new Panel();
		// center.setLayout(new GridLayout(2,1));
		center.setLayout(new BorderLayout());
		center.setBackground(Color.black);
		center.setForeground(Color.lightGray);
		centerTop = new Panel();
		// / centerTop.setLayout(new GridLayout(1,2));
		centerTop.setLayout(new BorderLayout());
		centerTop.setBackground(Color.black);
		centerTop.setForeground(Color.lightGray);

		topRight = new Panel();
		topRight.setLayout(new BorderLayout());
		topRight.setBackground(Color.black);
		topRight.setForeground(Color.lightGray);
		topRight.setSize(320, 240);

		centerBottom = new Panel();
		centerBottom.setLayout(new BorderLayout());
		centerBottom.setBackground(Color.black);
		centerBottom.setForeground(Color.lightGray);

		taEventText = new TextArea("", 1, 1, TextArea.SCROLLBARS_VERTICAL_ONLY);
		taEventText.setEditable(false);
		taEventText.setRows(12);
		taEventText.setFont(new Font("Monospaced", Font.PLAIN, 12));
		taEventText.setBackground(Color.black);
		taEventText.setForeground(Color.white);
		taEventText.setText(soEvent.getDescription());
		centerBottom.add("Center", taEventText);

		// / centerTop.add(topLeft);
		centerTop.add("East", new Label());
		centerTop.add("Center", topRight);
		centerTop.add("West", new Label());
		center.add("Center", centerTop);
		center.add("South", centerBottom);
		add("Center", center);

		south = new Panel();
		south.setLayout(new GridLayout(1, 4));
		south.setBackground(Color.black);
		south.setForeground(Color.white);
		quit = new Button("Go!");
		quit.addActionListener(this);
		quit.setActionCommand("quit");
		quit.setForeground(Color.black);
		quit.setBackground(Color.lightGray);
		south.add(new Label(""));
		south.add(new Label(""));
		south.add(new Label(""));
		south.add(quit);
		add("South", south);
	}

}