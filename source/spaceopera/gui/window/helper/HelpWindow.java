//The class Help is a small help in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.helper;

import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;

/** This class displays some help text.
 */
public class HelpWindow extends SOFrame implements ActionListener, SOConstants {

  private static final long serialVersionUID = 1L;
private TextArea ta1;
  private Button close;
  private Panel pCenter, pSouth;


  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CLOSE)) {
        this.dispose();
    }
  }


  private void doStaticLayout() {
      setLayout(new BorderLayout(2,2));
      // Center
      pCenter = new Panel();
      pCenter.setBackground(Color.white);
      pCenter.setForeground(Color.black);
      pCenter.setLayout(new GridLayout(1,1));
      ta1 = new TextArea();
      ta1.setBackground(Color.black);
      ta1.setForeground(Color.yellow);
      ta1.setFont(new Font("Monospaced", Font.PLAIN, 12));
      pCenter.add(ta1);
      add("Center", pCenter);
      // South
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,4));
      pSouth.setBackground(Color.black);
      pSouth.setForeground(Color.white);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(close);
      add("South",pSouth);
  }


    public HelpWindow () {
      setTitle("SpaceOpera Help");
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();   
      pack();
      setSize(WINDOWSIZEX,WINDOWSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
  }


  public void init() {
     ta1.setText("Main Window:");
     ta1.append("\n============");
     ta1.append("\nThis is the central part of the game. The top left part is "+
                "\nthe universe view with stars and ships. Click to select a ship, "+
                "\nthen set a target for this ship. The top right part shows a "+
                "\ndisplay of the selected starsystem, if explored. Click on a planet " +
                "\nto select it. Right-Click on a planet to pop-up the Planet-Menu. " +
                "\nUse the popup menu to display or to colonize planets. The bottom "+
                "\nright part shows a picture of the selected planet."+
                "\nMenu and buttons are self-explaining, I hope.");

     ta1.append("\n\nNew Game Settings");
     ta1.append("\n=================");
     ta1.append("\nSelect the size of the universe, the number of opponents, "+
                "\nand the difficulty of the game. Select, whether destroyed "+
                "\nopponents shall be replaced and whether random players shall " +
                "\npop up. (Sometimes they join your empire.)");

     ta1.append("\n\nPlanet Display");
     ta1.append("\n==============");
     ta1.append("\nShows an image of the planet and displays its most "+
                "\nimportant technical informations like size, temperature, "+
                "\ngravity, resources and so on.");

     ta1.append("\n\nColony Display");
     ta1.append("\n==============");
     ta1.append("\nShows an image of the colony and the existing buildings. "+
                "\nYou can add and remove projects from the production queue "+
                "\nfor this colony and distribute the production for this colony."+
                "\nExisting buildings also can be deleted if they are either too" +
                "\nexpensive or deprecated.");

     ta1.append("\n\nColony Resources Display");
     ta1.append("\n========================");
     ta1.append("\nShows a list of the most important resources that are stored, "+
                "\nproduced and used on a colony. Also evaluates the usage and " +
                "\ndisplays whether the demand for this resource is low," +
                "\naverage or high.");

     ta1.append("\n\nShip List Display");
     ta1.append("\n=================");
     ta1.append("\nShows a list of all your ships. Select a ship in the list and" +
                "\npress 'select' to activate this ship in the univers view.");

     ta1.append("\n\nSunsystem List Display");
     ta1.append("\n======================");
     ta1.append("\nShows a list of all sunsystems you have explored. Select a sunsystem in" +
                "\nthe list and press 'goto' to show this"+
                "\nsunsystem.");

     ta1.append("\n\nColony List Display");
     ta1.append("\n===================");
     ta1.append("\nShows a list of all your colonies. Select a colony in this" +
                "\nlist and press 'goto' to display this colony.");

     ta1.append("\n\nPlanet List Display");
     ta1.append("\n===================");
     ta1.append("\nShows a list of all known planets. Select a planet in this" +
                "\nlist and press 'goto' to display the planet.");

     ta1.append("\n\nScience Display");
     ta1.append("\n===============");
     ta1.append("\nWith this window you can define your research priorities."+
                "\nResearch is divided up into six categories: 'Physics', 'Biology',"+
                "\n'Mathematics','Economy','Social' and 'Military'."+
                "\nSelect which areas you want to be strong in. Sometimes, researching"+
                "\na technology from one category will also yield a breakthrough in"+
                "\nanother one.");

     ta1.append("\n\nShipyard Display");
     ta1.append("\n================");
     ta1.append("\nUse this window to build new ship categories. You can toggle between" +
                "\nthe 'edit' and the 'new' mode. Select a hull, add a drive, arm the ship,"+
                "\nadd colony modules or whatever to gain an advantage over your opponents."+
                "\nHit them with everything you've got. Ship categories can be deleted."+
                "\nYou can also define satellites to defend your planets.");

     ta1.append("\n\nFight Display");
     ta1.append("\n=============");
     ta1.append("\nIf ships of different players happen to be in a sunsystem, this window" +
                "\npops up and a fight is going to start. This also happens when a colony" +
                "\nfrom player A is in this system and a ship from player B enters the "+
                "\nsystem. You can set orders for your ships (red), then"+
                "\nyou can execute the fight in single step, slow motion or fast mode."+
                "\nThis window only pops up when you are one of the fighting players"+
                "\nand the ships or planets are armed."+
                "\n(not finished).");

     ta1.append("\n\nCheat Window");
     ta1.append("\n============");
     ta1.append("\nThe possible cheats should be quite self-explaining. They are"+
                "\nmainly used for test purposes. Whether the cheat window is going"+
                "\nto stay or not remains to be defined.");

     ta1.append("\n\nThanks for playing SpaceOpera. I hope you enjoy it! - Lorenz Beyeler");
  }

}