package spaceopera.gui.window.helper;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

import sun.swing.SwingLazyValue;

public class UiManager {

	public static void set() {
		UIManager.put("TabbedPane.contentBorderInsets", new Insets(0, 0, 1, 1));
		UIManager.put("TabbedPane.selected", new javax.swing.plaf.ColorUIResource(Color.gray));
		UIManager.put("TabbedPane.borderHightlightColor", new javax.swing.plaf.ColorUIResource(Color.gray));
		UIManager.put("TabbedPane.borderColor", new javax.swing.plaf.ColorUIResource(Color.gray));
		UIManager.put("TabbedPane.shadow", new javax.swing.plaf.ColorUIResource(Color.gray));
		UIManager.put("TabbedPane.darkShadow", new javax.swing.plaf.ColorUIResource(Color.black));
		UIManager.put("TabbedPane.light", new javax.swing.plaf.ColorUIResource(Color.black));
		UIManager.put("TabbedPane.highlight", new javax.swing.plaf.ColorUIResource(Color.gray));

		UIManager.put("TabbedPane.selectShadow", new javax.swing.plaf.ColorUIResource(Color.gray));
		UIManager.put("TabbedPane.selectDarkShadow", new javax.swing.plaf.ColorUIResource(Color.black));
		UIManager.put("TabbedPane.selectLight", new javax.swing.plaf.ColorUIResource(Color.black));
		UIManager.put("TabbedPane.selectHighlight", new javax.swing.plaf.ColorUIResource(Color.gray));

		UIManager.put("ScrollBar.background", Color.darkGray);
		UIManager.put("ScrollBar.foreground", Color.darkGray);
		UIManager.put("ScrollBar.darkShadow", Color.darkGray);
		UIManager.put("ScrollBar.shadow", Color.darkGray);

		UIManager.put("TableHeader.border", Color.darkGray);

		ArrayList<Object> gradients = new ArrayList<Object>();
		gradients.add(0.3);
		gradients.add(0.0);
		gradients.add(Color.darkGray);
		gradients.add(Color.darkGray);
		gradients.add(Color.gray);
		javax.swing.UIManager.put("Button.gradient", gradients);
		javax.swing.UIManager.put("Button.foreground", Color.yellow);
		// SwingUtilities.updateComponentTreeUI(Bd);

		UIManager.put("ScrollBar.gradient", gradients);
		UIManager.put("ScrollBar.thumb", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.thumbForeground", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.thumbShadow", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.thumbDarkShadow", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.thumbHighlight", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.highlight", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.track", new ColorUIResource(Color.black));
		UIManager.put("ScrollBar.trackHighlight", new ColorUIResource(Color.black));
		UIManager.put("Table.scrollPaneBorder", new javax.swing.plaf.ColorUIResource(Color.gray));

		// UIManager.put("TabbedPane.borderColor", new
		// ColorUIResource(Color.black));
		// UIManager.put("Table.focusCellHighlightBorder", new
		// ColorUIResource(Color.black));

		Integer twelve = new Integer(12);
		Integer fontPlain = new Integer(Font.PLAIN);
		// Integer fontBold = new Integer(Font.BOLD);
		Object dialogPlain12 = new SwingLazyValue("javax.swing.plaf.FontUIResource", null, new Object[] { "Lucida Sans Regular",
				fontPlain, twelve });
		// Object serifPlain12 = new
		// SwingLazyValue("javax.swing.plaf.FontUIResource", null, new Object[]
		// { Font.SERIF, fontPlain,
		// twelve });
		// Object sansSerifPlain12 = new
		// SwingLazyValue("javax.swing.plaf.FontUIResource", null, new Object[]
		// { Font.SANS_SERIF,
		// fontPlain, twelve });
		// Object monospacedPlain12 = new
		// SwingLazyValue("javax.swing.plaf.FontUIResource", null, new Object[]
		// { Font.MONOSPACED,
		// fontPlain, twelve });
		// Object dialogBold12 = new
		// SwingLazyValue("javax.swing.plaf.FontUIResource", null, new Object[]
		// { Font.DIALOG, fontBold,
		// twelve });
		UIManager.put("Button.font", dialogPlain12);
		UIManager.put("ToggleButton.font", dialogPlain12);
		UIManager.put("RadioButton.font", dialogPlain12);
		UIManager.put("CheckBox.font", dialogPlain12);
		UIManager.put("ColorChooser.font", dialogPlain12);
		UIManager.put("ComboBox.font", dialogPlain12);
		UIManager.put("Label.font", dialogPlain12);
		UIManager.put("List.font", dialogPlain12);
		UIManager.put("MenuBar.font", dialogPlain12);
		UIManager.put("MenuItem.font", dialogPlain12);
		UIManager.put("RadioButtonMenuItem.font", dialogPlain12);
		UIManager.put("CheckBoxMenuItem.font", dialogPlain12);
		UIManager.put("Menu.font", dialogPlain12);
		UIManager.put("PopupMenu.font", dialogPlain12);
		UIManager.put("OptionPane.font", dialogPlain12);
		UIManager.put("Panel.font", dialogPlain12);
		UIManager.put("ProgressBar.font", dialogPlain12);
		UIManager.put("ScrollPane.font", dialogPlain12);
		UIManager.put("Viewport.font", dialogPlain12);
		UIManager.put("TabbedPane.font", dialogPlain12);
		UIManager.put("Table.font", dialogPlain12);
		UIManager.put("TableHeader.font", dialogPlain12);
		UIManager.put("TextField.font", dialogPlain12);
		UIManager.put("PasswordField.font", dialogPlain12);
		UIManager.put("TextArea.font", dialogPlain12);
		UIManager.put("TextPane.font", dialogPlain12);
		UIManager.put("EditorPane.font", dialogPlain12);
		UIManager.put("TitledBorder.font", dialogPlain12);
		UIManager.put("ToolBar.font", dialogPlain12);
		UIManager.put("ToolTip.font", dialogPlain12);
		UIManager.put("Tree.font", dialogPlain12);

		UIManager.put("Slider.trackColor", Color.gray);
		UIManager.put("Slider.altTrackColor", Color.black);
		UIManager.put("Slider.thumb", Color.black);
		UIManager.put("Slider.trackWidth", 5);
		UIManager.put("Slider.highlight", Color.darkGray);
		UIManager.put("Slider.shadow", Color.darkGray);
		UIManager.put("Slider.gradient", gradients);
		UIManager.put("Slider.focusGradient", gradients);
		UIManager.put("Slider.focus", Color.gray);

		ArrayList<Object> rgradient = new ArrayList<Object>();
		rgradient.add(0.3);
		rgradient.add(0.0);
		rgradient.add(Color.gray);
		rgradient.add(Color.gray);
		rgradient.add(Color.white);
		UIManager.put("RadioButton.gradient", rgradient);
		javax.swing.UIManager.put("RadioButton.background", Color.darkGray);

		// javax.swing.UIManager.put("ComboBox.buttonBackground",
		// Color.darkGray);
		// javax.swing.UIManager.put("ComboBox.buttonDarkShadow",
		// Color.darkGray);
		javax.swing.UIManager.put("ComboBox.buttonHighlight", Color.gray);
		javax.swing.UIManager.put("ComboBox.buttonShadow", Color.gray);
		javax.swing.UIManager.put("ComboBox.selectionBackground", Color.gray);
		javax.swing.UIManager.put("ComboBox.selectionForeground", Color.cyan);

		javax.swing.UIManager.put("ComboBox.buttonBackground", Color.gray);
		javax.swing.UIManager.put("ComboBox.buttonDarkShadow", Color.darkGray);
		javax.swing.UIManager.put("ComboBox.buttonShadow", Color.gray);

		// javax.swing.UIManager.put("Button.background", Color.yellow);
		// javax.swing.UIManager.put("Button.border", Color.red);

	}
}
