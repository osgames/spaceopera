//The class CheatWindow is used mainly to test the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.helper;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SODialog;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Technology;
import spaceopera.universe.ai.Player;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;

/** The CheatWindow class is used to test SpaceOpera
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class CheatWindow extends SODialog implements ActionListener,
                                                     ItemListener,
                                                     SOConstants {
  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Panel pCenter,  pSouth;
  private Button cancel, go, addColony, doResearch;
  private Checkbox chShowShips, chShowColonies,
                   chShowDirections, chShowScanArea,
                   chAllowColonyDetail, chScoutAllSuns,
				   chShowAllFights;
  private int addColonies = 0;
  private boolean addResearch = false;


  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CANCEL)) {
        this.dispose();
    }
    if (c.equals(GO)) {
        // store cheats, refresh view and close window
        spaceOpera.setShowColonies(chShowColonies.getState());
        spaceOpera.setShowShips(chShowShips.getState());
        spaceOpera.setShowDirections(chShowDirections.getState());
        spaceOpera.setShowScanArea(chShowScanArea.getState());
        spaceOpera.allowColonyDetail(chAllowColonyDetail.getState());
        spaceOpera.setScoutAllSuns(chScoutAllSuns.getState());
        spaceOpera.setShowBattles(chShowAllFights.getState());
        for (int i=0; i<addColonies; i++) {
           addRandomColony();
        }
        if (addResearch) {
           doSomeResearch();
        }
        if (chScoutAllSuns.getState()) {
           researchAllSuns();
        }
        spaceOpera.cleanupUniverse();
        spaceOpera.displayUniverse();
        this.dispose();
    }
    if (c.equals("addColony")) {
        addColonies++;
    }
    if (c.equals("doResearch")) {
        addResearch=true;;
    }
  }


  private void addRandomColony() {
	List suns = spaceOpera.getSuns();
    boolean added=false;
    Planet colonizeMe = null;
    found: for (int i=0; i<suns.size();i++) {
       Sun sun=(Sun)suns.get(i);
       Vector planets = sun.getPlanets();
       for (int j=0; j<planets.size();j++) {
          Planet planet=(Planet)planets.elementAt(j);
          if (planet.getType().equals(EARTHLIKE)) {
             if (! planet.isColonized()) {
                colonizeMe = planet;
                added=true;
                break found;
             }
          }
       }
    }
    if (colonizeMe != null) {
      Player player = spaceOpera.getCurrentPlayer();
      colonizeMe.colonize(player);
      colonizeMe.addExploredByPlayer(player.getName());
      Vector planeten = colonizeMe.getSun().getPlanets();
      for (int i = 0; i<planeten.size(); i++) {
        Planet p = (Planet)planeten.elementAt(i);
        player.addPlanet(p);
      }
    }
  }


  public CheatWindow (SpaceOpera so) {
      super(so.getDisplay());
      spaceOpera = so;
      setTitle("Select cheats for this game");
      setLayout(new BorderLayout(2,2));
      setBackground(Color.lightGray);
      setForeground(Color.black);
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();   
      pack();
      setSize(SMALLWINDOWSIZEX,SMALLWINDOWSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
      go.requestFocus();
  }

private void doSomeResearch() {
    Player player = spaceOpera.getCurrentPlayer();
    List<Technology> possibleTechnologies = player.getPossibleTechnologies();
    Vector studyNext = new Vector();
    // next level of study
    for (int i = 0; i<possibleTechnologies.size(); i++) {
      Technology tech = possibleTechnologies.get(i);
      if (tech.isStudied()) {
        int techId = tech.getChildTech1();
        if (techId>0) {
          studyNext.addElement(new Integer(techId));
        }
        techId = tech.getChildTech2();
        if (techId>0) {
          studyNext.addElement(new Integer(techId));
        }
      } else if (tech.getAllowStudy()) {
        studyNext.addElement(new Integer(tech.getId()));
      }
    }
    // enable it
    for (int i=0; i<studyNext.size(); i++) {
      int techId = ((Integer)studyNext.elementAt(i)).intValue();
      for (int j = 0; j<possibleTechnologies.size(); j++) {
          Technology tech = possibleTechnologies.get(j);
        if ((techId == tech.getId()) && (!tech.isStudied())) {
          tech.addResearchCostPaid(tech.getResearchCost(),player);
        }
      }
    }
  }


  private void doStaticLayout() {
      pCenter = new Panel();
      pCenter.setBackground(Color.lightGray);
      pCenter.setForeground(Color.black);
      pCenter.setLayout(new GridLayout(8,2,2,0));
      chShowColonies = new Checkbox("Show enemy colonies");
      pCenter.add(chShowColonies);
      pCenter.add(new Label(""));
      chShowShips = new Checkbox("Show enemy ships");
      chShowShips.addItemListener(this);
      pCenter.add(chShowShips);
      pCenter.add(new Label(""));
      chShowDirections = new Checkbox("Show enemy ship targets");
      pCenter.add(chShowDirections);
      pCenter.add(new Label(""));
      chShowScanArea = new Checkbox("Show scan area of human player");
      pCenter.add(chShowScanArea);
      pCenter.add(new Label(""));
      chAllowColonyDetail = new Checkbox("Show of enemy-ColonyDetail");
      pCenter.add(chAllowColonyDetail);
      pCenter.add(new Label(""));
      chScoutAllSuns = new Checkbox("See all sunsystems");
      pCenter.add(chScoutAllSuns);
      pCenter.add(new Label(""));
      chShowAllFights = new Checkbox("Show fights between computer players");
      pCenter.add(chShowAllFights);
      pCenter.add(new Label(""));
      addColony = new Button("Add a colony");
      addColony.addActionListener(this);
      addColony.setActionCommand("addColony");
      addColony.setForeground(Color.black);
      addColony.setBackground(Color.lightGray);
      pCenter.add(addColony);
      doResearch = new Button("Do some research");
      doResearch.addActionListener(this);
      doResearch.setActionCommand("doResearch");
      doResearch.setForeground(Color.black);
      doResearch.setBackground(Color.lightGray);
      pCenter.add(doResearch);
      add("Center", pCenter);

      // Command-Buttons
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,6));
      pSouth.setBackground(Color.lightGray);
      pSouth.setForeground(Color.black);
      cancel = new Button(CANCEL);
      cancel.addActionListener(this);
      cancel.setActionCommand(CANCEL);
      cancel.setForeground(Color.black);
      cancel.setBackground(Color.lightGray);
      go = new Button(GO);
      go.addActionListener(this);
      go.setActionCommand(GO);
      go.setForeground(Color.black);
      go.setBackground(Color.lightGray);
      pSouth.add(new Label(""));
      pSouth.add(cancel);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(go);
      pSouth.add(new Label(""));
      add("South",pSouth);
  }


  public void init() {
      chShowColonies.setState(spaceOpera.getShowColonies());
      chShowShips.setState(spaceOpera.getShowShips());
      chShowDirections.setState(spaceOpera.getShowDirections());
      chShowScanArea.setState(spaceOpera.getShowScanArea());
      chAllowColonyDetail.setState(spaceOpera.allowColonyDetail());
      chScoutAllSuns.setState(spaceOpera.getScoutAllSuns());
      chShowAllFights.setState(spaceOpera.getShowBattles());
  }


  public void itemStateChanged(ItemEvent event) {
      if (event.getSource()==chShowShips) {
         chShowDirections.setState(chShowShips.getState());
      }
  }


  private void researchAllSuns() {
     List suns = spaceOpera.getSuns();
     Player player = spaceOpera.getCurrentPlayer();
     for (int i = 0; i<suns.size(); i++) {
       Sun sun = (Sun)suns.get(i);
       if (!sun.isExploredBy(player.getName())) {
         Vector planeten = sun.getPlanets();
         if (planeten.size()>0) {
           for (int j = 0; j<planeten.size(); j++) {
             Planet p = (Planet)planeten.elementAt(j);
             if (! p.isExploredBy(player.getName())) {
               p.addExploredByPlayer(player.getName());
             }
           	 player.addPlanet(p);
           }
         }
       } //else {
         sun.addExploredByPlayer(player.getName());
       //}
     }
  }

}

