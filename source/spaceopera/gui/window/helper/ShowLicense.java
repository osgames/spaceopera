//The class ShowLicense displays the license information for the SpaceOpera product
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.helper;

import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;

/**
 * This class displays the SpaceOpera license information
 */
public class ShowLicense extends SOFrame implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private TextArea ta1;
	private Button close;
	private Panel pCenter, pSouth;

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals(CLOSE)) {
			this.dispose();
		}
	}

	private void doStaticLayout() {
		setLayout(new BorderLayout(2, 2));
		pCenter = new Panel();
		pCenter.setBackground(Color.white);
		pCenter.setForeground(Color.black);
		pCenter.setLayout(new GridLayout(1, 1));
		ta1 = new TextArea();
		ta1.setBackground(Color.black);
		ta1.setForeground(Color.yellow);
		ta1.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pCenter.add(ta1);
		add("Center", pCenter);
		pSouth = new Panel();
		pSouth.setLayout(new GridLayout(1, 6));
		pSouth.setBackground(Color.black);
		pSouth.setForeground(Color.white);
		close = new Button(CLOSE);
		close.addActionListener(this);
		close.setActionCommand(CLOSE);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(close);
		pSouth.add(new Label(""));
		add("South", pSouth);
	}

	public void init() {
		ta1.setText("The SpaceOpera game is a turn based space colonization game\n"
				+ "Copyright (C) 1996-2002 The SpaceOpera Team\n" + "\n"
				+ "This program is free software; all accompanying material like\n"
				+ "sound files, images, prosa is also free; you can redistribute it\n"
				+ "or modify it under the terms of the GNU General Public License\n"
				+ "as published by the Free Software Foundation; either version 2\n"
				+ "of the License, or (at your option) any later version.\n" + "\n"
				+ "This program is distributed in the hope that it will be useful,\n"
				+ "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
				+ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
				+ "GNU General Public License for more details.\n" + "\n"
				+ "You should have received a copy of the GNU General Public License\n"
				+ "along with this program; if not, write to the Free Software\n"
				+ "Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.\n" + "\n"
				+ "For information about SpaceOpera and its authors, please visit\n"
				+ "the SpaceOpera Web Site at http://spaceopera.sourceforge.net/\n"
				+ "Copyright (C) 1996-2002 The SpaceOpera Team\n" + "\n");
	}

	public ShowLicense() {
		setTitle("SpaceOpera License Information");
		doStaticLayout();
		enableCloseButton(this);
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		setSize(WINDOWSIZEX, WINDOWSIZEY);
		setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);
		init();
		show();
	}

}
