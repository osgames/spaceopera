//The class PlayEffect is used to play sound effects
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

package spaceopera.gui.window.helper;

import javax.sound.midi.*;
import javax.sound.sampled.*;

import spaceopera.gui.SpaceOpera;

import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.net.URL;

/** The PlayEffect class is used to play sound effects in its own thread
 * x
 */
public class PlayEffect implements Runnable, LineListener, MetaEventListener {
//public class PlayEffect implements LineListener, MetaEventListener {

  private SpaceOpera spaceOpera;
  private Sequencer sequencer;
  private Thread thread;
  private boolean midiEnd, audioEnd;
  private Synthesizer synthesizer;
  private MidiChannel channels[];
  private Object soundObject;

  //Methods
  private void close() {
    if (sequencer != null) {
      sequencer.close();
      sequencer=null;
    }
  }

  private boolean loadSound(Object o) {
    if (o instanceof URL) {
      try {
        soundObject = AudioSystem.getAudioInputStream((URL) o);
      } catch(Exception e) {
        try {
          soundObject = MidiSystem.getSequence((URL) o);
        } catch (InvalidMidiDataException e2) {
          System.out.println("Unsupported audio file.");
          return false;
        } catch (Exception e3) {
          e3.printStackTrace();
          soundObject = null;
          return false;
        }
      }
    } else if (o instanceof File) {
      try {
        soundObject = AudioSystem.getAudioInputStream((File) o);
      } catch(Exception e1) {
        try {
          FileInputStream fis = new FileInputStream((File) o);
          soundObject = new BufferedInputStream(fis, 1024);
        } catch (Exception e2) {
          e2.printStackTrace();
          soundObject = null;
          return false;
        }
      }
    }
    if (sequencer == null) {
      soundObject = null;
      return false;
    }
    if (soundObject instanceof AudioInputStream) {
      try {
        AudioInputStream stream = (AudioInputStream) soundObject;
        AudioFormat format = stream.getFormat();
        // convert ALAW/ULAW to PCM format
        if ((format.getEncoding() == AudioFormat.Encoding.ULAW) ||
          (format.getEncoding() == AudioFormat.Encoding.ALAW)) {
          AudioFormat tmp = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,format.getSampleRate(),format.getSampleSizeInBits() * 2,format.getChannels(),format.getFrameSize() * 2,format.getFrameRate(),true);
          stream = AudioSystem.getAudioInputStream(tmp, stream);
          format = tmp;
        }
        DataLine.Info info = new DataLine.Info(Clip.class,stream.getFormat(),((int) stream.getFrameLength() * format.getFrameSize()));
        Clip clip = (Clip) AudioSystem.getLine(info);
        clip.addLineListener(this);
        clip.open(stream);
        soundObject = clip;
      } catch (Exception ex) {
        ex.printStackTrace();
        soundObject = null;
        return false;
      }
    } else if (soundObject instanceof Sequence || soundObject instanceof BufferedInputStream) {
      try {
        sequencer.open();
        if (soundObject instanceof Sequence) {
          sequencer.setSequence((Sequence) soundObject);
        } else {
          sequencer.setSequence((BufferedInputStream) soundObject);
        }
      } catch (InvalidMidiDataException e2) {
        System.out.println("Unsupported audio file.");
        soundObject = null;
        return false;
      } catch (Exception e3) {
        e3.printStackTrace();
        soundObject = null;
        return false;
      }
    }
    return true;
  }


  // test main method
  public static void main(String args[]) {
    String sound = "resources/effects/drum1.wav";
    PlayEffect ps = new PlayEffect(null);
  }


  public void meta(MetaMessage message) {
    if (message.getType() == 47) {  // 47 is end of midi track
      midiEnd = true;
    }
  }


  private void open() {
    try {
      sequencer = MidiSystem.getSequencer();
      if (sequencer instanceof Synthesizer) {
        synthesizer = (Synthesizer)sequencer;
        channels = synthesizer.getChannels();
      }
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    sequencer.addMetaEventListener(this);
  }


  public PlayEffect(SpaceOpera so) {
    spaceOpera = so;
    //open();
    start();
  }


  public void play(String effect) {
    open();
    File file = new File(effect);
    if (file != null) {
      if (loadSound(file) == true) {
        midiEnd = audioEnd = false;
        if (soundObject instanceof Sequence
            || soundObject instanceof BufferedInputStream) {
          sequencer.start();
          while (!midiEnd && thread != null) {
          //while (!midiEnd) {
             try { Thread.sleep(99); } catch(Exception e) {/* always ignore */}
          }
          sequencer.stop();
          sequencer.close();
        }
        else if (soundObject instanceof Clip && thread != null) {
        //else if (soundObject instanceof Clip) {
          Clip clip = (Clip) soundObject;
          clip.start();
          try { Thread.sleep(99); } catch(Exception e) {/* always ignore */}
          while ( (clip.isActive()) && thread != null) {
          //while ( (clip.isActive())) {
            try { Thread.sleep(99); } catch(Exception e) {/* always ignore */}
          }
          clip.stop();
          clip.close();
        }
        soundObject = null;
      }
    }
    close();
  }

  public void run() {
  }


  public void start() {
    thread = new Thread(this);
    thread.setName("SpaceOpera Sound Effects");
    thread.setDaemon(true);
    thread.start();
  }


  public void stop() {
    if (thread != null) {
        thread.interrupt();
    }
    thread = null;
  }


  public Thread getThread() {
    return thread;
  }



  public void update(LineEvent event) {
    if (event.getType() == LineEvent.Type.STOP) {
      audioEnd = true;
    }
  }

}