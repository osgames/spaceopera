package spaceopera.gui.window.helper;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

public class LabelHelper {

	public static JLabel createLabel(String text, int fontsize) {
		JLabel label = new JLabel();
		label.setForeground(Color.yellow);
		label.setBackground(Color.BLACK);
		label.setFont(new Font("Lucida Sans Regular", Font.PLAIN, fontsize));
		label.setText(text);
		label.setBorder(BorderFactory.createEmptyBorder());
		return label;
	}

	public static JLabel createBorderedLabel(String text, int fontsize, Border border) {
		JLabel label = new JLabel();
		label.setForeground(Color.yellow);
		label.setBackground(Color.BLACK);
		label.setFont(new Font("Lucida Sans Regular", Font.PLAIN, fontsize));
		label.setText(text);
		label.setBorder(border);
		return label;
	}

}
