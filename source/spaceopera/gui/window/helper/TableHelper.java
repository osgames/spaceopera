package spaceopera.gui.window.helper;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

public class TableHelper {
	public static JTable createDefaultTable(TableModel model, boolean sortable) {
		JTable table = new JTable();
		table.setOpaque(true);
		table.setBackground(Color.black);
		table.setForeground(Color.yellow);
		table.setSelectionBackground(Color.GRAY);
		table.setSelectionForeground(Color.yellow);
		JTableHeader header = table.getTableHeader();
		header.setBackground(Color.darkGray);
		header.setForeground(Color.yellow);
		final TableCellRenderer hr = table.getTableHeader().getDefaultRenderer();
		header.setDefaultRenderer(new TableCellRenderer() {
			private JLabel lbl;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				lbl = (JLabel) hr.getTableCellRendererComponent(table, value, false, false, row, column);
				lbl.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
				return lbl;
			}
		});
		header.setOpaque(true);
		header.setReorderingAllowed(false);
		table.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(sortable);
		table.setGridColor(Color.gray);
		table.setModel(model);
		return table;
	}

	public static JTable createDetailTable(TableModel model, boolean editable) {
		final JTable table = new JTable(model);
		table.setOpaque(true);
		table.setBackground(Color.black);
		table.setForeground(Color.yellow);
		table.setSelectionBackground(Color.GRAY);
		table.setSelectionForeground(Color.yellow);
		JTableHeader header = table.getTableHeader();
		header.setBackground(Color.black);
		header.setForeground(Color.yellow);
		final TableCellRenderer hr = table.getTableHeader().getDefaultRenderer();
		header.setDefaultRenderer(new TableCellRenderer() {
			private JLabel lbl;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				lbl = (JLabel) hr.getTableCellRendererComponent(table, value, false, false, row, column);
				lbl.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));
				lbl.setHorizontalAlignment(JLabel.LEFT);
				return lbl;
			}
		});
		header.setOpaque(true);
		header.setReorderingAllowed(false);
		if (editable) {
			table.setDefaultRenderer(Object.class, new WrappingCellRenderer(table.getDefaultRenderer(Object.class)));
		}
		table.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(false);
		table.setGridColor(Color.gray);
		return table;
	}

	public static JTable createColonyTable(TableModel model, boolean sortable) {
		JTable table = new JTable();
		table.setOpaque(true);
		table.setBackground(Color.black);
		table.setForeground(Color.yellow);
		table.setSelectionBackground(Color.GRAY);
		table.setSelectionForeground(Color.yellow);
		JTableHeader header = table.getTableHeader();
		header.setBackground(Color.darkGray.darker());
		header.setForeground(Color.yellow);
		final TableCellRenderer hr = table.getTableHeader().getDefaultRenderer();
		header.setDefaultRenderer(new TableCellRenderer() {
			private JLabel lbl;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				lbl = (JLabel) hr.getTableCellRendererComponent(table, value, false, false, row, column);
				lbl.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
				lbl.setHorizontalAlignment(JLabel.LEFT);
				return lbl;
			}
		});
		header.setOpaque(true);
		header.setReorderingAllowed(false);
		table.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(sortable);
		table.setGridColor(Color.gray);
		table.setModel(model);
		return table;
	}

}

class WrappingCellRenderer implements TableCellRenderer {

	private TableCellRenderer wrappedCellRenderer;

	public WrappingCellRenderer(TableCellRenderer cellRenderer) {
		super();
		this.wrappedCellRenderer = cellRenderer;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
			int column) {
		Component rendererComponent = wrappedCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
				column);

		if (row == 0 && column == 1) {
			rendererComponent.setBackground(Color.darkGray);
		} else {
			rendererComponent.setBackground(Color.black);
		}

		return rendererComponent;
	}

}
