package spaceopera.gui.window.helper;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class PanelHelper {

	public static JPanel createDefaultPanel() {
		JPanel panel = new JPanel(false);
		panel.setLayout(new GridLayout(1, 1));
		panel.setOpaque(true);
		panel.setBackground(Color.black);
		panel.setForeground(Color.yellow);
		panel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		return panel;
	}

	public static JPanel createTitlePanel(int x, int y, Color color) {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.setPreferredSize(new Dimension(x, y));
		panel.setMaximumSize(new Dimension(x, y));
		panel.setForeground(Color.yellow);
		panel.setBackground(color);
		panel.setBorder(BorderFactory.createEmptyBorder());
		return panel;
	}
}
