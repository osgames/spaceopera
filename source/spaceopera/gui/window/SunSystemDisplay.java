//The class SunSystemDisplay displays a SunSystem in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

import javax.swing.JPanel;

import spaceopera.gui.menu.PlanetMenu;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.SunSystem;

/**
 * The SunSystemDisplay displays the number of planets and the orbitals of a
 * sunsystem in the the top left part of the main window
 */
@SuppressWarnings({ "rawtypes" })
public class SunSystemDisplay extends JPanel implements MouseListener, MouseMotionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private SunSystem sunSystem;
	// private SpaceOpera spaceOpera;
	private Fullscreen fs;
	// private PlanetMenu planetMenu = null;
	private int planetMenu_X = 0;
	private int planetMenu_Y = 0;

	public void setSunSystem(SunSystem s) {
		sunSystem = s;
	}

	public SunSystemDisplay(SunSystem s, Fullscreen fs) {
		sunSystem = s;
		// spaceOpera = so;
		this.fs = fs;
		addMouseListener(this);
		addMouseMotionListener(this);
		setCursor(Cursor.getDefaultCursor());
		setBounds(0, 0, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
		setOpaque(true);
	}

	public void mouseClicked(MouseEvent event) {
		int x = event.getX();
		int y = event.getY();
		boolean refresh = false;
		boolean inside = false;
		Vector planets = sunSystem.getPlanets();
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			// TODO refactor this clickedy stuff in sunsystem display
			// if (event.getClickCount() > 1) {
			// // display planet details
			// if (planet.getSelected()) {
			// // TODO spaceOpera.displayPlanet(planet);
			// // display colony details if colonized
			// // XXX refactor this
			// // TODO if (planet.isColonized() &&
			// //
			// planet.getColony().getPlayer().equals(spaceOpera.getUniverse().getPlayer()))
			// // {
			// // spaceOpera.displayColony(planet.getColony());
			// // }
			// }
			// } else
			if (event.getModifiers() == InputEvent.BUTTON1_MASK) {
				// left Mousebutton
				planet.setSelected(false);
				if (planet.inside(x, y)) {
					if (!inside) {
						refresh = true;
						inside = true; // select only the first
						planet.setSelected(true);
						if (sunSystem.getExplored().containsKey(fs.getCurrentPlayerName())) {
							fs.getCurrentPlayer().setCurrentPlanet(planet);
							fs.displayPlanetImage(planet);
							if (planet.isColonized() && planet.getColony().getPlayerName().equals(fs.getCurrentPlayerName())) {
								fs.displayColonyImage(planet.getColony());
							} else {
								fs.displayColonyImage(null);
							}
						} else {
							fs.displayPlanetImage(null);
						}
					}
				}
			}
			// else {
			// // rechte Maustaste
			// if (planet.getSelected()) {
			// planetMenu = new PlanetMenu(fs, sunSystem.getSun(), planet);
			// this.add(planetMenu);
			// planetMenu_X = event.getX();
			// planetMenu_Y = event.getY();
			// planetMenu.show(this, planetMenu_X, planetMenu_Y);
			// }
			// }
		}
		// refresh erst ausfuehren, wenn alle auf selected=false
		if (refresh) {
			repaint();
		}
	}

	public void mouseDragged(MouseEvent event) {
	}

	public void mouseEntered(MouseEvent event) {
	}

	public void mouseExited(MouseEvent event) {
	}

	public void mouseMoved(MouseEvent event) {
	}

	public void mousePressed(MouseEvent event) {
	}

	public void mouseReleased(MouseEvent event) {
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setOpaque(true);
		Graphics g2 = g.create();
		Dimension d = getSize();
		g2.setColor(Color.black);
		g2.fillRect(0, 0, d.width, d.height);
		g.setColor(getForeground());
		// >>>Antialias-patch add by SR-040224

		// if (ANTIALIAS==true){
		// Graphics2D g2d = (Graphics2D)g2;
		// g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		// }
		// <<<

		// if (sunSystem.isGenerated()) {

		int x = SUNSYSTEMFRAMEX / 2;
		int y = SUNSYSTEMFRAMEY / 2;
		// background stars
		g2.setColor(Color.white);
		for (int i = 0; i < SSBACKGROUNDSTARS; i++) {
			g2.drawOval(sunSystem.getBackgroundStarX(i), sunSystem.getBackgroundStarY(i), 1, 1);
		}
		g2.setColor(sunSystem.getSun().getColor());
		int middle = STARSIZE / 2;
		g2.fillOval(x, y, STARSIZE, STARSIZE);
		g2.drawLine(x + middle - 1, y - 2, x + middle - 1, y + STARSIZE + 1); // vertical
		// line
		g2.drawLine(x + middle, y - 2, x + middle, y + STARSIZE + 1);
		g2.drawLine(x - 2, y + middle, x + STARSIZE + 1, y + middle); // horizontal
		// line
		g2.drawLine(x - 2, y + middle - 1, x + STARSIZE + 1, y + middle - 1);
		// Planets
		Vector planets = sunSystem.getPlanets();
		if (sunSystem.getExplored().containsKey(fs.getCurrentPlayerName())) {
			for (int i = 0; i < planets.size(); i++) {
				Planet p = (Planet) planets.elementAt(i);
				g2.setColor(Color.white);
				// Orbit of planet
				int radius = p.getOrbit().getRadius();
				int position = p.getOrbit().getPosition();
				g2.drawOval((x + STARSIZE / 2) - radius, (y + STARSIZE / 2) - radius, 2 * radius, 2 * radius);
				int px = x + STARSIZE / 2 + (int) (radius * Math.cos(position * 2.0 * Math.PI / 360.0));
				int py = y + STARSIZE / 2 + (int) (radius * Math.sin(position * 2.0 * Math.PI / 360.0));
				// Planet
				p.display(g2, px, py);

			}
//			g2.setColor(Color.black);
//			g2.fillRect(0, 0, 100, 30);
//			g2.setColor(Color.yellow);
//			g2.setFont(new Font("Arial", Font.BOLD, 12));
//			g2.drawString(sunSystem.getSun().getName(), 10, 20);
		} else {
			// not explored
			g2.setColor(Color.black);
			g2.fillRect(0, 0, 100, 25);
			g2.setColor(Color.yellow);
			g2.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
			//g2.drawString(sunSystem.getSun().getName(), 10, 20);
			g2.drawString("- not explored", 10, 10);
		}
		// }
		g2.dispose();
	}

}