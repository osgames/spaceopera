//The class ShipYard is used to define ShipDesigns in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JOptionPane;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.ImageCanvas;
import spaceopera.gui.components.MessageBox;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.ShipDesign;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Shipyard class displays a window to edit ship designs like colony ship,
 * scout ship, defense satellite...
 */
@SuppressWarnings({ "rawtypes" })
public class ShipYard extends SOFrame implements ActionListener, ItemListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private SpaceOpera spaceOpera = null;
	private Player player = null;
	private ImageCanvas imageCanvas1, imageCanvas2 = null;
	private TextField shipDesignName;
	private TextArea componentData, shipData, shipCost;
	private Label lbComponentName, lbShipDesignName;
	private Panel pNorth, pCenter, pSouth;
	private Panel pEditMode, pShipDesignName, pLeft, pRight, pHull, pHullText, pLeftTop, pLeftBottom, pComponent, pComponentText,
			pSelectedComp, pSelectedCompTitle, pSelectedCompTech, pCompList, pCompListCommands, pCompListText, pShipCost;
	// XXX: new function 'set obsolete' (cannot delete because some ships
	// exist/or are being built, but
	// remove product so that no new ships of this type are built)
	private Button save, delete, close, add, remove, reset;
	private Checkbox chEdit, chNew;
	private CheckboxGroup toggle;
	private Choice selectShipDesign, selectHull, selectShipArea, selectShipComponent;
	private ShipDesign currentShipDesign = null;
	private int totalWeight = 0;
	private int maxWeight = 0;
	private final String NEW_SHIP_NAME = "New Ship Name";
	private int hullsize = 1; // for components that are hullsize-dependent
								// (see class ShipDesign)

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("addComponent")) {
			String area = selectShipArea.getSelectedItem();
			String comp = selectShipComponent.getSelectedItem();
			try {
				if (area.equals(WEAPONS)) {
					if (!currentShipDesign.addWeapon(player, comp)) {
						MessageBox mb = new MessageBox(
								this,
								"Space Opera: Ship Yard",
								"Cannot add this weapon. \n\n"
										+ "You can add different categories of weapons (beamweapons, particle weapons, missiles, \n"
										+ "but you cannot add more than one type of each category (e.g. Laser and Laser Cannon does not work). \n\n"
										+ "Select one of each and stick with it.");
					}
				}
				if (area.equals(SPECIAL)) {
					currentShipDesign.addSpecial(player, comp);
				}
				if (area.equals(ELECTRONICS)) {
					currentShipDesign.addElectronics(player, comp);
				}
				if (area.equals(SHIELDING)) {
					if (!currentShipDesign.addShieldingOrArmor(player, comp)) {
						MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard",
								"Cannot add this component, because another one of this category was added already. Remove the other one first.");
					}
				}
				if (area.equals(DRIVES)) {
					currentShipDesign.addDrive(player, comp);
				}
				displayShipData();
			} catch (Exception e) {
				System.out.println("Component " + comp + " could not be added.");
			}
		} else if (c.equals("removeComponent")) {
			String area = selectShipArea.getSelectedItem();
			String comp = selectShipComponent.getSelectedItem();
			try {
				if (area.equals(WEAPONS)) {
					if (!currentShipDesign.removeWeapon(player, comp)) {
						showWarning(comp);
					}
				}
				if (area.equals(SPECIAL)) {
					currentShipDesign.removeSpecial(player, comp);
				}
				if (area.equals(ELECTRONICS)) {
					if (!currentShipDesign.removeElectronics(player, comp)) {
						showWarning(comp);
					}
				}
				if (area.equals(SHIELDING)) {
					if (!currentShipDesign.removeShieldingOrArmor(player, comp)) {
						showWarning(comp);
					}
				}
				if (area.equals(DRIVES)) {
					if (!currentShipDesign.removeDrive(comp)) {
						showWarning(comp);
					}
				}
				displayShipData();
			} catch (Exception e) {
				System.out.println("ShipYard: Component " + comp + " could not be removed.");
			}
		} else if (c.equals("resetWindow")) {
			init();
		} else if (c.equals("deleteShipDesign")) {
			// XXX error: delete does not remove product?
			if (currentShipDesignInUse()) {
				MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard",
						"Cannot delete this ship design as long as it is in use. Sorry.");
			} else {
				Object[] options = { YES, NO };

				String name = currentShipDesign.getShipClass();
				int answer = JOptionPane.showOptionDialog(this, "Do you really want to delete the shipdesign " + name + " ?",
						"Space Opera: " + this.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
						options[0]);
				if (answer == JOptionPane.OK_OPTION) {
					player.removeShipDesign(selectShipDesign.getSelectedItem());
					player.removeProduct(currentShipDesign.getShipClass());
					currentShipDesign = null;
					init();
				}
			}
		} else if (c.equals("saveShipDesign")) {
			// allowSave is true as long as ship design is conform to rules.
			// show only first error message
			boolean allowSave = true;
			String name = shipDesignName.getText();
			if (name.equals(NEW_SHIP_NAME)) {
				MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard", "Please enter a new name for this shipdesign.");
				allowSave = false;
			}
			if (allowSave) {
				Enumeration shipDesigns = player.getShipDesigns().keys();
				while (shipDesigns.hasMoreElements()) {
					String shipDesignName = (String) shipDesigns.nextElement();
					if (name.equals(shipDesignName)) {
						MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard",
								"Another shipdesign with this name already exists. Please change the name.");
						allowSave = false;
					}
				}
			}
			if (allowSave) {
				if (currentShipDesign.isShip() && currentShipDesign.getSublightDrive().equals("")) {
					MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard",
							"A ship needs at least a sublight drive. Please add one.");
					allowSave = false;
				}
			}
			if (allowSave) {
				ShipDesign temp = (ShipDesign) currentShipDesign.clone();
				String shipImage = temp.getImage();
				temp.setName(name);
				player.addShipDesign(name, temp);
				boolean unique = false;
				boolean allow = true;
				String productClass = SATELLITE;
				if (currentShipDesign.isShip()) {
					productClass = SHIP;
					allow = false;
				}
				if (currentShipDesign.canLandOnPlanets()) {
					allow = true;
				}
				Product p = new Product(-1, temp.getShipClass(), temp.getDescription(), unique, allow, shipImage, productClass);
				for (int i = R_MIN; i < R_MAX; i++) {
					if (temp.getCost(player, i) > 0) {
						p.setProductionCost(i, temp.getCost(player, i));
						p.setSupportCost(i, temp.getSupportCost(player, i));
					}
				}
				player.addProduct(p);
				ActionEvent f = new ActionEvent(toggle, ActionEvent.ACTION_PERFORMED, "toggle");
				actionPerformed(f);
				init();
			}
		} else if (c.equals(CLOSE)) {
			this.dispose();
		}
	}

	private void showWarning(String comp) {
		MessageBox mb = new MessageBox(this, "Space Opera: Ship Yard", "Cannot remove the component " + comp + ".");
	}

	// private void addComponents(Hashtable shipComponents, Vector components) {
	// int weight = 0;
	// for (int i = 0; i < components.size(); i++) {
	// weight = 0;
	// String name = (String) components.elementAt(i);
	// ShipComponent shipComponent = (ShipComponent) shipComponents.get(name);
	// weight = shipComponent.getWeight();
	// if (weight > 0) {
	// // XXX: ?? fuel scoop and other components weight depending on
	// // total ship size/weight?
	// // if (name.equals(FUEL_SCOOP)) {
	// // weight *= maxWeight/100;
	// // }
	// // XXX: when implementing the above, also change
	// // ShipDesign.getPayload()
	// addShipDataFormatted(weight, name, 1);
	// }
	// }
	// }

	private void addComponents(Hashtable shipComponents, String name, int count) {
		int weight = 0;
		ShipComponent shipComponent = (ShipComponent) shipComponents.get(name);
		if (shipComponent != null) {
			if (shipComponent.getHullSizeDepending() == SOME_DEPENDING) {
				double factor = Math.log(hullsize);
				if (factor < 1) {
					factor = 1;
				}
				weight = (int) (count * shipComponent.getWeight() * factor);
			} else if (shipComponent.getHullSizeDepending() == FULLY_DEPENDING) {
				weight = count * shipComponent.getWeight() * hullsize;
			} else {
				weight = count * shipComponent.getWeight();
			}
		}
		if (weight > 0) {
			// XXX: ?? fuel scoop and other components weight depending on
			// total ship size/weight?
			// if (name.equals(FUEL_SCOOP)) {
			// weight *= maxWeight/100;
			// }
			// XXX: when implementing the above, also change
			// ShipDesign.getPayload()
			addShipDataFormatted(weight, name, count);
		}
	}

	private void addShipCostFormatted(String resource, String unit, double amount) {
		String ctemp = resource + ".....................       ";
		ctemp = ctemp.substring(0, 25);
		String wtemp = "          " + amount + ".0";
		int pos = wtemp.indexOf("."); // decimal position
		wtemp = wtemp.substring(pos - 10, pos + 2);
		shipCost.append(ctemp + wtemp + " " + unit + "\n");
	}

	private void addShipDataFormatted(int weight, String comp, int count) {
		String ctemp = null;
		totalWeight += weight;
		if (count > 1) {
			ctemp = comp + " (" + count + ")....................       ";
		} else {
			ctemp = comp + ".....................       ";
		}
		ctemp = ctemp.substring(0, 25);
		String wtemp = "        " + weight;
		wtemp = wtemp.substring(wtemp.length() - 8, wtemp.length());
		shipData.append(ctemp + wtemp + " t\n");
	}

	private boolean currentShipDesignInUse() {
		boolean inUse = false;
		// Vector ships = spaceOpera.getUniverse().getShips();
		List<SpaceCraft> ships = player.getShips();
		for (int i = 0; i < ships.size(); i++) {
			Ship ship = (Ship) ships.get(i);
			if (ship.getShipDesign().getShipClass().equals(currentShipDesign.getShipClass())) {
				inUse = true;
			}
		}
		List<Colony> colonies = player.getColonies();
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony = colonies.get(i);
			List<BuildProject> pQ = colony.getProductionQueue();
			for (int j = 0; j < pQ.size(); j++) {
				// System.out.println(pQ.elementAt(j));
				Object pqElement = pQ.get(j);
				if (pqElement instanceof Product) {
					Product product = (Product) pqElement;
					if (product.getName().equals(currentShipDesign.getShipClass())) {
						inUse = true;
					}
				}
			}

		}
		return (inUse);
	}

	private void displayShipData() {
		shipData.setText("");
		shipCost.setText("");
		// int weight = 0;
		totalWeight = 0;
		maxWeight = 0;
		Hashtable shipComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) shipComponents.get(currentShipDesign.getHull());
		hullsize = sc.getHullSize();
		maxWeight = sc.getPayload();
		currentShipDesign.setImage(sc.getPicture());
		shipData.append("Hull capacity " + maxWeight + " tons.\n");
		shipData.append("\n");
		// hull
		addComponents(shipComponents, currentShipDesign.getHull(), 1);
		// electronics
		if (!"".equals(currentShipDesign.getScanner())) {
			addComponents(shipComponents, currentShipDesign.getScanner(), 1);
		}
		if (!"".equals(currentShipDesign.getTargetComputer())) {
			addComponents(shipComponents, currentShipDesign.getTargetComputer(), 1);
		}
		// weapons
		if (currentShipDesign.getBeamWeaponCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getBeamWeaponType(), currentShipDesign.getBeamWeaponCount());
		}
		if (currentShipDesign.getParticleWeaponCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getParticleWeaponType(), currentShipDesign.getParticleWeaponCount());
		}
		if (currentShipDesign.getMissileWeaponCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getMissileWeaponType(), currentShipDesign.getMissileWeaponCount());
		}
		if (currentShipDesign.getBombWeaponCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getBombWeaponType(), currentShipDesign.getBombWeaponCount());
		}
		// special
		if (!"".equals(currentShipDesign.getOreSeeker())) {
			addComponents(shipComponents, currentShipDesign.getOreSeeker(), 1);
		}
		if (!"".equals(currentShipDesign.getTactical())) {
			addComponents(shipComponents, currentShipDesign.getTactical(), 1);
		}
		if (!"".equals(currentShipDesign.getFuelTank())) {
			addComponents(shipComponents, currentShipDesign.getFuelTank(), 1);
		}
		if (!"".equals(currentShipDesign.getFuelScoop())) {
			addComponents(shipComponents, currentShipDesign.getFuelScoop(), 1);
		}
		if (!"".equals(currentShipDesign.getMineDeployer())) {
			addComponents(shipComponents, currentShipDesign.getMineDeployer(), 1);
		}
		if (!"".equals(currentShipDesign.getColonyModule())) {
			addComponents(shipComponents, currentShipDesign.getColonyModule(), 1);
		}
		if (!"".equals(currentShipDesign.getCargoModule())) {
			addComponents(shipComponents, currentShipDesign.getCargoModule(), currentShipDesign.getCargoModuleCount());
		}
		// shield and armor
		if (currentShipDesign.getShieldCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getShieldType(), currentShipDesign.getShieldCount());
		}
		if (currentShipDesign.getArmorCount() > 0) {
			addComponents(shipComponents, currentShipDesign.getArmorType(), currentShipDesign.getArmorCount());
		}
		sc = (ShipComponent) shipComponents.get(currentShipDesign.getSublightDrive());
		if (sc != null) {
			addComponents(shipComponents, currentShipDesign.getSublightDrive(), 1);
		}
		sc = (ShipComponent) shipComponents.get(currentShipDesign.getInterstellarDrive());
		if (sc != null) {
			addComponents(shipComponents, currentShipDesign.getInterstellarDrive(), 1);
		}
		if (totalWeight > maxWeight) {
			shipData.append("\nTotal weight of " + totalWeight + " exceeds hull capacity.\n");
			if (selectHull.isEnabled()) {
				save.setEnabled(false);
			}
		} else {
			shipData.append("\nTotal weight is " + totalWeight + " tons.\n");
			if (selectHull.isEnabled()) {
				save.setEnabled(true);
			}
		}
		shipData.setCaretPosition(0);
		// Ship cost
		String text = "Construction cost:\n";
		shipCost.append(text);
		for (int k = R_MIN; k < R_MAX; k++) {
			double amount = currentShipDesign.getCost(player, k);
			if (amount > 0) {
				addShipCostFormatted(spaceOpera.getResourceName(k), spaceOpera.getResourceUnit(k), amount);
			}
		}
		text = "\nSupport cost:\n";
		shipCost.append(text);
		for (int k = R_MIN; k < R_MAX; k++) {
			double amount = currentShipDesign.getSupportCost(player, k);
			if (amount > 0) {
				addShipCostFormatted(spaceOpera.getResourceName(k), spaceOpera.getResourceUnit(k), amount);
				// text = text + "\n " + spaceOpera.getResourceName(k) + ": " +
				// amount ;
			}
		}
		// shipCost.append(text);
		shipCost.setCaretPosition(0);
	}

	private void doStaticLayout() {
		setTitle("ShipYard");
		setLayout(new BorderLayout(2, 5));
		setForeground(Color.black);
		setBackground(Color.lightGray);
		// North
		pNorth = new Panel();
		pNorth.setLayout(new GridLayout(1, 2, 2, 0));
		pNorth.setForeground(Color.black);
		pNorth.setBackground(Color.lightGray);
		pEditMode = new Panel();
		pEditMode.setLayout(new GridLayout(1, 2, 2, 2));
		pEditMode.setBackground(Color.black);
		pEditMode.setForeground(Color.white);
		pEditMode.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
		toggle = new CheckboxGroup();
		chNew = new Checkbox("Create new shipdesign", false, toggle);
		chEdit = new Checkbox("Show existing shipdesign", true, toggle);
		chNew.addItemListener(this);
		chEdit.addItemListener(this);

		pEditMode.add(chNew);
		pEditMode.add(chEdit);

		pShipDesignName = new Panel();
		pShipDesignName.setLayout(new GridLayout(1, 2, 2, 2));
		pShipDesignName.setBackground(Color.black);
		pShipDesignName.setForeground(Color.white);
		pShipDesignName.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
		lbShipDesignName = new Label();
		lbShipDesignName.setBackground(Color.white);
		lbShipDesignName.setForeground(Color.black);
		lbShipDesignName.setText("Select existing design:");
		pShipDesignName.add(lbShipDesignName);
		shipDesignName = new TextField("", 30);
		shipDesignName.setEditable(true);
		shipDesignName.setForeground(Color.black);
		shipDesignName.setBackground(Color.white);
		shipDesignName.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
		selectShipDesign = new java.awt.Choice();
		selectShipDesign.setBackground(Color.white);
		selectShipDesign.setForeground(Color.black);
		pShipDesignName.add(selectShipDesign);
		pNorth.add(pEditMode);
		pNorth.add(pShipDesignName);
		add("North", pNorth);

		// Center
		pCenter = new Panel();
		pCenter.setForeground(Color.black);
		pCenter.setBackground(Color.lightGray);
		pCenter.setLayout(new GridLayout(1, 2, 2, 0));
		pLeft = new Panel();
		pLeft.setLayout(new GridLayout(2, 1, 2, 0));
		pLeft.setBackground(Color.lightGray);
		pLeft.setForeground(Color.white);
		// Left top: select hull and component
		pLeftTop = new Panel();
		pLeftTop.setLayout(new GridLayout(1, 2));
		pLeftTop.setBackground(Color.lightGray);
		pLeftTop.setForeground(Color.white);
		pHull = new Panel();
		pHull.setLayout(new BorderLayout());
		pHull.setBackground(Color.black);
		pHull.setForeground(Color.white);
		pHullText = new Panel();
		pHullText.setLayout(new GridLayout(3, 1));
		pHullText.setBackground(Color.black);
		pHullText.setForeground(Color.white);
		selectHull = new java.awt.Choice();
		selectHull.setBackground(Color.white);
		selectHull.setForeground(Color.black);
		imageCanvas1 = new ImageCanvas(pHull, "resources/images/empty.jpg");
		imageCanvas1.setBounds(0, 0, 150, 150);
		pHullText.add(new Label("Select Hull:"));
		pHullText.add(selectHull);
		pHullText.add(new Label(""));
		pHull.add("North", pHullText);
		pHull.add("Center", imageCanvas1);
		pLeftTop.add(pHull);
		pComponent = new Panel();
		pComponent.setLayout(new BorderLayout());
		pComponent.setBackground(Color.black);
		pComponent.setForeground(Color.white);
		pComponentText = new Panel();
		pComponentText.setLayout(new GridLayout(3, 1));
		pComponentText.setBackground(Color.black);
		pComponentText.setForeground(Color.white);
		selectShipArea = new java.awt.Choice();
		selectShipArea.setBackground(Color.white);
		selectShipArea.setForeground(Color.black);
		selectShipComponent = new java.awt.Choice();
		selectShipComponent.setBackground(Color.white);
		selectShipComponent.setForeground(Color.black);
		pComponentText.add(new Label("Select ship component:"));
		pComponentText.add(selectShipArea);
		pComponentText.add(selectShipComponent);
		imageCanvas2 = new ImageCanvas(pComponent, "resources/images/empty.jpg");
		imageCanvas2.setBounds(0, 0, 150, 150);
		pComponent.add("North", pComponentText);
		pComponent.add("Center", imageCanvas2);
		pLeftTop.add(pHull);
		pLeftTop.add(pComponent);
		pLeft.add(pLeftTop);
		// Left bottom: component details
		pLeftBottom = new Panel();
		pLeftBottom.setLayout(new BorderLayout());
		pLeftBottom.setBackground(Color.lightGray);
		pLeftBottom.setForeground(Color.white);
		pSelectedComp = new Panel();
		pSelectedComp.setLayout(new BorderLayout(3, 3));
		pSelectedComp.setBackground(Color.lightGray);
		pSelectedComp.setForeground(Color.white);
		pSelectedCompTech = new Panel();
		pSelectedCompTech.setLayout(new BorderLayout(3, 3));
		pSelectedCompTech.setBackground(Color.black);
		pSelectedCompTech.setForeground(Color.white);
		pSelectedCompTech.add("North", new Label("Technical data of selected component"));
		componentData = new java.awt.TextArea();
		componentData.setBackground(Color.white);
		componentData.setForeground(Color.black);
		componentData.setFont(new Font("Monospaced", Font.PLAIN, 12));
		componentData.setEditable(false);
		pSelectedCompTech.add("Center", componentData);
		pSelectedComp.add("Center", pSelectedCompTech);
		pLeftBottom.add(pSelectedComp);
		pLeft.add(pLeftBottom);
		pCenter.add(pLeft);
		pRight = new Panel();
		pRight.setLayout(new GridLayout(2, 2));
		pRight.setBackground(Color.lightGray);
		pRight.setForeground(Color.white);

		// right top: component list
		pCompList = new Panel();
		pCompList.setLayout(new BorderLayout());
		pCompList.setBackground(Color.black);
		pCompList.setForeground(Color.white);
		pCompListCommands = new Panel();
		pCompListCommands.setLayout(new GridLayout(1, 2));
		pCompListCommands.setBackground(Color.black);
		pCompListCommands.setForeground(Color.white);
		add = new Button("Add component");
		add.addActionListener(this);
		add.setActionCommand("addComponent");
		add.setForeground(Color.black);
		add.setBackground(Color.lightGray);
		add.setEnabled(false);
		remove = new Button("Remove component");
		remove.addActionListener(this);
		remove.setActionCommand("removeComponent");
		remove.setForeground(Color.black);
		remove.setBackground(Color.lightGray);
		remove.setEnabled(false);
		pCompListCommands.add(add);
		pCompListCommands.add(remove);
		pCompList.add("North", pCompListCommands);
		pCompListText = new Panel();
		pCompListText.setLayout(new BorderLayout());
		pCompListText.setBackground(Color.black);
		pCompListText.setForeground(Color.white);
		pCompListText.add("North", new Label("List of components in ship:"));
		shipData = new java.awt.TextArea();
		shipData.setBackground(Color.white);
		shipData.setForeground(Color.black);
		shipData.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pCompListText.add("Center", shipData);
		pCompList.add("Center", pCompListText);

		// bottom right: ship cost
		pShipCost = new Panel();
		pShipCost.setLayout(new BorderLayout());
		pShipCost.setBackground(Color.black);
		pShipCost.setForeground(Color.white);
		shipCost = new java.awt.TextArea();
		shipCost.setBackground(Color.white);
		shipCost.setForeground(Color.black);
		shipCost.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pShipCost.add("North", new Label("Ship cost:"));
		pShipCost.add("Center", shipCost);
		pRight.add(pCompList);
		pRight.add(pShipCost);
		pCenter.add(pRight);
		add("Center", pCenter);
		// South
		pSouth = new Panel();
		pSouth.setLayout(new GridLayout(1, 8));
		pSouth.setBackground(Color.black);
		pSouth.setForeground(Color.white);
		save = new Button("Save");
		save.addActionListener(this);
		save.setActionCommand("saveShipDesign");
		save.setForeground(Color.black);
		save.setBackground(Color.lightGray);
		save.setEnabled(false);
		close = new Button(CLOSE);
		close.addActionListener(this);
		close.setActionCommand(CLOSE);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		reset = new Button("Reset Window");
		reset.addActionListener(this);
		reset.setActionCommand("resetWindow");
		reset.setForeground(Color.black);
		reset.setBackground(Color.lightGray);
		delete = new Button("Delete design");
		delete.addActionListener(this);
		delete.setActionCommand("deleteShipDesign");
		delete.setForeground(Color.black);
		delete.setBackground(Color.lightGray);
		pSouth.add(reset);
		pSouth.add(delete);
		pSouth.add(new Label(""));
		pSouth.add(save);
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(close);
		add("South", pSouth);
	}

	private void init() {
		// display/new State
		readExistingShiphulls();
		readExistingShipDesigns();
		setExistingShipAreas();
		toggleToDisplayMode();
	}

	// Choices: Click/Select Event
	public void itemStateChanged(java.awt.event.ItemEvent e) {
		String chText = "";
		// toggle edit/new
		if ((e.getSource().equals(chNew)) || (e.getSource().equals(chEdit))) {
			if (toggle.getSelectedCheckbox().equals(chNew)) {
				toggleToNewMode();
			} else {
				toggleToDisplayMode();
			}
			show();
		}
		// ship designs
		if (e.getSource().equals(selectShipDesign)) {
			currentShipDesign = null;
			try {
				chText = selectShipDesign.getSelectedItem();
				Hashtable shipDesigns = player.getShipDesigns();
				ShipDesign temp = (ShipDesign) shipDesigns.get(chText);
				currentShipDesign = (ShipDesign) temp.clone();
				shipDesigns = null;
			} catch (Exception e2) {/* ignore this error */
			}
			if (currentShipDesign != null) {
				shipDesignName.setText(currentShipDesign.getShipClass());
				displayShipData();
				if (currentShipDesign.isShip()) {
					setExistingShipAreas();
				} else {
					setExistingSatelliteAreas();
				}
				selectHull.select(currentShipDesign.getHull());
				ItemEvent f = new ItemEvent(selectHull, e.getID(), selectHull, e.getStateChange());
				itemStateChanged(f);
			}
		}
		// possible hulls
		if (e.getSource().equals(selectHull)) {
			chText = selectHull.getSelectedItem();
			showComponentImage(imageCanvas1, chText);
			currentShipDesign.setHull(chText);
			ShipComponent temp = player.getShipComponent(chText);
			if (temp.getType().equals("Satellite")) {
				setExistingSatelliteAreas();
				currentShipDesign.setShip(false);
			} else {
				setExistingShipAreas();
				currentShipDesign.setShip(true);
			}
			selectShipArea.select(0);
			ItemEvent f = new ItemEvent(selectShipArea, e.getID(), selectShipArea, e.getStateChange());
			itemStateChanged(f);
			displayShipData();
		}
		// possible ship areas
		if (e.getSource().equals(selectShipArea)) {
			refreshSelectableShipComponents();
			ItemEvent f = new ItemEvent(selectShipComponent, e.getID(), selectShipComponent, e.getStateChange());
			itemStateChanged(f);
		}
		// possible ship components
		if (e.getSource().equals(selectShipComponent)) {
			chText = selectShipComponent.getSelectedItem();
			showComponentImage(imageCanvas2, chText);
			showComponentDetails(chText);
		}
	}

	private void readExistingShipDesigns() {
		selectShipDesign.removeItemListener(this);
		selectShipDesign.removeAll();
		Hashtable shipDesigns = player.getShipDesigns();
		Enumeration sdNames = shipDesigns.keys();
		String[] names = new String[shipDesigns.size()];
		int count = 0;
		while (sdNames.hasMoreElements()) {
			names[count] = (String) sdNames.nextElement();
			count++;
		}
		Arrays.sort(names);
		for (int i = 0; i < count; i++) {
			selectShipDesign.addItem(names[i]);
		}
		if (count == 0) {
			selectShipDesign.addItem("no existing design");
			ActionEvent f = new ActionEvent(toggle, ActionEvent.ACTION_PERFORMED, "toggle");
			actionPerformed(f);
		}
		shipDesigns = null;
		selectShipDesign.addItemListener(this);
	}

	private void readExistingShiphulls() {
		selectHull.removeItemListener(this);
		selectHull.removeAll();
		Enumeration possibleComponents = player.getPossibleComponents().elements();
		while (possibleComponents.hasMoreElements()) {
			ShipComponent sc = (ShipComponent) possibleComponents.nextElement();
			if ((sc.getType().equals("Hull")) || (sc.getType().equals("Satellite"))) {
				if (sc.getAllowBuild()) {
					selectHull.addItem(sc.getName());
				}
			}
		}
		selectHull.addItemListener(this);
	}

	public void refresh() {
		if (isVisible()) {
			init();
		}
	}

	private void refreshSelectableShipComponents() {
		selectShipComponent.removeItemListener(this);
		selectShipComponent.removeAll();
		String chText = selectShipArea.getSelectedItem();
		Enumeration possibleComponents = player.getPossibleComponents().elements();
		boolean isEmpty = true;
		while (possibleComponents.hasMoreElements()) {
			ShipComponent sc = (ShipComponent) possibleComponents.nextElement();
			if (sc.getType().equals(chText)) {
				if (sc.getAllowBuild()) {
					isEmpty = false;
					selectShipComponent.addItem(sc.getName());
				}
			}
		}
		if (isEmpty) {
			selectShipComponent.addItem("(no items available)");
		}
		// chShipArea.addItemListener(this);
		selectShipComponent.addItemListener(this);
	}

	public ShipYard(SpaceOpera so) {
		spaceOpera = so;
		player = spaceOpera.getCurrentPlayer();
		doStaticLayout();
		enableCloseButton(this);
		setLocationAndSize();
		init();
	}

	private void showComponentDetails(String name) {
		// lbComponentName.setText(name);
		Hashtable possibleComponents = player.getPossibleComponents();
		ShipComponent sc = (ShipComponent) possibleComponents.get(name);
		componentData.setText("Name:\n" + name);
		if (sc.getDescription() != "") {
			int start = 0;
			int next = 0;
			int lineLength = 40;
			String temp = "";
			componentData.append("\n\nDescription:\n");
			String description = sc.getDescription();
			while (description.length() > lineLength) {
				for (int j = lineLength; j > 20; j--) {
					if (description.charAt(j) == ' ') {
						componentData.append(description.substring(start, j) + "\n");
						description = description.substring(j + 1);
						break;
					}
				}
			}
			componentData.append(description + "\n");
		}

		if (sc.getBaseAmmo() != 0) {
			componentData.append("\nAmmunition:  " + sc.getBaseAmmo());
		}
		if (sc.getForce() != 0) {
			componentData.append("\nForce:       " + sc.getForce());
		}
		if (sc.getLifeCycles() != 0) {
			componentData.append("\nLifeCycles:  " + sc.getLifeCycles());
		}
		if (sc.getPayload() != 0) {
			componentData.append("\nPayLoad:     " + sc.getPayload());
		}
		if (sc.getRange() != 0) {
			componentData.append("\nRange:       " + sc.getRange());
		}
		if (sc.getWarpSpeed() != 0) {
			componentData.append("\nSpeed:       " + sc.getWarpSpeed());
		}
		if (sc.getWeight() != 0) {
			componentData.append("\nWeight:      " + sc.getWeight());
		}
		componentData.append("\n\n(Cost and weight may be depending on hull size.)");
	}

	private void showComponentImage(ImageCanvas ic, String chText) {
		try {
			ic.setImage(this, "resources/images/objects/" + player.getShipComponent(chText).getPicture() + ".jpg");
			ic.init();
			ic.display();
		} catch (Exception e) {
			ic.setImage(this, "resources/images/objects/comp.jpg");
			ic.init();
			ic.display();
		}
	}

	private void setExistingShipAreas() {
		selectShipArea.removeItemListener(this);
		selectShipArea.removeAll();
		selectShipArea.addItem(DRIVES);
		selectShipArea.addItem(ELECTRONICS);
		selectShipArea.addItem(SHIELDING);
		selectShipArea.addItem(SPECIAL);
		selectShipArea.addItem(WEAPONS);
		selectShipArea.addItemListener(this);
	}

	private void setExistingSatelliteAreas() {
		selectShipArea.removeItemListener(this);
		selectShipArea.removeAll();
		selectShipArea.addItem(ELECTRONICS);
		selectShipArea.addItem(SHIELDING);
		selectShipArea.addItem(WEAPONS);
		selectShipArea.addItemListener(this);
	}

	private void setLocationAndSize() {
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		setSize(SHIPYARD_SIZEX, SHIPYARD_SIZEY);
		// System.out.println(SHIPYARD_SIZEX + "x" +SHIPYARD_SIZEY); // only for
		// Debug - remove later
		setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);
		show();
	}

	private void toggleToDisplayMode() {
		selectHull.setEnabled(false);
		selectShipArea.setEnabled(false);
		selectShipComponent.setEnabled(false);
		add.setEnabled(false);
		remove.setEnabled(false);
		save.setEnabled(false);
		delete.setEnabled(true);
		lbShipDesignName.setText("Select existing design:");
		selectShipDesign.select(0);
		ItemEvent f = new ItemEvent(selectShipDesign, ItemEvent.ITEM_STATE_CHANGED, selectShipDesign, ItemEvent.SELECTED);
		itemStateChanged(f);
		pShipDesignName.remove(shipDesignName);
		pShipDesignName.add(selectShipDesign);
		toggle.setSelectedCheckbox(chEdit);
	}

	private void toggleToNewMode() {
		selectHull.setEnabled(true);
		selectShipArea.setEnabled(true);
		selectShipComponent.setEnabled(true);
		add.setEnabled(true);
		remove.setEnabled(true);
		save.setEnabled(true);
		delete.setEnabled(false);
		lbShipDesignName.setText("Enter name for new design:");
		pShipDesignName.remove(selectShipDesign);
		pShipDesignName.add(shipDesignName);
		shipDesignName.setText(NEW_SHIP_NAME);
		currentShipDesign.clear();
		// Select the first hull
		selectHull.select(0);
		ItemEvent f = new ItemEvent(selectHull, ItemEvent.ITEM_STATE_CHANGED, selectHull, ItemEvent.SELECTED);
		itemStateChanged(f);
		displayShipData();
	}
}
