//The class ShipMiniInfo is used to display some info about ships
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ships.ShipComponent;
import spaceopera.universe.ships.ShipOrderItem;
import spaceopera.universe.ships.SpaceCraft;

/**
 * Display the currently selected ship
 */
public class ShipMiniInfo extends JPanel implements SOConstants {

	private static final long serialVersionUID = 1L;
	// private JButton setOrder;
	private JPanel labelPanel, imagePanel; // left, right
	private JLabel labelEta, labelOrders, imageLabel;
	private SpaceCraft spaceCraft;
	private Fullscreen fs;

	public ShipMiniInfo(SpaceCraft s, Fullscreen fs) {
		spaceCraft = s;
		this.fs = fs;
	}

	// public void actionPerformed(ActionEvent event) {
	// String c = event.getActionCommand();
	// if (c.equals("setOrders")) {
	// fs.showShipOrders();
	// }
	// }

	public void setShipLayout(JPanel container) {

		labelEta = new JLabel();
		labelEta.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		labelEta.setForeground(Color.YELLOW);
		// labelEta.setPreferredSize(d);
		// labelEta.setMaximumSize(d);
		// labelEta.setMinimumSize(d);
		labelOrders = new JLabel();
		labelOrders.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		labelOrders.setForeground(Color.YELLOW);
		// labelOrders.setPreferredSize(d);
		// labelOrders.setMaximumSize(d);
		// labelOrders.setMinimumSize(d);

		labelPanel = new JPanel();
		labelPanel.setOpaque(true);
		labelPanel.setLayout(new GridLayout(6, 1));
		labelPanel.setBackground(Color.black);

		labelPanel.add(labelEta);
		labelPanel.add(labelOrders);
		labelPanel.add(new JLabel(""));
		labelPanel.add(new JLabel(""));
		labelPanel.add(new JLabel(""));
		labelPanel.add(new JLabel(""));

		// TODO see Fullscreen.getShipControl()
		// setOrder = new JButton("Ship orders");
		// setOrder.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
		// setOrder.setBackground(Color.DARK_GRAY);
		// setOrder.setForeground(Color.CYAN);
		// setOrder.setActionCommand("setOrders");
		// setOrder.addActionListener(this);
		// setOrder.setPreferredSize(new Dimension(120,30));

		// if (spaceCraft != null && spaceCraft.isShip()) {
		// labelPanel.add(setOrder);
		// } else {
		// labelPanel.add(new JLabel(""));
		// }
		// labelPanel.add(new JLabel(""));

		imagePanel = new JPanel();
		imagePanel.setBackground(Color.black);
		imagePanel.setForeground(Color.black);
		imageLabel = new JLabel();
		// todo: get actual image from ship.shipdesign
		if (spaceCraft != null) {
			String hull = spaceCraft.getShipDesign().getHull();
			ShipComponent sc = spaceCraft.getPlayer().getShipComponent(hull);
			String image = sc.getPicture();
			// String path = "images/s_colonyship.jpg";
			String path = "images/objects/" + image + ".jpg";
			try {
				imageLabel.setIcon(new ImageIcon(ClassLoader.getSystemResource(path)));
				imageLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
				imagePanel.add(imageLabel);
			} catch (Exception e) {
				System.err.println("ShipOrders exception " + e + ", couldn't find file: " + path);
			}
		}
		// container.setLayout(new BoxLayout(container, BoxLayout.LINE_AXIS ));

		container.setLayout(new GridLayout(1, 2, 0, 0));

		container.add(labelPanel);
		container.add(imagePanel);
		container.revalidate();
	}

	public void init() {
		if (spaceCraft != null) {
			labelEta.setText("ETA: " + spaceCraft.getTurns() + " turns.");
			if (spaceCraft.getCurrentOrder().getCommand().equals(NONE) && spaceCraft.getNextOrderItems().size() > 0) {
				ShipOrderItem nextOrder = (ShipOrderItem) spaceCraft.getNextOrderItems().elementAt(0);
				//XXX Xpand logic when other command options available
				if ("auto".equals(nextOrder.getOption())) {
					labelOrders.setText("Orders: " + nextOrder.getOption() + " " + nextOrder.getCommand());
				} else {
					labelOrders.setText("Orders: " + nextOrder.getCommand() + " " + nextOrder.getTarget());
				}

			} else {
				labelOrders.setText("Orders: " + spaceCraft.getCurrentOrder().toString());
			}
		}

	}

	public void refresh() {
		init();
	}

}