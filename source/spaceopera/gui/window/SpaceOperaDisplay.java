//The class SpaceOperaDisplay contains the main display elements
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.ImageCanvas;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Ship;

/**
 * Class SpaceOperaDisplay contains Menus, displays the Universe, the Sunsystem and the planet display.
 */
@SuppressWarnings({ "rawtypes" })
public class SpaceOperaDisplay extends JFrame implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private Universe universe;
	private SpaceOpera spaceOpera;
	// private SunSystem sunSystem;
	private JMenu gameMenu;
	private JMenu selectMenu;
	private JMenu listMenu;
	private JMenu taskMenu;
	private JMenu settingsMenu;
	private JMenu helpMenu;

	private JToolBar toolBar = new JToolBar();
	private JButton newMenuButton = new JButton();
	private JButton nextStarButton = new JButton();
	private JButton nextPlanetButton = new JButton();
	private JButton nextColonyButton = new JButton();
	private JButton nextShipButton = new JButton();
	private JButton scienceDetailButton = new JButton();
	private JButton shipYardButton = new JButton();
	private JButton nextTurnButton = new JButton();

	private Graphics2D g2;
	private ImageCanvas imageCanvas;
	private JPanel sunSystemAndPlanetImage;
	private JPanel commandButtons;
	private JButton nextTurn;
	private JLabel currentYear;
	private JLabel coordinates;
	private JLabel treasury;
	private Color colorOfHumanPlayer; // for scanners
	private BufferedImage bufferedImage = null; // for scanners

	// Menu text and commands
	private String[] gameLabels = new String[] { "New Game", "Open", "Save", "SaveAs", "Quit" };
	private String[] gameCommands = new String[] { "new", "open", "save", "saveAs", "exit" };
	private String[] selectLabels = new String[] { "Next Colony", "Previous Colony", "Next Star", "Previous Star", "Next Planet",
			"Previous Planet", "Next Ship", "Previous Ship" };

	private String[] selectCommands = new String[] { "displayNextColony", "displayPreviousColony", "displayNextStar",
			"displayPreviousStar", "displayNextPlanet", "displayPreviousPlanet", "selectNextShip", "selectPreviousShip" };

	private String[] listLabels = new String[] { "Show all SunSystems", "Show all Planets", "Show all colonies", "Show all Ships" };

	private String[] listCommands = new String[] { "showSunSystems", "showPlanets", "showColonies", "showShips" };

	private String[] taskLabels = new String[] { "Next Turn", "Science settings", "Shipyard", "Politics", "Trade" };

	private String[] taskCommands = new String[] { "nextTurn", "science", "shipyard", "politics", "trade" };

	private String[] settingsLabels = new String[] {
			// "Animations", "Effects", "Music", "Other", "Some cheats", "Test animation
			// event" };
			"Animations", "Effects", "Music", "Some cheats", "Test Shiporder class", "Test animation event" };

	private String[] settingsCommands = new String[] {
			// "animations", "effects", "music", "other", "cheat" , "testevent"};
			"animations", "effects", "music", "cheat", "shiporders", "testevent" };

	public SpaceOperaDisplay(Universe u, SpaceOpera so) {
		universe = u;
		spaceOpera = so;
		// getContentPane().setLayout(new BorderLayout(2, 2));
		setBackground(Color.black);
		setSize(500 + SUNSYSTEMFRAMEX + 14, 500 + 40 + 36 + 36);
		// 36 pixels for command-bar 36 pixels for Swing toolbar ?
		// setTitle("Space Opera V " + VERSIONSTRING);
		// >>> center-patch
		// Toolkit kit = Toolkit.getDefaultToolkit();
		// Dimension dim = kit.getScreenSize();
		// setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
		// setLocation(0, 0);
		// <<<
		addJMenuBar();
		addJToolBar();
		addUniverseDisplay();
		addSunSystemDisplay();
		addPlanetImageDisplay(null);
		addCommandButtonsDisplay();
		getContentPane().setCursor(Cursor.getDefaultCursor());

		Toolkit tk = Toolkit.getDefaultToolkit();
		int xSize = ((int) tk.getScreenSize().getWidth());
		int ySize = ((int) tk.getScreenSize().getHeight());
		this.setSize(xSize, ySize);
		this.setUndecorated(true);

		show();

		// setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		Sun sun = null;
		Graphics g = null;
		Ship ship = null;
		Player player = null;
		boolean ok = false;
		Vector playerships = null;
		Vector players = null;
		Vector suns = null;

		if (c.equals("new")) {

			startNewGame();

		} else if (c.equals("open")) {
		} else if (c.equals("save")) {
		} else if (c.equals("saveAs")) {
		} else if (c.equals("exit")) {

			spaceOpera.quit();

		} else if (c.equals("nextTurn")) {

			spaceOpera.nextTurn();

		} else if (c.equals("displayNextColony")) {

			spaceOpera.displayNextColony();

		} else if (c.equals("displayPreviousColony")) {

			spaceOpera.displayPreviousColony();

		} else if (c.equals("displayNextStar")) {

			spaceOpera.displayNextStar();

		} else if (c.equals("displayPreviousStar")) {

			spaceOpera.displayPreviousStar();

		} else if (c.equals("displayNextPlanet")) {

			spaceOpera.displayNextPlanet();

		} else if (c.equals("displayPreviousPlanet")) {

			spaceOpera.displayPreviousPlanet();

		} else if (c.equals("selectNextShip")) {

			spaceOpera.selectNextShip();

		} else if (c.equals("selectPreviousShip")) {

			spaceOpera.selectPreviousShip();

		} else if (c.equals("shiporders")) {

			spaceOpera.showShipOrders();

		} else if (c.equals("showShips")) {

			spaceOpera.showShipList();

		} else if (c.equals("showSunSystems")) {

			spaceOpera.showSunSystemList();

		} else if (c.equals("showColonies")) {

			spaceOpera.showColonyList();

		} else if (c.equals("showPlanets")) {

			spaceOpera.showPlanetList();

		} else if (c.equals("help")) {

			spaceOpera.showHelp();

		} else if (c.equals("license")) {

			spaceOpera.showLicense();

		} else if (c.equals("gpl")) {

			spaceOpera.showGPL();

		} else if (c.equals("cheat")) {

			spaceOpera.showCheatWindow();

		} else if (c.equals("testevent")) {

			spaceOpera.testEvent();

		} else if (c.equals("science")) {

			spaceOpera.showScienceDetail();

		} else if (c.equals("shipyard")) {

			spaceOpera.showShipYard();

		} else if (c.equals("music")) {

			spaceOpera.showMusicSettings();

		} else if (c.equals("animations")) {

			spaceOpera.showAnimSettings();

		} else if (c.equals("effects")) {

			spaceOpera.showEffectSettings();

			// >>> add by SR-040224 for more GameSettings
			// } else if (c.equals("other")) {
			// if (mnuSetOther != null) {
			// mnuSetOther.dispose();
			// mnuSetOther=null;
			// }
			// mnuSetOther = new MenuSettingsOther(this);
			// <<<

		} else if (c.equals("trade")) {

			// Menu trade

		} else if (c.equals("politics")) {

			// Menu Diplomacy

		}
	}

	private void addJMenuBar() {
		JMenuBar mb = new JMenuBar();
		gameMenu = new JMenu("Game");
		for (int i = 0; i < gameLabels.length; i++) {
			JMenuItem mi = new JMenuItem(gameLabels[i]);
			mi.setActionCommand(gameCommands[i]);
			mi.addActionListener(this);
			// deactivate unfinished menu-items
			if ((i > 0) && (i < 4)) {
				mi.setEnabled(false);
			}
			if ((i == 1) || (i == 4)) {
				gameMenu.addSeparator();
			}
			gameMenu.add(mi);
		}
		mb.add(gameMenu);
		selectMenu = new JMenu("Select Object");
		for (int i = 0; i < selectLabels.length; i++) {
			if ((i > 0) && (i % 2 == 0)) {
				selectMenu.addSeparator();
			}
			JMenuItem mi = new JMenuItem(selectLabels[i]);
			mi.setActionCommand(selectCommands[i]);
			mi.addActionListener(this);
			selectMenu.add(mi);
		}
		mb.add(selectMenu);
		listMenu = new JMenu("Show lists");
		for (int i = 0; i < listLabels.length; i++) {
			if (i > 0) {
				listMenu.addSeparator();
			}
			JMenuItem mi = new JMenuItem(listLabels[i]);
			mi.setActionCommand(listCommands[i]);
			mi.addActionListener(this);
			listMenu.add(mi);
		}
		mb.add(listMenu);

		taskMenu = new JMenu("Tasks");
		for (int i = 0; i < taskLabels.length; i++) {
			if (i > 0) {
				taskMenu.addSeparator();
			}
			JMenuItem mi = new JMenuItem(taskLabels[i]);
			mi.setActionCommand(taskCommands[i]);
			mi.addActionListener(this);
			// XXX: activate unfinished menu-items
			if (i > 2) {
				mi.setEnabled(false);
			}
			taskMenu.add(mi);
		}
		mb.add(taskMenu);

		settingsMenu = new JMenu("Settings");
		for (int i = 0; i < settingsLabels.length; i++) {
			if (i > 2) {
				settingsMenu.addSeparator();
			}
			JMenuItem mi = new JMenuItem(settingsLabels[i]);
			mi.setActionCommand(settingsCommands[i]);
			mi.addActionListener(this);
			settingsMenu.add(mi);
		}
		mb.add(settingsMenu);

		// setJMenuBar(mb);
		helpMenu = new JMenu("Help");
		JMenuItem mi = new JMenuItem("Help");
		mi.setActionCommand("help");
		mi.addActionListener(this);
		helpMenu.add(mi);
		helpMenu.addSeparator();
		JMenuItem li = new JMenuItem("License information");
		li.setActionCommand("license");
		li.addActionListener(this);
		helpMenu.add(li);
		mb.add(helpMenu);
		JMenuItem gi = new JMenuItem("GPL Information");
		gi.setActionCommand("gpl");
		gi.addActionListener(this);
		helpMenu.add(gi);
		mb.add(helpMenu);
		mb.setCursor(Cursor.getDefaultCursor());
		setJMenuBar(mb);
	}

	public void addJToolBar() {
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		flowLayout.setHgap(0);
		flowLayout.setVgap(0);
		toolBar.setLayout(flowLayout);

		// start a new game
		newMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NewGame.gif")));
		newMenuButton.setToolTipText("New Game");
		newMenuButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startNewGame();
			}
		});
		toolBar.add(newMenuButton);
		toolBar.addSeparator();

		// show next starsystem
		nextStarButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NextStar.gif")));
		nextStarButton.setToolTipText("Next Starsystem");
		nextStarButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.displayNextStar();
			}
		});
		toolBar.add(nextStarButton);

		// show next planet
		nextPlanetButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NextPlanet.gif")));
		nextPlanetButton.setToolTipText("Next Planet");
		nextPlanetButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.displayNextPlanet();
			}
		});
		toolBar.add(nextPlanetButton);

		// show next colony
		nextColonyButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NextColony.gif")));
		nextColonyButton.setToolTipText("Next Colony");
		nextColonyButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.displayNextColony();
			}
		});
		toolBar.add(nextColonyButton);

		// show next ship
		nextShipButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NextShip.gif")));
		nextShipButton.setToolTipText("Next Ship");
		nextShipButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.selectNextShip();
				// spaceOpera.shipOrders();
				// TODO spaceOpera.showShipInfo();
			}
		});
		toolBar.add(nextShipButton);

		// Research
		toolBar.addSeparator();
		scienceDetailButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/Research.gif")));
		scienceDetailButton.setToolTipText("Show Research Settings");
		scienceDetailButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.showScienceDetail();
			}
		});
		toolBar.add(scienceDetailButton);

		// Shipyard
		shipYardButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/ShipYard.gif")));
		shipYardButton.setToolTipText("Shipyard");
		shipYardButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.showShipYard();
			}
		});
		toolBar.add(shipYardButton);

		// next turn
		toolBar.addSeparator();
		toolBar.addSeparator();
		nextTurnButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/icons/NextTurn.gif")));
		nextTurnButton.setToolTipText("Next Turn");
		nextTurnButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spaceOpera.nextTurn();
			}
		});
		toolBar.add(nextTurnButton);

		toolBar.setFloatable(false); // Toolbar kann vom User nicht
		// verschoben werden
		getContentPane().add("North", toolBar);
	}

	public void changePlanetImage(Planet planet) {
		changePlanetImageDisplay(planet);
		repaint();
	}

	public void createBufferedImage() {
		bufferedImage = null;
		bufferedImage = new BufferedImage(1280, 1280, BufferedImage.TYPE_INT_ARGB);
		g2 = bufferedImage.createGraphics();
		// if (ANTIALIAS == true) {
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// }
	}

	private void addCommandButtonsDisplay() {
		commandButtons = new JPanel();
		commandButtons.setLayout(new BoxLayout(commandButtons, BoxLayout.LINE_AXIS));
		commandButtons.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		commandButtons.setBackground(Color.lightGray);
		// commandButtons.setBounds(0, universe.getUniverseSize() + 8 + 30, universe.getUniverseSize() + 258, 20);
		currentYear = new JLabel("   Year: " + (STARTYEAR + 1));
		commandButtons.add(currentYear);
		commandButtons.add(Box.createRigidArea(new Dimension(15, 0)));
		coordinates = new JLabel("x: 0, y: 0");
		commandButtons.add(coordinates);
		commandButtons.add(Box.createRigidArea(new Dimension(15, 0)));
		treasury = new JLabel("Total treasury: 0.0");
		commandButtons.add(treasury);
		commandButtons.add(Box.createHorizontalGlue());
		nextTurn = new JButton("next Turn");
		nextTurn.addActionListener(this);
		nextTurn.setActionCommand("nextTurn");
		commandButtons.add(nextTurn);
		getContentPane().add("South", commandButtons);
	}

	private void addUniverseDisplay() {

		// JScrollPane jsp = new JScrollPane(universe.getUniverseDisplay(), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
		// JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//
		// // jsp.setViewportView(universe.getUniverseDisplay());
		// jsp.getVerticalScrollBar().setUnitIncrement(10);
		// jsp.getHorizontalScrollBar().setUnitIncrement(10);
		// getContentPane().add("Center", jsp);
	}

	private void addSunSystemDisplay() {
		sunSystemAndPlanetImage = new JPanel();
		sunSystemAndPlanetImage.setLayout(new GridLayout(2, 1, 0, 2));
		sunSystemAndPlanetImage.setBackground(Color.lightGray);
		// sunSystemAndPlanetImage.add(spaceOpera.getSunSystemDisplay());

		// Dimension preferredSize=new Dimension(SUNSYSTEMFRAMEX,SUNSYSTEMFRAMEY);
		// sunSystemAndPlanetImage.setPreferredSize(preferredSize);
	}

	private void addPlanetImageDisplay(Planet planet) {
		String image = "";
		if (planet == null) {
			image = "resources/images/empty.jpg";
		} else {
			image = "resources/images/bodies/" + planet.getImage();
		}
		imageCanvas = new ImageCanvas(this, image);

		// imageCanvas.setBounds(universe.getUniverseSize() + SUNSYSTEMLEFT, PLANETTOP, SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
		sunSystemAndPlanetImage.add(imageCanvas);
		// imageCanvas.init();
		imageCanvas.display();
		getContentPane().add("East", sunSystemAndPlanetImage);
	}

	private void changePlanetImageDisplay(Planet planet) {
		String image = "";
		if (planet == null) {
			image = "resources/images/empty.jpg";
		} else {
			image = "resources/images/bodies/" + planet.getImage();
		}
		imageCanvas.setImage(this, image);
		imageCanvas.init();
		imageCanvas.display();
	}

	public void enableCloseButton(Frame me) {
		me.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				spaceOpera.disposeOpenWindows();
				try {
					Thread.sleep(500);
				} catch (Exception e2) {/* ignore this error */
				}
				dispose();
			}
		});
	}

	public boolean isColonyScanned(Colony colony) {
		int rgb = (bufferedImage.getRGB(colony.getSunX(), colony.getSunY()));
		if (colorOfHumanPlayer.getRGB() == rgb) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isShipScanned(Ship ship) {
		// System.out.println("X: " + ship.getX());
		// System.out.println("Y: " + ship.getY());
		int rgb = bufferedImage.getRGB(ship.getX(), ship.getY());
		if (colorOfHumanPlayer.getRGB() == rgb) {
			return true;
		} else {
			return false;
		}
	}

	public void paintHumanScanArea(Player player) {
		colorOfHumanPlayer = player.getColor();
		g2.setColor(Color.black);
		g2.fillRect(0, 0, 1280, 1280);
		g2.setColor(player.getColor());
		universe.paintScanArea(player, g2);
	}

	public void setCoordinates(String s) {
		if (s != null) {
			coordinates.setText(s);
		}
	}

	public void setCurrentYear(String year) {
		currentYear.setText(year);
	}

	public void setTreasury(String sum) {
		treasury.setText(sum);
	}

	private void startNewGame() {
		// clear remains of the old game?
		spaceOpera.disposeOpenWindows();
		// Settings for the new game
		// NewGameSettings newGameSettings = null;
		// newGameSettings = new NewGameSettings(spaceOpera);

	}

}
