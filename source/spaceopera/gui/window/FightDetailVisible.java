//The class FightDetailVisible shows fights between ships in sunsystems in the SpaceOpera game
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.objects.battle.FightShip;
import spaceopera.gui.objects.battle.FightTorpedoWeapon;
import spaceopera.universe.SOConstants;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;

/** The class FightDetailVisible shows the solar system and all your and your enemies ships,
 *  it let's you select ships, define actions for them, etc.
 */
public class FightDetailVisible extends FightDetail implements ActionListener, MouseListener,
                                                               MouseMotionListener, SOConstants {
  private static final long serialVersionUID = 1L;
private Image offScreenImage;
  private Graphics offScreenGraphics;
  private Dimension offScreenSize;

  private int[] bgStarsX;
  private int[] bgStarsY;
  private boolean shipSelected = false;
  private Panel pCenter;
  private Panel pSouth;
  private Button attack;
  private Button defend;
  private Button wait;
  private Button flee;
  private Button step;
  private Button slow;
  private Button fast;
  private Button close;

  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();

    if (c.equals("attack")) {
       shipSelected=false;
       if (orderSource!=null) {
         ((FightShip)orderSource).setOrder("attack");
       }
       ((FightShip)orderSource).setOrderTarget(orderTarget);
    }

    if (c.equals("defend")) {
      shipSelected=false;
      if (orderSource!=null) {
        ((FightShip)orderSource).setOrder("defend");
      }
      ((FightShip)orderSource).setOrderTarget(orderTarget);
    }

    if (c.equals("wait")) {
      shipSelected=false;
      if (orderSource!=null) {
        ((FightShip)orderSource).setOrder("wait");
      }
    }

    if (c.equals("flee")) {
      shipSelected=false;
      if (orderSource!=null) {
        ((FightShip)orderSource).setOrder("flee");
      }
    }

    // single step mode
    if (c.equals("step")) {
       stepSleepTime=200; // ms
       laserSleepTime=180; // ms
       bombStepTime=150;
       bombSleepTime=180;
       executeStep(this.getGraphics());
       display();
    }

    // continuous steps
    if (c.equals("slow")) {
       stepSleepTime=150; // ms
       laserSleepTime=80; // ms
       bombStepTime=150;
       bombSleepTime=80;
       display();
       executeAllSteps(this.getGraphics());
       display();
    }

    // fast steps
    if (c.equals("fast")) {
       stepSleepTime=50; // ms
       laserSleepTime=25; // ms
       bombStepTime=150;
       bombSleepTime=25;
       display();
       executeAllSteps(this.getGraphics());
       display();
    }

    // execute hidden and quit
    if (c.equals(CLOSE)) {
      this.hide();
      stepSleepTime=0; // ms
      laserSleepTime=0; // ms
      bombStepTime=0;
      bombSleepTime=0;
      display();
      executeAllSteps(null);
      this.dispose();
    }
  }


  private void disableOrderButtons() {
     attack.setEnabled(false);
     defend.setEnabled(false);
     wait.setEnabled(false);
     flee.setEnabled(false);
  }


  public void display() {
    this.paint(this.getGraphics());
  }


  private void  doStaticLayout() {
      setLayout(new BorderLayout(2,2));
      // command-Buttons
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(2,10));
      pSouth.setBackground(Color.gray);
      attack = new Button("Attack");
      attack.addActionListener(this);
      attack.setActionCommand("attack");
      attack.setForeground(Color.black);
      attack.setBackground(Color.lightGray);
      pSouth.add(attack);
      defend = new Button("Defend");
      defend.addActionListener(this);
      defend.setActionCommand("defend");
      defend.setForeground(Color.black);
      defend.setBackground(Color.lightGray);
      defend.setEnabled(false);
      pSouth.add(defend);
      step = new Button("Execute Step");
      step.addActionListener(this);
      step.setActionCommand("step");
      step.setForeground(Color.black);
      step.setBackground(Color.lightGray);
      pSouth.add(step);
      slow = new Button("Execute Slow");
      slow.addActionListener(this);
      slow.setActionCommand("slow");
      slow.setForeground(Color.black);
      slow.setBackground(Color.lightGray);
      pSouth.add(slow);
      fast = new Button("Execute Fast");
      fast.addActionListener(this);
      fast.setActionCommand("fast");
      fast.setForeground(Color.black);
      fast.setBackground(Color.lightGray);
      pSouth.add(fast);
      wait = new Button("Wait");
      wait.addActionListener(this);
      wait.setActionCommand("wait");
      wait.setForeground(Color.black);
      wait.setBackground(Color.lightGray);
      wait.setEnabled(false);
      pSouth.add(wait);
      flee = new Button("Flee");
      flee.addActionListener(this);
      flee.setActionCommand("flee");
      flee.setForeground(Color.black);
      flee.setBackground(Color.lightGray);
      flee.setEnabled(false);
      pSouth.add(flee);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      close = new Button("Execute & Close");
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      pSouth.add(close);
      add("South",pSouth);
      disableOrderButtons();
      enableCloseButton(this);
  }


  private void enableOrderButtons() {
     attack.setEnabled(true);
     //defend.enable();
     //wait.enable();
     //flee.enable();
  }


  /* FightDetail Constructor
   * initialize all ships, satellites and battlestations in this sunsystem
   * so they can be painted with the paint() method.
   * *****
   * entweder   class Ship mit neuen displayFight() methoden und neuen Variablen ausstatten
   * oder       eine neue Klasse FightDetailObject entwerfen, welche f�r alle solchen Objekte
   * instanziert wird.z.B. m�ssen auch Orders f�r die Ships festgelegt werden k�nnen,
   * z.B. 'Schiff 37' 'longrange' angreifen, oder 'Planet2' 'verteidigen' oder 'warten und verteidigen' etc.
   * ******
   * ebenfalls m�ssen tempor�re Koordinaten (g�ltig w�hrend des Kampfes, werden von Zug zu Zug
   * auf Null gestellt) verwaltet werden k�nnen.
   * ******
   * Alle Ships positionieren, keine Befehle gesetzt, zur Zeit nur f�r 2 Player geeignet,
   * wenn mehr als ca. 20 Ships, werden diese nicht mehr angezeigt.
   */
  public FightDetailVisible (Sun s, SpaceOpera so) {
      super(s, so);
      addMouseListener(this);
      addMouseMotionListener(this);
      setTitle("Battle for sunsystem " + sun.getName());

      bgStarsX = new int[FIGHTBACKGROUNDSTARS];
      bgStarsY = new int[FIGHTBACKGROUNDSTARS];

      for (int i = 0; i<FIGHTBACKGROUNDSTARS; i++) {
        bgStarsX[i] = (int)(Math.random() * FIGHTFRAMEX);
        bgStarsY[i] = (int)(Math.random() * FIGHTFRAMEY);
      }

      doStaticLayout();

      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();   
      pack();
      setSize(WINDOWSIZEX,WINDOWSIZEY+50);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      show();
  }


  public void mouseClicked(MouseEvent event) {
     int x = event.getX();
     int y = event.getY();
     oldX = x;
     oldY = y;
     boolean hitObject = false;
     Graphics g = this.getGraphics();
     orderTarget = null;
     if (shipSelected) {
     for (int i = 0; i<planets.size(); i++) {
       Planet p = (Planet)planets.elementAt(i);
       if (p.inside(x,y)) {
         orderTarget = p;
         p.setSelected(true);
         hitObject=true;
       }
     }
     if (!hitObject) {
       for (int i=0; i<fighterShips.size(); i++) {
         FightShip fs = (FightShip)fighterShips.elementAt(i);
         if (fs.inside(x,y)){
           orderTarget = fs;
           hitObject=true;
         }
       }
     }
     if (! hitObject) {
       orderSource=null;
       orderTarget=null;
       g.setColor(Color.black);
       g.drawLine(fromX, fromY, oldX, oldY);
       disableOrderButtons();
       shipSelected=false;
       for (int i=0; i<fighterShips.size(); i++) {
         FightShip fs = (FightShip)fighterShips.elementAt(i);
         fs.setSelected(false);
         fs.display(g);
       }
     }
   } else {
     disableOrderButtons();
     orderSource=null;
     g.setColor(Color.black);
     g.drawLine(fromX, fromY, oldX, oldY);
     for (int i=0; i<fighterShips.size(); i++) {
       FightShip fs = (FightShip)fighterShips.elementAt(i);
       fs.setSelected(false);
       if (fs.inside(x,y)){
         if (fs.getShip().getPlayer() == parent.getCurrentPlayer()) {
           orderSource=fs;
           enableOrderButtons();
           shipSelected=true;
           fromX = fs.getX();
           fromY = fs.getY();
           fs.setSelected(true);
         }
       }
       fs.display(g);
     }
   }
   display();
  }

  public void mouseDragged(MouseEvent event) { }
  public void mouseEntered(MouseEvent event) { }
  public void mouseExited(MouseEvent event) { }

  public void mouseMoved(MouseEvent event) {
     int x = event.getX();
     int y = event.getY();
     Graphics g = this.getGraphics();
     if ((shipSelected) && (orderTarget==null)) {
       moveCounter++;
       g.setColor(Color.black);
       g.drawLine(fromX, fromY, oldX, oldY);
       g.setColor(Color.red);
       g.drawLine(fromX, fromY, x, y);
       oldX = x;
       oldY = y;
     }
     if ((shipSelected) && (orderTarget!=null)) {
           g.setColor(Color.red);
           g.drawLine(fromX, fromY, oldX, oldY);
     }
     if (moveCounter%20==0) {
        moveCounter=1;
        display();
     }
   }

  public void mousePressed(MouseEvent event) { }
  public void mouseReleased(MouseEvent event) { }


  public void paint (Graphics g) {
      Dimension d = getSize();
      g.setColor(Color.black);
      g.fillRect(0,0,d.width,d.height);
      g.setColor(Color.white);
      for (int i = 0; i<SSBACKGROUNDSTARS; i++) {
        g.drawOval(bgStarsX[i],bgStarsY[i],1,1);
      }
      int x = FIGHTFRAMEX/2;
      int y = FIGHTFRAMEY/2;
      g.setColor(sun.getColor());
      int mitte = STARSIZE / 2;
      g.fillOval(x,y,STARSIZE,STARSIZE);
      g.drawLine(x+mitte-1, y-2, x+mitte-1, y+STARSIZE+1);  // vertical Line
      g.drawLine(x+mitte, y-2, x+mitte, y+STARSIZE+1);
      g.drawLine(x-2, y+mitte, x+STARSIZE+1, y+mitte);      // horizontal Line
      g.drawLine(x-2, y+mitte-1, x+STARSIZE+1, y+mitte-1);
      for (int i = 0; i<planets.size(); i++) {
          Planet p = (Planet)planets.elementAt(i);
          p.setSelected(false);
          //if (p.getColony() != null){
          //   defendingPlayer = p.getPlayer();
          //}
          g.setColor(Color.white);
          int radius = p.getOrbit().getRadius();
          int position = p.getOrbit().getPosition();
          g.drawOval((x+STARSIZE/2)-2*radius,
                    (y+STARSIZE/2)-2*radius,4*radius,4*radius);
          int px = x+STARSIZE/2 + (int)(2*radius * Math.cos(position));
          int py = y+STARSIZE/2 + (int)(2*radius * Math.sin(position));
          p.display(g,px,py);
      }
      for (int i=0; i<fighterShips.size(); i++) {
         FightShip fs = (FightShip)fighterShips.elementAt(i);
         fs.display(g);
      }
      for (int i=0; i<torpedos.size(); i++) {
         FightTorpedoWeapon ft = (FightTorpedoWeapon)torpedos.elementAt(i);
         ft.display(g);
      }
  }

  public final synchronized void update (Graphics g) {
    Dimension d = getSize();
    if ((offScreenImage == null) || (d.width != offScreenSize.width) || (d.height != offScreenSize.height)) {
      offScreenImage = createImage(d.width,d.height);
      offScreenSize = d;
      offScreenGraphics = offScreenImage.getGraphics();
      paint(offScreenGraphics);
      g.drawImage(offScreenImage,0,0,null);
    }
  }

}