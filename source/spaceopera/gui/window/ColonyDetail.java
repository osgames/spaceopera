//The class ColonyDetail displays a colony and provides a interface for colony management.
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.TableModel;

import spaceopera.gui.components.ExistingBuildingsDetail;
import spaceopera.gui.components.ProductionQueueDetail;
import spaceopera.gui.controls.SoButton;
import spaceopera.gui.window.helper.LabelHelper;
import spaceopera.gui.window.helper.PanelHelper;
import spaceopera.gui.window.helper.TableHelper;
import spaceopera.gui.window.list.model.ColonyDataModel;
import spaceopera.gui.window.list.model.ExistingBuildingsTableModel;
import spaceopera.gui.window.list.model.PossibleBuildingsTableModel;
import spaceopera.gui.window.list.model.QueueDataModel;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.colony.Product;

/**
 * The ColonyDetail class is used to display the colony details, set the production Queue, distribute production factors and display
 * some basic colony settings.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
// TODO implements ActionListener, , ItemListener, ?
public class ColonyDetail extends JPanel implements ChangeListener, SOConstants {
	private static final long serialVersionUID = 1L;
	private Colony colony = null;
	// private SpaceOpera spaceOpera = null;
	private Fullscreen fs = null;
	private Player player = null;
	private JSlider slider[] = new JSlider[4];
	private JLabel titleLabel, settingsTitleLabel, production, research, culture, taxes, optionsTitleLabel, queueTitleLabel;
	private JPanel titlePanel, centerPanel, dataPanel, settingsTitlePanel, colonyManager, optionsTitlePanel, settingsSliderPanel,
			productionQueue, leftTopPanel, leftBottomPanel, rightTopPanel, rightBottomPanel, distribution, buildingsControl;
	// jbShowBuilding, jbRushJob;
	private DefaultListModel listProductionQueueModel, listBuildingsModel;
	private ProductionQueueDetail productionQueueDetail;
	private ExistingBuildingsDetail existingBuildingsDetail;
	private JTable colonyData, queueData, possibleBuildings, existingBuildings;
	private QueueDataModel queueDataModel;
	private JTabbedPane buildings;
	protected String buildingFilter;

	public Colony getColony() {
		return (colony);
	}

	// //TODO spaceOpera.displayPreviousColony();

	// } else if (c.equals("showResources")) {
	// //TODO spaceOpera.displayColonyResources();

	// } else if (c.equals("showSelected")) {
	// i = listProductionQueue.getSelectedIndex();
	// getBuildProject(i);

	// }
	// }
	// } else if (c.equals("remove")) {
	// // remove from productionQueue
	// i = listProductionQueue.getSelectedIndex();
	// if ((i >= 0) && (i <= listProductionQueueModel.size() - 1)) {
	// item = (String) listProductionQueueModel.remove(i);
	// Vector productionQueue = colony.getProductionQueue();
	// for (int j = 0; j < productionQueue.size(); j++) {
	// BuildProject project = (BuildProject) productionQueue.elementAt(j);
	// if (item.startsWith(project.getName())) {
	// colony.removeProductionQueue(project);
	// if (project.isUnique()) {
	// chPossibleBuildings.addItem(project.getName());
	// chPossibleBuildings.repaint();
	// }
	// break;
	// }
	// }
	// }
	// } else if (c.equals("rushjob")) {
	// i = listProductionQueue.getSelectedIndex();
	// if ((i >= 0) && (i <= listProductionQueueModel.size() - 1)) {
	// item = (String) listProductionQueueModel.elementAt(i);
	// Vector productionQueue = colony.getProductionQueue();
	// for (int j = 0; j < productionQueue.size(); j++) {
	// BuildProject project = (BuildProject) productionQueue.elementAt(j);
	// if (item.startsWith(project.getName())) {
	// //XXX refactor this formatting code to a helper class or use
	// apache-commons?
	// double treasury = getColony().getPlayer().getTreasury();
	// String sTreasury = String.valueOf(treasury);
	// int pos = sTreasury.indexOf(".");
	// if (pos + 3 < sTreasury.length()) {
	// sTreasury = sTreasury.substring(0, pos + 3);
	// }
	// double rushJobCost = project.getRushJobCost()*3;
	// String sRushJobCost = String.valueOf(rushJobCost);
	// pos = sRushJobCost.indexOf(".");
	// if (pos + 3 < sRushJobCost.length()) {
	// sRushJobCost = sRushJobCost.substring(0, pos + 3);
	// }
	// //XXX make cost and possibility of rushjob depending on tradeable goods
	// if (treasury >= rushJobCost) {
	// project.setAllProductionCostPaid();
	// getColony().getPlayer().addTreasury(-rushJobCost);
	// listProductionQueueModel.remove(i);
	// break;
	// } else {
	// JOptionPane.showMessageDialog(this, "Not enough resources in treasury ("
	// + sTreasury + "). "+
	// "\nThe rush job cannot be completed because it costs " + sRushJobCost +
	// ". "+
	// "\nSorry about that.","Rushjob cannot be completed",JOptionPane.ERROR_MESSAGE);
	// break;
	// }
	// }
	// }
	// }
	// } else if (c.equals("showBuilding")) {
	// i = this.listBuildings.getSelectedIndex();
	// getExistingBuildingOrShip(i);
	// } else if (c.equals("delete")) {
	// // remove from list of existing buildings, remove building or scrap
	// // ship or satellite
	// i = listBuildings.getSelectedIndex();
	// if (i >= 0) {
	// Object[] options = { YES, NO };
	// item = (String) listBuildings.getSelectedValue();
	// int answer = JOptionPane.showOptionDialog(this,
	// "Do you really want to scrap " + item + " from this colony?",
	// "Space Opera: " + this.getTitle(), JOptionPane.YES_NO_OPTION,
	// JOptionPane.QUESTION_MESSAGE, null,
	// options, options[0]);
	// if (answer == JOptionPane.OK_OPTION) {
	//
	// // XXX: test delete from existing buildings
	//
	// int start = item.indexOf("(");
	// int end = item.indexOf(")");
	// String itemName = "";
	// if (start > 0) {
	// String sCount = item.substring(start + 1, end);
	// itemName = item.substring(0, start - 2); // remove
	// // blanks
	// // from: Factory
	// // (7)
	// int iCount = new Integer(sCount).intValue() - 1;
	// listBuildingsModel.removeElementAt(i);
	// if (iCount > 0) {
	// listBuildingsModel.add(i, item.substring(0, start + 1) + iCount + ")");
	// }
	// } else {
	// itemName = item;
	// listBuildingsModel.removeElementAt(i);
	// }
	// boolean removed = false;
	// Vector buildings = colony.getBuildings();
	// for (int j = 0; j < buildings.size(); j++) {
	// Building building = (Building) buildings.elementAt(j);
	// if (building.getName().equals(itemName)) {
	// removed = true;
	// if (building.isUnique()) {
	// if (!building.useOnlyOnce()) {
	// // add to choice for later rebuild
	// chPossibleBuildings.addItem(building.getName());
	// chPossibleBuildings.repaint();
	// }
	// if (building.getCounter() > 1) {
	// building.setCounter(building.getCounter() - 1);
	// } else {
	// colony.removeBuildings(building);
	// break; // remove only one
	// }

	// if (!removed) {
	// removeSatellites(itemName);

	// public void closeColonyDetail() {
	// saveColony();
	// closeProductionQueueDetail();
	// closeExistingBuildingsDetail();
	// this.dispose();
	// }

	// public void adjustmentValueChanged(java.awt.event.AdjustmentEvent e) {
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (source.getValueIsAdjusting()) {
			// System.out.println("value adjusting: " + source.getValue());
		} else {
			if (source.equals(slider[3])) {
				// subtract n% of everything, distribute the rest
				colony.setTaxBase(slider[3].getValue());
				taxes.setText("Taxes: (" + slider[3].getValue() + "%)");
			} else {
				// System.out.println("value not adjusting: " +
				// source.getValue());
				int prod = slider[0].getValue();
				int res = slider[1].getValue();
				int cult = slider[2].getValue();
				int sum = prod + res + cult;
				if (sum > 100) {
					while (sum > 100) {
						if (source.equals(slider[0])) {
							// distribute the difference to 1 and 2
							if (res > 0) {
								res--;
								sum--;
							}
							if (cult > 0 && sum > 100) {
								cult--;
								sum--;
							}
						} else if (source.equals(slider[1])) {
							// distribute the difference to 0 and 2
							if (prod > 0) {
								prod--;
								sum--;
							}
							if (cult > 0 && sum > 100) {
								cult--;
								sum--;
							}
						} else {
							// distribute the difference to 0 and 1
							if (prod > 0) {
								prod--;
								sum--;
							}
							if (res > 0 && sum > 100) {
								res--;
								sum--;
							}
						}
					}
				} else {
					// prod + res + cult = 100
					while (sum < 100) {
						if (source.equals(slider[0])) {
							// distribute the difference to 1 and 2
							if (res < 100) {
								res++;
								sum++;
							}
							if (cult < 100 && sum < 100) {
								cult++;
								sum++;
							}
						} else if (source.equals(slider[1])) {
							// distribute the difference to 0 and 2
							if (prod < 100) {
								prod++;
								sum++;
							}
							if (cult < 100 && sum < 100) {
								cult++;
								sum++;
							}
						} else {
							// distribute the difference to 0 and 1
							if (prod < 100) {
								prod++;
								sum++;
							}
							if (res < 100 && sum < 100) {
								res++;
								sum++;
							}
						}
					}
				}
				// System.out.println("new value for production: " + zero );
				// System.out.println("new value for research: " + one );
				// System.out.println("new value for culture: " + two );
				// System.out.println("new total: " + (zero+one+two) );
				slider[0].setValue(prod);
				slider[1].setValue(res);
				slider[2].setValue(cult);
				colony.setProduction(prod);
				colony.setResearch(res);
				colony.setCulture(cult);
				production.setText("Production: (" + prod + "%)");
				research.setText("Research: (" + res + "%)");
				culture.setText("Culture: (" + cult + "%)");
			}
		}

	}

	public ColonyDetail(Colony c, Fullscreen fs) {
		this.fs = fs;
		// TODO getCurrentPlayer???
		player = fs.getUniverse().getPlayer();
		colony = c;

		// setTitle("Colony on " + colony.getPlanetName());
		doStaticLayout();
		init();
	}

	private void doStaticLayout() {

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(BorderFactory.createLineBorder(Color.gray));
		setBackground(Color.BLACK);
		titleLabel = LabelHelper.createLabel("Colony Detail:", 12);
		titlePanel = PanelHelper.createTitlePanel(460, 25, Color.darkGray);
		titlePanel.add(titleLabel);
		add(titlePanel);

		centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(2, 2, 0, 0));
		centerPanel.setPreferredSize(new Dimension(460, 500));
		centerPanel.setBackground(Color.black);

		leftTopPanel = new JPanel();
		leftTopPanel.setLayout(new BoxLayout(leftTopPanel, BoxLayout.PAGE_AXIS));
		leftTopPanel.setPreferredSize(new Dimension(230, 250));
		leftTopPanel.setBackground(Color.black);
		leftTopPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.gray));

		rightTopPanel = new JPanel();
		rightTopPanel.setLayout(new BoxLayout(rightTopPanel, BoxLayout.PAGE_AXIS));
		rightTopPanel.setPreferredSize(new Dimension(230, 250));
		rightTopPanel.setBackground(Color.black);
		rightTopPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));

		leftBottomPanel = new JPanel();
		leftBottomPanel.setLayout(new BoxLayout(leftBottomPanel, BoxLayout.PAGE_AXIS));
		leftBottomPanel.setPreferredSize(new Dimension(230, 250));
		leftBottomPanel.setBackground(Color.gray);
		leftBottomPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));

		rightBottomPanel = new JPanel();
		rightBottomPanel.setLayout(new BoxLayout(rightBottomPanel, BoxLayout.PAGE_AXIS));
		rightBottomPanel.setPreferredSize(new Dimension(230, 250));
		rightBottomPanel.setBackground(Color.black);
		rightBottomPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));

		createDataPanel();
		createSliderPanel();
		createColonyManagerPanel();
		createQueuePanel();
		createBuildingsList();
		createBuildingsControl();

		leftTopPanel.add(dataPanel);
		leftTopPanel.add(distribution);
		rightTopPanel.add(colonyManager);
		leftBottomPanel.add(productionQueue);
		rightBottomPanel.add(buildings);
		rightBottomPanel.add(buildingsControl);

		centerPanel.add(leftTopPanel);
		centerPanel.add(rightTopPanel);
		centerPanel.add(leftBottomPanel);
		centerPanel.add(rightBottomPanel);

		add(centerPanel);

		add(Box.createVerticalGlue());

	}

	private void createBuildingsControl() {

		SoButton build = new SoButton("Build selected");
		build.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selected = possibleBuildings.getSelectedRow();
				if (selected >= 0) {
					// System.out.println(selected);
					String name = (String) possibleBuildings.getModel().getValueAt(selected, 0);
					// System.out.println(name);

					List<Building> buildings = player.getPossibleBuildings();
					for (Building b : buildings) {
						if (name.equals(b.getName())) {
							// TODO if (building.isUnique()) {
							// chPossibleBuildings.removeItem(building.getName());
							colony.addProductionQueue(b.clone());
						}
					}
					List<Product> products = player.getPossibleProducts();
					for (Product p : products) {
						if (name.equals(p.getName())) {
							// TODO if (product.isUnique()) {
							// chPossibleBuildings.removeItem(building.getName());
							colony.addProductionQueue(p.clone());
						}
					}
					refreshQueueTable();

				}
			}
		});

		buildingsControl = new JPanel();
		buildingsControl.setLayout(new BoxLayout(buildingsControl, BoxLayout.LINE_AXIS));
		buildingsControl.setBackground(Color.BLACK);
		buildingsControl.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));

		JPanel addBuilding = new JPanel();
		addBuilding.setLayout(new FlowLayout(FlowLayout.RIGHT, 1, 1));
		addBuilding.setForeground(Color.YELLOW);
		addBuilding.setBackground(Color.BLACK);
		addBuilding.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		addBuilding.add(build);
		addBuilding.add(Box.createHorizontalGlue());

		JPanel filterPanel = new JPanel();
		filterPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 1, 1));
		filterPanel.setForeground(Color.YELLOW);
		filterPanel.setBackground(Color.black);
		filterPanel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		JLabel filterLabel = new JLabel("Filter :");
		filterLabel.setForeground(Color.yellow);
		filterLabel.setBackground(Color.BLACK);
		filterLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		filterPanel.add(filterLabel);

		// TODO refactor filter types for buildings and products
		// String BIOLOGY = "Biology";
		// String ECONOMY = "Economy";
		// String MILITARY = "Military";
		// String PHYSICS = "Physics";
		// String SOCIAL = "Social";
		// String MATHEMATICS = "Mathematics";
		String[] filterOptions = { "None", "Physics", "Economy", "Military" };
		final JComboBox filterCombo = new JComboBox(filterOptions);
		filterCombo.setSelectedIndex(0);
		filterCombo.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		filterCombo.setForeground(Color.YELLOW);
		filterCombo.setBackground(Color.darkGray);
		((BasicComboBoxRenderer) filterCombo.getRenderer()).setBorder(new EmptyBorder(0, 0, 0, 0));
		filterCombo.setRenderer(new DefaultListCellRenderer());
		filterCombo.setMinimumSize(new Dimension(1, 1));
		filterCombo.setPreferredSize(new Dimension(100, 19));
		filterCombo.setBorder(BorderFactory.createEmptyBorder());

		filterCombo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buildingFilter = (String) filterCombo.getSelectedItem();
				refreshPossibleBuildings();
			}
		});
		filterPanel.add(filterCombo);

		buildingsControl.add(filterPanel);
		buildingsControl.add(Box.createHorizontalGlue());
		buildingsControl.add(addBuilding);

	}

	private void createBuildingsList() {
		buildings = new JTabbedPane();
		buildings.setBackground(Color.black);
		buildings.setForeground(Color.yellow);
		buildings.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		buildings.setMaximumSize(new Dimension(230, 100));
		buildings.setMaximumSize(new Dimension(230, 100));
		buildings.addTab("Add Building", null, getPossibleBuildings(), "");
		buildings.addTab("Existing Buildings", null, getExistingBuildings(), "");
	}

	private void createQueuePanel() {
		productionQueue = new JPanel();
		// queuePanel.setPreferredSize(new Dimension(230, 160));
		productionQueue.setLayout(new BorderLayout());
		productionQueue.setForeground(Color.yellow);
		productionQueue.setBackground(Color.black);
		queueTitleLabel = LabelHelper.createLabel("Production Queue:", 12);
		productionQueue.add("North", queueTitleLabel);
		queueData = TableHelper.createColonyTable(getQueueDataModel(), false);
		queueData.getColumnModel().getColumn(0).setPreferredWidth(110); // Item
		queueData.getColumnModel().getColumn(1).setPreferredWidth(50); // %
		JScrollPane scrollQueue = new JScrollPane(queueData);
		scrollQueue.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.gray));
		productionQueue.add("Center", scrollQueue);
		productionQueue.add("South", createQueueController());
	}

	private void refreshQueueTable() {
		queueData.setModel(getQueueDataModel());
		queueData.revalidate();
	}

	private TableModel getQueueDataModel() {
		queueDataModel = new QueueDataModel(colony);
		return queueDataModel;
	}

	private void refreshQueueSelection() {
		String selected = (String) queueData.getValueAt(queueData.getSelectedRow(), 0);
		queueData.setModel(getQueueDataModel());
		queueData.getColumnModel().getColumn(0).setPreferredWidth(110); // Item
		queueData.getColumnModel().getColumn(1).setPreferredWidth(50); // %
		for (int i = 0; i < queueData.getRowCount(); i++) {
			if (selected.equals((String) queueData.getValueAt(i, 0))) {
				queueData.setRowSelectionInterval(i, i);
				break;
			}
		}
		productionQueue.revalidate();
	}

	private void createColonyManagerPanel() {
		colonyManager = new JPanel();
		colonyManager.setMaximumSize(new Dimension(230, 80));
		colonyManager.setLayout(new GridLayout(4, 1));
		colonyManager.setForeground(Color.yellow);
		colonyManager.setBackground(Color.black);
		optionsTitleLabel = LabelHelper.createBorderedLabel("Colony Manager:", 12,
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));
		optionsTitlePanel = PanelHelper.createTitlePanel(230, 20, Color.yellow);
		optionsTitlePanel.add(optionsTitleLabel);
		colonyManager.add(optionsTitleLabel);

		final JRadioButton shutup = new JRadioButton("Shutup");
		shutup.setActionCommand("shutup");
		shutup.setForeground(Color.yellow);
		shutup.setBackground(Color.black);
		shutup.setPreferredSize(new Dimension(230, 20));
		shutup.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		shutup.setSelected(colony.getColonyManager().getShutup());
		shutup.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (shutup.isSelected()) {
					colony.getColonyManager().setShutup(true);
					colony.getColonyManager().setAdvice(false);
					colony.getColonyManager().setAuto(false);
				}
			}
		});
		final JRadioButton advice = new JRadioButton("Give recommendations only");
		advice.setActionCommand("advice");
		advice.setForeground(Color.yellow);
		advice.setBackground(Color.black);
		advice.setPreferredSize(new Dimension(230, 20));
		advice.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		advice.setSelected(colony.getColonyManager().getAdvice());
		advice.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (advice.isSelected()) {
					colony.getColonyManager().setShutup(false);
					colony.getColonyManager().setAdvice(true);
					colony.getColonyManager().setAuto(false);
				}
			}
		});
		final JRadioButton auto = new JRadioButton("Do what you think best");
		auto.setActionCommand("auto");
		auto.setForeground(Color.yellow);
		auto.setBackground(Color.black);
		auto.setPreferredSize(new Dimension(230, 20));
		auto.setSelected(colony.getColonyManager().getAuto());
		auto.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (auto.isSelected()) {
					colony.getColonyManager().setShutup(false);
					colony.getColonyManager().setAdvice(false);
					colony.getColonyManager().setAuto(true);
				}
			}
		});
		auto.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		ButtonGroup group = new ButtonGroup();
		group.add(shutup);
		group.add(advice);
		group.add(auto);
		colonyManager.add(shutup);
		colonyManager.add(advice);
		colonyManager.add(auto);
		// TODO colony manager personality //
		// (priorities: attack / defense / research / growth / ... )

		// TODO auto upgrade buildings
		// TODO auto delete obsolete buildings
	}

	private void createDataPanel() {
		dataPanel = new JPanel();
		dataPanel.setPreferredSize(new Dimension(230, 60));
		dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.PAGE_AXIS));
		dataPanel.setForeground(Color.yellow);
		dataPanel.setBackground(Color.black);
		colonyData = TableHelper.createDetailTable(new ColonyDataModel(fs, colony), true);
		colonyData.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		JScrollPane scrollData = new JScrollPane(colonyData);
		scrollData.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		dataPanel.add(scrollData);
	}

	private void createSliderPanel() {
		distribution = new JPanel();
		distribution.setPreferredSize(new Dimension(230, 140));
		distribution.setLayout(new BoxLayout(distribution, BoxLayout.PAGE_AXIS));
		distribution.setForeground(Color.yellow);
		distribution.setBackground(Color.black);

		settingsTitleLabel = LabelHelper.createLabel("Distribution:", 12);
		settingsTitlePanel = PanelHelper.createTitlePanel(230, 20, Color.black);
		settingsTitlePanel.add(settingsTitleLabel);
		((FlowLayout) settingsTitlePanel.getLayout()).setVgap(0);
		((FlowLayout) settingsTitlePanel.getLayout()).setHgap(0);
		distribution.add(settingsTitlePanel);

		settingsSliderPanel = new JPanel();
		settingsSliderPanel.setOpaque(true);
		settingsSliderPanel.setLayout(new GridLayout(6, 2));
		settingsSliderPanel.setBackground(Color.black);
		settingsSliderPanel.setForeground(Color.black);

		for (int i = 0; i < 4; i++) {
			slider[i] = new JSlider(JSlider.HORIZONTAL);
			slider[i].setForeground(Color.black);
			slider[i].setBackground(Color.darkGray.darker());
			slider[i].setMinimum(0);
			slider[i].setMaximum(100);
			slider[i].setMinorTickSpacing(1);
		}
		slider[0].setValue(80);
		slider[1].setValue(10);
		slider[2].setValue(0);
		slider[3].setValue(10);
		slider[0].setBorder(BorderFactory.createMatteBorder(1, 0, 0, 1, Color.gray));
		slider[1].setBorder(BorderFactory.createMatteBorder(1, 0, 0, 1, Color.gray));
		slider[2].setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
		slider[3].setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
		slider[0].addChangeListener(this);
		slider[1].addChangeListener(this);
		slider[2].addChangeListener(this);
		slider[3].addChangeListener(this);
		production = LabelHelper.createBorderedLabel("Production: (80%)", 11,
				BorderFactory.createMatteBorder(1, 1, 0, 1, Color.gray));
		research = LabelHelper.createBorderedLabel("Research: (10%)", 11, BorderFactory.createMatteBorder(1, 1, 0, 1, Color.gray));
		culture = LabelHelper.createBorderedLabel("Culture (0%)", 11, BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		taxes = LabelHelper.createBorderedLabel("Taxes (10%)", 11, BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		settingsSliderPanel.add(production);
		settingsSliderPanel.add(slider[0]);
		settingsSliderPanel.add(research);
		settingsSliderPanel.add(slider[1]);
		settingsSliderPanel.add(culture);
		settingsSliderPanel.add(slider[2]);
		settingsSliderPanel.add(LabelHelper.createLabel("Colony Taxes", 12));
		settingsSliderPanel.add(new JLabel());
		settingsSliderPanel.add(taxes);
		settingsSliderPanel.add(slider[3]);
		settingsSliderPanel.add(new JLabel());
		settingsSliderPanel.add(new JLabel());
		distribution.add(settingsSliderPanel);
		// sliderPanel.add(Box.createVerticalGlue());
	}

	private void fillListOfExistingBuildings(String filter) { // filter =
		// // All/Physics/...
		// // XXX test fill and refill of list
		// if (listBuildingsModel.size() > 0) {
		// listBuildingsModel.removeAllElements();
		// }
		// Vector buildings = colony.getBuildings();
		// for (int i = 0; i < buildings.size(); i++) {
		// Building building = (Building) buildings.elementAt(i);
		// if (filter.equalsIgnoreCase(building.getType()) ||
		// filter.equalsIgnoreCase("All")) {
		// if (building.getCounter() > 1) {
		// listBuildingsModel.addElement(building.getName() + "  (" +
		// building.getCounter() + ")");
		// } else {
		// listBuildingsModel.addElement(building.getName() + "  (1)");
		// }
		// }
		// }
		//
		// // XXX: a ship belongs to a colony, even if its far and away. May be
		// // misleading though.
		// List<SpaceCraft> ships = player.getShips(); //
		// colony.getPlanet().getSpaceCraft();
		// Hashtable hCounter = new Hashtable();
		// for (int i = 0; i < ships.size(); i++) {
		// SpaceCraft ship = ships.get(i);
		//
		// // TEST
		// // System.out.println("Ship " + ship.getShipName() +
		// // ship.getColony());
		// if (ship.getColony() != null && ship.getColony().equals(colony)) {
		// ShipDesign shipDesign = ship.getShipDesign();
		//
		// // TEST
		// // System.out.println("Shipdesign: " +
		// // shipDesign.getShipClass());
		// // if (!shipDesign.isShip()) {
		// if (hCounter.containsKey(shipDesign.getShipClass())) {
		// Integer count = (Integer) hCounter.get(shipDesign.getShipClass());
		// hCounter.put(shipDesign.getShipClass(), new Integer(count.intValue()
		// +
		// 1));
		// } else {
		// hCounter.put(shipDesign.getShipClass(), new Integer(1));
		// }
		// // }
		// }
		// // }
		// }
		// Enumeration kCounter = hCounter.keys();
		// while (kCounter.hasMoreElements()) {
		// String name = (String) kCounter.nextElement();
		// Integer count = (Integer) hCounter.get(name);
		// if (filter.equalsIgnoreCase("Ships") ||
		// filter.equalsIgnoreCase("All")) {
		// listBuildingsModel.addElement(name + "  (" + count.toString() + ")");
		// }
		// }
	}

	private void init() {

		colonyData = TableHelper.createDetailTable(new ColonyDataModel(fs, colony), true);

		slider[0].setValue(colony.getProduction());
		slider[1].setValue(colony.getResearch());
		slider[2].setValue(colony.getCulture());

		slider[3].setValue(colony.getTaxBase());

		// setPopulationCount();
		// refreshPossibilities();
		// //addPossibleBuildingsAndProducts((String)
		// chGroupPossibleBuildings.getSelectedItem());
		// initProductionQueue();
		// fillListOfExistingBuildings((String)
		// chGroupExistingBuildings.getSelectedItem());
		// getCurrentValues();
		// //setScrollbars();
		// setLabels();
	}

	// private void initProductionQueue() {
	// if (listProductionQueueModel.getSize() > 0) {
	// listProductionQueueModel.removeAllElements();
	// }
	// Vector productionQueue = colony.getProductionQueue();
	// for (int i = 0; i < productionQueue.size(); i++) {
	// BuildProject project = (BuildProject) productionQueue.elementAt(i);
	// if (project.isUnique()) {
	// try {
	// // XXX: test removal of unique projects
	// chPossibleBuildings.removeItem(project.getName());
	// chPossibleBuildings.repaint();
	// } catch (Exception e) {
	// // no error if already removed!
	// // System.out.println("ColonyDetail: Project " +
	// // project.getName() +
	// // " could not be removed.");
	// }
	// }
	// String str = project.getName() + "                                  ";
	// str = str.substring(0, 30);
	// double perc = project.getPercentComplete();
	// if (perc < 10) {
	// str = str + "  ";
	// } else if (perc < 100) {
	// str = str + " ";
	// }
	// str = str + perc + "     ";
	// str = str.substring(0, 35);
	// listProductionQueueModel.addElement(str + " %");
	// }
	// }

	// public void itemStateChanged(java.awt.event.ItemEvent e) {
	// if (e.getSource().equals(chGroupExistingBuildings)) {
	// fillListOfExistingBuildings((String)
	// chGroupExistingBuildings.getSelectedItem());
	// }
	// if (e.getSource().equals(chGroupPossibleBuildings)) {
	// addPossibleBuildingsAndProducts((String)
	// chGroupPossibleBuildings.getSelectedItem());
	// }
	// }

	public void refresh() {
		// if (colony.getPopulationCount() < 1) {
		// // XXX:
		// dispose();
		// } else {
		// // XXX: test
		// if (isVisible()) {
		// init();
		// }
		// }
		if (productionQueueDetail != null) {
			productionQueueDetail.refresh();
		}
		if (existingBuildingsDetail != null) {
			existingBuildingsDetail.refresh();
		}
	}

	private void refreshPossibilities() {
		colony.refreshPossibilities();
	}

	// private void removeSatellites(String itemName) {
	// Planet planet = colony.getPlanet();
	// Vector ships = planet.getSpaceCraft();
	// for (int k = 0; k < ships.size(); k++) {
	// SpaceCraft ship = (SpaceCraft) ships.elementAt(k);
	// if (!ship.isShip()) {
	// ShipDesign shipDesign = ship.getShipDesign();
	// String shipDesignName = shipDesign.getShipClass();
	// if (itemName.equals(shipDesignName)) {
	// //XXX spaceOpera.closeShipInfo();
	// //TODO spaceOpera.deleteShipFromUniverse(planet.getSunSystem(), ship);
	// break;
	// }
	// }
	// }
	// }

	public void saveColony() {
		// colony.setName(taColonyName.getText());
		// colony.setWorkingHours(values[1]);
		// colony.setConstructionRate(values[2]);
		// colony.setOperationRate(values[3]);
		// colony.setMilitaryRate(values[4]);
		// colony.setEducationRate(values[5]);
		// colony.setAdministrationRate(values[6]);
	}

	/**
	 * @param index
	 *            index of the selected list item
	 */
	private void getBuildProject(int index) {
		if ((index >= 0) && (index < listProductionQueueModel.size())) {
			BuildProject temp = colony.getProductionQueue().get(index);
			showProductionQueueDetail(temp);
		}
	}

	private void showProductionQueueDetail(BuildProject bp) {
		closeProductionQueueDetail();
		// TODO productionQueueDetail = new ProductionQueueDetail(spaceOpera,
		// ColonyDetail.this, bp);
	}

	/**
	 * @param i
	 *            index of the selected list item
	 */
	private void getExistingBuildingOrShip(int i) {
		if ((i >= 0) && (i < listBuildingsModel.size())) {
			BuildProject temp = null;
			try {
				temp = (BuildProject) colony.getBuildings().get(i);
			} catch (ArrayIndexOutOfBoundsException e) {
				// occurs when selected building is not a building but a ship
				String name = (String) this.listBuildingsModel.getElementAt(i); // e.g.
				// "Colony
				// Ship
				// (1)"
				// System.out.println("ColonyDetail.actionPerformed() " + name);
				name = name.substring(0, name.lastIndexOf(' ')).trim();
				Vector products = colony.getPossibleProducts();
				for (int j = 0; j < products.size(); j++) {
					Product p = (Product) products.elementAt(j);
					if (p.getName().equals(name)) {
						temp = p;
					}
				}
			}
			showExistingBuildingsDetail(temp);
		}
	}

	private void showExistingBuildingsDetail(BuildProject bp) {
		closeExistingBuildingsDetail();
		// TODO existingBuildingsDetail = new
		// ExistingBuildingsDetail(spaceOpera, ColonyDetail.this, bp);
	}

	private void closeExistingBuildingsDetail() {
		if (existingBuildingsDetail != null)
			existingBuildingsDetail.dispose();
	}

	private void closeProductionQueueDetail() {
		if (productionQueueDetail != null)
			productionQueueDetail.dispose();
	}

	// TODO move to controller class
	private Component createQueueController() {

		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);

		SoButton shipOrders = new SoButton("Delete");
		shipOrders.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (queueData.getSelectedRow() >= 0) {
					colony.removeProductionQueue((String) queueData.getModel().getValueAt(queueData.getSelectedRow(), 0));
					refreshQueueSelection();
				}
			}
		});
		SoButton previous = new SoButton("Up");
		previous.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (queueData.getSelectedRow() > 0) {
					colony.swapProductionQueue(queueData.getSelectedRow(), queueData.getSelectedRow() - 1);
					refreshQueueSelection();
				}
			}
		});
		SoButton next = new SoButton("Down");
		next.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (queueData.getSelectedRow() >= 0 && //
						queueData.getSelectedRow() < (queueData.getRowCount() - 1)) {
					colony.swapProductionQueue(queueData.getSelectedRow(), queueData.getSelectedRow() + 1);
					refreshQueueSelection();

				}
			}
		});

		JPanel control = new JPanel();
		control.setLayout(new BoxLayout(control, BoxLayout.LINE_AXIS));
		control.setBackground(Color.BLACK);

		JPanel controlLeft = new JPanel();
		controlLeft.setLayout(left);
		controlLeft.setForeground(Color.YELLOW);
		controlLeft.setBackground(Color.black);
		controlLeft.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		// shipLabel = new JLabel("Ship: none");
		// shipLabel.setForeground(Color.yellow);
		// shipLabel.setBackground(Color.BLACK);
		// shipLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		// controlLeft.add(shipLabel);

		JPanel controlRight = new JPanel();
		controlRight.setLayout(right);
		controlRight.setForeground(Color.YELLOW);
		controlRight.setBackground(Color.BLACK);
		controlRight.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		controlRight.add(shipOrders);
		controlRight.add(Box.createHorizontalGlue());
		controlRight.add(previous);
		controlRight.add(next);
		control.add(controlLeft);
		control.add(Box.createHorizontalGlue());
		control.add(controlRight);
		return control;
	}

	private JComponent getExistingBuildings() {
		existingBuildings = TableHelper.createDefaultTable(new ExistingBuildingsTableModel(colony, "None"), true);
		JScrollPane scrollPane = new JScrollPane(existingBuildings);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	private void refreshExistingBuildings() {
		existingBuildings = TableHelper.createDefaultTable(new ExistingBuildingsTableModel(colony, "None"), true);
	}

	private JComponent getPossibleBuildings() {
		possibleBuildings = TableHelper.createDefaultTable(new PossibleBuildingsTableModel(player, "None"), true);
		JScrollPane scrollPane = new JScrollPane(possibleBuildings);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	private void refreshPossibleBuildings() {
		PossibleBuildingsTableModel model = new PossibleBuildingsTableModel(player, buildingFilter);
		possibleBuildings.setModel(model);
		possibleBuildings.revalidate();
		buildings.revalidate();
	}

}
