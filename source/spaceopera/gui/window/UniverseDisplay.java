//The class Universe contains sunsystems, nebulae, spaceships for the spaceopera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.menu.ShipMenu;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Universe;
import spaceopera.universe.ai.MPlayer;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The Universe class displays all the sunsystems, all known ships and some background stars
 */
@SuppressWarnings({ "rawtypes" })
public class UniverseDisplay extends JPanel implements MouseListener, MouseMotionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private Universe universe;
	private SpaceOpera spaceOpera;
	private Fullscreen fs;
	private SpaceCraft currentShip;
	private ShipMenu shipMenu = null;

	public UniverseDisplay(Universe u, Fullscreen fs) {

		universe = u;
		this.fs = fs;
		addMouseListener(this);
		addMouseMotionListener(this);
		this.setPreferredSize(new Dimension(universe.getUniverseSize(), universe.getUniverseSize()));

		// setPreferredSize(ULEFT,UTOP,FRAMEX,FRAMEY);
		setOpaque(true);
	}

	public void cleanup() {
		List suns = universe.getSuns();
		Vector ships = universe.getShips();
		Graphics g = this.getGraphics();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.cleanup(g);
		}
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft SpaceCraft = (SpaceCraft) ships.elementAt(i);
			SpaceCraft.cleanup();
		}
		if (spaceOpera.getShowScanArea()) {
			clearScanArea();
		}
	}

	public void clear() {
		Graphics g = this.getGraphics();
		Dimension d = getSize();
		g.setColor(Color.black);
		g.fillRect(0, 0, d.width, d.height);
	}

	public void clearScanArea() {
		Graphics g = this.getGraphics();
		int scanRange;
		Vector players = universe.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			Player player = (Player) players.elementAt(i);
			if (player instanceof MPlayer) {
				paintScanArea(player, g, true);
			}
		}
	}

	public void mouseClicked(MouseEvent event) {
		List suns = universe.getSuns();
		int x = event.getX();
		int y = event.getY();

		// Graphics g = this.getGraphics();
		boolean refresh = false;
		boolean isSunClicked = false;
		for (int i = 0; i < suns.size(); i++) {
			Sun currentSun = (Sun) suns.get(i);
			currentSun.setSelected(false);
			if (currentSun.isClicked(x, y)) {
				isSunClicked = true;
				refresh = true;
				currentSun.setSelected(true);
				fs.updateSunSystem(currentSun.getSunSystem(), null);
				// TODO refactor similar to fs.displayPreviousSystem
				if (currentSun.isExploredBy(universe.getPlayerName()) && currentSun.getSunSystem().getPlanets().size() > 0) {
					Planet planet = (Planet) currentSun.getSunSystem().getPlanets().elementAt(0);
					fs.displayPlanetImage(planet);
					universe.getPlayer().setCurrentPlanet(planet);
					Colony colony = planet.getColony();
					if (colony != null && colony.getPlayerName().equals(universe.getPlayerName())) {
						fs.displayColonyImage(colony);
					} else {
						fs.displayColonyImage(null);
					}
				} else {
					fs.displayPlanetImage(null);
					fs.displayColonyImage(null);
				}
			}
		}
		if (!isSunClicked) { // not inside a sun
			Vector ships = universe.getShips();
			// select the clicked ship or popup shipmenu
			for (int i = 0; i < ships.size(); i++) {
				currentShip = (SpaceCraft) ships.elementAt(i);
				// System.out.println("current ship from: " +
				// currentShip.getPlayer().getName());

				// TODO !important; refactor this click logic
				if (currentShip.getPlayer().equals(universe.getPlayer())) {
					if (currentShip.isClicked(x, y)) {
						if (event.getModifiers() == InputEvent.BUTTON1_MASK) {
							// left Mousebutton selects ship
							for (int j = 0; j < ships.size(); j++) {
								// unselect all
								SpaceCraft temp = (SpaceCraft) ships.elementAt(j);
								temp.setSelected(false);
							}
							fs.refreshShipInfo(currentShip);
							refresh = true;
							currentShip.setSelected(true);
							universe.setCurrentSpaceCraft(currentShip);
							// TODO currentShip.display(g, spaceOpera);
							break; // select only one if stacked ships
						} else {
							// middle or right button shows Popup-Menu
							if (currentShip.isSelected()) {
								shipMenu = new ShipMenu(currentShip, fs);
								this.add(shipMenu);
								shipMenu.show(this, event.getX(), event.getY());
								// break; // select only one
							}
						}
					}
				}
			}
		}

		if (refresh) {
			// f
			// display();
			repaint();
		}
	}

	public void mouseDragged(MouseEvent event) {
		Vector ships = universe.getShips();
		if (event != null) {
			int x = event.getX();
			int y = event.getY();
			try {
				spaceOpera.setCoordinates("x: " + x + ", y: " + y);
			} catch (Exception e) {
				// ignore sequence errors for now
			}
			for (int i = 0; i < ships.size(); i++) {
				// needs local variable
				SpaceCraft ship = (SpaceCraft) ships.elementAt(i);
				if (ship.isDragging()) {
					if (ship.isShip() && ship.hasInterstellarDrive() && !ship.targetIsSelected()) {
						((Ship) ship).moveDragLine(getGraphics(), spaceOpera, x, y);
					}
				}
			}
		}
	}

	public void mouseEntered(MouseEvent event) {
	}

	public void mouseExited(MouseEvent event) {
	}

	public void mouseMoved(MouseEvent event) {
	}

	public void mousePressed(MouseEvent event) {
		Vector ships = universe.getShips();
		int x = event.getX();
		int y = event.getY();
		boolean done = false;
		for (int i = 0; i < ships.size(); i++) {
			currentShip = (SpaceCraft) ships.elementAt(i);
			currentShip.setDragging(false);
			if (currentShip.getPlayer().equals(universe.getPlayer())) {
				if (currentShip.isClicked(x, y) && !currentShip.isTraveling()) {
					if (!done) {
						currentShip.setDragging(true);
						done = true;
					}
				}
			}
		}
	}

	// mouse-up after drag may select target if it happens close by a star
	public void mouseReleased(MouseEvent event) {
		List suns = universe.getSuns();
		Vector ships = universe.getShips();
		int x = event.getX();
		int y = event.getY();
		Graphics g = this.getGraphics();
		for (int i = 0; i < suns.size(); i++) {
			Sun currentSun = (Sun) suns.get(i);
			currentSun.setSelected(false);
			if (currentSun.isClicked(x, y)) {
				// set ship-order
				for (int j = 0; j < ships.size(); j++) {
					SpaceCraft spaceCraft = (SpaceCraft) ships.elementAt(j);
					if (spaceCraft.isShip()) {
						Ship ship = (Ship) spaceCraft;
						if (ship.isDragging()) {
							ship.setNextOrder(currentSun.getName(), "", EXPLORE);
							ship.paintDragLine(g, spaceOpera);
							ship.display(g, spaceOpera);
							ship.setDragging(false);
							currentSun.setSelected(true);
							fs.updateSunSystem(currentSun.getSunSystem(), null);
							// TODO spaceOpera.changePlanetImage(null);
						}
					}
				}
			}
		}
		// TODO spaceOpera.getDisplay().show();
		repaint();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setOpaque(true);
		Graphics g2 = g.create();
		Dimension d = this.getSize();
		// this.setBackground(Color.black);
		paintBackgroundStars(g2);
		// TODO if (spaceOpera.getShowScanArea()) {
		// paintScanArea(universe.getPlayer(), g2, false);
		// }
		paintSuns(g2);
		paintSpaceships(g2);
		g2.dispose();
	}

	private void paintBackgroundStars(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(getForeground());
		for (int i = 0; i < universe.getUniverseSize(); i++) {
			g.setColor(universe.getBgStarColor(i));
			g.drawOval(universe.getBgStarX(i), universe.getBgStarY(i), 1, 1);
		}
	}

	public void paintScanArea(Player player, Graphics g, boolean clear) {
		int scanRange;
		if (player instanceof MPlayer) {
			if (clear) {
				g.setColor(Color.black);
			} else {
				g.setColor(player.getColor());
			}
			List<Colony> colonies = player.getColonies();
			for (int j = 0; j < colonies.size(); j++) {
				Colony colony = colonies.get(j);
				scanRange = colony.getScanRange() * universe.getPixelPerLightYear();
				g.fillOval(colony.getSunX() + STARSIZE / 2 - scanRange / 2, colony.getSunY() + STARSIZE / 2 - scanRange / 2,
						scanRange, scanRange);
			}
			List<SpaceCraft> ships = player.getShips();
			for (int j = 0; j < ships.size(); j++) {
				Sun sun = null;
				SpaceCraft ship = ships.get(j);
				try {
					sun = ship.getFromSunSystem().getSun();
				} catch (RuntimeException e) {
					// ignore nullpointer (happens in testmode at beginning of
					// game)
				}
				scanRange = ship.getScanRange() * universe.getPixelPerLightYear();
				if (ship.isTraveling() || sun == null) {
					// draw at ship position
					g.fillOval(ship.getX() - scanRange / 2, ship.getY() - scanRange / 2, scanRange, scanRange);
				} else {
					// draw at sun position (because ship position is off some
					// pixels

					g.fillOval(sun.getX() + STARSIZE / 2 - scanRange / 2, sun.getY() + STARSIZE / 2 - scanRange / 2, scanRange,
							scanRange);
				}
			}
		}
	}

	private void paintSpaceships(Graphics g) {
		// first unselected, then selected, so the select marker shows.
		Vector ships = universe.getShips();
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft ship = (SpaceCraft) ships.elementAt(i);
			if (!ship.isSelected()) {
				ship.display(g, spaceOpera);
			}
		}
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft ship = (SpaceCraft) ships.elementAt(i);
			if (ship.isSelected()) {
				ship.display(g, spaceOpera);
			}
		}
	}

	private void paintSuns(Graphics g) {
		List suns = universe.getSuns();
		for (int i = 0; i < suns.size(); i++) {
			Sun sun = (Sun) suns.get(i);
			sun.display(g);
		}
	}

}
