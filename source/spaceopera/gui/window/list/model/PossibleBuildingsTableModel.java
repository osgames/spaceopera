package spaceopera.gui.window.list.model;

import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Product;

/**
 * possible buildings for each player
 * 
 */
public class PossibleBuildingsTableModel implements TableModel {

	private String[] columnNames = { "Name", "Cost" };
	private Object[][] tableData = { {} };

	public PossibleBuildingsTableModel(Player player, String filter) {

		tableData = new Object[1][1];

		if (player == null) {
			tableData[0][0] = "";
			tableData[0][1] = "";
		} else {
			List<Building> buildings = player.getPossibleBuildings();
			int count = 0;
			for (Building b : buildings) {
				if (b.getAllowBuild()) {
					if (filter.equalsIgnoreCase(b.getType()) || "none".equalsIgnoreCase(filter)) {
						count++;
					}
				}
			}
			List<Product> products = player.getPossibleProducts();
			for (Product p : products) {
				if (p.getAllowBuild()) {
					if (filter.equalsIgnoreCase(p.getType()) || "none".equalsIgnoreCase(filter)) {
						count++;
					}
				}
			}

			tableData = new Object[count][2];
			int i = 0;
			for (Building b : buildings) {
				if (b.getAllowBuild()) {
					if (filter.equalsIgnoreCase(b.getType()) || "none".equalsIgnoreCase(filter)) {
						tableData[i][0] = b.getName();
						tableData[i][1] = b.getProduct();
						i++;
					}
				}
			}
			for (Product p : products) {
				if (p.getAllowBuild()) {
					if (filter.equalsIgnoreCase(p.getType()) || "none".equalsIgnoreCase(filter)) {
						tableData[i][0] = p.getName();
						tableData[i][1] = p.getProduct();
						i++;
					}
				}
			}
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
