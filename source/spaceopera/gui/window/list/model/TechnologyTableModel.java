package spaceopera.gui.window.list.model;

import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.Technology;

public class TechnologyTableModel implements TableModel {

    private String[] columnNames = {"Technology", "Type", "Level"};
    private Object[][] tableData = {{}};
	
	public TechnologyTableModel(Fullscreen fs) {
		List<Technology> techList = fs.getCurrentPlayer().getPossibleTechnologies();
		int size=0;
		boolean empty=true;
		//TODO refactor this double loop
		for (Technology tech : techList) {
			if (tech.isStudied()) {
				size++;
			}
		}
		tableData = new Object[size][3];
		int i=0;
		for (Technology tech : techList) {
			if (tech.isStudied()) {
				empty = false;
				tableData[i][0] = tech.getName();
				tableData[i][1] = tech.getCls();
				tableData[i][2] = "TODO Level";
				i++;
			}
		}
		if (empty) {
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
