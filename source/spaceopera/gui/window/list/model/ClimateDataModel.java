package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.universe.elements.Planet;

public class ClimateDataModel implements TableModel {

	private String[] columnNames = { "Climate", "" };
	private Object[][] tableData = { {} };

	public ClimateDataModel(Planet planet) {
		tableData = new Object[5][2];
		tableData[0][0] = "Temperature:";
		tableData[1][0] = "Atmospheric pressure:";
		tableData[2][0] = "Wind velocity:";
		tableData[3][0] = "Atmospheric composition:";
		tableData[4][0] = "Seasons:";
		if (planet == null) {
			tableData[0][1] = "";
			tableData[1][1] = "";
			tableData[2][1] = "";
			tableData[3][1] = "";
			tableData[4][1] = "";
		} else {
			String temp = String.valueOf(planet.getAvgTemp());
			int pos = temp.indexOf(".");
			if (pos + 3 < temp.length()) {
				temp = temp.substring(0, pos + 3);
			}
			tableData[0][1] = temp + " degrees C";

			String air = String.valueOf(planet.getAtmosphericPressure());
			pos = air.indexOf(".");
			if (pos + 3 < air.length()) {
				air = air.substring(0, pos + 3);
			}
			tableData[1][1] = air + " kg/cm2";

			String speed = String.valueOf(planet.getAvgAirSpeed());
			pos = speed.indexOf(".");
			if (pos + 3 < speed.length()) {
				speed = speed.substring(0, pos + 3);
			}
			tableData[2][1] = speed + " km/h";

			tableData[3][1] = "TODO";

			String seasons = "";
			double pSeasons = planet.getAxialTilt();
			int axialTiltDegrees = (int) (pSeasons * 20);
			if (planet.getAxialTilt() < 0.01) {
				seasons = "none";
			} else if (planet.getAxialTilt() < 10.01) {
				seasons = "little";
			} else if (planet.getAxialTilt() < 30.01) {
				seasons = "earthlike";
			} else {
				seasons = "extreme";
			}

			tableData[4][1] = seasons;

		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
