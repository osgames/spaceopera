package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.ships.ShipDesign;

public class ShipDesignTableModel implements TableModel {

	private String[] columnNames = { "Name", "Type", "Colonymodule", "Armed", "Built", "Existing" };
	private Object[][] tableData = { {} };

	public ShipDesignTableModel(Fullscreen fs) {
		if (fs.getCurrentPlayer().getShipDesigns().size() > 0) {
			tableData = new Object[fs.getCurrentPlayer().getShipDesigns().size()][6];
			int i=0;
			for (String key : fs.getCurrentPlayer().getShipDesigns().keySet()) {
				ShipDesign s = fs.getCurrentPlayer().getShipDesigns().get(key);

				tableData[i][0] = s.getShipClass();
				tableData[i][1] = s.getHull();
				tableData[i][2] = s.getColonyModule()!=null?"Yes":"No";
				tableData[i][3] = s.getBeamWeaponCount()>0||s.getBombWeaponCount()>0||s.getMissileWeaponCount()>0?"Yes":"No";
				tableData[i][4] = "TODO: 0";
				tableData[i][5] = "TODO: 0";
				i++;
			}
		} else {
			tableData = new Object[1][6];
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
			tableData[0][3] = "";
			tableData[0][4] = "";
			tableData[0][5] = "";
		}
		//fireTableDataChanged();
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
