package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.elements.Planet;

public class PlanetTableModel implements TableModel {

	private String[] columnNames = { "Name", "System", "Orbit", "Type", "Colonized by" };
	private Object[][] tableData = { { "", "", "", "", "" } };

	public PlanetTableModel(Fullscreen fs) {
		if (fs.getCurrentPlayer().getPlanetList().size() > 0) {
			tableData = new Object[fs.getCurrentPlayer().getPlanetList().size()][5];
			for (int i = 0; i < fs.getCurrentPlayer().getPlanetList().size(); i++) {
				Planet p = fs.getCurrentPlayer().getPlanetList().get(i);
				tableData[i][0] = p.getName();
				tableData[i][1] = p.getSunName();
				tableData[i][2] = ""+p.getOrbit().getNumber();
				tableData[i][3] = p.getType();
				if (p.getColony() != null) {
					tableData[i][4] = p.getColony().getPlayerName();
				} else {
					tableData[i][4] = "";
				}
			}
		} else {
			tableData = new Object[1][5];
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
			tableData[0][3] = "";
			tableData[0][4] = "";
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
