package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.colony.Colony;

public class ColonyTableModel implements TableModel {
        
    private String[] columnNames = {"Name", "System", "Planet #" , "Population", "Production"};
    private Object[][] tableData = {{}};
	
	public ColonyTableModel(Fullscreen fs) {
		if (fs.getCurrentPlayer().getColonies().size() > 0) {
			tableData = new Object[fs.getCurrentPlayer().getColonies().size()][5];
			for (int i = 0; i < fs.getCurrentPlayer().getColonies().size(); i++) {
				Colony c = fs.getCurrentPlayer().getColonies().get(i);
				tableData[i][0] = c.getName();
				tableData[i][1] = c.getSun().getName();
				tableData[i][2] = "Planet "+c.getPlanetNumber();
				tableData[i][3] = ""+c.getPopulationCount();
				if (c.getProductionQueue().size()>0) {
					tableData[i][4] = c.getProductionQueue().get(0);
				} else {
					tableData[i][4] = "";
				}
			}
		} else {
			tableData = new Object[1][5];
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
			tableData[0][3] = "";
			tableData[0][4] = "";
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
