package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.Technology;
import spaceopera.universe.elements.Planet;

public class SpecialDataModel implements TableModel {

	private String[] columnNames = { "Other", "" };
	private Object[][] tableData = { {} };

	public SpecialDataModel(Fullscreen fs, Planet planet) {
		tableData = new Object[2][2];
		tableData[0][0] = "Special:";
		if (planet != null && planet.isColonized()) {
			tableData[1][0] = "Colonized by:";
		} else {
			tableData[1][0] = "Colonizable:";
		}
		if (planet == null) {
			tableData[0][1] = "";
			tableData[1][0] = "";
		} else {
			tableData[0][1] = planet.getSpecial();
			if (planet.isColonized()) {
				tableData[1][1] = planet.getPlayerName();
			} else {
				String module = planet.getNeededColonyModuleName(fs.getCurrentPlayer());
				//System.out.println(planet.getName() + ": " + module);
				boolean canColonize=false;
				for (Technology c : fs.getCurrentPlayer().getPossibleTechnologies()) {
					if (c.isStudied() && c.getName().equals(module)) {
						canColonize=true;
					}
				}
				if (canColonize) {
					tableData[1][1] = "yes";
				} else {
					tableData[1][1] = "no";
				}
			}
		}
	}

	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
