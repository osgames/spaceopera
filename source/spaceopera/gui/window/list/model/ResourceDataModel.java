package spaceopera.gui.window.list.model;

import javax.swing.JLabel;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.universe.SOConstants;
import spaceopera.universe.elements.Planet;

public class ResourceDataModel implements TableModel, SOConstants {

	private String[] columnNames = { "Resources", "" };
	private Object[][] tableData = { {} };

	// Raw Material, resources
	// flora = new JLabel("");
	// fauna = new JLabel("");
	// salt = new JLabel("");
	// wood = new JLabel("");
	// metal = new JLabel("");
	// uranium = new JLabel("");
	// fossils = new JLabel("");
	// special = new JLabel("");
	// @Override
	public ResourceDataModel(Planet planet) {
		tableData = new Object[7][2];
		tableData[0][0] = "Animals:";
		tableData[1][0] = "Plants:";
		tableData[2][0] = "Salt:";
		tableData[3][0] = "Wood:";
		tableData[4][0] = "Metal:";
		tableData[5][0] = "Uranium:";
		tableData[6][0] = "Fossil fuels:";
		if (planet == null) {
			tableData[0][1] = "";
			tableData[1][1] = "";
			tableData[2][1] = "";
			tableData[3][1] = "";
			tableData[4][1] = "";
			tableData[5][1] = "";
			tableData[6][1] = "";
		} else {
			String animals = "";
			if (planet.getFauna() < 0.01) {
				animals = "none";
			}
			if (planet.faunaIsVeryLow()) {
				animals = "some bacteria";
			}
			if (planet.faunaIsLow()) {
				animals = "some insects";
			}
			if (planet.faunaIsAverage()) {
				animals = "small animals";
			}
			if (planet.faunaIsHigh()) {
				animals = "large animals";
			}
			if (planet.faunaIsVeryHigh()) {
				animals = "dinosaurs";
			}
			tableData[0][1] = animals;

			String plants = "";
			if (planet.getFlora() < 0.01) {
				plants = "none";
			}
			if (planet.floraIsVeryLow()) {
				plants = "some moss";
			}
			if (planet.floraIsLow()) {
				plants = "mostly grass";
			}
			if (planet.floraIsAverage()) {
				plants = "earthlike ";
			}
			if (planet.floraIsHigh()) {
				plants = "forest world";
			}
			if (planet.floraIsVeryHigh()) {
				plants = "jungle world";
			}
			tableData[1][1] = plants;

			String salt = "";
			double pSalt = planet.getSalt();
			// XXX: replace with planet.saltXXX methods
			if (pSalt < 0.01) {
				salt = NONE;
			} else if (pSalt < 0.4) {
				salt = VERY_POOR;
			} else if (pSalt < 0.8) {
				salt = POOR;
			} else if (pSalt < 1.2) {
				salt = SOME;
			} else if (pSalt < 1.6) {
				salt = RICH;
			} else {
				salt = VERY_RICH;
			}
			tableData[2][1] = salt;
			
			String wood = "";
			// XXX: replace with planet.woodxXX methods
			double pWood = planet.getWood();
			if (pWood < 0.01) {
				wood = NONE;
			} else if (pWood < 0.2) {
				wood = VERY_POOR;
			} else if (pWood < 0.8) {
				wood = POOR;
			} else if (pWood < 1.3) {
				wood = SOME;
			} else if (pWood < 1.7) {
				wood = RICH;
			} else {
				wood = VERY_RICH;
			}
			tableData[3][1] = wood;
			
			String metal = "";
			double pMetal = planet.getMetals();
			// XXX: replace with planet.metalsXXX methods
			if (pMetal < 0.01) {
				metal = NONE;
			} else if (pMetal < 0.4) {
				metal = VERY_POOR;
			} else if (pMetal < 0.8) {
				metal = POOR;
			} else if (pMetal < 1.2) {
				metal = SOME;
			} else if (pMetal < 1.6) {
				metal = RICH;
			} else {
				metal = VERY_RICH;
			}
			tableData[4][1] =metal;
			
			String uranium = "";
			double pUranium = planet.getUranium();
			// XXX: replace with planet.uraniumXXX methods
			if (pUranium < 0.01) {
				uranium = NONE;
			} else if (pUranium < 0.4) {
				uranium = VERY_POOR;
			} else if (pUranium < 0.8) {
				uranium = POOR;
			} else if (pUranium < 1.2) {
				uranium = SOME;
			} else if (pUranium < 1.6) {
				uranium = RICH;
			} else {
				uranium = VERY_RICH;
			}
			tableData[5][1] = uranium;
			
			String fossil = "";
			// XXX: replace with planet.fossilsXXX methods
			double pFossils = planet.getFossils();
			if (pFossils < 0.01) {
				fossil = NONE;
			} else if (pFossils < 0.4) {
				fossil = VERY_POOR;
			} else if (pFossils < 0.8) {
				fossil = POOR;
			} else if (pFossils < 1.2) {
				fossil = SOME;
			} else if (pFossils < 1.6) {
				fossil = RICH;
			} else {
				fossil = VERY_RICH;
			}
			tableData[6][1] = fossil;
		}
	}

	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
