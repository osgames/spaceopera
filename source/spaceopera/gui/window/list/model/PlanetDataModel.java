package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.elements.Planet;

public class PlanetDataModel implements TableModel {

	private String[] columnNames = { "Planet Data", "" };
	private Object[][] tableData = { {} };
	
	private Planet planet;
	private Fullscreen fullscreen;

	public PlanetDataModel(Fullscreen fullscreen, Planet planet) {
		this.fullscreen=fullscreen;
		this.planet=planet;
		tableData = new Object[9][2];
		tableData[0][0] = "Name:";		
		tableData[1][0] = "Type:";
		tableData[2][0] = "Hours per Day:";
		tableData[3][0] = "Days per year:";
		tableData[4][0] = "Gravity:";
		tableData[5][0] = "Density:";
		tableData[6][0] = "Water/Land ratio:";
		tableData[7][0] = "Fertility:";
		tableData[8][0] = "Radiation:";		
		if (planet == null) {
			tableData[0][1] = "";
			tableData[1][1] = "";
			tableData[2][1] = "";
			tableData[3][1] = "";
			tableData[4][1] = "";
			tableData[5][1] = "";
			tableData[6][1] = "";
			tableData[7][1] = "";
			tableData[8][1] = "";
		} else {
			tableData[0][1] = planet.getName();
			tableData[1][1] = planet.getType();
			String day = String.valueOf(planet.getHoursPerDay());
			int pos = day.indexOf(".");
			if (pos + 3 < day.length()) {
				day = day.substring(0, pos + 3);
			}
			tableData[2][1] = day + "hours";
			String year = String.valueOf(planet.getDaysPerYear());
			pos = year.indexOf(".");
			if (pos + 3 < year.length()) {
				year = year.substring(0, pos + 3);
			}
			tableData[3][1] = year + "days";
			String sGravity = String.valueOf(planet.getGravitation());
			pos = sGravity.indexOf(".");
			if (pos + 3 < sGravity.length()) {
				sGravity = sGravity.substring(0, pos + 3);
			}
			tableData[4][1] = sGravity + "G";
			String sDensity = String.valueOf(planet.getDensity());
			pos = sDensity.indexOf(".");
			if (pos + 3 < sDensity.length()) {
				sDensity = sDensity.substring(0, pos + 3);
			}
			tableData[5][1] = sDensity + " g/cm3";
			String water = String.valueOf(planet.getWaterLandRatio() / 2);
			String land = String.valueOf(1 - planet.getWaterLandRatio() / 2);
			pos = water.indexOf(".");
			if (pos + 3 < water.length()) {
				water = water.substring(0, pos + 3);
			}
			pos = land.indexOf(".");
			if (pos + 3 < land.length()) {
				land = land.substring(0, pos + 3);
			}
			// TODO String wlRatio = "ideal";
			// if
			// (preferences.waterLandRatioIsExtreme(planet.getWaterLandRatio()))
			// {
			// wlRatio = "extreme";
			// }
			// if (preferences.waterLandRatioIsPoor(planet.getWaterLandRatio()))
			// {
			// wlRatio = "poor";
			// }
			// if
			// (preferences.waterLandRatioIsAverage(planet.getWaterLandRatio()))
			// {
			// wlRatio = "acceptable";
			// }
			// else "ideal"
			tableData[6][1] = water + ":" + land;

			String sFertility = String.valueOf(planet.getFertility());
			pos = sFertility.indexOf(".");
			if (pos + 3 < sFertility.length()) {
				sFertility = sFertility.substring(0, pos + 3);
			}
			tableData[7][1] = sFertility;
			
			String sRadiation = String.valueOf(planet.getRadiation());
			pos = sRadiation.indexOf(".");
			if (pos + 3 < sRadiation.length()) {
				sRadiation = sRadiation.substring(0, pos + 3);
			}
			tableData[8][1] = sRadiation + " mRem/year";
			
		}

	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		if(arg0==0 && arg1==1) {
			return true;
		}
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		tableData[arg1][arg2]=arg0;
		planet.setName((String) arg0);
		fullscreen.refreshPlanetLabel(planet);
	}

}
