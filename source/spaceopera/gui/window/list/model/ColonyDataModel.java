package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.colony.Colony;

public class ColonyDataModel implements TableModel {

	private String[] columnNames = { "", "" };
	private Object[][] tableData = { {} };

	private Colony colony;
	private Fullscreen fullscreen;

	public ColonyDataModel(Fullscreen fullscreen, Colony colony) {
		this.fullscreen = fullscreen;
		this.colony = colony;
		tableData = new Object[2][2];
		tableData[0][0] = "Name:";
		tableData[1][0] = "Population Size:";
		if (colony == null) {
			tableData[0][1] = "";
			tableData[1][1] = "";

		} else {
			tableData[0][1] = colony.getName();
			tableData[1][1] = colony.getPopulationCount();
		}

	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		if (arg0 == 0 && arg1 == 1) {
			return true;
		}
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		tableData[arg1][arg2] = arg0;
		colony.setName((String) arg0);
		fullscreen.refreshColonyLabel(colony);
	}

}
