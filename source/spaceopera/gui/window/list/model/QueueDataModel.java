package spaceopera.gui.window.list.model;

import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Colony;

/**
 * production queue
 */
public class QueueDataModel implements TableModel {

	private String[] columnNames = { "Item", "Completion" };
	private Object[][] tableData = { {} };

	public QueueDataModel(Colony colony) {

		if (colony == null) {
			tableData = new Object[1][2];
			tableData[0][1] = "";
			tableData[1][1] = "";
		} else {
			List<BuildProject> queue = colony.getProductionQueue();
			tableData = new Object[queue.size()][2];
			for (int i = 0; i < queue.size(); i++) {
				BuildProject bp = queue.get(i);
				tableData[i][0] = bp.getName();
				tableData[i][1] = bp.getPercentComplete() + "%";
			}
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
