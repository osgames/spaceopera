package spaceopera.gui.window.list.model;

import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Colony;

/**
 * existing buildings of a colony
 * 
 */
public class ExistingBuildingsTableModel implements TableModel {

	private String[] columnNames = { "Building", "Count" };
	private Object[][] tableData = { {} };

	// TODO Filter?
	public ExistingBuildingsTableModel(Colony colony, String filter) {
		tableData = new Object[2][2];
		tableData[0][0] = "Name:";
		tableData[1][0] = "Count:";
		if (colony == null) {
			tableData[0][1] = "";
			tableData[1][1] = "";
		} else {
			List<BuildProject> buildings = colony.getBuildings();
			tableData = new Object[buildings.size()][2];
			int i = 0;
			for (BuildProject b : buildings) {
				tableData[i][0] = b.getName();
				tableData[i][1] = ((Building) b).getCounter();
				i++;
			}
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
