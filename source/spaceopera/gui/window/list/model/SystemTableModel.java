package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.elements.Sun;

public class SystemTableModel implements TableModel {

	private String[] columnNames = { "Nr", "Name", "Coord", "# Planets" };
	private Object[][] tableData = { {} };

	public SystemTableModel(Fullscreen fs) {
		boolean empty = true;
		int size = 0;
		//TODO refactor this double loop
		for (Sun s : fs.getUniverse().getSuns()) {
			if (s.getSunSystem().isExploredBy(fs.getCurrentPlayerName())) {
				size++;
			}
		}
		int i = 0;
		if (size == 0) {
			size = 1;
		}
		tableData = new Object[size][4];
		for (Sun s : fs.getUniverse().getSuns()) {
			if (s.getSunSystem().isExploredBy(fs.getCurrentPlayerName())) {
				empty = false;
				tableData[i][0] = "" + s.getSunSystem().getNumber();
				tableData[i][1] = s.getSunSystem().getName();
				tableData[i][2] = s.getX() + "," + s.getY();
				tableData[i][3] = "" + s.getSunSystem().getPlanets().size();
				i++;
			}
		}
		if (empty) {
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
			tableData[0][3] = "";
		}
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
