package spaceopera.gui.window.list.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import spaceopera.test.Fullscreen;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.ShipOrderItem;

public class ShipTableModel implements TableModel {

	private String[] columnNames = { "Ship Name", "Type", "Coordinates", "Target", "Order", "ETA" };
	private Object[][] tableData = { {} };

	public ShipTableModel(Fullscreen fs) {
		if (fs.getCurrentPlayer().getShips().size() > 0) {
			tableData = new Object[fs.getCurrentPlayer().getShips().size()][6];
			for (int i = 0; i < fs.getCurrentPlayer().getShips().size(); i++) {
				Ship s = (Ship) fs.getCurrentPlayer().getShips().get(i);

				tableData[i][0] = s.getShipName();
				tableData[i][1] = s.getShipDesign().getShipClass();
				tableData[i][2] = "" + s.getX() + "," + s.getY();

				if (s.getCurrentOrder() != null && !"none".equals(s.getCurrentOrder().getCommand())) {
					tableData[i][3] = s.getCurrentOrder().getTarget();
					tableData[i][4] = s.getCurrentOrder().getCommand() + " " + s.getCurrentOrder().getOption();
				} else if (s.getNextOrderItems().size() > 0) {
					ShipOrderItem nextOrder = (ShipOrderItem) s.getNextOrderItems().elementAt(0);
					if ("auto".equals(nextOrder.getOption())) {
						tableData[i][3] = "";
						tableData[i][4] = nextOrder.getOption() + " " + nextOrder.getCommand();
					} else {
						tableData[i][3] = nextOrder.getTarget();
						tableData[i][4] = nextOrder.getCommand() + " " + nextOrder.getTarget();
					}
				} else {
					tableData[i][3] = "";
					tableData[i][4] = "none";
				}

				tableData[i][5] = s.getTurns() + " turns";
			}
		} else {
			tableData = new Object[1][6];
			tableData[0][0] = "";
			tableData[0][1] = "";
			tableData[0][2] = "";
			tableData[0][3] = "";
			tableData[0][4] = "";
			tableData[0][5] = "";
		}
		//fireTableDataChanged();
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public Class<?> getColumnClass(int column) {
		return getValueAt(0, column).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return tableData.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return tableData[row][col];
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {

		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// empty
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// empty
	}

}
