//The class SunSystemList displays a a List of all known SunSystems
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.list;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.elements.SunSystem;

/** The SunSystemList shows a list of all known sunsystems
 * @deprecated
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class SunSystemList extends SOFrame implements ActionListener, SOConstants {
  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Player player = null;
  private TextArea ta1;
  private TextField tf1,tf2,tf3;
  private java.awt.List sunsystemList;
  private Panel pCenter, pTitle, pList, pSouth;
  private Label label1, label2;
  private Button showSystem, close;
  private Vector knownSystems;
  Object[] knownSystemsArray;

  public SunSystemList (SpaceOpera so) {
      spaceOpera = so;
      player = spaceOpera.getCurrentPlayer();
      knownSystems=new Vector();
      setTitle("List all known sunsystems for player \"" + player.getName() + "\"");
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();    
      pack();
      setSize(WINDOWSIZEX,WINDOWSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
  }

  public void init() {
     List suns = spaceOpera.getSuns();
     knownSystems.removeAllElements();
     String s1, s2, s3, s4;
     label1.setText(" Nr. " +
                    " Name               " +
                  " Coordinates    " +
                  " Number      ");
     label2.setText("     " +
                    "                    " +
                  "                " +
                  " of planets  ");
     if (sunsystemList.getItemCount()>0) {
       sunsystemList.removeAll();
     }
     for (int i=0; i<suns.size(); i++) {
        Sun sun = (Sun)suns.get(i);
        SunSystem sunSystem = sun.getSunSystem();
        Hashtable explored = sunSystem.getExplored();
        if (explored.containsKey(spaceOpera.getCurrentPlayerName())) {
           knownSystems.add(sun);
        }
     }
     knownSystemsArray = knownSystems.toArray();
     Arrays.sort(knownSystemsArray);
     
     for (int i=0; i<knownSystemsArray.length;i++) {
         Sun sun = (Sun)knownSystemsArray[i];
         s1 = " " + sun.getNumber() + "          ";
         s2 = " " + sun.getName() + "                              ";
         s3 = " " + sun.getX() + "/" + sun.getY() + "                         ";
         s4 = " " + sun.getPlanets().size() + "                                ";
         sunsystemList.add(s1.substring(0,5) + s2.substring(0,20) +
                               s3.substring(0,16) + s4.substring(0,13));         
     }
  }

  public void refresh() {
    if (isVisible()) {
      init();
    }
  }

  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CLOSE)) {
        this.dispose();
    } else if (c.equals(GOTO)) {
        // show and select sunsystem, if none selected, show first
        if (knownSystems.size()>0 && knownSystemsArray != null && knownSystemsArray.length>0) {
           int selectedSun = sunsystemList.getSelectedIndex();
           if (selectedSun < 0) {
              selectedSun=0;
           }
           Sun sun = (Sun)knownSystemsArray[selectedSun];
           SunSystem sunSystem = sun.getSunSystem();
           spaceOpera.selectSunInUniverse(sun);
           spaceOpera.updateSunSystem(sunSystem,null);
           spaceOpera.displayUniverse();
        }
        this.dispose();
    }
  }

  private void doStaticLayout() {
//      setLayout(new BorderLayout());
      pCenter = new Panel();
      pCenter.setBackground(Color.white);
      pCenter.setForeground(Color.black);
      pCenter.setLayout(new GridLayout(1,1,2,0));
      pTitle = new Panel();
      pTitle.setLayout(new GridLayout(2,1));
      pTitle.setBackground(Color.white);
      pTitle.setForeground(Color.black);
      pTitle.setFont(new Font("Monospaced", Font.PLAIN, 12));
      pList = new Panel();
      pList.setLayout(new BorderLayout(1,2));
      pList.setBackground(Color.white);
      pList.setForeground(Color.black);
      pList.setFont(new Font("Monospaced", Font.PLAIN, 12));
      label1 = new Label("");
      label2 = new Label("");
      pTitle.add(label1);
      pTitle.add(label2);
      sunsystemList = new java.awt.List();
      sunsystemList.setBackground(Color.white);
      sunsystemList.setForeground(Color.black);
      sunsystemList.setFont(new Font("Monospaced", Font.PLAIN, 12));
      pList.add("North",pTitle);
      pList.add("Center",sunsystemList);
      pCenter.add(pList);
      add("Center", pCenter);
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,6));
      pSouth.setBackground(Color.black);
      pSouth.setForeground(Color.white);
      showSystem = new Button(GOTO);
      showSystem.addActionListener(this);
      showSystem.setActionCommand(GOTO);
      showSystem.setForeground(Color.black);
      showSystem.setBackground(Color.lightGray);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      pSouth.add(showSystem);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(close);
      add("South",pSouth);
  }
}
