//The class PlanetList list all known planets in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.list;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.elements.Planet;

/** The PlanetList shows a list of all known planets of a player
 * @deprecated
 */
@SuppressWarnings({"rawtypes"})
public class PlanetList extends SOFrame implements ActionListener, SOConstants {

  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Player player = null;
  private TextArea ta1;
  private TextField tf1,tf2,tf3;
  private java.awt.List planetList;  // ev. ColorTextCanvas?
  private ScrollPane scrollPane;
  private Panel scrollPaneContent;
  private Panel contentLine[];
  private Panel pCenter, pTitle, pList, pSouth;
  private Label label1, label2;
  private Button close, showPlanet[];


  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();

    if (c.equals(CLOSE)) {
        this.dispose();
    } else { // if (c.equals(GOTO)) {          // goto selected planet
         // XXX: for all the selectable planets a number is passed as command,
         // XXX: this is a hack, what happens when other buttons are added?

        List<Planet> planets = player.getPlanetList();
        if ((planets != null) && (planets.size()>0)) {
           int selectedPlanet = new Integer(c).intValue();
           Planet planet = (Planet)planets.get(selectedPlanet);
           spaceOpera.displayPlanet(planet);
        }
        this.dispose();
    }
  }

  private void doStaticLayout() {
      // Center
      setLayout(new BorderLayout(2,2));
      pCenter = new Panel();
      pCenter.setBackground(Color.white);
      pCenter.setForeground(Color.black);
      pCenter.setLayout(new GridLayout(1,1,2,0));
      pTitle = new Panel();
      pTitle.setLayout(new GridLayout(2,1));
      pTitle.setBackground(Color.white);
      pTitle.setForeground(Color.black);
      pTitle.setFont(new Font("Monospaced", Font.PLAIN, 12));
      pList = new Panel();
      pList.setLayout(new BorderLayout(1,1));
      pList.setBackground(Color.white);
      pList.setForeground(Color.black);
      pList.setFont(new Font("Monospaced", Font.PLAIN, 12));
      label1 = new Label("");
      label2 = new Label("");
      pTitle.add(label1);
      pTitle.add(label2);
//      planetList = new java.awt.List();
//      planetList.setBackground(Color.white);
//      planetList.setForeground(Color.black);
//      planetList.setFont(new Font("Monospaced", Font.PLAIN, 12));
//      pList.add("North",pTitle);
//      pList.add("Center",planetList);
//      pCenter.add(pList);
      scrollPane = new java.awt.ScrollPane();
      scrollPaneContent = new java.awt.Panel();
	  scrollPane.add(scrollPaneContent);
	  pCenter.add(scrollPane);

      add("Center", pCenter);
      // South
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,6));
      pSouth.setBackground(Color.black);
      pSouth.setForeground(Color.white);
//    showPlanet = new Button(GOTO);
//    showPlanet.addActionListener(this);
//    showPlanet.setActionCommand(GOTO);
//    showPlanet.setForeground(Color.black);
//    showPlanet.setBackground(Color.lightGray);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
//    pSouth.add(showPlanet);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(close);
      add("South",pSouth);
  }


  public void init() {
     player = spaceOpera.getCurrentPlayer();
     List<Planet> planets = player.getPlanetList();
     String s1, s2, s3, s4, s5, s6;
	 label1.setFont(new Font("Monospaced", Font.PLAIN, 12));
	 label1.setForeground(Color.white);
     label1.setBackground(Color.black);
     label1.setText(" Planet name      " +
                    " Sunsystem name   " +
                    " Planet type   " +
                    " Colonized by   " +
                    " Mining     " +
                    " Colonization     ");
	 label2.setFont(new Font("Monospaced", Font.PLAIN, 12));
	 label2.setForeground(Color.white);
	 label2.setBackground(Color.black);
     label2.setText("                  " +
                    "                  " +
                    "               " +
					"                " +
					" Value      " +
                    " possible with    ");
//     if (planetList.getItemCount()>0) {
//       planetList.removeAll();
//     }

     scrollPaneContent.removeAll();
     int gridSize = planets.size() + 2;
     if (gridSize < 20) {
     	gridSize=20;
     }

	 scrollPaneContent.setLayout(new GridLayout(gridSize,1));
	 scrollPaneContent.add(label1);
	 scrollPaneContent.add(label2);

	 showPlanet = new Button[planets.size()];
	 contentLine = new Panel[planets.size()];
     for (int i=0; i<planets.size(); i++) {
        Planet planet = (Planet)planets.get(i);
        s1 = " " + planet.getName() + "                               ";
        s2 = " " + planet.getSunName() + "                            ";
        s3 = " " + planet.getType() + "                             ";
        if (planet.isColonized()) {
           s4 = " " + planet.getPlayerName() + "                                  ";
        } else {
           s4 = "                                                 ";
        }
		s5 = " " + planet.getMiningValue() + "                               ";
		s6 = " " + planet.getNeededColonyModuleName(player) + "                               ";

//        planetList.add(s1.substring(0,18) + s2.substring(0,18) +
//      s3.substring(0,18+ s4.substring(0,18));

        Label planetText = new Label();
        planetText.setFont(new Font("Monospaced", Font.PLAIN, 12));
		planetText.setForeground(Color.black);
		if (planet.getType().equals(MERCURYLIKE)) {
			planetText.setBackground(Color.yellow);
			planetText.setBackground(new Color(1.0f, 1.0f, 0.0f));
		}
		if (planet.getType().equals(VENUSLIKE)) {
			planetText.setBackground(Color.orange);
			//planetText.setBackground(new Color(1.0f, 0.9f, 0.0f));
		}
		if (planet.getType().equals(EARTHLIKE)) {
			//planetText.setBackground(Color.green.brighter());
			planetText.setBackground(new Color(0.5f, 1.0f, 0.5f));
		}
		if (planet.getType().equals(MARSLIKE)) {
			//planetText.setBackground(Color.red);
			planetText.setBackground(new Color(1.0f, 0.6f, 0.6f));
		}
		if (planet.getType().equals(GASGIANT)) {
			//planetText.setBackground(Color.pink);
			planetText.setBackground(new Color(1.0f, 0.8f, 1.0f));
		}
		if (planet.getType().equals(TRANSSATURN)) {
			//planetText.setBackground(Color.lightBlue);
			planetText.setBackground(new Color(0.0f, 1.0f, 1.0f));
		}
		if (planet.getType().equals(ASTEROIDBELT)) {
			planetText.setBackground(Color.lightGray);
		}
        planetText.setText(s1.substring(0,18) + s2.substring(0,18) +
		                   s3.substring(0,15) + s4.substring(0,16) +
		                   s5.substring(0,12) + s6.substring(0,18));


		showPlanet[i] = new Button(GOTO);
		showPlanet[i].addActionListener(this);
		showPlanet[i].setActionCommand(new Integer(i).toString());
		//showPlanet[i].setActionCommand(GOTO);
		showPlanet[i].setForeground(Color.black);
		showPlanet[i].setBackground(Color.lightGray);
		contentLine[i] = new Panel();
		contentLine[i].setLayout(new BorderLayout());
		contentLine[i].add("Center",planetText);
		contentLine[i].add("East",showPlanet[i]);
		scrollPaneContent.add(contentLine[i]);
     }
  }


  public PlanetList (SpaceOpera so) {
      spaceOpera = so;
      player = spaceOpera.getCurrentPlayer();
      setTitle("List of all known planets for player \"" + player.getName() + "\"");
      doStaticLayout();
      enableCloseButton(this);      
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();                        
      pack();
      setSize(PLANETLISTWINDOWSIZEX,PLANETLISTWINDOWSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);      
      init();
      show();      
  }


  public void refresh() {
    if (isVisible()) {
      init();
    }
  }

}