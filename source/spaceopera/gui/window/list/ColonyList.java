//The class ColonyList shows a list of all colonies in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.list;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Sun;

/**
 * The ColonyList shows a list of all the player's colonies
 * 
 * @deprecated
 */
@SuppressWarnings({ "rawtypes" })
public class ColonyList extends SOFrame implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private SpaceOpera spaceOpera = null;
	private Player player = null;
	private TextArea ta1;
	private TextField tf1, tf2, tf3;
	private java.awt.List colonyList;
	private Panel pCenter, pTitle, pList, pSouth;
	private Label label1, label2;
	private Button showColony, close;
	private List<Colony> colonies;

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals(CLOSE)) {
			this.dispose();
		} else if (c.equals(GOTO)) {
			// goto selected colony, if none selected, goto first
			if (colonies.size() > 0) {
				int selectedColony = colonyList.getSelectedIndex();
				if (selectedColony < 0) {
					selectedColony = 0;
				}
				Colony colony = colonies.get(selectedColony);
				spaceOpera.displayColony(colony);
			}
			this.dispose();
		}
	}

	public ColonyList(SpaceOpera so) {
		spaceOpera = so;
		player = spaceOpera.getCurrentPlayer();
		setTitle("List all colonies for player \"" + player.getName() + "\"");
		doStaticLayout();
		enableCloseButton(this);
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		setSize(WINDOWSIZEX, WINDOWSIZEY);
		setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);
		init();
		show();
	}

	private void doStaticLayout() {
		setLayout(new BorderLayout(2, 2));

		// Center
		pCenter = new Panel();
		pCenter.setBackground(Color.white);
		pCenter.setForeground(Color.black);
		pCenter.setLayout(new GridLayout(1, 1, 2, 0));
		pTitle = new Panel();
		pTitle.setLayout(new GridLayout(2, 1));
		pTitle.setBackground(Color.white);
		pTitle.setForeground(Color.black);
		pTitle.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pList = new Panel();
		pList.setLayout(new BorderLayout(1, 2));
		pList.setBackground(Color.white);
		pList.setForeground(Color.black);
		pList.setFont(new Font("Monospaced", Font.PLAIN, 12));
		label1 = new Label("");
		label2 = new Label("");
		pTitle.add(label1);
		pTitle.add(label2);
		colonyList = new java.awt.List();
		colonyList.setBackground(Color.white);
		colonyList.setForeground(Color.black);
		colonyList.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pList.add("North", pTitle);
		pList.add("Center", colonyList);
		pCenter.add(pList);
		add("Center", pCenter);

		// South
		pSouth = new Panel();
		pSouth.setLayout(new GridLayout(1, 6));
		pSouth.setBackground(Color.black);
		pSouth.setForeground(Color.white);
		showColony = new Button(GOTO);
		showColony.addActionListener(this);
		showColony.setActionCommand(GOTO);
		showColony.setForeground(Color.black);
		showColony.setBackground(Color.lightGray);
		close = new Button(CLOSE);
		close.addActionListener(this);
		close.setActionCommand(CLOSE);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		pSouth.add(showColony);
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(new Label(""));
		pSouth.add(close);
		add("South", pSouth);
	}

	public void init() {
		player = spaceOpera.getCurrentPlayer();
		colonies = player.getColonies();
		String s1, s2, s3, s4, s5, s6, s7, s8;
		BuildProject bp = null;
		label1.setText(" Sun   " + " Planet " + " Coordinates  " + " Colony Name        " + " Population " + " Current            "
				+ " Colony             " + " Highest Demanded   ");
		label2.setText(" Nr.   " + " Nr.    " + "              " + "                    " + " Count      " + " Production         "
				+ " Output             " + " Resource           ");
		if (colonyList.getItemCount() > 0) {
			colonyList.removeAll();
		}
		for (int i = 0; i < colonies.size(); i++) {
			Colony colony = colonies.get(i);
			Sun sun = colony.getSun();
			if (colony.getProductionQueue().size() > 0) {
				bp = colony.getProductionQueue().get(0);
			}
			s1 = " " + sun.getNumber() + "           ";
			s2 = " " + colony.getPlanetNumber() + "             ";
			s3 = " " + sun.getX() + "/" + sun.getY() + "                      ";
			s4 = " " + colony.getName() + "                            ";
			s5 = " " + ((int) 100.0 * colony.getPopulationCount()) / 100 + "                ";
			if (bp != null) {
				s6 = " " + bp.getName() + "                                   ";
			} else {
				s6 = " none                            ";
			}
			s7 = " " + colony.getTotalProduction() + "                    ";
			s8 = " " + colony.getHighestDemand() + "                ";
			colonyList.add(s1.substring(0, 8) + s2.substring(0, 8) + s3.substring(0, 13) + s4.substring(0, 20) + s5.substring(0, 12)
					+ s6.substring(0, 20) + s7.substring(0, 20) + s8.substring(0, 20));
		}
	}

	public void refresh() {
		if (isVisible()) {
			init();
		}
	}

}