package spaceopera.gui.window.list;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import spaceopera.gui.window.helper.PanelHelper;
import spaceopera.gui.window.helper.TableHelper;
import spaceopera.gui.window.list.model.ColonyTableModel;
import spaceopera.gui.window.list.model.PlanetTableModel;
import spaceopera.gui.window.list.model.ShipDesignTableModel;
import spaceopera.gui.window.list.model.ShipTableModel;
import spaceopera.gui.window.list.model.SystemTableModel;
import spaceopera.gui.window.list.model.TechnologyTableModel;
import spaceopera.test.Fullscreen;

/**
 * panel with lists (Colonies, Planets, Technologies, ...)
 */
public class ListPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Fullscreen fs;
	private JTabbedPane tabs;
	private JComponent colonies;
	private JComponent ships;
	private JComponent technologies;
	private JComponent systems;
	private JComponent planets;
	private JComponent shipDesigns;

	private JTable colonyTable;
	private JTable shipTable;
	private JTable technologyTable;
	private JTable systemTable;
	private JTable planetTable;
	private JTable shipDesignTable;

	public ListPanel(Fullscreen fs) {
		super();
		this.fs = fs;

		setBackground(Color.black);
		setLayout(new GridLayout(1, 1));

		tabs = new JTabbedPane();

		tabs.setBackground(Color.black);
		tabs.setForeground(Color.yellow);
		tabs.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));

		colonies = getColonyTableView();
		tabs.addTab("Colonies", null, colonies, "");

		ships = getShipTableView();
		tabs.addTab("Ships", null, ships, "");

		technologies = getTechnologyTableView();
		tabs.addTab("Technologies", null, technologies, "");

		systems = getSystemTableView();
		tabs.addTab("Systems", null, systems, "");

		planets = getPlanetTableView();
		tabs.addTab("Planets", null, planets, "");

		shipDesigns = getShipDesignTableView();
		tabs.addTab("Ship Designs", null, shipDesigns, "");

		add(tabs);
	}

	public JComponent getColonyTableView() {
		colonyTable = TableHelper.createDefaultTable(new ColonyTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(colonyTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public JComponent getPlanetTableView() {
		planetTable = TableHelper.createDefaultTable(new PlanetTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(planetTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public JComponent getShipTableView() {
		shipTable = TableHelper.createDefaultTable(new ShipTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(shipTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public JComponent getTechnologyTableView() {
		technologyTable = TableHelper.createDefaultTable(new TechnologyTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(technologyTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public JComponent getSystemTableView() {
		systemTable = TableHelper.createDefaultTable(new SystemTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(systemTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public JComponent getShipDesignTableView() {
		shipDesignTable = TableHelper.createDefaultTable(new ShipDesignTableModel(fs), true);
		JScrollPane scrollPane = new JScrollPane(shipDesignTable);
		scrollPane.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		scrollPane.setForeground(Color.YELLOW);
		scrollPane.setBackground(Color.BLACK);
		JPanel panel = PanelHelper.createDefaultPanel();
		panel.add(scrollPane);
		return panel;
	}

	public void refreshPlanetTable() {
		PlanetTableModel model = new PlanetTableModel(fs);
		planetTable.setModel(model);
		planetTable.getColumnModel().getColumn(0).setPreferredWidth(110); // Name
		planetTable.getColumnModel().getColumn(1).setPreferredWidth(110); // System
		planetTable.getColumnModel().getColumn(2).setPreferredWidth(60); // Orbit
		planetTable.getColumnModel().getColumn(3).setPreferredWidth(100); // Type
		planetTable.getColumnModel().getColumn(4).setPreferredWidth(100); // Colonized by
	}

	public void refreshColonyTable() {
		ColonyTableModel model = new ColonyTableModel(fs);
		colonyTable.setModel(model);
		colonyTable.getColumnModel().getColumn(0).setPreferredWidth(100); // Name
		colonyTable.getColumnModel().getColumn(1).setPreferredWidth(100); // System
		colonyTable.getColumnModel().getColumn(2).setPreferredWidth(50); // Planet
		colonyTable.getColumnModel().getColumn(3).setPreferredWidth(50); // Pop
																			// Count
		colonyTable.getColumnModel().getColumn(4).setPreferredWidth(180); // Prod.
	}

	public void refreshShipTable() {
		ShipTableModel model = new ShipTableModel(fs);
		shipTable.setModel(model);
		shipTable.getColumnModel().getColumn(0).setPreferredWidth(100); // Name
		shipTable.getColumnModel().getColumn(1).setPreferredWidth(100); // Type
		shipTable.getColumnModel().getColumn(2).setPreferredWidth(60); // Coords
		shipTable.getColumnModel().getColumn(3).setPreferredWidth(60); // Target
		shipTable.getColumnModel().getColumn(4).setPreferredWidth(100); // Order
		shipTable.getColumnModel().getColumn(5).setPreferredWidth(60); // ETA
	}

	public void refreshShipDesignTable() {
		ShipDesignTableModel model = new ShipDesignTableModel(fs);
		shipDesignTable.setModel(model);
		shipDesignTable.getColumnModel().getColumn(0).setPreferredWidth(100); // Name
		shipDesignTable.getColumnModel().getColumn(1).setPreferredWidth(100); // Hull
		shipDesignTable.getColumnModel().getColumn(2).setPreferredWidth(100); // Colony
																				// Module
		shipDesignTable.getColumnModel().getColumn(3).setPreferredWidth(50); // Armed
		shipDesignTable.getColumnModel().getColumn(4).setPreferredWidth(50); // Build
		shipDesignTable.getColumnModel().getColumn(4).setPreferredWidth(80); // Still
																				// Existing
	}

	public void refreshSystemTable() {
		SystemTableModel model = new SystemTableModel(fs);
		systemTable.setModel(model);
	}

	public void refreshTechnologyTable() {
		TechnologyTableModel model = new TechnologyTableModel(fs);
		technologyTable.setModel(model);
	}

	public JTable getColonyTable() {
		return colonyTable;
	}

	public JTable getShipTable() {
		return shipTable;
	}

	public JTable getTechnologyTable() {
		return technologyTable;
	}

	public JTable getSystemTable() {
		return systemTable;
	}

	public JTable getPlanetTable() {
		return planetTable;
	}

	public JTable getShipDesignTable() {
		return shipDesignTable;
	}

}
