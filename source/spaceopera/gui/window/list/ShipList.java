//The class ShipList list all own ships in the SpaceOpera game
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.list;

import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/** The ShipList shows a list of all the player's ships
 * @deprecated
 */
@SuppressWarnings({"rawtypes"})
public class ShipList extends SOFrame implements ActionListener, SOConstants {
  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Player player = null;
  private TextArea ta1;
  private TextField tf1,tf2,tf3;
  private java.awt.List shipList;
  private Panel pCenter, pTitle, pList, pSouth;
  private Label label1, label2;
  private Button select, order, close;
  private List<SpaceCraft>  ships;


  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals("setOrders")) {
    	selectShip();
    	spaceOpera.setShipOrders();
        this.dispose();
    }else if (c.equals(CLOSE)) {
    	this.dispose();
    } else if (c.equals("select")) {
    	selectShip();
      	this.dispose();
    }
  }

  private void doStaticLayout() {
    setLayout(new BorderLayout());
    pCenter = new Panel();
    pCenter.setBackground(Color.white);
    pCenter.setForeground(Color.black);
    pCenter.setLayout(new GridLayout(1,1,2,0));
    pTitle = new Panel();
    pTitle.setLayout(new GridLayout(1,1));
    pTitle.setBackground(Color.white);
    pTitle.setForeground(Color.black);
    pTitle.setFont(new Font("Monospaced", Font.PLAIN, 12));
    pList = new Panel();
    pList.setLayout(new BorderLayout());
    pList.setBackground(Color.white);
    pList.setForeground(Color.black);
    pList.setFont(new Font("Monospaced", Font.PLAIN, 12));
    label1 = new Label("");
    //label2 = new Label("");
    pTitle.add(label1);
    //pTitle.add(label2");
    shipList = new java.awt.List();
    shipList.setBackground(Color.white);
    shipList.setForeground(Color.black);
    shipList.setFont(new Font("Monospaced", Font.PLAIN, 12));
    pList.add("North",pTitle);
    pList.add("Center",shipList);
    pCenter.add(pList);
    add("Center", pCenter);
    // South
    pSouth = new Panel();
    pSouth.setLayout(new GridLayout(1,6));
    pSouth.setBackground(Color.black);
    pSouth.setForeground(Color.white);
    order = new Button("Set Orders");
    order.addActionListener(this);
    order.setActionCommand("setOrders");
    order.setForeground(Color.black);
    order.setBackground(Color.lightGray);    
    close = new Button(CLOSE);
    close.addActionListener(this);
    close.setActionCommand(CLOSE);
    close.setForeground(Color.black);
    close.setBackground(Color.lightGray);
    select = new Button("Select");
    select.addActionListener(this);
    select.setActionCommand("select");
    select.setForeground(Color.black);
    select.setBackground(Color.lightGray);    
    pSouth.add(select);
    pSouth.add(new Label(""));
    pSouth.add(order);
    pSouth.add(new Label(""));
    pSouth.add(new Label(""));
    pSouth.add(close);
    add("South",pSouth);
  }


  public void init() {
    player = spaceOpera.getCurrentPlayer();
    ships = player.getShips();
    String shipName, shipCoordinates, shipTarget, shipOrder, shipEta;
    label1.setText("Ship name       " +
                   "Coordinates      " +
                   "Target                     " +
                   "Order                      " +
                   "ETA         ");
    if (shipList.getItemCount()>0) {
      shipList.removeAll();
    }
    for (int i=0; i<ships.size(); i++) {
      SpaceCraft ship = ships.get(i);
      if (ship instanceof Ship) {
        shipName = ship.getShipName() + "                              ";
        shipCoordinates = ship.getX() + "/" + ship.getY() + "                         ";
        if (ship.getTargetSun() != null) {
          shipTarget = ship.getTargetSun().getName() + "                                    ";
        } else {
          shipTarget = "none                             ";
        }
        if (ship.getCurrentOrder() != null) {
          shipOrder = ship.getCurrentOrder() + "                                   ";
        } else {
          shipOrder = "none                                 ";
        }
        shipEta = ship.getTurns() + " turns              ";
        shipList.add(shipName.substring(0,18) + shipCoordinates.substring(0,15) +
                         shipTarget.substring(0,27) + shipOrder.substring(0,27) + shipEta.substring(0,12));
      }
    }
  }

  public void refresh() {
    if (isVisible()) {
      init();
    }
  }

  private void selectShip() {
	if (ships.size()>0) {
    	int selectedShip = shipList.getSelectedIndex();
    	if (selectedShip < 0) {
      		selectedShip=0;
    	}
    	Ship ship = (Ship)ships.get(selectedShip);
    	spaceOpera.selectShip(ship);
  	}
  }
  
  public ShipList (SpaceOpera so) {
    spaceOpera = so;
    player = spaceOpera.getCurrentPlayer();
    setTitle("List all ships from player \"" + player.getName() + "\"");
    doStaticLayout();
    enableCloseButton(this);
    Toolkit kit = Toolkit.getDefaultToolkit();
    Dimension dim = kit.getScreenSize();       
    pack();
    setSize(WINDOWSIZEX,WINDOWSIZEY);
    setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
    init();
    show();
  }
}