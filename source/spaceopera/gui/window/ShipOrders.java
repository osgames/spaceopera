//The class ShipOrders displays or sets new ship orders
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

//XXX: more orders (patrol, lay mines, gather resources...)
//XXX: define order details/settings (auto attack enemies, ...)
//XXX: (?) ship history (built in, orders, visited sunsystems, damage, repair, upgrades...) 

package spaceopera.gui.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import spaceopera.gui.controls.SoButton;
import spaceopera.gui.window.helper.LabelHelper;
import spaceopera.gui.window.helper.PanelHelper;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.ai.Player;
import spaceopera.universe.elements.SunSystem;
import spaceopera.universe.ships.ShipOrderItem;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The ShipOrders shows the current ship's orders.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ShipOrders extends JPanel implements ActionListener, ListSelectionListener, SOConstants {
	private static final long serialVersionUID = 1L;
	private Fullscreen fs;
	// private SpaceOpera spaceOpera = null;
	private SpaceCraft currentShip = null;
	Player player = null;
	private JButton deleteOrder, previousShip, nextShip, setOrder;
	private JPanel titlePanel, topPanel, topLeftPanel, currentOrdersPanel, topRightPanel, settingsPanel;
	private JPanel centerPanel, actionOrderPanel, targetOrderPanel;
	private JPanel bottomPanel;
	private JLabel titleLabel, currentOrderLabel, nextOrderLabel, createOrderLabel, imageLabel, topRightPanelLabel, selectFromLabel,
			selectToLabel, selectOptionsLabel, selectPlanetAndColonizeLabel, selectSunAndColonizeLabel;
	private JTextField currentOrderText;
	private JList nextOrders; // , orderHistory;
	// JScrollPane listScrollPane
	private DefaultListModel nextOrdersModel; // , orderHistoryModel;
	private JRadioButton attackButton, defendButton, fleeButton, exploreClosestButton, exploreFromList, colonizeClosestButton,
			colonizeBestButton, colonizeFirstButton;
	private JCheckBox waitTillFull;
	private ButtonGroup settingsGroup, colonizeGroup, exploreGroup;
	private TitledBorder centerPanelTitle;
	private JTabbedPane ordersPane;
	private JPanel gotoPanel, explorePanel, colonizePanel, transportPanel, tradePanel, attackPanel, defendPanel;
	private JComboBox planetCombo, sunCombo, sunComboUnexplored, transportToCombo, selectCargoCombo;
	private Vector nextOrderItems;
	private JPanel nextOrdersPanel;
	private JPanel buttonPanel;
	private JLabel centerTitleLabel;
	private JPanel centerTitlePanel;
	private JPanel gotoPanelLeft;
	private JPanel gotoPanelRight;
	private JPanel explorePanelLeft;
	private JPanel explorePanelRight;
	private JRadioButton selectAndColonizeButton;
	private JPanel colonizePanelLeft;
	private JPanel colonizePanelRight;

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("deleteOrder")) {
			int selectedOrder = nextOrders.getSelectedIndex();
			if (selectedOrder >= 0) {
				nextOrdersModel.removeElementAt(selectedOrder);
				nextOrderItems.removeElementAt(selectedOrder);
			}
		} else if (c.equals("setOrders")) {
			ShipOrderItem nextOrderItem;
			switch (ordersPane.getSelectedIndex()) {
			case 0: // goto
				if (ordersPane.isEnabledAt(GOTO_TAB)) {
					String sunName = (String) sunCombo.getSelectedItem();
					removeAnyAutoCommands();
					if (!sunName.equals("all explored")) {
						nextOrderItem = new ShipOrderItem("", GOTO, sunName);
						String nextOrder = nextOrderItem.toString();
						String lastOrder = "";
						if (nextOrdersModel.size() > 0) {
							lastOrder = (String) nextOrdersModel.elementAt(nextOrdersModel.size() - 1);
						}
						if (!nextOrder.equals(lastOrder)) {
							nextOrdersModel.addElement(nextOrder);
							nextOrderItems.addElement(nextOrderItem);
						}
					}
				}
				break;
			case 1: // scout
				if (exploreClosestButton.isSelected()) {
					removeAllNextOrders();
					nextOrderItem = new ShipOrderItem(AUTO, EXPLORE, "");
					nextOrdersModel.addElement(nextOrderItem.toString());
					nextOrderItems.addElement(nextOrderItem);
				} else if (exploreFromList.isSelected()) {
					String sunName = (String) sunComboUnexplored.getSelectedItem();
					if (!sunName.equals(NONE_AVAILABLE)) {
						removeAnyAutoCommands();
						nextOrderItem = new ShipOrderItem("", EXPLORE, sunName);
						nextOrdersModel.addElement(nextOrderItem.toString());
						nextOrderItems.addElement(nextOrderItem);
					}
				}
				break;
			case 2: // colonize
				if (colonizeFirstButton.isSelected()) {
					removeAllNextOrders();
					nextOrderItem = new ShipOrderItem(AUTO, COLONIZE, FIRST);
					nextOrdersModel.addElement(nextOrderItem.toString());
					nextOrderItems.addElement(nextOrderItem);
				} else if (colonizeBestButton.isSelected()) {
					removeAllNextOrders();
					nextOrderItem = new ShipOrderItem(AUTO, COLONIZE, BEST);
					nextOrdersModel.addElement(nextOrderItem.toString());
					nextOrderItems.addElement(nextOrderItem);
				} else if (colonizeClosestButton.isSelected()) {
					removeAllNextOrders();
					nextOrderItem = new ShipOrderItem(AUTO, COLONIZE, CLOSEST);
					nextOrdersModel.addElement(nextOrderItem.toString());
					nextOrderItems.addElement(nextOrderItem);
				} else if (selectAndColonizeButton.isSelected()) {
					String planetName = (String) planetCombo.getSelectedItem();
					if (!planetName.equals(NONE_AVAILABLE)) {
						removeAllNextOrders();
						nextOrderItem = new ShipOrderItem("", COLONIZE, planetName);
						nextOrdersModel.addElement(nextOrderItem.toString());
						nextOrderItems.addElement(nextOrderItem);
					}
				}
				break;
			case 3: // transport
				// XXX: transport logic for "transport x to y, fill"
				String cargo = (String) selectCargoCombo.getSelectedItem();
				String target = (String) transportToCombo.getSelectedItem();
				String fill = "";
				if (waitTillFull.isSelected()) {
					fill = "fill";
				}
				if ((!cargo.equals(NONE_AVAILABLE)) && (!target.equals(NONE_AVAILABLE))) {
					nextOrderItem = new ShipOrderItem(TRANSPORT, cargo, target, fill);
					nextOrdersModel.addElement(nextOrderItem.toString());
					nextOrderItems.addElement(nextOrderItem);
				}

				break;

			// XXX: other commands

			default:
				break;
			}

		} else if (c.equals("previousShip")) {
			// TODO spaceOpera.selectPreviousShip();
			init();
		} else if (c.equals("nextShip")) {
			// TODO spaceOpera.selectNextShip();
			init();
		}
		fs.refreshShipInfo(currentShip);
		fs.refreshShipTable();
	}

	public void valueChanged(ListSelectionEvent e) {
		// ignore
	}

	private void doStaticLayout() {

		LayoutManager left = new FlowLayout(FlowLayout.LEFT, 1, 1);
		LayoutManager right = new FlowLayout(FlowLayout.RIGHT, 1, 1);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(Color.black);
		setForeground(Color.yellow);

		titleLabel = LabelHelper.createLabel("Ship Orders", 12);
		titlePanel = PanelHelper.createTitlePanel(460, 25, Color.darkGray);
		titlePanel.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.gray));
		titlePanel.add(titleLabel);
		add(titlePanel);

		// Top
		topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.LINE_AXIS));
		topPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		topPanel.setBackground(Color.black);
		topPanel.setForeground(Color.yellow);
		// Orders
		topLeftPanel = new JPanel();
		topLeftPanel.setLayout(new BoxLayout(topLeftPanel, BoxLayout.PAGE_AXIS));
		topLeftPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.gray));
		topLeftPanel.setMaximumSize(new Dimension(230, 500));
		topLeftPanel.setBackground(Color.black);
		topLeftPanel.setForeground(Color.yellow);

		currentOrdersPanel = new JPanel();
		currentOrdersPanel.setLayout(new BoxLayout(currentOrdersPanel, BoxLayout.LINE_AXIS));
		currentOrdersPanel.setBackground(Color.black);
		currentOrdersPanel.setForeground(Color.yellow);
		currentOrdersPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		currentOrdersPanel.setPreferredSize(new Dimension(230, 25));
		currentOrderLabel = new JLabel("current orders:  ");
		currentOrderLabel.setBackground(Color.black);
		currentOrderLabel.setForeground(Color.yellow);
		currentOrderText = new JTextField();
		currentOrderText.setBackground(Color.black);
		currentOrderText.setForeground(Color.yellow);
		currentOrderText.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		currentOrdersPanel.add(currentOrderLabel);
		currentOrdersPanel.add(currentOrderText);

		nextOrdersPanel = new JPanel();
		nextOrdersPanel.setLayout(new BorderLayout());
		nextOrdersPanel.setBackground(Color.black);
		nextOrdersPanel.setForeground(Color.yellow);
		nextOrdersPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		nextOrdersPanel.setPreferredSize(new Dimension(230, 125));

		nextOrderLabel = new JLabel("next orders:");
		nextOrderLabel.setBackground(Color.black);
		nextOrderLabel.setForeground(Color.yellow);
		// nextOrderLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		nextOrdersModel = new DefaultListModel();
		nextOrders = new JList(nextOrdersModel);
		nextOrders.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		nextOrders.addListSelectionListener(this);
		nextOrders.setVisibleRowCount(6);
		nextOrders.setBackground(Color.black);
		nextOrders.setForeground(Color.yellow);
		JScrollPane listScrollPane = new JScrollPane(nextOrders);
		listScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// listScrollPane.setPreferredSize(new Dimension(230, 100));
		listScrollPane.setBackground(Color.black);
		listScrollPane.setForeground(Color.yellow);
		listScrollPane.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.gray));

		nextOrdersPanel.add("North", nextOrderLabel);
		nextOrdersPanel.add("Center", listScrollPane);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(right);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		buttonPanel.setBackground(Color.black);
		buttonPanel.setForeground(Color.yellow);
		buttonPanel.setAlignmentX(JPanel.RIGHT_ALIGNMENT);
		buttonPanel.setMaximumSize(new Dimension(230, 25));
		deleteOrder = new SoButton("Delete selected order");
		deleteOrder.setMnemonic(KeyEvent.VK_D);
		deleteOrder.setActionCommand("deleteOrder");
		deleteOrder.addActionListener(this);
		buttonPanel.add(deleteOrder);

		topLeftPanel.add(currentOrdersPanel);
		topLeftPanel.add(Box.createVerticalGlue());
		topLeftPanel.add(nextOrdersPanel);
		topLeftPanel.add(buttonPanel);

		// Settings
		topRightPanel = new JPanel();
		topRightPanel.setLayout(new BorderLayout());
		topRightPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.gray));
		topRightPanel.setBackground(Color.black);
		topRightPanel.setForeground(Color.yellow);
		topRightPanelLabel = LabelHelper.createLabel("Default settings", 11);
		settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.PAGE_AXIS));
		settingsPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		settingsPanel.setBackground(Color.black);
		settingsPanel.setForeground(Color.yellow);
		attackButton = new JRadioButton("Attack Enemies");
		attackButton.setMnemonic(KeyEvent.VK_A);
		attackButton.setSelected(true);
		attackButton.setEnabled(false);
		attackButton.setBackground(Color.black);
		attackButton.setForeground(Color.yellow);
		// attackButton.setActionCommand("attack");
		defendButton = new JRadioButton("Defend When Attacked");
		defendButton.setMnemonic(KeyEvent.VK_D);
		defendButton.setEnabled(false);
		defendButton.setBackground(Color.black);
		defendButton.setForeground(Color.yellow);
		// defendButton.setActionCommand("defend");
		fleeButton = new JRadioButton("Flee When Attacked");
		fleeButton.setMnemonic(KeyEvent.VK_F);
		fleeButton.setEnabled(false);
		fleeButton.setBackground(Color.black);
		fleeButton.setForeground(Color.yellow);
		// fleeButton.setActionCommand("flee");
		settingsGroup = new ButtonGroup();
		settingsGroup.add(attackButton);
		settingsGroup.add(defendButton);
		settingsGroup.add(fleeButton);
		settingsPanel.add(attackButton);
		settingsPanel.add(defendButton);
		settingsPanel.add(fleeButton);
		topRightPanel.add(topRightPanelLabel, BorderLayout.PAGE_START);
		topRightPanel.add(settingsPanel, BorderLayout.CENTER);
		topPanel.add(topLeftPanel);
		topPanel.add(topRightPanel);

		// Center
		centerTitleLabel = LabelHelper.createLabel("Define next orders", 11);
		centerTitlePanel = PanelHelper.createTitlePanel(460, 25, Color.black);
		centerTitlePanel.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.gray));
		centerTitlePanel.add(centerTitleLabel);

		centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.LINE_AXIS));
		centerPanel.setMaximumSize(new Dimension(460, 160));
		centerPanel.setBackground(Color.black);
		centerPanel.setForeground(Color.yellow);
		centerPanel.setBorder(centerPanelTitle);
		//
		actionOrderPanel = new JPanel();
		actionOrderPanel.setLayout(new BoxLayout(actionOrderPanel, BoxLayout.PAGE_AXIS));
		actionOrderPanel.setBackground(Color.black);
		actionOrderPanel.setForeground(Color.yellow);
		actionOrderPanel.setMaximumSize(new Dimension(460, 160));
		actionOrderPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.gray));
		// Action command
		// createOrderLabel = new JLabel("Select action command:");
		// String[] commandStrings = { "Scout", COLONIZE, "deploy mines",
		// "attack", TRANSPORT };
		// JComboBox commandList = new JComboBox(commandStrings);
		// commandList.setSelectedIndex(0);
		// commandList.addActionListener(this);

		// send ship to
		gotoPanel = new JPanel();
		gotoPanel.setLayout(new BoxLayout(gotoPanel, BoxLayout.LINE_AXIS));
		gotoPanel.setBackground(Color.black);
		gotoPanel.setForeground(Color.yellow);
		gotoPanelLeft = new JPanel();
		gotoPanelLeft.setLayout(new BoxLayout(gotoPanelLeft, BoxLayout.PAGE_AXIS));
		gotoPanelLeft.setMaximumSize(new Dimension(230, 160));
		gotoPanelLeft.setBackground(Color.black);
		gotoPanelLeft.setForeground(Color.yellow);
		gotoPanelLeft.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
		gotoPanelRight = new JPanel();
		gotoPanelRight.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		gotoPanelRight.setLayout(new BoxLayout(gotoPanelRight, BoxLayout.PAGE_AXIS));
		gotoPanelRight.setMaximumSize(new Dimension(230, 160));
		gotoPanelRight.setBackground(Color.black);
		gotoPanelRight.setForeground(Color.yellow);
		gotoPanelRight.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));

		// gotoPanel.setMaximumSize(new Dimension(230, 140));
		// test data
		// String[] sunStrings = { "all explored", "Tarot", "Jamba", "Mickey",
		// "George Washington" };

		gotoPanelLeft.add(Box.createVerticalGlue());
		gotoPanelLeft.add(LabelHelper.createLabel("Select sunsystem:", 11));
		gotoPanelLeft.add(Box.createVerticalGlue());
		gotoPanelLeft.add(Box.createVerticalGlue());
		gotoPanelLeft.add(Box.createVerticalGlue());
		gotoPanelLeft.add(Box.createVerticalGlue());
		sunCombo = new JComboBox();
		sunCombo.setMaximumSize(new Dimension(230, 20));
		sunCombo.setBackground(Color.DARK_GRAY);
		sunCombo.setForeground(Color.cyan);
		sunCombo.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		gotoPanelRight.add(Box.createVerticalGlue());
		gotoPanelRight.add(sunCombo);
		gotoPanelRight.add(Box.createVerticalGlue());
		gotoPanelRight.add(Box.createVerticalGlue());
		gotoPanelRight.add(Box.createVerticalGlue());
		gotoPanelRight.add(Box.createVerticalGlue());

		gotoPanel.add(gotoPanelLeft);
		gotoPanel.add(gotoPanelRight);

		// TODO JLabel warning1 = LabelHelper.createLabel("<html>Warning: Setting this command <br>nremoves any 'auto' commands",
		// 11);
		// warning1.setForeground(Color.red);
		// warning1.setBackground(Color.black);
		// gotoPanel.add(warning1);

		// explore
		explorePanel = new JPanel();
		explorePanel.setLayout(new BoxLayout(explorePanel, BoxLayout.LINE_AXIS));
		explorePanel.setBackground(Color.black);
		explorePanel.setForeground(Color.yellow);
		explorePanel.setMaximumSize(new Dimension(230, 160));
		explorePanelLeft = new JPanel();
		explorePanelLeft.setLayout(new BoxLayout(explorePanelLeft, BoxLayout.PAGE_AXIS));
		explorePanelLeft.setMaximumSize(new Dimension(230, 160));
		explorePanelLeft.setBackground(Color.black);
		explorePanelLeft.setForeground(Color.yellow);
		explorePanelLeft.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
		explorePanelRight = new JPanel();
		explorePanelRight.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		explorePanelRight.setLayout(new BoxLayout(explorePanelRight, BoxLayout.PAGE_AXIS));
		explorePanelRight.setMaximumSize(new Dimension(230, 160));
		explorePanelRight.setBackground(Color.black);
		explorePanelRight.setForeground(Color.yellow);
		explorePanelRight.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));

		sunComboUnexplored = new JComboBox();
		sunComboUnexplored.setMaximumSize(new Dimension(230, 20));
		sunComboUnexplored.setBackground(Color.DARK_GRAY);
		sunComboUnexplored.setForeground(Color.cyan);
		sunComboUnexplored.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		exploreClosestButton = new JRadioButton("Auto explore closest");
		exploreClosestButton.setBackground(Color.black);
		exploreClosestButton.setForeground(Color.yellow);
		exploreClosestButton.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		exploreFromList = new JRadioButton("Explore sunsystem:");
		exploreFromList.setBackground(Color.black);
		exploreFromList.setForeground(Color.yellow);
		exploreFromList.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		exploreGroup = new ButtonGroup();
		exploreGroup.add(exploreClosestButton);
		exploreGroup.add(exploreFromList);
		// scoutButton.setMnemonic(KeyEvent.VK_C);
		exploreClosestButton.setSelected(true);
		// TODO JLabel warning2 = LabelHelper.createLabel("<html>Warning: Setting 'auto explore' <br>clears the 'next order' list,"
		// + "<br>and setting 'Explore sunsystem' <br>removes any 'auto' commands.", 11);
		// warning2.setForeground(Color.red);
		// warning2.setBackground(Color.black);
		explorePanelLeft.add(exploreClosestButton);
		explorePanelLeft.add(exploreFromList);
		explorePanelLeft.add(Box.createVerticalGlue());
		explorePanelRight.add(Box.createVerticalGlue());
		explorePanelRight.add(sunComboUnexplored);
		explorePanelRight.add(Box.createVerticalGlue());
		explorePanelRight.add(Box.createVerticalGlue());
		explorePanelRight.add(Box.createVerticalGlue());
		explorePanel.add(explorePanelLeft);
		explorePanel.add(explorePanelRight);
		// explorePanel.add(warning2);
		// explorePanel.add(new JLabel(" "));

		// colonize
		colonizePanel = new JPanel();
		colonizePanel.setLayout(new BoxLayout(colonizePanel, BoxLayout.LINE_AXIS));
		colonizePanel.setBackground(Color.black);
		colonizePanel.setForeground(Color.yellow);
		colonizePanel.setMaximumSize(new Dimension(230, 160));

		colonizePanelLeft = new JPanel();
		colonizePanelLeft.setLayout(new BoxLayout(colonizePanelLeft, BoxLayout.PAGE_AXIS));
		colonizePanelLeft.setMaximumSize(new Dimension(230, 160));
		colonizePanelLeft.setBackground(Color.black);
		colonizePanelLeft.setForeground(Color.yellow);
		colonizePanelLeft.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));
		colonizePanelRight = new JPanel();
		colonizePanelRight.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		colonizePanelRight.setLayout(new BoxLayout(colonizePanelRight, BoxLayout.PAGE_AXIS));
		colonizePanelRight.setMaximumSize(new Dimension(230, 160));
		colonizePanelRight.setBackground(Color.black);
		colonizePanelRight.setForeground(Color.yellow);
		colonizePanelRight.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.gray));

		colonizeFirstButton = new JRadioButton("colonize first planet found");
		colonizeFirstButton.setSelected(true);
		colonizeFirstButton.setBackground(Color.black);
		colonizeFirstButton.setForeground(Color.yellow);
		colonizeFirstButton.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		colonizeBestButton = new JRadioButton("colonize best planet found");
		colonizeBestButton.setBackground(Color.black);
		colonizeBestButton.setForeground(Color.yellow);
		colonizeBestButton.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		colonizeClosestButton = new JRadioButton("colonize closest planet found");
		colonizeClosestButton.setBackground(Color.black);
		colonizeClosestButton.setForeground(Color.yellow);
		colonizeClosestButton.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		selectAndColonizeButton = new JRadioButton("select planet to colonize");
		selectAndColonizeButton.setBackground(Color.black);
		selectAndColonizeButton.setForeground(Color.yellow);
		selectAndColonizeButton.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		colonizeGroup = new ButtonGroup();
		colonizeGroup.add(colonizeFirstButton);
		colonizeGroup.add(colonizeBestButton);
		colonizeGroup.add(colonizeClosestButton);
		colonizeGroup.add(selectAndColonizeButton);
		// TODO
		// TODO JLabel warning3 = LabelHelper.createLabel("<html>Warning: Setting this command <br>clears the 'next order' list",
		// 11);
		// warning3.setForeground(Color.red);
		// warning3.setBackground(Color.black);

		selectPlanetAndColonizeLabel = LabelHelper.createLabel("Select planet from list:", 11);
		selectPlanetAndColonizeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		planetCombo = new JComboBox();
		planetCombo.setMaximumSize(new Dimension(230, 20));
		planetCombo.setBackground(Color.DARK_GRAY);
		planetCombo.setForeground(Color.cyan);
		planetCombo.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		// TODO JLabel warning4 = LabelHelper.createLabel("Warning: Setting this command clears the 'next order' list", 11);
		// warning4.setForeground(Color.red);
		// warning4.setBackground(Color.black);
		colonizePanelLeft.add(colonizeFirstButton);
		colonizePanelLeft.add(colonizeBestButton);
		colonizePanelLeft.add(colonizeClosestButton);
		// exploreAndColonizePanel.add(new JLabel(" "));
		// exploreAndColonizePanel.add(warning3);
		colonizePanelLeft.add(selectAndColonizeButton);

		colonizePanelRight.add(Box.createVerticalGlue());
		colonizePanelRight.add(Box.createVerticalGlue());
		colonizePanelRight.add(Box.createVerticalGlue());
		colonizePanelRight.add(Box.createVerticalGlue());
		colonizePanelRight.add(planetCombo);
		colonizePanelRight.add(Box.createVerticalGlue());

		colonizePanelLeft.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));
		colonizePanelRight.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));
		// selectAndColonizePanel.add(warning4);

		colonizePanel.add(colonizePanelLeft);
		colonizePanel.add(colonizePanelRight);

		// transport
		transportPanel = new JPanel();
		transportPanel.setBackground(Color.black);
		transportPanel.setForeground(Color.yellow);
		transportPanel.setLayout(new GridLayout(3, 2));
		transportPanel.setMaximumSize(new Dimension(230, 160));
		JLabel selectGoods = LabelHelper.createLabel("Select cargo:", 11);
		JLabel selectColony = LabelHelper.createLabel("Send cargo to:", 11);
		selectCargoCombo = new JComboBox();
		transportToCombo = new JComboBox();
		waitTillFull = new JCheckBox("Wait until ship is fully loaded.");
		// TODO JLabel transportWarning = LabelHelper.createLabel("<html>Warning: waiting until a ship is fully loaded may "
		// + "<br>take many turns, depending on the selected cargo.", 11);
		// transportWarning.setForeground(Color.red);
		// transportWarning.setBackground(Color.black);
		transportPanel.add(selectGoods);
		transportPanel.add(selectCargoCombo);
		transportPanel.add(selectColony);
		transportPanel.add(transportToCombo);
		transportPanel.add(waitTillFull);
		transportPanel.add(new JLabel(" "));
		// transportPanel.add(transportWarning);
		// transportPanel.add(new JLabel(" "));

		// trade
		tradePanel = new JPanel();
		tradePanel.add(new JTextField("Trade", 20));
		// attack
		attackPanel = new JPanel();
		attackPanel.add(new JTextField("Attack", 20));
		// defend
		defendPanel = new JPanel();
		defendPanel.add(new JTextField("Defend", 20));
		// add panels
		ordersPane = new JTabbedPane();
		ordersPane.setBackground(Color.black);
		ordersPane.setForeground(Color.yellow);
		ordersPane.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		ordersPane.addTab("Send ship to", gotoPanel);
		ordersPane.addTab("Scout", explorePanel);
		ordersPane.addTab(COLONIZE, colonizePanel);
		ordersPane.addTab("Transport", transportPanel);
		// ordersPane.setEnabledAt(TRANSPORT_TAB,false);
		ordersPane.addTab("Trade", tradePanel);
		ordersPane.setEnabledAt(TRADE_TAB, false);
		ordersPane.addTab("Attack", attackPanel);
		ordersPane.setEnabledAt(ATTACK_TAB, false);
		ordersPane.addTab("Defend", defendPanel);
		ordersPane.setEnabledAt(DEFEND_TAB, false);
		// actionOrderPanel.add(createOrderLabel);
		actionOrderPanel.add(ordersPane);
		// actionOrderPanel.add(commandList);

		centerPanel.add(actionOrderPanel);

		// Bottom
		// Commands
		previousShip = new SoButton("Previous Ship");
		previousShip.setMnemonic(KeyEvent.VK_P);
		previousShip.setActionCommand("previousShip");
		previousShip.addActionListener(this);

		nextShip = new SoButton("Next Ship");
		nextShip.setMnemonic(KeyEvent.VK_N);
		nextShip.setActionCommand("nextShip");
		nextShip.addActionListener(this);

		setOrder = new SoButton("Set Next Orders");
		setOrder.setMnemonic(KeyEvent.VK_O);
		setOrder.setActionCommand("setOrders");
		setOrder.addActionListener(this);

		bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
		bottomPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.gray));
		bottomPanel.setPreferredSize(new Dimension(460, 21));
		bottomPanel.setBackground(Color.black);
		bottomPanel.setForeground(Color.yellow);
		bottomPanel.add(setOrder);
		bottomPanel.add(Box.createHorizontalGlue());
		bottomPanel.add(previousShip);
		bottomPanel.add(nextShip);

		add(topPanel);
		add(centerTitlePanel);
		add(centerPanel);
		add(bottomPanel);
		add(Box.createVerticalGlue());

	}

	public void init() {

		player = fs.getCurrentPlayer();
		nextOrderItems = new Vector();
		try {
			currentShip = fs.getUniverse().getCurrentSpaceCraft();
		} catch (Exception e) {
			// ignore nullpointer at when no ship is selected or none exist
		}

		String[] sunStrings = fs.getUniverse().getSunNames();
		sunCombo.removeAllItems();
		sunComboUnexplored.removeAllItems();

		for (int i = 0; i < sunStrings.length; i++) {
			sunCombo.addItem(sunStrings[i]);
		}
		sunCombo.repaint();

		sunStrings = fs.getUniverse().getUnexploredSunNames(player.getName());
		for (int i = 0; i < sunStrings.length; i++) {
			sunComboUnexplored.addItem(sunStrings[i]);
		}
		sunComboUnexplored.repaint();

		String[] planetStrings = player.getColonizablePlanetNames(currentShip);
		planetCombo.removeAllItems();
		for (int i = 0; i < planetStrings.length; i++) {
			planetCombo.addItem(planetStrings[i]);
		}
		planetCombo.repaint();

		// XXX: fill selectCargoCombo and transportToCombo
		// abh�ngig von der aktuellen Kolonie / Sonnensystem / Handelsstation
		// ..., wo das Schiff gerade ist
		SunSystem sunSystem = currentShip.getFromSunSystem();
		String[] cargoStrings = { NONE_AVAILABLE };
		if (sunSystem != null) {
			cargoStrings = sunSystem.getAvailableCargo(currentShip.getPlayer());
		}
		selectCargoCombo.removeAllItems();
		for (int i = 0; i < cargoStrings.length; i++) {
			selectCargoCombo.addItem(cargoStrings[i]);
		}
		selectCargoCombo.repaint();

		// XXX: add other player's colonies, or outposts, waypoints, ...?
		String[] targetStrings = player.getColonyPlanetNames();
		if (targetStrings.length == 0) {
			targetStrings = new String[] { NONE_AVAILABLE };
		}
		transportToCombo.removeAllItems();
		for (int i = 0; i < targetStrings.length; i++) {
			transportToCombo.addItem(targetStrings[i]);
		}
		transportToCombo.repaint();

		if (currentShip != null) {
			// set title
			String shipName = currentShip.getShipClass();
			String shipPosition = currentShip.getX() + "/" + currentShip.getY();
			titleLabel.setText("Ship Orders for " + shipName + " at " + shipPosition);
			// display current order text
			if (!currentShip.getCurrentOrder().equals("none")) {
				currentOrderText.setText(currentShip.getCurrentOrder().toString());
			} else {
				currentOrderText.setText(currentShip.getCurrentOrder().getCommand());
			}
			// display the next orders text
			nextOrderItems = currentShip.getNextOrderItems();
			nextOrdersModel.removeAllElements();
			for (int i = 0; i < nextOrderItems.size(); i++) {
				String order = ((ShipOrderItem) nextOrderItems.elementAt(i)).toString();
				nextOrdersModel.addElement(order);
			}
			nextOrders.setModel(nextOrdersModel);

			// reset from satellite orders
			sunCombo.setEnabled(true);
			ordersPane.setEnabledAt(GOTO_TAB, true);

			// disable colonize tab
			if (currentShip.getColonyModuleNumber() == 0) {
				ordersPane.setEnabledAt(COLONIZE_TAB, false);
			} else {
				ordersPane.setEnabledAt(COLONIZE_TAB, true);
			}
			// disable transport tab
			if (currentShip.getTransportCapacity() == 0) {
				ordersPane.setEnabledAt(TRANSPORT_TAB, false);
			} else {
				ordersPane.setEnabledAt(TRANSPORT_TAB, true);
			}
			// disable tabs for satellites and ships with sublight-only drive
			if (!currentShip.isShip() || !currentShip.hasInterstellarDrive()) {
				ordersPane.setEnabledAt(GOTO_TAB, false);
				sunCombo.setEnabled(false);
				ordersPane.setEnabledAt(SCOUT_TAB, false);
				ordersPane.setEnabledAt(COLONIZE_TAB, false);
				ordersPane.setEnabledAt(TRANSPORT_TAB, false);
				// XXX: add new tabs when necessary
			}
		}
	}

	public void refresh() {
		init();
	}

	public void removeAnyAutoCommands() {
		if (nextOrdersModel.size() > 0) {
			String order = (String) nextOrdersModel.elementAt(0);
			if (order.startsWith(AUTO)) {
				removeAllNextOrders();
			}
		}
	}

	public void removeAllNextOrders() {
		nextOrderItems.removeAllElements();
		nextOrdersModel.removeAllElements();
	}

	private static void createAndShowGUI() {

	}

	public ShipOrders(Fullscreen fs) {
		this.fs = fs;

		doStaticLayout();

		init();
	}

}
