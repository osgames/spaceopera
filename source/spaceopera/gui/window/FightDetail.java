//The class FightDetail executes fights between ships in sunsystems in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.util.*;
import java.awt.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SODialog;
import spaceopera.gui.objects.battle.FightBeamWeapon;
import spaceopera.gui.objects.battle.FightBombWeapon;
import spaceopera.gui.objects.battle.FightColony;
import spaceopera.gui.objects.battle.FightExplosion;
import spaceopera.gui.objects.battle.FightObject;
import spaceopera.gui.objects.battle.FightSatellite;
import spaceopera.gui.objects.battle.FightShip;
import spaceopera.gui.objects.battle.FightShipColony;
import spaceopera.gui.objects.battle.FightShipScout;
import spaceopera.gui.objects.battle.FightTorpedoWeapon;
import spaceopera.gui.objects.weapons.BeamWeapon;
import spaceopera.gui.objects.weapons.Bomb;
import spaceopera.gui.objects.weapons.BundledLaser;
import spaceopera.gui.objects.weapons.Laser;
import spaceopera.gui.objects.weapons.LaserCannon;
import spaceopera.gui.objects.weapons.PelletGun;
import spaceopera.gui.objects.weapons.RailGun;
import spaceopera.gui.objects.weapons.Torpedo;
import spaceopera.gui.objects.weapons.Weapon;
import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.Colony;
import spaceopera.universe.elements.Orbit;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Satellite;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/**
 * this is the parent class for FightDetailVisible and contains all relevant
 * code to handle the fights (between computer players)
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class FightDetail extends SODialog implements SOConstants {
	private static final long serialVersionUID = 1L;
	protected Vector fighterShips = null;
	protected Vector fighterColonies = null;
	protected Vector torpedos = null;
	protected Vector beamWeapons = null;
	protected Vector bombs = null;
	protected Vector planets = null;
	protected Vector explosions = null;
	protected Vector shipExplosions = null;
	protected Vector torpedoExplosions = null;
	protected Sun sun = null;
	protected SpaceOpera parent = null;
	protected int fromX;
	protected int fromY;
	protected int oldX;
	protected int oldY;
	protected int moveCounter = 1;
	protected Object orderSource = null;
	protected Object orderTarget = null;
	protected int stepSleepTime = 0; // ms
	protected int laserSleepTime = 50;
	protected int bombSleepTime = 50;
	protected int bombStepTime = 150;

	// Constructor
	public FightDetail(Sun s, SpaceOpera so) {
		super(so.getDisplay());
		sun = s;
		parent = so;
		init();
		addShips();
		addColonies();
	}

	// Methods
	// executeAllSteps calls the single steps.
	// this function runs as long as anything important happens
	//
	// called from SpaceOpera.executeFight
	// called from FightDetailVisible (step, slow, fast or immediate(=not
	// visible))
	public void executeAllSteps(Graphics g) {
		boolean finished = false;
		int counter = 0;
		while (!finished) {
			boolean action = executeStep(g);
			if (!action) {
				counter++;
				if (counter >= 5) {
					finished = true;
				}
			}
		}
	}

	// one single 'battleturn' for Ships / Torpedos
	// movement is (battleSpeed * 10) pixels
	//
	// called from executeAllSteps
	public boolean executeStep(Graphics g) {
		boolean action = false;
		// expire Torpedoes after a while
		for (int i = torpedos.size() - 1; i >= 0; i--) {
			boolean exploded = false;
			FightTorpedoWeapon ft = (FightTorpedoWeapon) torpedos.elementAt(i);
			if (g != null) {
				ft.display(g);
			}
			exploded = checkLifeCyclesAndExplode(ft, g);
		}
		for (int h = 0; h < MAXBATTLESPEED; h++) {
			boolean move = false;
			for (int i = 0; i < fighterShips.size(); i++) {
				FightShip fs = (FightShip) fighterShips.elementAt(i);
				if (fs.getOrderTarget() == null) {
					fs.checkOrders(fs, fighterShips, sun);
				}
				if (((fs.getShipAttacks()) && (fs.getOrderTarget() instanceof FightShip))
						|| ((fs.getPlanetAttacks()) && (fs.getOrderTarget() instanceof Planet))) {
					if (h < fs.getBattleSpeed()) {
						move = moveObject(fs, g);
						if (move) {
							action = true;
						}
					}
				}
				if ((h == 0) && (g != null)) {
					g.setColor(Color.black);
					g.drawLine(fromX, fromY, oldX, oldY);
					fs.display(g);
				}
			}
			// check distance and fire
			for (int i = 0; i < fighterShips.size(); i++) {
				FightShip fs = (FightShip) fighterShips.elementAt(i);
				if (!fs.isDestroyed()) {
					if (fs.getOrderTarget() != null) {
						boolean continueFire = checkDistanceAndFire(fs, h + 1, g);
						action = action || continueFire;
					}
				}
			}
			for (int i = 0; i < fighterColonies.size(); i++) {
				FightColony fc = (FightColony) fighterColonies.elementAt(i);
				checkDistanceAndFire(fc, g);
			}
			// move and explode torpedos
			for (int i = 0; i < torpedos.size(); i++) {
				boolean exploded = false;
				FightTorpedoWeapon ft = (FightTorpedoWeapon) torpedos.elementAt(i);
				if (ft.getOrderTarget() != null) {
					if (h < ft.getWeaponBattleSpeed()) {
						move = moveObject(ft, this.getGraphics());
						exploded = checkDistanceAndExplode(ft, g);
						action = move || exploded;
					}
				} else {
					if (h < ft.getWeaponBattleSpeed()) {
						move = moveObject(ft, g);
					}
					action = true;
					move = true;
				}
			}
			if (!move) {
				h = MAXBATTLESPEED;
			}
		}
		// BeamWeapons
		if (g != null) { // error !!! must be executed always
			for (int i = 0; i < beamWeapons.size(); i++) {
				FightBeamWeapon fl = (FightBeamWeapon) beamWeapons.elementAt(i);
				// Test
				// System.out.println("Fire weapon ");
				fl.fire();
			}
			for (int i = 0; i < explosions.size(); i++) {
				FightExplosion fe = (FightExplosion) explosions.elementAt(i);
				// Test
				// System.out.println("Explode ");
				fe.fire();
			}
			try {
				Thread.sleep(laserSleepTime);
			} catch (Exception e) {/* always ignore */
			}
			for (int i = 0; i < beamWeapons.size(); i++) {
				FightBeamWeapon fl = (FightBeamWeapon) beamWeapons.elementAt(i);
				fl.unFire();
			}
			for (int i = 0; i < explosions.size(); i++) {
				FightExplosion fe = (FightExplosion) explosions.elementAt(i);
				fe.unFire();
			}
			// Bombs
			for (int i = 0; i < bombs.size(); i++) {
				FightBombWeapon fb = (FightBombWeapon) bombs.elementAt(i);
				if (g != null) {
					// System.out.println("Bomb exploded!");
					fb.fire(); // step 1
					try {
						Thread.sleep(bombStepTime);
					} catch (Exception e) {/* always ignore */
					}
					fb.fire2(); // step 2
					try {
						Thread.sleep(bombStepTime);
					} catch (Exception e) {/* always ignore */
					}
					fb.unFire(); // step 2
				}
				float damage = fb.getWeaponForce();
				damage = damage * (0.5f + (float) Math.random() * 1.0f);
				distributeDamage(fb.getWeapon(), fb, damage, g);
				// applyDamage(g, (Weapon)fb.getWeapon(),
				// (Planet)fb.orderTarget, damage);
			}
			// Ship Explosions
			for (int i = 0; i < shipExplosions.size(); i++) {
				FightShip fs = (FightShip) shipExplosions.elementAt(i);
				if (g != null) {
					fs.explode(g);
					fs.clear(g);
				}
			}
			// Torpedo explosions
			if (torpedoExplosions.size() > 0) {
				for (int i = 0; i < torpedoExplosions.size(); i++) {
					FightTorpedoWeapon ft = (FightTorpedoWeapon) torpedoExplosions.elementAt(i);
					if (g != null) {
						ft.explode(g, 1);
					}
				}
				try {
					Thread.sleep(120);
				} catch (Exception e) {/* always ignore */
				}
				for (int i = 0; i < torpedoExplosions.size(); i++) {
					FightTorpedoWeapon ft = (FightTorpedoWeapon) torpedoExplosions.elementAt(i);
					if (g != null) {
						ft.explode(g, 2);
					}
				}
				try {
					Thread.sleep(120);
				} catch (Exception e) {/* always ignore */
				}
				for (int i = 0; i < torpedoExplosions.size(); i++) {
					FightTorpedoWeapon ft = (FightTorpedoWeapon) torpedoExplosions.elementAt(i);
					if (g != null) {
						ft.explode(g, 3);
					}
				}
			}
			// try { Thread.sleep(bombSleepTime); } catch(Exception e) {/*
			// always ignore */}
			// remove destroyed ships from universe
			for (int i = 0; i < shipExplosions.size(); i++) {
				FightShip fs = (FightShip) shipExplosions.elementAt(i);
				// System.out.println("deleteShipFromUniverse is called for ship
				// " + fs.getShipType());
				parent.deleteShipFromUniverse(sun.getSunSystem(), fs.getShip());
			}
			beamWeapons.removeAllElements();
			explosions.removeAllElements();
			bombs.removeAllElements();
			shipExplosions.removeAllElements();
			torpedoExplosions.removeAllElements();
		}
		return (action);
	}

	// apply damage to ships and stations
	//
	// called by distributeDamage
	public void applyDamage(Graphics g, Weapon w, FightShip fs, float damage) {
		fs.doDamage(damage, w);
		// does ship explode?
		if (fs.isDestroyed()) {
			// XXX: add fs to list of ships to explode and remove, and do this
			// after all fire and explode events have been shown
			shipExplosions.addElement(fs);
			// test
			System.out.println(">> " + fs.getShipType() + " from player " + fs.getPlayerName() + " has been destroyed in system "
					+ sun.getName() + ".");
			boolean b = fighterShips.removeElement(fs);
			// remove target from other ships, colonies and torpedos
			for (int i = 0; i < fighterShips.size(); i++) {
				FightShip f = (FightShip) fighterShips.elementAt(i);
				if (f.getOrderTarget() == fs) {
					f.setOrderTarget(null);
				}
			}
			for (int i = 0; i < fighterColonies.size(); i++) {
				FightColony c = (FightColony) fighterColonies.elementAt(i);
				if (c.getOrderTarget() == fs) {
					c.setOrderTarget(null);
				}
			}
			// System.out.println("applyDamage: Torpedo vector size: " +
			// torpedos.size());
			for (int i = 0; i < torpedos.size(); i++) {
				FightTorpedoWeapon f = (FightTorpedoWeapon) torpedos.elementAt(i);
				if (f.getOrderTarget() == fs) {
					f.setOrderTarget(null);
				}
			}
		}
	}

	// apply damage to colonies
	//
	// called by distributeDamage
	public void applyDamage(Graphics g, Weapon weapon, Planet planet, float damage) {
		// Planetary equipment affects damage
		// (Defense batteries, shields, missile defense, SDI,...)
		// damage = damage*planet.modifiers;

		// this may affect both planet and (if existing) colony
		planet.doDamage(weapon, damage);

	}

	// check distance from torpedo to target, explode if close enough
	//
	// called by executeStep
	public boolean checkDistanceAndExplode(FightTorpedoWeapon ft, Graphics g) {
		float n, distanceX, distanceY, distance;
		float damage = 0.0f;
		int toX = 0, toY = 0;
		int proxy = MOVEPIXELS / 2;
		boolean exploded = false;
		// calculate distance
		if (ft.getOrderTarget() instanceof FightShip) {
			toX = ((FightShip) ft.getOrderTarget()).getX();
			toY = ((FightShip) ft.getOrderTarget()).getY();
		}
		if (ft.getOrderTarget() instanceof Planet) {
			toX = ((Planet) ft.getOrderTarget()).getX();
			toY = ((Planet) ft.getOrderTarget()).getY();
		}
		distanceX = toX - ft.getX();
		distanceY = toY - ft.getY();
		distance = (float) Math.sqrt(distanceX * distanceX + distanceY * distanceY);
		// test
		// System.out.println("Torpedo distance " + d + ", proxy " + proxy);
		// Torpedo explodes:
		if (distance < proxy) {
			exploded = true;
			if (distance > 0) {
				damage = ft.getWeaponForce() - ((float) distance / 2);
				// System.out.println("a) damage " + damage);
			} else {
				damage = ft.getWeaponForce();
				// System.out.println("b) damage " + damage);
			}
			// XXX: add more target computers when defined, tune this?
			// XXX: target computers affect both beam weapons and torpedos in a
			// similar way, better torpedos may have their own tc?
			FightObject fo = ft.getFightObject();
			if (fo.getTargetComputer() == 0) {
				// no target computer
				damage = damage * (float) (Math.random() * 0.5);
			} else if (fo.getTargetComputer() == 1) {
				// target computer of TL 1
				damage = damage * (float) (Math.random() * 1.0);
			} else if (fo.getTargetComputer() == 2) {
				// target computer of TL 2
				damage = damage * (float) (0.5 + Math.random() * 1.0);
			}

			distributeDamage(ft.getWeapon(), ft, damage, g);
			// applyDamage(g, ft.getWeapon(), (Planet)ft.orderTarget, damage);

			// test
			// System.out.println(ft.playerName + "-torpedo exploded, doing
			// damage " + damage);
			// System.out.println("exploded torpedo after contact");
			torpedoExplosions.addElement(ft);
			torpedos.removeElement(ft);
		}
		// Ships: apply damage
		// if (ft.orderTarget instanceof FightShip) {
		// FightShip temp = (FightShip)ft.orderTarget;
		// distributeDamage(ft.getWeapon(),ft,damage,g);
		// //applyDamage(g, (Weapon)ft.getWeapon(), temp, damage);
		// // XXX: Apply the damage in the surrounding area
		// // ...
		// temp=null;
		// }

		return (exploded);
	}

	// check distance from ship to target, fire if possible
	//
	// XXX: laser-, torpedo- and bomb-like weapons are implemented,
	// what about other types? (field/area weapons...)
	//
	// called by executeStep
	protected boolean checkDistanceAndFire(FightShip fs, int battleTurn, Graphics g) {
		float damage = 0.0f;
		int toX = 0, toY = 0;
		float distance, n, distanceX, distanceY;
		boolean continueFire = false;
		// calculateDistance
		if (fs.getOrderTarget() instanceof FightShip) {
			toX = ((FightShip) fs.getOrderTarget()).getX();
			toY = ((FightShip) fs.getOrderTarget()).getY();
		}
		if (fs.getOrderTarget() instanceof Planet) {
			toX = ((Planet) fs.getOrderTarget()).getX() + 4;
			toY = ((Planet) fs.getOrderTarget()).getY() + 4;
		}
		distanceX = toX - fs.getX();
		distanceY = toY - fs.getY();
		distance = (float) Math.sqrt(distanceX * distanceX + distanceY * distanceY);
		// fire a weapon
		Vector weapons = fs.getShipWeapons();

		// >>>> Beam weapon loop
		// XXX: change to 'instanceof BeamWeapon' and make BeamWeapon parent of
		// Laser and such...
		// XXX: or weapon instanceof LaserCannon or weapon instanceof
		// beamweapon3 ....
		// XXX: With this change, all (different) beamweapons are treated like
		// the first, i.e. Megazork blue laser is shown as simple red laser
		boolean firstLaserOfShip = true; // only the first of a kind is
											// displayed
		boolean firstBundledLaserOfShip = true;
		boolean firstLaserCannonOfShip = true;
		for (int k = 0; k < weapons.size(); k++) {
			float singleDamage = 0;
			Weapon weapon = (Weapon) weapons.elementAt(k);
			Weapon currentWeapon = null;
			if (distance < weapon.getRange()) {
				if (fs.getOrderTarget() instanceof FightShip) {
					if (weapon instanceof BeamWeapon) {
						singleDamage = weapon.getForce() * (float) weapon.getRange() / distance;
						// XXX: add more target computers when defined, tune this?
						if (fs.getTargetComputer() == 0) {
							// no target computer
							singleDamage = singleDamage * (float) (Math.random() * 0.5);
						} else if (fs.getTargetComputer() == 1) {
							// target computer of TL 1
							singleDamage = singleDamage * (float) (Math.random() * 1.0);
						} else if (fs.getTargetComputer() == 2) {
							// target computer of TL 2
							singleDamage = singleDamage * (float) (0.5 + Math.random() * 1.0);
						}

						currentWeapon = weapon;
						if (weapon instanceof Laser) {
							if ((firstLaserOfShip) && (g != null)) {
								// System.out.println(fs.getPlayerName() +
								// " fired Laser with singleDamage " +
								// singleDamage);
								firstLaserOfShip = false;
								beamWeapons.addElement(new FightBeamWeapon(weapon, g, fs.getX(), fs.getY(), toX, toY));
								// show explosion at target of laser
								if ((fs.getX() - toX) > 0) {
									toX += 2;
								} else {
									toX -= 2;
								}
								if ((fs.getY() - toY) > 0) {
									toY += 2;
								} else {
									toY -= 2;
								}
								explosions.addElement(new FightExplosion(g, toX, toY));
							}
							continueFire = true;
						}
						if (weapon instanceof BundledLaser) {
							if ((firstBundledLaserOfShip) && (g != null)) {
								// System.out.println(fs.getPlayerName() +
								// " fired BundledLaser with singleDamage " +
								// singleDamage);
								firstBundledLaserOfShip = false;
								beamWeapons.addElement(new FightBeamWeapon(weapon, g, fs.getX(), fs.getY(), toX, toY));
								// show explosion at target of laser
								if ((fs.getX() - toX) > 0) {
									toX += 2;
								} else {
									toX -= 2;
								}
								if ((fs.getY() - toY) > 0) {
									toY += 2;
								} else {
									toY -= 2;
								}
								explosions.addElement(new FightExplosion(g, toX, toY));
							}
							continueFire = true;
						}
						if (weapon instanceof LaserCannon) {
							if ((firstLaserCannonOfShip) && (g != null)) {
								// System.out.println(fs.getPlayerName() +
								// " fired LaserCannon with singleDamage " +
								// singleDamage);
								firstLaserCannonOfShip = false;
								beamWeapons.addElement(new FightBeamWeapon(weapon, g, fs.getX(), fs.getY(), toX, toY));
								// show explosion at target of lasercannon
								if ((fs.getX() - toX) > 0) {
									toX += 2;
								} else {
									toX -= 2;
								}
								if ((fs.getY() - toY) > 0) {
									toY += 2;
								} else {
									toY -= 2;
								}
								explosions.addElement(new FightExplosion(g, toX, toY));
							}
							continueFire = true;
						}
						// XXX: Pellet Gun and Rail Gun are not exactly beam weapons 8-)
						if ((weapon instanceof PelletGun) || (weapon instanceof RailGun)) {
							// System.out.println(fs.getPlayerName() +
							// " fired PelletGun or RailGun with singleDamage "
							// +
							// singleDamage);
							beamWeapons.addElement(new FightBeamWeapon(weapon, g, fs.getX(), fs.getY(), toX, toY));
							// show explosion at target of lasercannon
							if ((fs.getX() - toX) > 0) {
								toX += 2;
							} else {
								toX -= 2;
							}
							if ((fs.getY() - toY) > 0) {
								toY += 2;
							} else {
								toY -= 2;
							}
							explosions.addElement(new FightExplosion(g, toX, toY));
							continueFire = true;
						}
					}
				}
				damage += singleDamage;
			}
			// System.out.println("Ship from " + fs.getPlayerName() +
			// " fired Beam weapon with total damage " + damage);
			distributeDamage(currentWeapon, fs, damage, g);
		}

		// >>>>>>> Torpedo Firing Loop (explosion and distributeDamage comes
		// later...)
		for (int k = 0; k < weapons.size(); k++) {
			Weapon weapon = (Weapon) weapons.elementAt(k);
			Weapon currentWeapon = null;
			if (weapon instanceof Torpedo) {
				// todo: or weapon instance of torpedo2 or weapon
				// instanceof....?
				damage = 0.0f;
				// if (battleTurn == fs.getBattleSpeed()) { // fire only in
				// first round??
				// TEST
				// System.out.println("distance: " + distance);
				// System.out.println("weapon.getRange(): " +
				// weapon.getRange());
				if (distance < weapon.getRange()) {
					// XXX: don't use all the torpedos for weak targets
					if ((weapon.getAmmo() > 0) && (battleTurn == 1)) {
						// Test
						// System.out.println("Battleturn " + battleTurn + ",
						// fired a torpedo");
						// System.out.println("Target is " +
						// fs.orderTarget.getClass());
						currentWeapon = weapon;
						weapon.useAmmo(1);
						FightTorpedoWeapon ft = new FightTorpedoWeapon(fs.getOrderTarget(), fs, weapon, fs.getX(), fs.getY());
						ft.playerName = fs.getPlayerName(); // XXX: ???
						torpedos.addElement(ft);
						// Test
						// System.out.println("Ship from " + fs.getPlayerName()
						// + " fired torpedo");
						continueFire = true;
					}
				}
			}
			// distributeDamage(currentWeapon, fs, damage, g);
		}

		// >>>>>>> Bomb Loop
		for (int k = 0; k < weapons.size(); k++) {
			Weapon weapon = (Weapon) weapons.elementAt(k);
			Weapon currentWeapon = null;
			if (weapon instanceof Bomb) {
				// todo: or weapon instanceof bomb2 or weapon instanceof bomb3
				// ...
				damage = 0.0f;
				if (fs.getOrderTarget() instanceof Planet) {
					if (weapon.getAmmo() > 0) {
						if (distance < weapon.getRange()) {
							currentWeapon = weapon;
							weapon.useAmmo(1);
							if (g != null) {
								bombs.addElement(new FightBombWeapon(weapon, g, fs.getOrderTarget(), fs.getX(), fs.getY(), toX, toY));
							}
							distributeDamage(currentWeapon, fs, damage, g);
						}
						continueFire = true;
					}
				}
			}

		}
		return (continueFire);
	}

	// check distance from colony to enemy ship, fire if possible
	//
	// called by executeStep
	protected void checkDistanceAndFire(FightColony fc, Graphics g) {
		float damage = 0.0f;
		int toX = 0, toY = 0;
		float distance, n, distanceX, distanceY;
		Vector weapons = fc.getWeapons();
		// calculateDistance
		for (int i = 0; i < fighterShips.size(); i++) {
			FightShip fs = (FightShip) fighterShips.elementAt(i);
			if (!fs.getPlayer().equals(fc.getPlayer())) {
				toX = fs.getX();
				toY = fs.getY();
				distanceX = toX - fc.getX();
				distanceY = toY - fc.getY();
				distance = (float) Math.sqrt(distanceX * distanceX + distanceY * distanceY);

				// fire a weapon
				for (int k = 0; k < weapons.size(); k++) {
					Weapon weapon = (Weapon) weapons.elementAt(k);
					if (distance < weapon.getRange()) {
						if (weapon instanceof Torpedo) {
							// damage = weapon.force * (0.5f +
							// (float)Math.random()*1.0f) ;
							// fire at one target per ship per turn
							if (weapon.getAmmo() > 0) {
								weapon.useAmmo(1);
								FightTorpedoWeapon ft = new FightTorpedoWeapon(fs, fc, weapon, fc.getX(), fc.getY());
								// System.out.println("Colony: Added torpedo");
								torpedos.addElement(ft);
							}
						}
					}
				}
			}
		}
	}

	// check life cycles of torpedoes. explode/remove them when life cycles are
	// used up
	//
	// called by executeStep
	public boolean checkLifeCyclesAndExplode(FightTorpedoWeapon ft, Graphics g) {
		boolean exploded = false;
		ft.useLifeCycle();
		// System.out.println(ft.playerName + "-torpedo life cycles: " +
		// ft.getLifeCycles());
		if (ft.getLifeCycles() < 0) {
			// Test
			// System.out.println("removed " + ft.playerName + "-torpedo after
			// expiring");
			// V 0.5.070: don't explode expired torpedos anymore. It's not
			// really logical and it may be quite slow
			// torpedoExplosions.addElement(ft);
			torpedos.removeElement(ft);
			exploded = true;
		}
		return (exploded);
	}

	// distribute damage calls the appropriate applydamage methods
	//
	// called by checkDistanceAndExplode, checkDistanceAndFire and executeStep
	protected void distributeDamage(Weapon currentWeapon, FightObject fo, float damage, Graphics g) {
		if (currentWeapon != null) {
			// Test
			// System.out.println("Damage " + damage + " done to " +
			// fo.orderTarget.getClass() + " by " + currentWeapon.name);
			if (g != null) {
				fo.display(g);
			}
			// Ships: apply damage
			if (fo.getOrderTarget() instanceof FightShip) {
				FightShip fsTemp = (FightShip) fo.getOrderTarget();
				applyDamage(g, currentWeapon, fsTemp, damage);
				fsTemp = null;
			}

			// Planets: apply damage
			if (fo.getOrderTarget() instanceof Planet) {
				Planet planet = (Planet) fo.getOrderTarget();
				applyDamage(g, currentWeapon, planet, damage);
			}
		}
	}

	// move ships and torpedos
	//
	// called from executeStep
	protected boolean moveObject(FightObject fo, Graphics g) {
		float stepX, stepY, n, distanceX, distanceY, distance;
		int toX = 0, toY = 0;
		int proxy = 0;
		boolean move = false;
		// calculate distance
		if (fo.getOrderTarget() instanceof FightShip) {
			toX = ((FightShip) fo.getOrderTarget()).getX();
			toY = ((FightShip) fo.getOrderTarget()).getY();
			fo.setOldx(toX);
			fo.setOldy(toY);
		}
		if (fo.getOrderTarget() instanceof Planet) {
			toX = ((Planet) fo.getOrderTarget()).getX() + 5;
			toY = ((Planet) fo.getOrderTarget()).getY() + 5;
			fo.setOldx(toX);
			fo.setOldy(toY);
		}
		// XXX: the same for stations and ...
		// ...

		if (toX == 0) {
			if (fo instanceof FightTorpedoWeapon) {
				// Torpedos go on!
				toX = fo.getOldx();
				toY = fo.getOldy();
			}
		}
		if (toX != 0) {
			distanceX = toX - fo.getX();
			distanceY = toY - fo.getY();
			distance = (float) Math.sqrt(distanceX * distanceX + distanceY * distanceY);
			if (g != null) {
				fo.clear(g);
			}
			// move not closer than
			if (fo instanceof FightShip) {
				((FightShip) fo).setSelected(false);
				proxy = MOVEPIXELS + 8;
			}
			if (fo instanceof FightTorpedoWeapon) {
				proxy = MOVEPIXELS / 2;
			}
			if (distance > proxy) {
				move = true;
				n = distance / (float) MOVEPIXELS;
				stepX = distanceX / n;
				stepY = distanceY / n;
				fo.setX(fo.getX() + (int) stepX);
				fo.setY(fo.getY() + (int) stepY);
				if (g != null) {
					fo.display(g);
				}
			}
		}
		return (move);
	}

	// method called from constructor
	private void init() {
		planets = sun.getPlanets();
		fighterShips = new Vector();
		fighterColonies = new Vector();
		torpedos = new Vector();
		beamWeapons = new Vector();
		bombs = new Vector();
		explosions = new Vector();
		shipExplosions = new Vector();
		torpedoExplosions = new Vector();
	}

	// method called from constructor
	private void addShips() {
		Vector ships = sun.getSpaceCraft();
		Vector battlePlayers = new Vector();
		// int defendingShips=0;
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft ship = (SpaceCraft) ships.elementAt(i);
			// todo: the defender is not necessarily the first, better would be
			// the one who has a colony in this system or the one who was first
			// in this system
			// or the one who does not attack. 8-)
			if (!battlePlayers.contains(ship.getPlayer())) {
				battlePlayers.addElement(ship.getPlayer());
			}
		}
		int startingPositionX[] = new int[battlePlayers.size()];
		int startingPositionY[] = new int[battlePlayers.size()];
		for (int i = 0; i < battlePlayers.size(); i++) {
			// test
			// System.out.println("Player no " + i + " is: " +
			// ((Player)battlePlayers.elementAt(i)).getName());
			switch (i) {
			case 0:
				// left side top
				startingPositionX[i] = (int) (50 + Math.random() * 50);
				startingPositionY[i] = (int) (30 + Math.random() * 200);
				break;
			case 1:
				// right side top
				startingPositionX[i] = (int) (550 + Math.random() * 50);
				startingPositionY[i] = (int) (30 + Math.random() * 200);
				break;
			case 2:
				// left side bottom
				startingPositionX[i] = (int) (50 + Math.random() * 50);
				startingPositionY[i] = (int) (230 + Math.random() * 200);
				break;
			case 3:
				// right side bottom
				startingPositionX[i] = (int) (550 + Math.random() * 50);
				startingPositionY[i] = (int) (230 + Math.random() * 200);
				break;
			case 4:
				// top center
				startingPositionX[i] = (int) (300 + Math.random() * 100);
				startingPositionY[i] = (int) (400 + Math.random() * 40);
				break;
			case 5:
				// bottom center
				startingPositionX[i] = (int) (300 + Math.random() * 100);
				startingPositionY[i] = (int) (400 + Math.random() * 40);
				break;
			default:
				// anywhere
				startingPositionX[i] = (int) (50 + Math.random() * 550);
				startingPositionY[i] = (int) (30 + Math.random() * 400);
			}

		}
		for (int i = 0; i < ships.size(); i++) {
			SpaceCraft ship = (SpaceCraft) ships.elementAt(i);
			int posNr = battlePlayers.indexOf(ship.getPlayer());
			if (ship instanceof Ship) {
				// XXX: difference between scout and colony ship does not
				// really matter here.
				// Better use ship size/weight
				// XXX: position ships nearby colonized planet(?)
				if (ship.getShipDesign().getColonyModuleNumber() > 0) {
					FightShipColony fs = new FightShipColony((Ship) ship, startingPositionX[posNr], startingPositionY[posNr]);
					fighterShips.addElement(fs);
				} else {
					FightShipScout fs = new FightShipScout((Ship) ship, startingPositionX[posNr], startingPositionY[posNr]);
					fighterShips.addElement(fs);
				}
			} else {
				// Satellite
				int radius = ship.getPlanet().getOrbit().getRadius();
				int position = ship.getPlanet().getOrbit().getPosition();
				int x = FIGHTFRAMEX / 2;
				int y = FIGHTFRAMEY / 2;
				int px = x + STARSIZE / 2 + (int) (2 * radius * Math.cos(position));
				int py = y + STARSIZE / 2 + (int) (2 * radius * Math.sin(position));
				FightSatellite fsa = new FightSatellite((Satellite) ship, px - 8, py + 6);
				fighterShips.addElement(fsa);
			}
		}

	}

	// method called from constructor
	private void addColonies() {
		for (int i = 0; i < planets.size(); i++) {
			Planet planet = (Planet) planets.elementAt(i);
			if (planet.isColonized()) {
				Colony colony = planet.getColony();
				int x = FIGHTFRAMEX / 2;
				int y = FIGHTFRAMEY / 2;
				Orbit orbit = planet.getOrbit();
				int radius = orbit.getRadius();
				int position = orbit.getPosition();
				int px = x + STARSIZE / 2 + (int) (2 * radius * Math.cos(position));
				int py = y + STARSIZE / 2 + (int) (2 * radius * Math.sin(position));
				FightColony fc = new FightColony(colony, px, py);
				fighterColonies.addElement(fc);
			}
		}
	}

}