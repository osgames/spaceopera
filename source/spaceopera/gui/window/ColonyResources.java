//The class ColonyResources displays the resources of a colony in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.Colony;

public class ColonyResources extends SOFrame implements ActionListener, SOConstants {

  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Colony colony = null;
  private Panel pNorth, pCenter, pTitle, pGridLeft, pGrid, pSouth;
  private Label label1, label2, label3, label4, label5;
  private Label resourceName[] = new Label[R_MAX];
  private Label resourceStored[] = new Label[R_MAX];
  private Label resourceProduced[] = new Label[R_MAX];
  private Label resourceUsed[] = new Label[R_MAX];
  private Label resourceDemand[] = new Label[R_MAX];
  private Button next, previous, close;


  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals("previous")) {
    	spaceOpera.displayPreviousColony();  
        spaceOpera.displayColonyResources();
    }
    if (c.equals("next")) {
    	spaceOpera.displayNextColony();
        spaceOpera.displayColonyResources();
    }
    if (c.equals(CLOSE)) {
        this.dispose();
    }
  }



  public ColonyResources (Colony c, SpaceOpera so) {
      spaceOpera = so;
      colony = c;
      setTitle("Colony resources for colony \"" + colony.getName() + "\"");
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();  
      pack();
      setSize(COLONYRESOURCESIZEX,COLONYRESOURCESIZEY);
      //setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      setLocation((dim.width - getWidth()),(dim.height - getHeight()) - TASKBAR_HEIGHT);
      init();
      show();
  }

  private void doStaticLayout() {
      setLayout(new BorderLayout(2,2));
      pCenter = new Panel();
      pCenter.setBackground(Color.black);
      pCenter.setForeground(Color.white);
      pCenter.setLayout(new BorderLayout(1,2));
      pCenter.setFont(new Font("Monospaced", Font.PLAIN, 12));
      pTitle = new Panel();
      pTitle.setLayout(new GridLayout(1,5));
      pTitle.setBackground(Color.white);
      pTitle.setForeground(Color.black);
      pTitle.setFont(new Font("Monospaced", Font.BOLD, 12));
      label1 = new Label("Resource");
      label2 = new Label("stored");
      label2.setAlignment(Label.CENTER);
      label3 = new Label("produced");
      label3.setAlignment(Label.CENTER);
      label4 = new Label("used");
      label4.setAlignment(Label.CENTER);
      label5 = new Label("demand");
      label5.setAlignment(Label.CENTER);
      pTitle.add(label1);
      pTitle.add(label2);
      pTitle.add(label3);
      pTitle.add(label4);
      pTitle.add(label5);
      pGridLeft = new Panel();
      pGridLeft.setLayout(new GridLayout(R_MAX,1));
      pGridLeft.setBackground(Color.white);
      pGridLeft.setForeground(Color.black);
      pGridLeft.setFont(new Font("Monospaced", Font.BOLD, 12));
      pGrid = new Panel();
      pGrid.setLayout(new GridLayout(R_MAX,4));
      pGrid.setBackground(Color.white);
      pGrid.setForeground(Color.black);
      pGrid.setFont(new Font("Monospaced", Font.PLAIN, 12));

      // Work
      resourceName[R_WORKUNITS] = new Label("Total work:");
      resourceStored[R_WORKUNITS] = new Label("0 units");
      resourceProduced[R_WORKUNITS] = new Label("10 units");
      resourceUsed[R_WORKUNITS] = new Label("10 units");
      resourceDemand[R_WORKUNITS] = new Label("none");

      resourceName[R_WORKUNITS_CONSTRUCTION] = new Label("Construction:");
      resourceStored[R_WORKUNITS_CONSTRUCTION] = new Label("0 units");
      resourceProduced[R_WORKUNITS_CONSTRUCTION] = new Label("10 units");
      resourceUsed[R_WORKUNITS_CONSTRUCTION] = new Label("10 units");
      resourceDemand[R_WORKUNITS_CONSTRUCTION] = new Label("none");

      resourceName[R_WORKUNITS_OPERATION] = new Label("Operation");
      resourceStored[R_WORKUNITS_OPERATION] = new Label("0 units");
      resourceProduced[R_WORKUNITS_OPERATION] = new Label("10 units");
      resourceUsed[R_WORKUNITS_OPERATION] = new Label("10 units");
      resourceDemand[R_WORKUNITS_OPERATION] = new Label("none");

      resourceName[R_WORKUNITS_MILITARY] = new Label("Military:");
      resourceStored[R_WORKUNITS_MILITARY] = new Label("0 units");
      resourceProduced[R_WORKUNITS_MILITARY] = new Label("10 units");
      resourceUsed[R_WORKUNITS_MILITARY] = new Label("10 units");
      resourceDemand[R_WORKUNITS_MILITARY] = new Label("none");

      resourceName[R_WORKUNITS_EDUCATION] = new Label("Education:");
      resourceStored[R_WORKUNITS_EDUCATION] = new Label("0 units");
      resourceProduced[R_WORKUNITS_EDUCATION] = new Label("10 units");
      resourceUsed[R_WORKUNITS_EDUCATION] = new Label("10 units");
      resourceDemand[R_WORKUNITS_EDUCATION] = new Label("none");

      resourceName[R_WORKUNITS_ADMINISTRATION] = new Label("Administration:");
      resourceStored[R_WORKUNITS_ADMINISTRATION] = new Label("0 units");
      resourceProduced[R_WORKUNITS_ADMINISTRATION] = new Label("10 units");
      resourceUsed[R_WORKUNITS_ADMINISTRATION] = new Label("10 units");
      resourceDemand[R_WORKUNITS_ADMINISTRATION] = new Label("none");      
      
      resourceName[R_END_WORKUNITS] = new Label(""); //("* Electricity:");
      resourceStored[R_END_WORKUNITS] = new Label("");
      resourceProduced[R_END_WORKUNITS] = new Label("");
      resourceUsed[R_END_WORKUNITS] = new Label("");
      resourceDemand[R_END_WORKUNITS] = new Label("");

      resourceName[R_ELECTRICITY] = new Label(spaceOpera.getResourceName(R_ELECTRICITY)+ ":   ");
      resourceStored[R_ELECTRICITY] = new Label("0 kW");
      resourceProduced[R_ELECTRICITY] = new Label("180 kW");
      resourceUsed[R_ELECTRICITY] = new Label("100 kW");
      resourceDemand[R_ELECTRICITY] = new Label("none");

      resourceName[R_RAWMATERIAL] = new Label(""); //("* Raw material:");
      resourceStored[R_RAWMATERIAL] = new Label("");
      resourceProduced[R_RAWMATERIAL] = new Label("");
      resourceUsed[R_RAWMATERIAL] = new Label("");
      resourceDemand[R_RAWMATERIAL] = new Label("");

      // Stone
      resourceName[R_STONE] = new Label(spaceOpera.getResourceName(R_STONE)+ ":   ");
      resourceStored[R_STONE] = new Label("10 tons");
      resourceProduced[R_STONE] = new Label("13 tons");
      resourceUsed[R_STONE] = new Label("8 tons");
      resourceDemand[R_STONE] = new Label("none");

      // Wood
      resourceName[R_WOOD] = new Label(spaceOpera.getResourceName(R_WOOD)+ ":   ");
      resourceStored[R_WOOD] = new Label("50 tons");
      resourceProduced[R_WOOD] = new Label("18 tons");
      resourceUsed[R_WOOD] = new Label("2 tons");
      resourceDemand[R_WOOD] = new Label("none");

      // Coal, Oil, Gas
      resourceName[R_COAL] = new Label(spaceOpera.getResourceName(R_COAL)+ ":   ");
      resourceStored[R_COAL] = new Label("100 tons");
      resourceProduced[R_COAL] = new Label("80 tons");
      resourceUsed[R_COAL] = new Label("40 tons");
      resourceDemand[R_COAL] = new Label("none");

      resourceName[R_OIL] = new Label(spaceOpera.getResourceName(R_OIL)+ ":   ");
      resourceStored[R_OIL] = new Label("25 barrels");
      resourceProduced[R_OIL] = new Label("10 barrels");
      resourceUsed[R_OIL] = new Label("3 barrels");
      resourceDemand[R_OIL] = new Label("none");

      // Metals
      resourceName[R_IRON] = new Label(spaceOpera.getResourceName(R_IRON)+ ":   ");
      resourceStored[R_IRON] = new Label("200 tons");
      resourceProduced[R_IRON] = new Label("100 tons");
      resourceUsed[R_IRON] = new Label("80 tons");
      resourceDemand[R_IRON] = new Label("none");

      resourceName[R_COPPER] = new Label(spaceOpera.getResourceName(R_COPPER)+ ":   ");
      resourceStored[R_COPPER] = new Label("100 tons");
      resourceProduced[R_COPPER] = new Label("80 tons");
      resourceUsed[R_COPPER] = new Label("40 tons");
      resourceDemand[R_COPPER] = new Label("none");

      resourceName[R_ALUMINUM] = new Label(spaceOpera.getResourceName(R_ALUMINUM)+ ":   ");
      resourceStored[R_ALUMINUM] = new Label("100 tons");
      resourceProduced[R_ALUMINUM] = new Label("80 tons");
      resourceUsed[R_ALUMINUM] = new Label("40 tons");
      resourceDemand[R_ALUMINUM] = new Label("none");

      resourceName[R_SILICIUM] = new Label(spaceOpera.getResourceName(R_SILICIUM)+ ":   ");
      resourceStored[R_SILICIUM] = new Label("10 tons");
      resourceProduced[R_SILICIUM] = new Label("13 tons");
      resourceUsed[R_SILICIUM] = new Label("8 tons");
      resourceDemand[R_SILICIUM] = new Label("none");

      resourceName[R_SILVER] = new Label(spaceOpera.getResourceName(R_SILVER)+ ":   ");
      resourceStored[R_SILVER] = new Label("100 tons");
      resourceProduced[R_SILVER] = new Label("20 tons");
      resourceUsed[R_SILVER] = new Label("0 tons");
      resourceDemand[R_SILVER] = new Label("none");

      resourceName[R_GOLD] = new Label(spaceOpera.getResourceName(R_GOLD)+ ":   ");
      resourceStored[R_GOLD] = new Label("0 tons");
      resourceProduced[R_GOLD] = new Label("0 tons");
      resourceUsed[R_GOLD] = new Label("0 tons");
      resourceDemand[R_GOLD] = new Label("none");

      resourceName[R_URANIUM] = new Label(spaceOpera.getResourceName(R_URANIUM)+ ":   ");
      resourceStored[R_URANIUM] = new Label("5 tons");
      resourceProduced[R_URANIUM] = new Label("2 tons");
      resourceUsed[R_URANIUM] = new Label("2 tons");
      resourceDemand[R_URANIUM] = new Label("none");

      // Food
      resourceName[R_FOOD] = new Label(""); // ("* Food:");
      resourceStored[R_FOOD] = new Label("");
      resourceProduced[R_FOOD] = new Label("");
      resourceUsed[R_FOOD] = new Label("");
      resourceDemand[R_FOOD] = new Label("");

      resourceName[R_MEAT] = new Label(spaceOpera.getResourceName(R_MEAT)+ ":   ");
      resourceStored[R_MEAT] = new Label("30 tons");
      resourceProduced[R_MEAT] = new Label("20 tons");
      resourceUsed[R_MEAT] = new Label("15 tons");
      resourceDemand[R_MEAT] = new Label("none");

      resourceName[R_FISH] = new Label(spaceOpera.getResourceName(R_FISH)+ ":   ");
      resourceStored[R_FISH] = new Label("10 tons");
      resourceProduced[R_FISH] = new Label("5 tons");
      resourceUsed[R_FISH] = new Label("4 tons");
      resourceDemand[R_FISH] = new Label("none");

      resourceName[R_RICE] = new Label(spaceOpera.getResourceName(R_RICE)+ ":   ");
      resourceStored[R_RICE] = new Label("10 tons");
      resourceProduced[R_RICE] = new Label("8 tons");
      resourceUsed[R_RICE] = new Label("6 tons");
      resourceDemand[R_RICE] = new Label("none");

      resourceName[R_CROP] = new Label(spaceOpera.getResourceName(R_CROP)+ ":   ");
      resourceStored[R_CROP] = new Label("10 tons");
      resourceProduced[R_CROP] = new Label("20 tons");
      resourceUsed[R_CROP] = new Label("18 tons");
      resourceDemand[R_CROP] = new Label("none");

      resourceName[R_DAIRY] = new Label(spaceOpera.getResourceName(R_DAIRY)+ ":   ");
      resourceStored[R_DAIRY] = new Label("10 tons");
      resourceProduced[R_DAIRY] = new Label("20 tons");
      resourceUsed[R_DAIRY] = new Label("15 tons");
      resourceDemand[R_DAIRY] = new Label("none");

      resourceName[R_VEGETABLES] = new Label(spaceOpera.getResourceName(R_VEGETABLES)+ ":   ");
      resourceStored[R_VEGETABLES] = new Label("6 tons");
      resourceProduced[R_VEGETABLES] = new Label("26 tons");
      resourceUsed[R_VEGETABLES] = new Label("24 tons");
      resourceDemand[R_VEGETABLES] = new Label("none");

      resourceName[R_FRESHWATER] = new Label(spaceOpera.getResourceName(R_FRESHWATER)+ ":   ");
      resourceStored[R_FRESHWATER] = new Label("100 barrels");
      resourceProduced[R_FRESHWATER] = new Label("500 barrels");
      resourceUsed[R_FRESHWATER] = new Label("450 barrels");
      resourceDemand[R_FRESHWATER] = new Label("none");

      resourceName[R_WINE] = new Label(spaceOpera.getResourceName(R_WINE)+ ":   ");
      resourceStored[R_WINE] = new Label("100 barrels");
      resourceProduced[R_WINE] = new Label("100 barrels");
      resourceUsed[R_WINE] = new Label("50 barrels");
      resourceDemand[R_WINE] = new Label("none");

      resourceName[R_SALT] = new Label(spaceOpera.getResourceName(R_SALT)+ ":   ");
      resourceStored[R_SALT] = new Label("10 tons");
      resourceProduced[R_SALT] = new Label("8 tons");
      resourceUsed[R_SALT] = new Label("4 tons");
      resourceDemand[R_SALT] = new Label("none");

      // Processed Goods
      resourceName[R_PROCESSED] = new Label(""); //("* Processed goods:");
      resourceStored[R_PROCESSED] = new Label("");
      resourceProduced[R_PROCESSED] = new Label("");
      resourceUsed[R_PROCESSED] = new Label("");
      resourceDemand[R_PROCESSED] = new Label("");

      resourceName[R_GOODS] = new Label(spaceOpera.getResourceName(R_GOODS)+ ":   ");
      resourceStored[R_GOODS] = new Label("10 tons");
      resourceProduced[R_GOODS] = new Label("8 tons");
      resourceUsed[R_GOODS] = new Label("4 tons");
      resourceDemand[R_GOODS] = new Label("none");

      resourceName[R_STEEL] = new Label(spaceOpera.getResourceName(R_STEEL)+ ":   ");
      resourceStored[R_STEEL] = new Label("1 tons");
      resourceProduced[R_STEEL] = new Label("1 tons");
      resourceUsed[R_STEEL] = new Label("1 tons");
      resourceDemand[R_STEEL] = new Label("none");

      resourceName[R_ALLOY] = new Label(spaceOpera.getResourceName(R_ALLOY)+ ":   ");
      resourceStored[R_ALLOY] = new Label("10 tons");
      resourceProduced[R_ALLOY] = new Label("13 tons");
      resourceUsed[R_ALLOY] = new Label("8 tons");
      resourceDemand[R_ALLOY] = new Label("none");

      resourceName[R_TOOLS] = new Label(spaceOpera.getResourceName(R_TOOLS)+ ":   ");
      resourceStored[R_TOOLS] = new Label("0 tons");
      resourceProduced[R_TOOLS] = new Label("1 tons");
      resourceUsed[R_TOOLS] = new Label("1 tons");
      resourceDemand[R_TOOLS] = new Label("none");

      resourceName[R_MACHINES] = new Label(spaceOpera.getResourceName(R_MACHINES)+ ":   ");
      resourceStored[R_MACHINES] = new Label("0 tons");
      resourceProduced[R_MACHINES] = new Label("0 tons");
      resourceUsed[R_MACHINES] = new Label("0 tons");
      resourceDemand[R_MACHINES] = new Label("none");

      resourceName[R_ELECTRONICS] = new Label(spaceOpera.getResourceName(R_ELECTRONICS)+ ":   ");
      resourceStored[R_ELECTRONICS] = new Label("10 tons");
      resourceProduced[R_ELECTRONICS] = new Label("13 tons");
      resourceUsed[R_ELECTRONICS] = new Label("8 tons");
      resourceDemand[R_ELECTRONICS] = new Label("none");

      resourceName[R_GLASS] = new Label(spaceOpera.getResourceName(R_GLASS)+ ":   ");
      resourceStored[R_GLASS] = new Label("10 tons");
      resourceProduced[R_GLASS] = new Label("13 tons");
      resourceUsed[R_GLASS] = new Label("8 tons");
      resourceDemand[R_GLASS] = new Label("none");

      resourceName[R_CHEMICALS] = new Label(spaceOpera.getResourceName(R_CHEMICALS)+ ":   ");
      resourceStored[R_CHEMICALS] = new Label("10 tons");
      resourceProduced[R_CHEMICALS] = new Label("13 tons");
      resourceUsed[R_CHEMICALS] = new Label("8 tons");
      resourceDemand[R_CHEMICALS] = new Label("none");

      resourceName[R_DRUGS] = new Label(spaceOpera.getResourceName(R_DRUGS)+ ":   ");
      resourceStored[R_DRUGS] = new Label("10 tons");
      resourceProduced[R_DRUGS] = new Label("13 tons");
      resourceUsed[R_DRUGS] = new Label("8 tons");
      resourceDemand[R_DRUGS] = new Label("none");

      resourceName[R_PLASTIC] = new Label(spaceOpera.getResourceName(R_PLASTIC)+ ":   ");
      resourceStored[R_PLASTIC] = new Label("10 tons");
      resourceProduced[R_PLASTIC] = new Label("13 tons");
      resourceUsed[R_PLASTIC] = new Label("8 tons");
      resourceDemand[R_PLASTIC] = new Label("none");

      resourceName[R_GARMENTS] = new Label(spaceOpera.getResourceName(R_GARMENTS)+ ":   ");
      resourceStored[R_GARMENTS] = new Label("10 tons");
      resourceProduced[R_GARMENTS] = new Label("13 tons");
      resourceUsed[R_GARMENTS] = new Label("8 tons");
      resourceDemand[R_GARMENTS] = new Label("none");

      resourceName[R_WASTE] = new Label(spaceOpera.getResourceName(R_WASTE)+ ":   ");
      resourceStored[R_WASTE] = new Label("10 tons");
      resourceProduced[R_WASTE] = new Label("8 tons");
      resourceUsed[R_WASTE] = new Label("4 tons");
      resourceDemand[R_WASTE] = new Label("");

      for (int i=R_MIN; i<R_MAX; i++) {
        resourceStored[i].setAlignment(Label.RIGHT);
        resourceProduced[i].setAlignment(Label.RIGHT);
        resourceUsed[i].setAlignment(Label.RIGHT);
        pGridLeft.add(resourceName[i]);        
        pGrid.add(resourceStored[i]);
        pGrid.add(resourceProduced[i]);
        pGrid.add(resourceUsed[i]);
        pGrid.add(resourceDemand[i]);
      }
      pCenter.add("North",pTitle);
      pCenter.add("West", pGridLeft);
      pCenter.add("Center", pGrid);
      add("Center", pCenter);

      // South
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,4));
      pSouth.setBackground(Color.black);
      pSouth.setForeground(Color.white);
      next = new Button("Next Colony");
      next.addActionListener(this);
      next.setActionCommand("next");
      next.setForeground(Color.black);
      next.setBackground(Color.lightGray);
      previous = new Button("Previous Colony");
      previous.addActionListener(this);
      previous.setActionCommand("previous");
      previous.setForeground(Color.black);
      previous.setBackground(Color.lightGray);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      pSouth.add(previous);
      pSouth.add(next);
      pSouth.add(new Label(""));
      pSouth.add(close);
      add("South",pSouth);
  }


  public void init() {
     DecimalFormat myFormatter = new DecimalFormat("###,###,##0.00");
     
     for (int i=R_MIN; i<R_MAX; i++) {
        String unit = spaceOpera.getResourceUnit(i);  
        double count;       
 
        if ((i != R_RAWMATERIAL) && (i!=R_FOOD) && (i!=R_PROCESSED) && (i != R_END_WORKUNITS)) {
          // stored          
          count = colony.getStoredResources(i);
          resourceStored[i].setText(myFormatter.format(count) + " " + unit);
                          
          // produced
          count = colony.getProducedResources(i);
	      resourceProduced[i].setText(myFormatter.format(count) + " " + unit);
    
          // used
          count = colony.getConsumedResources(i);
          resourceUsed[i].setText(myFormatter.format(count) + " " + unit);

          // demand
          if (colony.getResourceDemandString(i)==null) {
             resourceDemand[i].setText("          none");
          } else {
             resourceDemand[i].setText("          "+colony.getResourceDemandString(i));
          }
          
        }
     }
  }


  public void refresh() {
    if (isVisible()) {
      init();
    }
  }

}