//The class PlanetDetail displays planets in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import spaceopera.gui.window.helper.TableHelper;
import spaceopera.gui.window.list.model.ClimateDataModel;
import spaceopera.gui.window.list.model.PlanetDataModel;
import spaceopera.gui.window.list.model.ResourceDataModel;
import spaceopera.gui.window.list.model.SpecialDataModel;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.elements.Planet;

/**
 * The PlanetDetail class shows the major details of a planet.
 */
public class PlanetDetail extends JPanel implements SOConstants {

	private static final long serialVersionUID = 1L;
	// private SpaceOpera spaceOpera = null;
	private Planet planet = null;
	private JLabel titleLabel;
	private JTable planetData, climateData, resourceData, specialData;
	private JPanel titlePanel, centerPanel, dataPanel, climatePanel, resourcePanel, specialPanel;
	private Fullscreen fullscreen;

	private void doStaticLayout() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(BorderFactory.createLineBorder(Color.gray));
		setBackground(Color.BLACK);

		titlePanel = new JPanel();
		titlePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		titlePanel.setPreferredSize(new Dimension(460, 25));
		titlePanel.setMaximumSize(new Dimension(460, 25));
		titlePanel.setForeground(Color.yellow);
		titlePanel.setBackground(Color.darkGray);
		titleLabel = new JLabel();
		titleLabel.setForeground(Color.yellow);
		titleLabel.setBackground(Color.BLACK);
		titleLabel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
		titlePanel.add(titleLabel);
		add(titlePanel);

		centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(2, 2, 1, 1));
		centerPanel.setPreferredSize(new Dimension(460, 500));
		centerPanel.setBackground(Color.gray);

		dataPanel = new JPanel();
		dataPanel.setPreferredSize(new Dimension(230, 160));
		dataPanel.setLayout(new GridLayout(1, 1));
		dataPanel.setForeground(Color.yellow);
		dataPanel.setBackground(Color.black);
		planetData = TableHelper.createDetailTable(new PlanetDataModel(fullscreen, planet), true);
		planetData.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		JScrollPane scrollData = new JScrollPane(planetData);
		scrollData.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		dataPanel.add(scrollData);

		climatePanel = new JPanel();
		climatePanel.setPreferredSize(new Dimension(230, 160));
		climatePanel.setLayout(new GridLayout(1, 1));
		climatePanel.setForeground(Color.yellow);
		climatePanel.setBackground(Color.blue);
		climateData = TableHelper.createDetailTable(new ClimateDataModel(planet), false);
		JScrollPane scrollClimate = new JScrollPane(climateData);
		scrollClimate.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		climatePanel.add(scrollClimate);

		resourcePanel = new JPanel();
		resourcePanel.setPreferredSize(new Dimension(230, 160));
		resourcePanel.setLayout(new GridLayout(1, 1));
		resourcePanel.setForeground(Color.yellow);
		resourcePanel.setBackground(Color.red);
		resourceData = TableHelper.createDetailTable(new ResourceDataModel(planet), false);
		JScrollPane scrollResource = new JScrollPane(resourceData);
		scrollResource.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		resourcePanel.add(scrollResource);

		specialPanel = new JPanel();
		specialPanel.setPreferredSize(new Dimension(230, 160));
		specialPanel.setLayout(new GridLayout(1, 1));
		specialPanel.setForeground(Color.yellow);
		specialPanel.setBackground(Color.gray);
		specialData = TableHelper.createDetailTable(new SpecialDataModel(fullscreen, planet), false);
		JScrollPane scrollSpecial = new JScrollPane(specialData);
		scrollSpecial.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.gray));
		specialPanel.add(scrollSpecial);

		centerPanel.add(dataPanel);
		centerPanel.add(climatePanel);
		centerPanel.add(resourcePanel);
		centerPanel.add(specialPanel);
		add(centerPanel);

		add(Box.createVerticalGlue());
		add(Box.createVerticalGlue());
		add(Box.createVerticalGlue());
		add(Box.createVerticalGlue());
		add(Box.createVerticalGlue());

		// XXX: is this always the actual 'human' player?
		// TODO colPossible = new
		// JLabel(planet.getNeededColonyModuleName(spaceOpera.getCurrentPlayer()));
		// colPossible = new JLabel("TODO");

	}

	private void init() {
		// TODO Player player = spaceOpera.getCurrentPlayer();
		// TODO PreferredPlanetValues preferences =
		// player.getPreferredPlanetValues();
		titleLabel.setText("Planet Detail:");

		planetData = TableHelper.createDetailTable(new PlanetDataModel(fullscreen, planet), true);
		climateData = TableHelper.createDetailTable(new ClimateDataModel(planet), false);
		resourceData = TableHelper.createDetailTable(new ResourceDataModel(planet), false);
		specialData = TableHelper.createDetailTable(new SpecialDataModel(fullscreen, planet), false);

		// TODO old, poor code
		// String sAtmosphere = "";
		// // XXX: the label "earthlike" is a bit misleading. earthlike means
		// // home-planet-like for aliens
		// if (planet.getAtmosphericPressure() < 0.01) {
		// sAtmosphere = "none";
		// }
		// // TODO } else if
		// (preferences.atmosphericCompositionIsOutOfRange(planet.getAtmosphericComposition()))
		// {
		// // sAtmosphere = "poisonous";
		// // } else if
		// (preferences.atmosphericCompositionIsExtreme(planet.getAtmosphericComposition()))
		// {
		// // sAtmosphere = "hardly breathable";
		// // } else if
		// (preferences.atmosphericCompositionIsAcceptable(planet.getAtmosphericComposition()))
		// {
		// // sAtmosphere = "breathable";
		// // } else if
		// (preferences.atmosphericCompositionIsIdeal(planet.getAtmosphericComposition()))
		// {
		// // sAtmosphere = "earthlike";
		// // }
		// atmosphere.setText(sAtmosphere);

	}

	public PlanetDetail(Planet p, Fullscreen fs) {
		fullscreen = fs;
		planet = p;
		// spaceOpera = so;
		// TODO setTitle("Planet " + planet.getName());
		doStaticLayout();
		init();
	}

}