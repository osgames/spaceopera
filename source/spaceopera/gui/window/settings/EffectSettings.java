//The class EffectSettings is used to set animations on/off
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.settings;

import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SODialog;
import spaceopera.universe.SOConstants;

/** The EffectSettings class is used to set effects on/off and later maybe
 *  to select different effect bits for different events
 */
public class EffectSettings extends SODialog implements ActionListener,
                                                     ItemListener,
                                                     SOConstants {
  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Panel pCenter, pCenterCenter, pSouth;
  private Button cancel, go;
  private CheckboxGroup cbgEffects;
  private Checkbox chOn, chOff;

  //Methods
  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CANCEL)) {
        this.dispose();
    }
    if (c.equals(GO)) {
        if (chOn.getState()) {
          // activate settings
          spaceOpera.storeEffectConfig(true);
        } else {
          // activate settings
          spaceOpera.storeEffectConfig(false);
        }
        this.dispose();
    }
  }

  public EffectSettings (SpaceOpera so) {
      super(so.getDisplay());
      spaceOpera = so;
      setTitle("Effect settings  (work in progress)");
      setLayout(new BorderLayout(2,2));
      setBackground(Color.lightGray);
      setForeground(Color.black);
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();   
      pack();
      setSize(ANIMSIZEX,ANIMSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
      go.requestFocus();
  }


  private void doStaticLayout() {
      setLayout(new BorderLayout(2,2));
      setBackground(Color.black);
      setForeground(Color.lightGray);

      pCenter = new Panel();
      pCenter.setBackground(Color.lightGray);
      pCenter.setForeground(Color.black);
      pCenter.setLayout(new GridLayout(1,1));

      pCenterCenter = new Panel();
      pCenterCenter.setBackground(Color.lightGray);
      pCenterCenter.setForeground(Color.black);
      pCenterCenter.setLayout(new GridLayout(2,2));
      cbgEffects = new CheckboxGroup();
      chOn = new Checkbox("on", cbgEffects,true);
      chOff = new Checkbox("off", cbgEffects,false);
      pCenterCenter.add(new Label("Effects:"));
      pCenterCenter.add(new Label(""));
      pCenterCenter.add(chOn);
      pCenterCenter.add(chOff);
      pCenter.add(pCenterCenter);
      add("Center", pCenter);

      // Command-Buttons
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,6));
      pSouth.setBackground(Color.lightGray);
      pSouth.setForeground(Color.black);
      cancel = new Button(CANCEL);
      cancel.addActionListener(this);
      cancel.setActionCommand(CANCEL);
      cancel.setForeground(Color.black);
      cancel.setBackground(Color.lightGray);
      go = new Button(GO);
      go.addActionListener(this);
      go.setActionCommand(GO);
      go.setForeground(Color.black);
      go.setBackground(Color.lightGray);
      pSouth.add(new Label(""));
      pSouth.add(cancel);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(go);
      pSouth.add(new Label(""));
      add("South",pSouth);
  }


  public void init() {
      if (spaceOpera.getEffects()) {
          cbgEffects.setSelectedCheckbox(chOn);
      } else {
          cbgEffects.setSelectedCheckbox(chOff);
      }
  }


  public void itemStateChanged(ItemEvent event) {
  }

}