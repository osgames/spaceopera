//The class MusicSettings is used to set sound on/off and to define the sound bites
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.settings;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SODialog;
import spaceopera.universe.SOConstants;

/** The MusicSettings class is used to set sound on/off and select music
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class MusicSettings extends SODialog implements ActionListener,
                                                     ItemListener,
                                                     SOConstants {
  private static final long serialVersionUID = 1L;
private SpaceOpera spaceOpera = null;
  private Panel pNorth, pNorthCenter,
                pCenter, pCenterTop, pCenterCenter, pCenterEast,
                pSouth;
  private Button add, remove, close, go;
  private CheckboxGroup cbgSound, cbgSoundSelect;
  private Checkbox chOn, chOff, chDir, chList;
  private java.awt.List musicList;
  private java.awt.TextField musicPath;

  //Methods

  public MusicSettings (SpaceOpera so) {
      super(so.getDisplay());
      spaceOpera= so;
      setTitle("Sound settings  ...work in progress");
      setLayout(new BorderLayout(2,2));
      setBackground(Color.lightGray);
      setForeground(Color.black);
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();   
      pack();
      setSize(SOUNDSETTINGSSIZEX,SOUNDSETTINGSSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
      go.requestFocus();
  }


  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CLOSE)) {
        this.dispose();
    }
    if (c.equals("go")) {
        boolean onOff;
        boolean fromDir;
        String musicDir="";
        Vector musicVector=new Vector();
        if (chOn.getState()) {
          // activate settings
          onOff = true;
        } else {
          // activate settings

          onOff = false;
        }
        if (chDir.getState()) {
          //random from directory
          fromDir = true;
          musicDir = musicPath.getText();
        } else {
          //random from list
          fromDir = false;
          for (int i = 0; i<musicList.getItemCount(); i++) {
            musicVector.addElement(musicList.getItem(i));
          }
        }
        spaceOpera.storeMusicConfig(onOff,fromDir, musicDir, musicVector);
        this.dispose();
    }
    if (c.equals("add")) {

      FileDialog fd = new FileDialog(spaceOpera.getDisplay(), "Select SpaceOpera sound file...",FileDialog.LOAD);
      fd.show();
      String file = fd.getDirectory() + fd.getFile();
      musicList.add(file);
    }
    if (c.equals("remove")) {
      int i = musicList.getSelectedIndex();
      if (i>=0) {
         String item = musicList.getSelectedItem();
         musicList.remove(item);
      }
     }
  }

  
  private void doStaticLayout() {
      setLayout(new BorderLayout(2,2));
      setBackground(Color.black);
      setForeground(Color.lightGray);

      pNorth = new Panel();
      pNorth.setBackground(Color.lightGray);
      pNorth.setForeground(Color.black);
      pNorth.setLayout(new GridLayout(1,1));

      pNorthCenter = new Panel();
      pNorthCenter.setBackground(Color.lightGray);
      pNorthCenter.setForeground(Color.black);
      pNorthCenter.setLayout(new GridLayout(1,3));
      cbgSound = new CheckboxGroup();
      pNorthCenter.add(new Label("Sound:"));
      chOn = new Checkbox("on", cbgSound, true);
      pNorthCenter.add(chOn);
      chOff = new Checkbox("off", cbgSound, false);
      pNorthCenter.add(chOff);
      pNorth.add(pNorthCenter);
      add("North", pNorth);

      pCenter = new Panel();
      pCenter.setBackground(Color.black);
      pCenter.setForeground(Color.lightGray);
      pCenter.setLayout(new BorderLayout(2,2));

      pCenterTop = new Panel();
      pCenterTop.setBackground(Color.lightGray);
      pCenterTop.setForeground(Color.black);
      pCenterTop.setLayout(new GridLayout(5,1));
      cbgSoundSelect = new CheckboxGroup();
      pCenterTop.add(new Label("Select Sound:"));
      chDir = new Checkbox("Random from directory:", cbgSoundSelect, true);
      pCenterTop.add(chDir);
      musicPath = new java.awt.TextField("");
      musicPath.setBackground(Color.white);
      musicPath.setForeground(Color.black);
      pCenterTop.add(musicPath);
      pCenterTop.add(new Label(""));
      chList = new Checkbox("Random from list:", cbgSoundSelect, false);
      pCenterTop.add(chList);
      pCenter.add("North",pCenterTop);

      pCenterCenter = new Panel();
      pCenterCenter.setBackground(Color.lightGray);
      pCenterCenter.setForeground(Color.black);
      pCenterCenter.setLayout(new GridLayout(1,1));
      musicList = new java.awt.List();
      musicList.setBackground(Color.white);
      musicList.setForeground(Color.black);
      musicList.setFont(new Font("Monospaced", Font.PLAIN, 12));
      //soundList.add("/sounds/spaceopera1.mid");
      //soundList.add("/sounds/spaceopera2.wav");
      //soundList.add("/sounds/spaceopera3.au");
      musicList.setSize(1,5);
      pCenterCenter.add(musicList);
      pCenter.add("Center",pCenterCenter);

      pCenterEast = new Panel();
      pCenterEast.setBackground(Color.lightGray);
      pCenterEast.setForeground(Color.black);
      pCenterEast.setLayout(new GridLayout(5,1));
      add = new Button("Add");
      add.addActionListener(this);
      add.setActionCommand("add");
      add.setForeground(Color.black);
      add.setBackground(Color.lightGray);
      add.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
      pCenterEast.add(add);
      remove = new Button("Remove");
      remove.addActionListener(this);
      remove.setActionCommand("remove");
      remove.setForeground(Color.black);
      remove.setBackground(Color.lightGray);
      remove.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 12));
      pCenterEast.add(remove);
      pCenterEast.add(new Label(""));
      pCenterEast.add(new Label(""));
      pCenterEast.add(new Label(""));
      pCenter.add("East",pCenterEast);


      add("Center", pCenter);

      // Command-Buttons
      pSouth = new Panel();
      pSouth.setLayout(new GridLayout(1,6));
      pSouth.setBackground(Color.lightGray);
      pSouth.setForeground(Color.black);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      go = new Button("Go!");
      go.addActionListener(this);
      go.setActionCommand("go");
      go.setForeground(Color.black);
      go.setBackground(Color.lightGray);
      pSouth.add(new Label(""));
      pSouth.add(close);
      pSouth.add(new Label(""));
      pSouth.add(new Label(""));
      pSouth.add(go);
      pSouth.add(new Label(""));
      add("South",pSouth);
  }


  public void init() {
    if (spaceOpera.getMusicState()) {
        chOn.setState(true);
    } else {
        chOff.setState(true);
    }
//System.out.println("MusicSettings:");
//System.out.println("sound state: " +  spaceOpera.getSoundState());
//System.out.println("from dir: " + spaceOpera.getSoundFromDir());
    if (spaceOpera.getMusicFromDir()) {
        chDir.setState(true);
        musicPath.setText(spaceOpera.getMusicDirectory());
//System.out.println("directory: " + spaceOpera.getSoundDirectory());
    } else {
        chList.setState(true);
        Vector music = spaceOpera.getMusicList();
        for (int i=0; i<music.size(); i++){
          String song = (String)music.elementAt(i);
//System.out.println("file: " + soundFile);
          musicList.add(song);
        }
    }

  }


  public void itemStateChanged(ItemEvent event) {
//      if (event.getSource()==chEffectsOn) {
//         chShowDirections.setState(chEffectsOn.getState());
//      }
  }

}
