//The class AnimSettings is used to set animations on/off
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

//reworked by SR-040224 based on AnimSettings.java

package spaceopera.gui.window.settings;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOJDialog;
import spaceopera.gui.helper.TechTreeExport;
import spaceopera.universe.SOConstants;

/**
 * The MenuSettingsOther class is used to set all other parameters that can
 * switch on/off or select. <br>
 * AntiAlias - switch Window Game Size Generate Documentation (TechTree-Html)
 */
public class MenuSettingsOther extends SOJDialog implements ActionListener, ItemListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private SpaceOpera spaceOpera = null;
	private JButton close, use, make;

	private ButtonGroup rbGroupA = new ButtonGroup();
	private ButtonGroup rbGroupS = new ButtonGroup();
	private JRadioButton rbAntialiasOn = new JRadioButton();
	private JRadioButton rbAntialiasOff = new JRadioButton();

	private JRadioButton rbS0640 = new JRadioButton();
	private JRadioButton rbS0800 = new JRadioButton();
	private JRadioButton rbS1024 = new JRadioButton();
	private JRadioButton rbS1280 = new JRadioButton();
	private JRadioButton rbS1600 = new JRadioButton();

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals(CLOSE)) {
			this.dispose();
		}
		if (c.equals(USE)) {
			this.dispose();
		}
		if (c.equals("make")) {
			TechTreeExport x = new TechTreeExport();
			x.export2Html(spaceOpera.getUniverse());
			this.dispose();
		} else {
			System.out.println("Switches currently not in function - see MenuSettingsOther why.");
			// if (c.equals("Aon")) { ANTIALIAS = true; }
			// if (c.equals("Aoff")) { ANTIALIAS = false; }
			// if (c.equals("s06")) { GAMEWINDOWSIZEX = 640; GAMEWINDOWSIZEY =
			// 480;}
			// if (c.equals("s08")) { GAMEWINDOWSIZEX = 800; GAMEWINDOWSIZEY =
			// 600;}
			// if (c.equals("s10")) { GAMEWINDOWSIZEX =1024; GAMEWINDOWSIZEY =
			// 768;}
			// if (c.equals("s12")) { GAMEWINDOWSIZEX =1280; GAMEWINDOWSIZEY
			// =1024;}
			// if (c.equals("s16")) { GAMEWINDOWSIZEX =1600; GAMEWINDOWSIZEY
			// =1200;}
		}
	}

	public MenuSettingsOther(SpaceOpera so) {
		super(so);

		this.getContentPane().setLayout(new FlowLayout());
		this.setTitle("Other Games settings  (work in progress)");
		this.setBackground(Color.black);
		this.setForeground(Color.green);

		doStaticLayout();
		enableCloseButton(this);
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		// this.setSize(SMALLWINDOWSIZEX,SMALLWINDOWSIZEY); to small
		this.setSize(300, 300);
		this.setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);
		init();
		show();
		use.requestFocus();
	}

	private void doStaticLayout() {
		JPanel pAll = new JPanel();
		pAll.setLayout(new GridLayout(3, 1));

		// Antialias
		rbAntialiasOn.setText("switch On");
		rbAntialiasOn.setActionCommand("Aon");
		rbAntialiasOn.addActionListener(this);
		rbAntialiasOff.setText("switch Off");
		rbAntialiasOff.setActionCommand("Aoff");
		rbAntialiasOff.addActionListener(this);
		rbGroupA.add(rbAntialiasOn);
		rbGroupA.add(rbAntialiasOff);
		JPanel pAnti = new JPanel();

		pAnti.setLayout(new FlowLayout());
		Border lineBorder = BorderFactory.createLineBorder(Color.darkGray, 1);
		TitledBorder tb1 = BorderFactory.createTitledBorder(lineBorder, "  Antialias  ");
		pAnti.setBorder(tb1);
		pAnti.add(rbAntialiasOn);
		pAnti.add(rbAntialiasOff);

		// TODO check the current user-desktop-screensize to disable unusably
		// screens
		// ScreenSize
		rbS0640.setText("640x480");
		rbS0640.setActionCommand("s06");
		rbS0640.addActionListener(this);
		rbS0800.setText("800x600");
		rbS0800.setActionCommand("s08");
		rbS0800.addActionListener(this);
		rbS1024.setText("1024x768");
		rbS1024.setActionCommand("s10");
		rbS1024.addActionListener(this);
		rbS1280.setText("1280x1024");
		rbS1280.setActionCommand("s12");
		rbS1280.addActionListener(this);
		rbS1600.setText("1600x1200");
		rbS1600.setActionCommand("s16");
		rbS1600.addActionListener(this);

		rbGroupS.add(rbS0640);
		rbGroupS.add(rbS0800);
		rbGroupS.add(rbS1024);
		rbGroupS.add(rbS1280);
		rbGroupS.add(rbS1600);

		JPanel pScreen = new JPanel();
		pScreen.setLayout(new GridLayout(2, 3));
		TitledBorder tb2 = BorderFactory.createTitledBorder(lineBorder, "  Game-Window-Size  ");
		pScreen.setBorder(tb2);
		pScreen.add(rbS0640);
		pScreen.add(rbS0800);
		pScreen.add(rbS1024);
		pScreen.add(rbS1280);
		pScreen.add(rbS1600);

		JPanel pMake = new JPanel();
		pMake.setLayout(new FlowLayout());
		TitledBorder tb3 = BorderFactory.createTitledBorder(lineBorder, "  Generate a HTML-Techtree Document  ");
		make = new JButton("Generate");
		make.addActionListener(this);
		make.setActionCommand("make");
		pMake.setBorder(tb2);
		pMake.add(make);

		// Command-Buttons
		JPanel pSouth = new JPanel();
		pSouth.setLayout(new GridLayout(1, 4));
		// pSouth.setBackground(Color.lightGray);
		// pSouth.setForeground(Color.black);
		close = new JButton("Cancel");
		close.addActionListener(this);
		close.setActionCommand(CLOSE);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		use = new JButton(USE);
		use.addActionListener(this);
		use.setActionCommand(USE);
		use.setForeground(Color.black);
		use.setBackground(Color.lightGray);

		pSouth.add(close);
		pSouth.add(new JLabel(""));
		// pSouth.add(new JLabel(""));
		pSouth.add(use);

		pAll.add(pAnti);
		pAll.add(pScreen);
		pAll.add(pMake);

		getContentPane().add(pAll);
		getContentPane().add(pSouth);
	}

	public void init() {
//		if (ANTIALIAS == true) {
			rbAntialiasOn.setSelected(true);
//		} else {
//			rbAntialiasOff.setSelected(true);
//		}
		switch (ALLWINDOWSIZEX) {
		case 640:
			rbS0640.setSelected(true);
			break;
		case 800:
			rbS0800.setSelected(true);
			break;
		case 1024:
			rbS1024.setSelected(true);
			break;
		case 1280:
			rbS1280.setSelected(true);
			break;
		case 1600:
			rbS1600.setSelected(true);
			break;
		default:
			rbS0640.setSelected(true);
			break;
		}
	}

	public void itemStateChanged(ItemEvent event) {
		System.out.println("Status changed");
	}
}
