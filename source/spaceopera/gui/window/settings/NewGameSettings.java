//The class NewGameSettings is used to start a new SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window.settings;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import spaceopera.gui.components.SODialog;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;

/**
 * When starting a new game, the NewGameSettings determine the most important
 * game parameters like # of opponents, universe size...
 */
public class NewGameSettings extends SODialog implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;
	private TextArea ta1;
	private TextField tf1, tf2, tf3;
	private java.awt.List l1, l2;
	private Panel pNorth, pCenter, p1, p2, p5, p6;
	private Button close, go;
	private Choice chGalaxySize, chDifficultyLevel, chNumOfOpponents, chRandomPlayers, chReplaceEliminated;
	private Fullscreen fullscreen;

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals(CANCEL)) {
			this.dispose();
		}
		if (c.equals(GO)) {
			fullscreen.setGalaxySize(chGalaxySize.getSelectedIndex() + 1);
			fullscreen.setDifficultyLevel(chDifficultyLevel.getSelectedIndex() + 1);
			fullscreen.setNumberOfOpponents(chNumOfOpponents.getSelectedIndex() + 1);
			fullscreen.setRandomPlayers(chRandomPlayers.getSelectedItem().equals("yes") ? true : false);
			fullscreen.setReplaceEliminated(chReplaceEliminated.getSelectedItem().equals("yes") ? true : false);
			fullscreen.initiateNewGame();
			fullscreen.setPlayerName(tf1.getText());
			this.dispose();
		}
	}

	private void doStaticLayout() {
		setLayout(new BorderLayout(2, 2));
		setBackground(Color.lightGray);
		setForeground(Color.black);
		// North
		pNorth = new Panel();
		pNorth.setLayout(new GridLayout(1, 2, 2, 0));
		pNorth.setBackground(Color.lightGray);
		pNorth.setForeground(Color.black);
		p1 = new Panel();
		p1.setLayout(new GridLayout(1, 2, 1, 0));
		p1.add(new Label("Enter name for human player:"));
		p2 = new Panel();
		p2.setLayout(new BorderLayout(0, 0));
		tf1 = new TextField("Humans");
		p2.add(tf1);
		pNorth.add(p1);
		pNorth.add(p2);
		add("North", pNorth);
		// Center
		pCenter = new Panel();
		pCenter.setBackground(Color.lightGray);
		pCenter.setForeground(Color.black);
		pCenter.setLayout(new GridLayout(5, 2, 2, 0));
		pCenter.add(new Label("Galaxy size: "));
		chGalaxySize = new Choice();
		pCenter.add(chGalaxySize);
		pCenter.add(new Label("Difficulty level: "));
		chDifficultyLevel = new Choice();
		pCenter.add(chDifficultyLevel);
		pCenter.add(new Label("Number of opponents: "));
		chNumOfOpponents = new Choice();
		pCenter.add(chNumOfOpponents);
		pCenter.add(new Label("Random player may pop up : "));
		chRandomPlayers = new Choice();
		pCenter.add(chRandomPlayers);
		pCenter.add(new Label("Replace eliminated players : "));
		chReplaceEliminated = new Choice();
		pCenter.add(chReplaceEliminated);
		add("Center", pCenter);
		// South
		p6 = new Panel();
		p6.setLayout(new GridLayout(1, 6));
		p6.setBackground(Color.lightGray);
		p6.setForeground(Color.black);
		close = new Button(CANCEL);
		close.addActionListener(this);
		close.setActionCommand(CANCEL);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		go = new Button(GO);
		go.addActionListener(this);
		go.setActionCommand(GO);
		go.setForeground(Color.black);
		go.setBackground(Color.lightGray);
		p6.add(new Label(""));
		p6.add(close);
		p6.add(new Label(""));
		p6.add(new Label(""));
		p6.add(go);
		p6.add(new Label(""));
		add("South", p6);
	}

	public void init() {
		// item 0=tiny
		chGalaxySize.addItem("Tiny   (5-10 Stars)");
		chGalaxySize.addItem("Small  (10-25 Stars)");
		chGalaxySize.addItem("Medium (25-50 Stars)");
		chGalaxySize.addItem("Large  (50-80 Stars)");
		chGalaxySize.addItem("Huge   (80-120 Stars)");
		chGalaxySize.select(2); // default medium
		// chGalaxySize.select(0); // test tiny
		// item 0=Kindergarden
		chDifficultyLevel.addItem("Very Easy");
		chDifficultyLevel.addItem("Easy");
		chDifficultyLevel.addItem("Medium");
		chDifficultyLevel.addItem("Hard");
		chDifficultyLevel.addItem("Impossible");
		chDifficultyLevel.select(1);
		// item 0 = one opponent
		chNumOfOpponents.addItem("1");
		chNumOfOpponents.addItem("2");
		chNumOfOpponents.addItem("3");
		chNumOfOpponents.addItem("4");
		chNumOfOpponents.addItem("5");
		chNumOfOpponents.addItem("6");
		chNumOfOpponents.addItem("7");
		chNumOfOpponents.addItem("8");
		chNumOfOpponents.addItem("9");
		chNumOfOpponents.addItem("10");
		chNumOfOpponents.addItem("11");
		chNumOfOpponents.addItem("12");
		chNumOfOpponents.addItem("13");
		chNumOfOpponents.addItem("14");
		chNumOfOpponents.addItem("15");
		chNumOfOpponents.addItem("16");
		chNumOfOpponents.addItem("17");
		chNumOfOpponents.addItem("18");
		chNumOfOpponents.addItem("19");
		chNumOfOpponents.addItem("20");
		chNumOfOpponents.select(0); // default 1 opponent
		// chNumOfOpponents.select(3); //test 4 opponents

		// XXX: random players are non-spacefaring players
		// (comparable to settlers in civilization)
		chRandomPlayers.addItem("yes");
		chRandomPlayers.addItem("no");

		// XXX: if a computer player gets eliminated,
		// a new player pops up, at least early in game
		chReplaceEliminated.addItem("yes");
		chReplaceEliminated.addItem("no");
	}

	public NewGameSettings (Fullscreen fullscreen) {
      super(fullscreen);
      this.fullscreen = fullscreen;
      setTitle("Settings for new game");
      doStaticLayout();
      enableCloseButton(this);
      Toolkit kit = Toolkit.getDefaultToolkit();
      Dimension dim = kit.getScreenSize();
      pack();
      setSize(SMALLWINDOWSIZEX,SMALLWINDOWSIZEY);
      setLocation((dim.width - getWidth())/2,(dim.height - getHeight())/2);
      init();
      show();
      go.requestFocus();
      fullscreen.initiateNewGame();
   }
}