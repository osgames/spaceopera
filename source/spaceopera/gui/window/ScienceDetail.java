//The class ScienceDetail is used for technology settings in the SpaceOpera game
//Copyright (C) 1996-2005 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.window;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusListener;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.components.SOFrame;
import spaceopera.universe.SOConstants;
import spaceopera.universe.Technology;
import spaceopera.universe.ai.Player;
import spaceopera.universe.colony.BuildProject;
import spaceopera.universe.colony.Building;

/**
 * The class ScienceDetail displays all the currently researched sciences, the
 * already researched sciences plus the new sciences.
 */
@SuppressWarnings({ "rawtypes" })
public class ScienceDetail extends SOFrame implements ActionListener, AdjustmentListener, ItemListener, FocusListener, SOConstants {
	private static final long serialVersionUID = 1L;
	private SpaceOpera spaceOpera = null;
	private Player player = null;
	private Scrollbar sb[] = new Scrollbar[8];
	private int values[] = new int[8];
	private Label showPercent[] = new java.awt.Label[8];
	private Choice choice[] = new java.awt.Choice[8];
	private Label lbTechTitel, lblResearched, lbCurrentResearch, lbDistribution;
	private int rotateScrollbar = 1;
	private TextArea taTechtext;
	private TextField tf1, tf2, tf3;
	private java.awt.List listResearched;
	private Panel pNorth, pCenter;
	private Panel pDistribution, pSelect, pChoice, pPercent, pList, pTechDescr, pCommands, p8, p9, p10;
	private Button save, close;
	private Button add, remove;
	private Button up, down;
	private Button delete;
	private Button next, previous;
	private Choice chGrpTech = new java.awt.Choice();

	// private Technology techTemp=null;

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		String item = "";
		BuildProject bp = null;
		int i = 0;
		if (c.equals("save")) {
			saveSettings();
		} else if (c.equals(CLOSE)) {
			this.dispose();
		}
	}

	private void addTechnologies() {
		List<Technology> possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = (Technology) possibleTechnologies.get(i);
			if (tech.getAllowStudy()) {
				int technology = 0;
				if (tech.getCls().equals("Physics")) {
					technology = 1;
				}
				if (tech.getCls().equals("Biology")) {
					technology = 2;
				}
				if (tech.getCls().equals("Mathematics")) {
					technology = 3;
				}
				if (tech.getCls().equals("Social")) {
					technology = 4;
				}
				if (tech.getCls().equals("Economy")) {
					technology = 5;
				}
				if (tech.getCls().equals("Military")) {
					technology = 6;
				}

				if (tech.getInitialState() == false) {
					if (!tech.isStudied()) {
						choice[technology].addItem(tech.getName());
					}
				}
			}
		}
	}

	public void adjustmentValueChanged(java.awt.event.AdjustmentEvent e) {
		for (int i = 1; i < 7; i++) {
			if ((e.getSource() == sb[i])) {
				calculateNewDistribution(i);
			}
		}
	}

	public void calculateNewDistribution(int myScrollbar) {
		int i, j, alt, newVal, diff, mod, incr;
		boolean setflag = false;
		newVal = sb[myScrollbar].getValue();
		if (newVal != values[myScrollbar]) {
			diff = 0;
			incr = 0;
			if (newVal < values[myScrollbar]) {
				incr = 1;
			} else {
				incr = -1;
			}
			diff = Math.abs(newVal - values[myScrollbar]);
			while (diff != 0) {
				if (rotateScrollbar != myScrollbar) {
					if (incr > 0) {
						if (values[rotateScrollbar] < 100) {
							values[rotateScrollbar] += incr;
							diff -= 1;
						}
					} else {
						if (values[rotateScrollbar] > 0) {
							values[rotateScrollbar] += incr;
							diff -= 1;
						}
					}
				}
				rotateScrollbar++;
				if (rotateScrollbar > 6) {
					rotateScrollbar = 1;
				}
			}
			for (i = 1; i < 7; i++) {
				if (i != myScrollbar) {
					sb[i].setValue(values[i]);
				} else {
					values[i] = sb[i].getValue();
				}
			}
		}
	}

	private void clearTechnologies() {
		if (listResearched.getItemCount() > 0) {
			listResearched.removeAll();
		}
		for (int i = 1; i < 7; i++) {
			choice[i].removeAll();
		}
	}

	private void displayKnownTechnologies(String strTech) {
		List<Technology> possibleTechnologies = player.getPossibleTechnologies();
		String[] names = new String[possibleTechnologies.size()];
		int count = 0;
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = possibleTechnologies.get(i);
			if (tech.isStudied()) {
				if (strTech == tech.getCls()) {
					names[count++] = tech.getName();
					// listResearched.add(tech.getName());
				}
				if (strTech == "All") {
					names[count++] = tech.getName();
					// listResearched.add(tech.getName());
				}
			}
		}
		String[] sortedNames = new String[count];
		for (int i = 0; i < count; i++) {
			sortedNames[i] = names[i];
		}
		Arrays.sort(sortedNames);
		for (int i = 0; i < sortedNames.length; i++) {
			listResearched.add(sortedNames[i]);
		}
	}

	private void displayResearchProgress(Technology tech) {
		int technology = 0;
		if (tech.getName().equals(player.getCurrentPhysics())) {
			technology = 1;
		}
		if (tech.getName().equals(player.getCurrentBiology())) {
			technology = 2;
		}
		if (tech.getName().equals(player.getCurrentMathematics())) {
			technology = 3;
		}
		if (tech.getName().equals(player.getCurrentSocial())) {
			technology = 4;
		}
		if (tech.getName().equals(player.getCurrentEconomy())) {
			technology = 5;
		}
		if (tech.getName().equals(player.getCurrentMilitary())) {
			technology = 6;
		}
		if (technology != 0) {
			choice[technology].select(tech.getName());
			showPercent[technology].setText("   " + tech.getComplete() + "%");
		}
	}

	private void displayTechDescription(Technology tech) {
		if (tech != null) {
			String description = "";
			taTechtext.setText("");
			int start = 0;
			int lineLength = 40;
			String temp = "";
			taTechtext.append("\nDescription:\n");
			if (!tech.isStudied()) {
				// not yet researched
				description = tech.getDescription();
			} else {
				// known technology
				// Buildings get the description from the building definition
				List<Building> buildings = player.getPossibleBuildings();
				for (int i = 0; i < buildings.size(); i++) {
					BuildProject bp = (BuildProject) buildings.get(i);
					if (bp.getId() == tech.getAllowsBuilding()) {
						description = bp.getDescription();
						break;
					}
				}
				// ShipComponents get the description from the shipComponent
				// definition
				if (description.length() < 1) {
					Collection comp = player.getPossibleComponents().values();
					for (Iterator it = comp.iterator(); it.hasNext();) {
						BuildProject bp = (BuildProject) it.next();
						if (bp.getId() == tech.getAllowsComponent()) {
							description = bp.getDescription();
							break;
						}
					}
				}
			}
			while (description.length() > lineLength) {
				for (int j = lineLength; j > 20; j--) {
					if (description.charAt(j) == ' ') {
						taTechtext.append(description.substring(start, j) + "\n");
						description = description.substring(j + 1);
						break;
					}
				}
			}
			taTechtext.append(description);

			// more technology details:
			if (tech.isStudied()) {
				int count = 0;
				String text = "";
				if (tech.getAllowsBuilding() != 0) {
					String building = spaceOpera.getBuildingName(tech.getAllowsBuilding());
					if (count == 0) {
						count += 1;
						text = "\nbuild '" + building + "' on planets.";
					}
				}
				if (tech.getAllowsProduct() != 0) {
					String product = spaceOpera.getProductName(tech.getAllowsProduct());
					if (count == 0) {
						count += 1;
						text = "\nbuild the product '" + product + "'";
					} else {
						count += 1;
						text += "\nand the product '" + product + "'";
					}
				}
				if (tech.getAllowsComponent() != 0) {
					String comp = spaceOpera.getComponentName(tech.getAllowsComponent());
					if (count == 0) {
						count += 1;
						text = "\nuse the ship component '" + comp + "'";
					} else if (count == 1) {
						count += 1;
						text += "\nand the ship component '" + comp + "'";
					} else {
						count += 1;
						text += "\nand the ship component '" + comp + "'";
					}
				}

				if (tech.getChildTech1() != 0) {
					String tech1 = spaceOpera.getTechnologyName(tech.getChildTech1());
					if (count > 0) {
						text += "\nand study '" + tech1 + "'";
					} else {
						text += "\nstudy '" + tech1 + "'";
					}
				}
				if (tech.getChildTech2() != 0) {
					String tech2 = spaceOpera.getTechnologyName(tech.getChildTech2());
					text += "\nand '" + tech2 + "'";
				}
				int obs = 0;
				if (tech.getObsoletesBuilding() != 0) {
					String building = spaceOpera.getBuildingName(tech.getObsoletesBuilding());
					text += "\n\nThe building '" + building + "'";
					obs++;
				}
				if (tech.getObsoletesShipComponent() != 0) {
					String comp = spaceOpera.getComponentName(tech.getObsoletesShipComponent());
					if (obs > 0) {
						text += "\nand the ship component '" + comp + "'";
					} else {
						text += "\n\nThe ship component '" + comp + "'";
					}
					obs++;
				}
				if (obs > 0) {
					text += "\ncan no longer be built.";
				}
				if (count > 0) {
					taTechtext.append("\n\nWhen this technology is mastered, you can");
					// taTechtext.append("\n build xxx, yyy and zzz"); // if
					// comp,
					// building or prod != 0
					// taTechtext.append("\n and study aaa and bbb"); // if
					// childtech 1 or 2 != 0
					taTechtext.append(text);
				}
				// known technology
				taTechtext.append("\n\nCost for Research: " + tech.getResearchCost() + "\n");
			} else {
				// not yet researched
				taTechtext.append("\n\nEstimated cost for Research: " + tech.getResearchCost() + "\n");
			}

			lbTechTitel.setText("Selected research project:    " + tech.getName());
		}
	}

	private void doStaticLayout() {
		setLayout(new BorderLayout(2, 2));
		// North
		pNorth = new Panel();
		pNorth.setLayout(new GridLayout(1, 2, 2, 0));
		pNorth.setBackground(Color.white);
		pNorth.setForeground(Color.black);
		pNorth.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pDistribution = new Panel();
		pDistribution.setLayout(new GridLayout(8, 2, 1, 0));
		pDistribution.setBackground(Color.black);
		pDistribution.setForeground(Color.white);
		for (int i = 1; i < 7; i++) {
			sb[i] = new Scrollbar(Scrollbar.HORIZONTAL);
			sb[i].setMaximum(110);
			sb[i].setMinimum(0);
			sb[i].setVisibleAmount(10);
			sb[i].addAdjustmentListener(this);
			sb[i].setForeground(Color.black);
			sb[i].setBackground(Color.lightGray);
		}
		lbDistribution = new Label("Research area:");
		lbDistribution.setFont(new Font("Monospaced", Font.BOLD, 12));
		pDistribution.add(lbDistribution);
		Label lb = new Label("");
		lb.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pDistribution.add(lb);
		pDistribution.add(new Label("Physics: "));
		pDistribution.add(sb[1]);
		pDistribution.add(new Label("Biology: "));
		pDistribution.add(sb[2]);
		pDistribution.add(new Label("Mathematics: "));
		pDistribution.add(sb[3]);
		pDistribution.add(new Label("Social: "));
		pDistribution.add(sb[4]);
		pDistribution.add(new Label("Economy: "));
		pDistribution.add(sb[5]);
		pDistribution.add(new Label("Military: "));
		pDistribution.add(sb[6]);
		pDistribution.add(new Label(""));
		pDistribution.add(new Label(""));
		pSelect = new Panel();
		pSelect.setLayout(new BorderLayout(0, 0));
		pSelect.setBackground(Color.black);
		pSelect.setForeground(Color.white);
		pSelect.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pChoice = new Panel();
		pChoice.setLayout(new GridLayout(8, 1));
		pChoice.setBackground(Color.black);
		pChoice.setForeground(Color.white);
		lbCurrentResearch = new Label("Current research projects:");
		lbCurrentResearch.setFont(new Font("Monospaced", Font.BOLD, 12));
		for (int i = 1; i < 7; i++) {
			choice[i] = new java.awt.Choice();
			choice[i].setBackground(Color.white);
			choice[i].setForeground(Color.black);
			choice[i].addItemListener(this);
			choice[i].addFocusListener(this);
		}
		pChoice.add(lbCurrentResearch);
		pChoice.add(choice[1]);
		pChoice.add(choice[2]);
		pChoice.add(choice[3]);
		pChoice.add(choice[4]);
		pChoice.add(choice[5]);
		pChoice.add(choice[6]);
		pChoice.add(new Label(""));
		pPercent = new Panel();
		pPercent.setLayout(new GridLayout(8, 1));
		pPercent.setBackground(Color.black);
		pPercent.setForeground(Color.white);
		pPercent.add(new Label(" % finished"));
		for (int i = 1; i < 7; i++) {
			showPercent[i] = new Label(" 0 %");
			pPercent.add(showPercent[i]);
		}
		pPercent.add(new Label(""));
		pSelect.add("Center", pChoice);
		pSelect.add("East", pPercent);
		pNorth.add(pDistribution);
		pNorth.add(pSelect);
		add("North", pNorth);
		// Center
		pCenter = new Panel();
		pCenter.setBackground(Color.white);
		pCenter.setForeground(Color.black);
		pCenter.setLayout(new GridLayout(1, 2, 2, 0));

		Panel pCont = new Panel();
		pCont.setLayout(new GridLayout(1, 2));
		lblResearched = new Label("Known technologies:");
		lblResearched.setForeground(Color.white);
		lblResearched.setBackground(Color.black);
		lblResearched.setFont(new Font("Monospaced", Font.BOLD, 12));
		pCont.add(lblResearched);
		chGrpTech.addItem("All");
		chGrpTech.addItem("Physics");
		chGrpTech.addItem("Biology");
		chGrpTech.addItem("Social");
		chGrpTech.addItem("Mathematics");
		chGrpTech.addItem("Economy");
		chGrpTech.addItem("Military");
		chGrpTech.addItemListener(this);
		pCont.setBackground(Color.black);
		pCont.add(chGrpTech);

		pList = new Panel();
		pList.setLayout(new BorderLayout(2, 2));
		pList.setForeground(Color.black);
		pList.setBackground(Color.white);
		pList.add("North", pCont);

		listResearched = new java.awt.List();
		listResearched.setBackground(Color.lightGray);
		listResearched.setForeground(Color.black);
		listResearched.setFont(new Font("Monospaced", Font.PLAIN, 12));
		listResearched.addItemListener(this);
		pList.add("Center", listResearched);
		pCenter.add("South", pList);

		pTechDescr = new Panel();
		pTechDescr.setLayout(new BorderLayout(2, 2));
		pTechDescr.setBackground(Color.white);
		pTechDescr.setForeground(Color.black);
		lbTechTitel = new Label("Warp 1 drive: ");
		lbTechTitel.setBackground(Color.black);
		lbTechTitel.setForeground(Color.white);
		lbTechTitel.setFont(new Font("Monospaced", Font.BOLD, 12));
		pTechDescr.add("North", lbTechTitel);
		taTechtext = new TextArea();
		taTechtext.setEditable(false);
		taTechtext.setForeground(Color.black);
		taTechtext.setBackground(Color.white);
		taTechtext.setFont(new Font("Monospaced", Font.PLAIN, 12));
		pTechDescr.add("Center", taTechtext);
		pCenter.add("North", pTechDescr);
		add("Center", pCenter);
		// South
		pCommands = new Panel();
		pCommands.setLayout(new GridLayout(1, 8));
		pCommands.setBackground(Color.black);
		pCommands.setForeground(Color.white);
		save = new Button("Save");
		save.addActionListener(this);
		save.setActionCommand("save");
		save.setForeground(Color.black);
		save.setBackground(Color.lightGray);
		close = new Button(CLOSE);
		close.addActionListener(this);
		close.setActionCommand(CLOSE);
		close.setForeground(Color.black);
		close.setBackground(Color.lightGray);
		pCommands.add(new Label(""));
		pCommands.add(new Label(""));
		pCommands.add(new Label(""));
		pCommands.add(new Label(""));
		pCommands.add(new Label(""));
		pCommands.add(new Label(""));
		pCommands.add(save);
		pCommands.add(close);
		add("South", pCommands);
	}

	public void focusGained(java.awt.event.FocusEvent event) {
		String techName = "";
		String proz = "";
		for (int i = 1; i < 7; i++) {
			if (event.getSource() == choice[i]) {
				techName = choice[i].getSelectedItem();
			}
		}
		try {
			Technology techTemp = getTechnology(techName);
			displayTechDescription(techTemp);
			for (int i = 1; i < 7; i++) {
				if (event.getSource() == choice[i]) {
					showPercent[i].setText("   " + techTemp.getComplete() + "%");
				}
			}
		} catch (Exception e) {
			// ignore exception in case no technology available to research
		}
	}

	public void focusLost(java.awt.event.FocusEvent e) {
	}

	public Technology getTechnology(String techName) {
		Technology techTemp = null;
		List<Technology> possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = (Technology) possibleTechnologies.get(i);
			if (tech.getName().equals(techName)) {
				techTemp = (Technology) tech.clone();
				displayTechDescription(techTemp);
				break;
			}
		}
		return techTemp;
	}

	public void itemStateChanged(java.awt.event.ItemEvent e) {
		String techName = "";
		String proz = "";
		for (int i = 1; i < 7; i++) {
			if (e.getSource() == choice[i]) {
				techName = choice[i].getSelectedItem();
			}
		}
		List<Technology> possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = possibleTechnologies.get(i);
			if (tech.getName().equals(techName)) {
				Technology techTemp = (Technology) tech.clone();
				displayTechDescription(techTemp);
				proz = "   " + tech.getComplete() + "%";
				break;
			}
		}
		for (int j = 1; j < 7; j++) {
			if (e.getSource() == choice[j]) {
				showPercent[j].setText(proz);
			}
		}
		if (e.getSource() == listResearched) {
			techName = listResearched.getSelectedItem();
			Technology techTemp = getTechnology(techName);
			displayTechDescription(techTemp);
		}
		if (e.getSource() == chGrpTech) {
			if (listResearched.getItemCount() > 0) {
				listResearched.removeAll();
			}
			displayKnownTechnologies(chGrpTech.getSelectedItem());
		}
	}

	public void init() {
		clearTechnologies();
		displayKnownTechnologies("All");
		addTechnologies();
		noEmptyChoices();
		setScrollBars();
		List<Technology> possibleTechnologies = player.getPossibleTechnologies();
		for (int i = 0; i < possibleTechnologies.size(); i++) {
			Technology tech = (Technology) possibleTechnologies.get(i);
			displayResearchProgress(tech);
		}
	}

	private void noEmptyChoices() {
		for (int i = 1; i < 7; i++) {
			if (choice[i].getItemCount() == 0) {
				choice[i].addItem("No technology to research");
			}
		}
	}

	public void refresh() {
		if (isVisible()) {
			init();
		}
	}

	public void saveSettings() {
		player.setiPhysics(values[1]);
		player.setiBiology(values[2]);
		player.setiMathematics(values[3]);
		player.setiSocial(values[4]);
		player.setiEconomy(values[5]);
		player.setiMilitary(values[6]);
		player.setCurrentPhysics(choice[1].getSelectedItem());
		player.setCurrentBiology(choice[2].getSelectedItem());
		player.setCurrentMathematics(choice[3].getSelectedItem());
		player.setCurrentSocial(choice[4].getSelectedItem());
		player.setCurrentEconomy(choice[5].getSelectedItem());
		player.setCurrentMilitary(choice[6].getSelectedItem());
	}

	private void setScrollBars() {
		values[1] = player.getPhysicsPercent();
		values[2] = player.getBiologyPercent();
		values[3] = player.getMathematicsPercent();
		values[4] = player.getSocialPercent();
		values[5] = player.getEconomyPercent();
		values[6] = player.getMilitaryPercent();
		for (int i = 1; i < 7; i++) {
			sb[i].setValue(values[i]);
		}
	}

	public ScienceDetail(SpaceOpera so) {
		spaceOpera = so;
		player = spaceOpera.getCurrentPlayer();
		setTitle("Science settings");
		doStaticLayout();
		enableCloseButton(this);
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		pack();
		setSize(WINDOWSIZEX, WINDOWSIZEY);
		setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);
		show();
		init();
	}

}
