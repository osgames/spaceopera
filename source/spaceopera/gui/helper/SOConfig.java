//The class SOConfig is used to read/write configurations and settings to files
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.helper;

import java.util.*;
import java.io.*;

// Xerces 1 or 2 additional classes.
import org.apache.xml.serialize.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import org.apache.xerces.parsers.SAXParser;

import spaceopera.gui.SpaceOpera;
import spaceopera.universe.SOConstants;


/** The SOConfig class is used to read/write configurations and settings to files
 */
@SuppressWarnings({"rawtypes", "unchecked", "deprecation"})
public class SOConfig extends DefaultHandler implements SOConstants {

  private SpaceOpera spaceOpera = null;
  private String currentElement = null;
  private int count = 0;
  private boolean animOn = false;
  private boolean musicOn=false;
  private boolean effectsOn = false;
  private boolean fromDir=false;
  private String musicDir = "";
  private String musicFile = "";
  private Vector musicList = null;


  //Methods
  public void characters(char[] ch, int start, int length){
     if (currentElement.equals("Animations")) {
       String str = getText(ch, start, length);
       if (str.equals("on")) {
         animOn = true;
       }
     }
     if (currentElement.equals("Effects")) {
       String str = getText(ch, start, length);
       if (str.equals("on")) {
         effectsOn = true;
       }
     }
     if (currentElement.equals("Music")) {
       String str = getText(ch,start,length);
       if (str.equals("on")) { // else off
         musicOn = true;
       }
     }
     if (currentElement.equals("Source")) {
       String str = getText(ch,start,length);
       if (str.equals("dir")) {  // else list
         fromDir = true;
       }
     }
     if (currentElement.equals("Directory")) {
       musicDir = getText(ch,start,length);
     }


     if (currentElement.equals("List_Count")) {
       count = new Integer(getText(ch,start,length)).intValue();
//System.out.println("List count " + count);
     }
     for (int i=1; i<= count; i++) {
       String songNumber = "song-" + i;
       if (currentElement.equals(songNumber)) {
         musicFile = getText(ch,start,length);
//System.out.println("music " + i + ": " + musicFile);
         musicList.addElement(musicFile);
       }
     }
  }


  private String getText(char[] ch, int start, int length) {
    StringBuffer sb = new StringBuffer();
    for (int i = start; i < start + length; i++) {
      sb.append(ch[i]);
    }
    //System.out.println(sb);
    return(new String(sb));
  }


  public void readConfig(String file) {

    try {
      SAXParser parser = new SAXParser();
      parser.setContentHandler(this);
      parser.setErrorHandler(this);
      parser.parse(file);
    }
    catch (Exception ex) {
      System.out.println("The ext directory has not been setup correctly?" + ex);
    }
    spaceOpera.setAnimConfig(animOn);
    spaceOpera.setMusicConfig(musicOn,fromDir, musicDir, musicList);
    spaceOpera.setEffectConfig(effectsOn);
  }

  public void startElement(String uri, String localName,
     String rawName, Attributes attributes)
  {
     currentElement = rawName;
  }

  public void endElement(String uri, String localName, String qName) {
    currentElement = "";
  }

  public void endDocument()
  {
        //System.out.println("xml file parsed");
  }

  public static void main(String[] args) {
        try {
              SOConfig SOConfig = new SOConfig(null);

              SAXParser parser = new SAXParser();
              parser.setContentHandler(SOConfig);
              parser.setErrorHandler(SOConfig);
              parser.parse(args[0]);
        }
              catch (Exception ex) {
                    System.out.println("Test SOConfig main: " + ex);
              }
  }


  public void setAnimConfig(boolean animOnOff) {

      // example anim.xml
      //<?xml version="1.0" encoding="ISO-8859-1"?>
      //<ANIMATION>
      //    <Animations>off</Animations>
      //</ANIMATION>


      try {
        FileOutputStream fos = new FileOutputStream("resources/config/anim.xml");
		OutputFormat of = new OutputFormat("XML","ISO-8859-1",true);
        of.setIndent(1);
        of.setIndenting(true);
        //of.setDoctype(null,"SpaceOpera.dtd");
        XMLSerializer serializer = new XMLSerializer(fos,of);
        ContentHandler ch = serializer.asContentHandler();
        ch.startDocument();
        //ch.processingInstruction("xml-stylesheet","type=\"text/xsl\"href=\"spaceopera.xsl\"");
        AttributesImpl ai = new AttributesImpl();
        ch.startElement("","","ANIMATION",ai);
        String onoff;
        if (animOnOff) {
          onoff = "on";
        } else {
          onoff = "off";
        }
        ai.clear();
        //ai.addAttribute("","","Version","CDATA","short");
        ch.startElement("","","Animations",ai);
        ch.characters(onoff.toCharArray(),0,onoff.length());
        ch.endElement("","","Animations");
        ch.endElement("","","ANIMATION");
        ch.endDocument();
        fos.close();

      } catch(Exception e) {
        System.out.println("SOConfig (writeAnimConfig): Exception " + e);
      }

  }

  public void setEffectsConfig(boolean effectsOnOff) {

      // example effects.xml
      //<?xml version="1.0" encoding="ISO-8859-1"?>
      //<EFFECTS>
      //    <Effects>off</Effects>
      //</EFFECTS>


      try {
        FileOutputStream fos = new FileOutputStream("resources/config/effects.xml");
        OutputFormat of = new OutputFormat("XML","ISO-8859-1",true);
        of.setIndent(1);
        of.setIndenting(true);
        //of.setDoctype(null,"SpaceOpera.dtd");
        XMLSerializer serializer = new XMLSerializer(fos,of);
        ContentHandler ch = serializer.asContentHandler();
        ch.startDocument();
        //ch.processingInstruction("xml-stylesheet","type=\"text/xsl\"href=\"spaceopera.xsl\"");
        AttributesImpl ai = new AttributesImpl();
        ch.startElement("","","EFFECTS",ai);
        String onoff;
        if (effectsOnOff) {
          onoff = "on";
        } else {
          onoff = "off";
        }
        ai.clear();
        //ai.addAttribute("","","Version","CDATA","short");
        ch.startElement("","","Effects",ai);
        ch.characters(onoff.toCharArray(),0,onoff.length());
        ch.endElement("","","Effects");
        ch.endElement("","","EFFECTS");
        ch.endDocument();
        fos.close();

      } catch(Exception e) {
        System.out.println("SOConfig (writeEffectConfig): Exception " + e);
      }

  }


  public void setMusicConfig(boolean musicOnOff, boolean musicFromDir,
                            String musicDir, Vector musicList) {
      try {
        FileOutputStream fos = new FileOutputStream("resources/config/music.xml");
        OutputFormat of = new OutputFormat("XML","ISO-8859-1",true);
        of.setIndent(1);
        of.setIndenting(true);
        //of.setDoctype(null,"SpaceOpera.dtd");
        XMLSerializer serializer = new XMLSerializer(fos,of);
        ContentHandler ch = serializer.asContentHandler();
        ch.startDocument();
        //ch.processingInstruction("xml-stylesheet","type=\"text/xsl\"href=\"spaceopera.xsl\"");
        AttributesImpl ai = new AttributesImpl();
        ch.startElement("","","MUSIC",ai);
        String onoff; // on or off
        String fromDir; // dir or list
        String source; // dir name or list entry
        if (musicOnOff) {
          onoff = "on";
        } else {
          onoff = "off";
        }
        //ai.addAttribute("","","Version","CDATA","short");
        ch.startElement("","","Music",ai);
        ch.characters(onoff.toCharArray(),0,onoff.length());
        ch.endElement("","","Music");
        if (musicFromDir) {

          //example music.xml
          //
          //<?xml version="1.0" encoding="ISO-8859-1"?>
          //<music>
          //    <Music>on</Music>
          //    <Source>dir</Source>
          //    <Directory>/test/music/xxx</Directory>
          //</music>

          ch.startElement("","","Source",ai);
          fromDir = "dir";
          ch.characters(fromDir.toCharArray(),0,fromDir.length());
          ch.endElement("","","Source");

          ch.startElement("","","Directory",ai);
          source = musicDir;
          ch.characters(source.toCharArray(),0,source.length());
          ch.endElement("","","Directory");


        } else {

          //example music.xml
          //
          //<?xml version="1.0" encoding="ISO-8859-1"?>
          //<MUSIC>
          //    <Music>on</Music>
          //    <Source>list</Source>
          //    <List_Count>3</List_Count>
          //    <song-1>/home/music/spacemusic.au</song-1>
          //    <song-2>/home/music/SpaceOperaTheme.wav</song-2>
          //    <song-3>/home/music/song.mid</song-3>

          //</MUSIC>


          ch.startElement("","","Source",ai);
          fromDir = "list";
          ch.characters(fromDir.toCharArray(),0,fromDir.length());
          ch.endElement("","","Source");

          //ai.clear();
          //ai.addAttribute("","","count","CDATA",new Integer(musicList.size()).toString());
          ch.startElement("", "", "List_Count", ai);
          source = new Integer(musicList.size()).toString();
          ch.characters(source.toCharArray(), 0, source.length());
          ch.endElement("","","List_Count");

          for (int i=0; i<musicList.size(); i++) {
            String music = "song-" + (i+1);
            source =(String)musicList.elementAt(i);
            ch.startElement("", "", music, ai);
            //dir = musicDir;
            ch.characters(source.toCharArray(), 0, source.length());
            ch.endElement("","",music);
            //ai.addAttribute("","",music,"CDATA",(String)musicList.elementAt(i));
          }

        }
        ch.endElement("","","MUSIC");
        ch.endDocument();
        fos.close();

      } catch(Exception e) {
        System.out.println("SOConfig (writeMusicConfig):  Exception " + e);
      }

  }


  public SOConfig (SpaceOpera so) {
      spaceOpera = so;
      musicList = new Vector();
  }


}