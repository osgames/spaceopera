/*
 * Created on 20.02.2004
 *
 */
package spaceopera.gui.helper;

/**
 * @author S.Retter
 * @version 0.1 
 */

import java.io.*;
import java.util.*;

import spaceopera.universe.Technology;
import spaceopera.universe.Universe;
import spaceopera.universe.colony.Building;
import spaceopera.universe.colony.Product;
import spaceopera.universe.ships.ShipComponent;
@SuppressWarnings({"rawtypes"})
public class TechTreeExport
{
	
	private Vector b;		//private Building b;
	private Vector p;		//private Product p;
	private Vector t;		//private Technology t;
	private Vector sc;		//private ShipComponent sc;
	
	public void export2Html(Universe u)
	{		
		String acls="";
		String ocls="";
		//make menu		
		try{
			FileWriter fw1 = new FileWriter("index.html"); 			
			fw1.write("<html>\n");
			fw1.write("<frameset cols='200,*' border=1>\n");
			fw1.write("<frame src='menu.html' name='menu'>\n");
			fw1.write("<frame src='info.html' name='info'>\n");
			fw1.write("</html>\n");
			fw1.close();
						
			FileWriter fw2 = new FileWriter("info.html"); 						
			fw2.write("<html><body bgcolor=#444444 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw2.write("<center>\n");
			fw2.write("<br><br><br><h2>SpaceOpera TechTree-Infos</h2>\n");
			fw2.write("</center>\n");
			fw2.write("</html>\n");
			fw2.close();

			FileWriter fw3 = new FileWriter("menu.html"); 			
			fw3.write("<html>\n");
			fw3.write("<html><body bgcolor=#444444 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");						
			fw3.write("<center>\n");
			fw3.write("<br>SpaceOpera TechTree<br><br>\n");
			fw3.write("<a href='buildings.html' target='info' >Buildings</a><br>\n");
			fw3.write("<a href='technology.html' target='info'>Technology</a><br>\n");
			fw3.write("<a href='shipcomponents.html' target='info'>Ship-Components</a><br>\n");
			fw3.write("<a href='products.html' target='info'>Products</a><br><br>\n");
			fw3.write("<a href='tree.html' target='info'>Complete Tree</a><br>\n");
			fw3.write("</center>\n");
			fw3.write("</html>\n");
			fw3.close();						
		}
		catch (IOException e) {			
			System.err.println(e);
		}
		
		
		//make technology.html
		t = u.getTechnologies();		
		try{
			FileWriter fw = new FileWriter("technology.html"); 			
			fw.write("<html><body bgcolor=#333333 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw.write("<center><h2>Technology</h2><br><table border=1 cellspacing=1 cellpadding=2 width=90%>\n");			
			
			fw.write("<tr>\n");
			fw.write("<th>#Id</th>\n");
			fw.write("<th>Name</th>\n");			
			fw.write("<th>Description</th>\n");			
			
			ocls = "";
			for (int i=0; i<t.size(); i++){				
				Technology te = (Technology)t.elementAt(i);
				acls=te.getCls();
				if (acls.equals(ocls)){
					fw.write("<tr>\n");
					fw.write("<td>"+te.getId()+"</td>\n");
				//	fw.write("<td><img src='images/"+bo.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+te.getName()+"</td>\n");
					fw.write("<td>"+te.getDescription()+"</td></tr>\n");
				}
				if (! acls.equals(ocls)){
					fw.write("<tr><td bgcolor=#444444 colspan=5>"+te.getCls());
					ocls = acls;
					fw.write("<tr>\n");
					fw.write("<td>"+te.getId()+"</td>\n");
					//	fw.write("<td><img src='images/"+bo.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+te.getName()+"</td>\n");
					fw.write("<td>"+te.getDescription()+"</td></tr>\n");
				}
			}
									
			fw.write("</table></html>\n");
			fw.close();
		}
		catch (IOException e) {			
			System.err.println(e);
		}
		
		//make products.html
		p = u.getProducts();		
		try{
			FileWriter fw = new FileWriter("products.html"); 			
			fw.write("<html><body bgcolor=#333333 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw.write("<center><h2>Products</h2><br><table border=1 cellspacing=1 cellpadding=2 width=90%>\n");			
			
			fw.write("<tr>\n");
			fw.write("<th>#Id</th>\n");
			fw.write("<th>.</th>\n");
			fw.write("<th>Name</th>\n");
			fw.write("<th>Description</th>\n");
			fw.write("<th>Effect</th></tr>\n");
			
			for (int i=0; i<p.size(); i++){
				Product pr = (Product)p.elementAt(i);				
				fw.write("<tr>\n");
				fw.write("<td>"+pr.getId()+"</td>\n");
				fw.write("<td><img src='images/"+pr.getPicture()+".jpg'></td>\n");
				fw.write("<td>"+pr.getName()+"</td>\n");
				fw.write("<td>"+pr.getDescription()+"</td>\n");
				fw.write("<td> </td></tr>\n");				
			}
						
			fw.write("</table></html>\n");
			fw.close();
		}
		catch (IOException e) {			
			System.err.println(e);
		}

		//make shipcomponents.html
		sc = u.getSOComponents();	
		try{
			FileWriter fw = new FileWriter("shipcomponents.html"); 			
			fw.write("<html><body bgcolor=#333333 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw.write("<center><h2>Ship-Components</h2><br><table border=1 cellspacing=1 cellpadding=2 width=90%>\n");			
			fw.write("<tr>\n");
			fw.write("<th>#Id</th>\n");
			fw.write("<th>.</th>\n");
			fw.write("<th>Name</th>\n");
			fw.write("<th>Description</th>\n");
			fw.write("<th>Effect</th></tr>\n");
			
			ocls = "";
			for (int i=0; i<sc.size(); i++){
				ShipComponent s = (ShipComponent)sc.elementAt(i);				
				acls=s.getType();
				if (acls.equals(ocls)){
					fw.write("<tr>\n");
					fw.write("<td>"+s.getId()+"</td>\n");
					fw.write("<td><img src='images/"+s.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+s.getName()+"</td>\n");
					fw.write("<td>"+s.getDescription()+"</td>\n");
					fw.write("<td> </td></tr>\n");
				}
				if (! acls.equals(ocls)){
					fw.write("<tr><td bgcolor=#444444 colspan=5>"+s.getType());
					ocls = acls;
					fw.write("<tr>\n");
					fw.write("<td>"+s.getId()+"</td>\n");
					fw.write("<td><img src='images/"+s.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+s.getName()+"</td>\n");
					fw.write("<td>"+s.getDescription()+"</td>\n");
					fw.write("<td> </td></tr>\n");					
				}
			}					
			fw.write("</table></html>\n");
			fw.close();
		}
		catch (IOException e) {			
			System.err.println(e);
		}
				
		//make buildings.html
		b = u.getBuildings();
		try{
			FileWriter fw = new FileWriter("buildings.html"); 			
			fw.write("<html><body bgcolor=#333333 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw.write("<center><h2>Buildings</h2><br><table border=1 cellspacing=1 cellpadding=2 width=90%>\n");			
			fw.write("<tr>\n");
			fw.write("<th>#Id</th>\n");
			fw.write("<th>.</th>\n");
			fw.write("<th>Name</th>\n");
			fw.write("<th>Description</th>\n");
			fw.write("<th>Effect</th></tr>\n");

			ocls = "";
			for (int i=0; i<b.size(); i++){
				Building bo = (Building)b.elementAt(i);								
				acls=bo.getType();
				if (acls.equals(ocls)){									
					fw.write("<tr>\n");
					fw.write("<td>"+bo.getId()+"</td>\n");
					fw.write("<td><img src='images/"+bo.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+bo.getName()+"</td>\n");
					fw.write("<td>"+bo.getDescription()+"</td>\n");
					fw.write("<td> </td></tr>\n");				
				}				
				if (! acls.equals(ocls)){
					fw.write("<tr><td bgcolor=#444444 colspan=5>"+bo.getType());
					ocls = acls;
					fw.write("<tr>\n");
					fw.write("<td>"+bo.getId()+"</td>\n");
					fw.write("<td><img src='images/"+bo.getPicture()+".jpg'></td>\n");
					fw.write("<td>"+bo.getName()+"</td>\n");
					fw.write("<td>"+bo.getDescription()+"</td>\n");
					fw.write("<td> </td></tr>\n");	
				}
			}
			fw.write("</table></center>\n");
			fw.write("</html>\n");
			fw.close();
		}
		catch (IOException e) {			
			System.err.println(e);
		}
		
		//make tree.html
		sc = u.getSOComponents();	
		try{
			FileWriter fw = new FileWriter("tree.html"); 			
			fw.write("<html><body bgcolor=#333333 text=#FFCC99 link=#FF9966 vlink=#FF9966 alink=#FFFFFF></body>\n");			
			fw.write("<center><h2>Complete SpaceOpera - TechTree</h2><br>\n");			
			fw.write("</html>\n");
			fw.close();
		}
		catch (IOException e) {			
			System.err.println(e);
		}
		
	}	
}
