//The class SOJar is used to access resource files in a jar file
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

package spaceopera.gui.helper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

@SuppressWarnings({"rawtypes", "unchecked"})
public class SOJar {

	private String jarFileName;
	private Hashtable zipSizes;

	// public SOJar(String jarFileName) {
	public SOJar() {
		zipSizes = new Hashtable();
		this.jarFileName = "j:\\so\\spaceopera\\SO_Resource.jar";
		init();
	}

	public InputStream getInputStream(String entryName) {
		InputStream input = null;
		try {
			JarFile jarFile = new JarFile(jarFileName);
			JarEntry entry = jarFile.getJarEntry(entryName);
			input = jarFile.getInputStream(entry);
		} catch (Exception e) {
			System.out.println("Error reading jar resource: " + e);
		}
		return (input);
	}

	public byte[] getResource(String name) {
		byte myResource[] = null;
		ZipInputStream zipInputStream = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(jarFileName);
			BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
			zipInputStream = new ZipInputStream(bufferedInputStream);
			ZipEntry zipEntry = null;
			while ((zipEntry = zipInputStream.getNextEntry()) != null) {
				if (zipEntry.isDirectory()) {
					continue;
				}
				String entryName = zipEntry.getName();
				if (entryName.equals(name)) {
					int size = (int) zipEntry.getSize();
					if (size == -1) {
						size = ((Integer) zipSizes.get(zipEntry.getName())).intValue();
					}
					myResource = new byte[(int) size];
					int offset = 0;
					int chunk = 0;
					while ((size - offset) > 0) {
						chunk = zipInputStream.read(myResource, offset, size - offset);
						if (chunk == -1) {
							break;
						}
						offset += chunk;
					}
				}
			}
			zipInputStream.close();
		} catch (Exception e) {
			System.out.println("Jar Resource error: " + e);
		}
		return (myResource);
	}

	public int getResourceSize(String name) {
		int size = -1; // -1 means 'Resource not found'
		try {
			ZipFile zf = new ZipFile(jarFileName);
			Enumeration e = zf.entries();
			while (e.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) e.nextElement();
				if (zipEntry.getName().equals(name)) {
					size = (int) zipEntry.getSize();
				}
			}
			zf.close();
		} catch (Exception e) {
			System.out.println("Jar Resource Size error: " + e);
		}
		return (size);
	}

	private void init() {
		try {
			ZipFile zipFile = new ZipFile(jarFileName);
			Enumeration fileList = zipFile.entries();
			while (fileList.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) fileList.nextElement();
				zipSizes.put(zipEntry.getName(), new Integer((int) zipEntry.getSize()));
			}
			zipFile.close();
		} catch (Exception e) {
			System.out.println("Zipfile error: " + e);
		}
	}

}