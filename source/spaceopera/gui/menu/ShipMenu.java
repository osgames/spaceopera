//The class ShipMenu is used for a popupmenu for ships in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.menu;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import spaceopera.test.Fullscreen;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The ShipMenu is a popup menu for ship commands (set order, display order,
 * dismiss, recall, ... (?))
 */
public class ShipMenu extends PopupMenu implements ActionListener {

	private static final long serialVersionUID = 1L;
	private SpaceCraft ship = null;
	//private SpaceOpera spaceOpera = null;
	private Fullscreen fs;

	String[] labels = new String[] { "Ship Orders" };

	String[] commands = new String[] { "orders" };

	public ShipMenu(SpaceCraft rs, Fullscreen fs) {
		super("Ship Menu");
		//spaceOpera = so;
		this.fs = fs;
		ship = rs;
		for (int i = 0; i < labels.length; i++) {
			MenuItem mi = new MenuItem(labels[i]);
			mi.setActionCommand(commands[i]);
			mi.addActionListener(this);
			if (commands[i].equals("orders")) {
				mi.enable(true);
			} else if (commands[i].equals("drag")) {
				mi.enable(true);
			} else {
				mi.enable(false);
			}
			add(mi);
		}
	}

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("orders")) {
			// set orders
			fs.displayShipOrders();
		}

	}
}