//The class PlanetMenu is the popupmenu for planets in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.menu;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;
import spaceopera.universe.elements.Planet;
import spaceopera.universe.elements.Sun;
import spaceopera.universe.ships.Ship;
import spaceopera.universe.ships.SpaceCraft;

/**
 * The PlanetMenu is a popup menu for displaying planets, colonies, colonizing
 * planets or attacking colonies, and maybe others...
 */
@SuppressWarnings({ "rawtypes" })
public class PlanetMenu extends PopupMenu implements ActionListener, SOConstants {
	private static final long serialVersionUID = 1L;
	private Planet planet = null;
	private Fullscreen fs;
	private Sun sun = null;
	private Ship colonizingShip = null;

	private String[] labels = new String[] { "Display planet", "Display colony", "colonize planet", "display asteroid belt",
			"Attack colony" };

	private String[] commands = new String[] { "planet", "colony", COLONIZE, "asteroidbelt", "attack" };

	// Methods
	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("planet")) {
			// display planet details
			fs.displayPlanetImage(planet);
		} else if (c.equals("asteroidbelt")) {
			// display asteroid belt details
			fs.displayPlanetImage(planet);
		} else if (c.equals("colony")) {
			// display colony
			fs.displayColonyImage(planet.getColony());
		}
//			if (fs.getColonyDetail() != null) {
//				fs.disposeColonyDetail();
//			}
//			fs.setColonyDetail(new ColonyDetail(planet.getColony(), fs));
//		} else if (c.equals(COLONIZE)) {
//			// colonize planet
//			if (!planet.isColonized()) {
//				if (colonizingShip != null) {
//					planet.colonize(colonizingShip);
//				}
//			}
//		}
//		} else if (c.equals("attack")) {
//			// Attack colony
//			if (planet.isColonized()) {
//				fs.executeFight(sun, true);
//			}
//		}
	}

	public PlanetMenu(Fullscreen fs, Sun s, Planet p) {
		super("Planet Menu");
		planet = p;
		this.fs = fs;
		sun = s;
		MenuItem mi = null;
		int i = 0;
		if (planet.getType().equals(ASTEROIDBELT)) {
			i = 3; // Asteroid belt display
			mi = new MenuItem(labels[i]);
			mi.setActionCommand(commands[i]);
			mi.addActionListener(this);
			add(mi);
		} else {
			i = 0; // Planet display
			mi = new MenuItem(labels[i]);
			mi.setActionCommand(commands[i]);
			mi.addActionListener(this);
			add(mi);
		}
		if (planet.isColonized()) {
			if ((planet.getPlayer() == fs.getCurrentPlayer()) || (fs.allowColonyDetail())) {
				i = 1; // Colony display
				mi = new MenuItem(labels[i]);
				mi.setActionCommand(commands[i]);
				mi.addActionListener(this);
				add(mi);
			} else {
				Vector ships = sun.getSpaceCraft();
				for (int j = 0; j < ships.size(); j++) {
					SpaceCraft ship = (SpaceCraft) ships.elementAt(j);
					if (ship instanceof Ship) {
						if (ship.getPlayer() == fs.getCurrentPlayer()) {
							if (ship.getWeapons().size() > 0) {
								i = 4; // Attack Colony
								mi = new MenuItem(labels[i]);
								mi.setActionCommand(commands[i]);
								mi.addActionListener(this);
								add(mi);
								break;
							}
						}
					}
				}
			}
		} else {
			i = 2; // colonize
			// if there's a ship in Orbit and the planet is suitable
			Vector ships = sun.getSpaceCraft();
			for (int j = 0; j < ships.size(); j++) {
				SpaceCraft ship = (SpaceCraft) ships.elementAt(j);
				if ((ship.getPlayer() == fs.getCurrentPlayer()) && (ship.canColonize(planet))) {
					// if (p.getType().equals(EARTHLIKE)) {
					mi = new MenuItem(labels[i]);
					mi.setActionCommand(commands[i]);
					mi.addActionListener(this);
					add(mi);
					colonizingShip = (Ship) ship;
					break;
					// }
				}
			}
		}
	}
}
