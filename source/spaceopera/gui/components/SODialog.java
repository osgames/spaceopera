//The class SODialog is the parent class for dialog style classes in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.Dialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.ShipYard;
import spaceopera.gui.window.SpaceOperaDisplay;
import spaceopera.test.Fullscreen;

/**
 * This class is used to enable the close button for dialogs
 */
public class SODialog extends Dialog {

	private static final long serialVersionUID = 1L;

	// Methods
	public void enableCloseButton(Dialog me) {
		me.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
	}

	public SODialog(Fullscreen fs) {
		super(fs, true);
	}
	
	public SODialog(SpaceOperaDisplay s) {
		super(s, true);
	}

	public SODialog(ShipYard s) {
		super(s);
	}

}