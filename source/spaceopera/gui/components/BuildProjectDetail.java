//The class BuildProjectDetail is the parent class for ProductionQueueDetail and ExistingBuildingsDetail
//Copyright (C) 1996-2006 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.BuildProject;

public abstract class BuildProjectDetail extends JDialog implements ActionListener, SOConstants {

	
	private static final long serialVersionUID = 1L;
	protected JButton quit;
	protected JPanel mainPanel, labelPanel, imagePanel, topPanel, pCenter, pCommands;
	protected JLabel listTitle, imageLabel;
	protected ColonyDetail colonyDetail;
	protected BuildProject buildProject;
	protected SpaceOpera spaceOpera;
	protected JList resourceListDetail;
	
	public BuildProjectDetail(SpaceOpera so, ColonyDetail cd, BuildProject p) {
		spaceOpera = so;
		colonyDetail = cd;
		buildProject = p;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		UIManager.put("Label.foreground", Color.white);
		doStaticLayout();
		init();

		setSize(340,120);
		pack();
		setResizable(false);
		position();
		setVisible(true);		
		show();
	}

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("quit")) {
			dispose();
		}
	}

	private void doStaticLayout() {
		mainPanel = new JPanel();
		mainPanel.setOpaque(true);
		mainPanel.setBackground(Color.black);
		mainPanel.setForeground(Color.white);
		mainPanel.setLayout(new BorderLayout(0, 0));

		listTitle = new JLabel();
		listTitle.setBackground(Color.black);
		listTitle.setForeground(Color.white);
		listTitle.setFont(new Font("Monospaced", Font.BOLD, 12));
		mainPanel.add("North",listTitle);
		
		pCenter = new JPanel();
		pCenter.setLayout(new BoxLayout(pCenter, BoxLayout.LINE_AXIS));
		pCenter.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		pCenter.setBackground(Color.black);
		pCenter.setForeground(Color.white);
		
		labelPanel = new JPanel();
		labelPanel.setOpaque(true);
		labelPanel.setLayout(new GridLayout(1, 1));
		labelPanel.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));
		labelPanel.setBackground(Color.black);
		labelPanel.setForeground(Color.white);

		resourceListDetail = new JList();      
		resourceListDetail.setBackground(Color.black);
		resourceListDetail.setForeground(Color.white);
		resourceListDetail.setFont(new Font("Monospaced", Font.BOLD, 12));
        JScrollPane productionQueueScrollPane = new JScrollPane(resourceListDetail);
        productionQueueScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pCenter.add(productionQueueScrollPane);

	    imagePanel = new JPanel();
	    imagePanel.setBackground(Color.black);
	    imagePanel.setForeground(Color.white);	    
	    imageLabel = new JLabel();
		pCenter.add(imagePanel);
		
		mainPanel.add("Center",pCenter);

		pCommands = new JPanel();
		pCommands.setLayout(new GridLayout(1, 4));
		pCommands.setBackground(Color.black);
		pCommands.setFont(new Font("Lucida Sans Regular", Font.PLAIN, 11));

		quit = new JButton("Quit");
		quit.setActionCommand("quit");
		quit.addActionListener(this);

		pCommands.add(new JLabel(""));
		pCommands.add(new JLabel(""));
		pCommands.add(quit);

		mainPanel.add("South",pCommands);
		getContentPane().add(mainPanel);
	}

	public void showImage() {
	    String image = buildProject.getPicture();
	    String path = "images/objects/"+image+".jpg";
	    imageLabel.setPreferredSize(new Dimension(150,150));

	    try {
	    	imageLabel.setIcon(getImageIcon(path));   
	    	imageLabel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	    	imagePanel.add(imageLabel);
	    } catch (Exception e) {
	        System.err.println("ProductionQueueDetail exception " + e + ", couldn't find file: " + path);
	        path="images/objects/building.jpg";
	    	imageLabel.setIcon(getImageIcon(path));   
	    	imageLabel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	    	imagePanel.add(imageLabel);
	    } 
	}

	/**
	 * @param path path to image file
	 * @return ImageIcon with loaded image or null
	 */
	private ImageIcon getImageIcon(String path) {
		ImageIcon ii = null;
		try {
		ii = new ImageIcon(ClassLoader.getSystemResource(path));   	
		ii.setImage(ii.getImage().getScaledInstance(150,150,Image.SCALE_SMOOTH));
		} catch (Exception e) {
			System.out.println("Exception in BuildProjectDetail: " + e + ", image path = " + path);
		}
		return ii;
	}
	
	public abstract void init();
	public abstract void refresh();
	public abstract void position();
}