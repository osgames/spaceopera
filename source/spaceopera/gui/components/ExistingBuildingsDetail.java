//The class ExistingBuildingsDetail shows details of existing buildings and ships of a colony 
//Copyright (C) 1996-2006 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.universe.SOConstants;
import spaceopera.universe.colony.BuildProject;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ExistingBuildingsDetail extends BuildProjectDetail implements ActionListener, SOConstants {

	private static final long serialVersionUID = 1L;

	public ExistingBuildingsDetail(SpaceOpera so, ColonyDetail cd, BuildProject p) {
		super(so, cd, p);
	}

	public void actionPerformed(ActionEvent event) {
		String c = event.getActionCommand();
		if (c.equals("quit")) {
			dispose();
		}
	}

	public void init() {
		setTitle("Support cost overview");
		resourceListDetail.removeAll();
		Vector listData = new Vector();
		if (buildProject != null) {
			listTitle.setText("Support cost per " + buildProject.getName() + ":");
			for (int i = R_MIN; i < R_MAX; i++) {
				if (buildProject.getSupportCost(i) > 0) {
					// XXX: print name and required support cost and percent
					// efficience/available???
					String str = spaceOpera.getResourceName(i) + "                             ";
					str = str.substring(0, 15);
					double units = buildProject.getSupportCost(i);
					str = str + units + "     ";
					str = str.substring(0, 20) + " " + spaceOpera.getResourceUnit(i);

					listData.addElement(str);
					// System.out.println(spaceOpera.getResourceName(i) + ": " +
					// buildProject.getPercentComplete(i) + "% complete");
				}
			}
			showImage();
		}
		resourceListDetail.setListData(listData);
	}

	public void refresh() {
		List<BuildProject> bQ = colonyDetail.getColony().getBuildings();
		if (!bQ.contains(buildProject)) {
			if (bQ.size() > 0) {
				buildProject = (BuildProject) bQ.get(0);
			} else {
				buildProject = null;
			}
		}
		if (isVisible()) {
			init();
		}
	}

	public void position() {
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dim = kit.getScreenSize();
		setLocation((dim.width - WINDOWSIZEX), (dim.height - getHeight()));
	}
}