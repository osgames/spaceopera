//The class MessageBox displays messages in the SpaceOpera game (some VB heritage 8-)
//Copyright (C) 1996-2003 Lorenz Beyeler
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.*;
import java.awt.event.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.ShipYard;
import spaceopera.universe.SOConstants;

/** A messageBox, used to display simple modal text information
 */
public class MessageBox extends SODialog implements ActionListener,
                                                         SOConstants {


  private static final long serialVersionUID = 1L;
private TextArea ta1;
  private Button close;
  private Panel commandPanel;

  public void actionPerformed (ActionEvent event) {
    String c = event.getActionCommand();
    if (c.equals(CLOSE)) {
        this.dispose();
    }
  }

  private void doStaticLayout(){
      setLayout(new BorderLayout(2,2));
      setBackground(Color.lightGray);
      setForeground(Color.black);
      add("Center", ta1);
      commandPanel = new Panel();
      commandPanel.setLayout(new GridLayout(1,5));
      commandPanel.setBackground(Color.lightGray);
      commandPanel.setForeground(Color.black);
      close = new Button(CLOSE);
      close.addActionListener(this);
      close.setActionCommand(CLOSE);
      close.setForeground(Color.black);
      close.setBackground(Color.lightGray);
      commandPanel.add(new Label(""));
      commandPanel.add(new Label(""));
      commandPanel.add(close);
      commandPanel.add(new Label(""));
      commandPanel.add(new Label(""));
      add("South",commandPanel);
  }


  private void init() {
      doStaticLayout();
      enableCloseButton(this);
      setLocation(WINDOWSIZEX/2,WINDOWSIZEY/2);
      pack();
      setSize(MESSAGEBOXSIZEX,MESSAGEBOXSIZEY);
      show();
  }


  public MessageBox (ShipYard s, String title, String message) {
      super(s);
      setTitle(title);
      setText(message);
      init();
  }

  public MessageBox (SpaceOpera s, String title, String message) {
      super(s.getDisplay());
      setTitle(title);
      setText(message);
      init();
  }

//  public MessageBox (ColonyDetail cd, String title, String message) {
//      super(cd);
//      setTitle(title);
//      setText(message);
//      init();
//  }

  private void setText(String message) {
      ta1 = new TextArea("");
      int start = 0;
      int lineLength = 55;
      String temp = "";
      while (message.length()>lineLength) {
         int mi = message.indexOf("\n");
         if ((mi == -1) || (mi > lineLength)) {
           for (int j = lineLength; j>35; j--)  {
              if (message.charAt(j) == ' ') {
                 ta1.append(message.substring(start,j)+"\n");
                 message = message.substring(j+1);
                 break;
              }
           }
         } else {
            ta1.append(message.substring(start,mi)+"\n");
            message = message.substring(mi+1);
         }
      }
      ta1.append(message);
  }

}