//The class RangeMarker is used for the GUI in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.Color;
import java.awt.Graphics;

/**
 * class Rangemarker shows distance from Shipposition to targetposition on the
 * universe display
 */
public class RangeMarker {

	private boolean targetSelected = false;
	private Color myColor;
	private int aX;
	private int aY;
	private int bX;
	private int bY;

	// access methods
	public boolean isTargetSelected() {
		return (targetSelected);
	}

	public void setTargetSelected(boolean b) {
		targetSelected = b;
	}

	// Methods
	public void display(Graphics g) {
		if (targetSelected) {
			myColor = Color.green;
		} else {
			myColor = Color.red;
		}
		g.setColor(myColor);
		g.drawLine(aX, aY, bX, bY);
		return;
	}

	public void clear(Graphics g) {
		g.setColor(Color.black);
		g.drawLine(aX, aY, bX, bY);
		return;
	}

	public RangeMarker(int x1, int y1, int x2, int y2) {
		aX = x1;
		aY = y1;
		bX = x2;
		bY = y2;
	}

}