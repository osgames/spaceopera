//The class SOEvent is used to create Events from the Resource Data
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

/** Class SOEvent contains the event data
  */
public class SOEvent {
	private int id=0;
	private String name="";
	private String title="";
	private String descr="";
	private String picture="";
	private String animation="";
	
	public SOEvent(int id, String name, String title, String descr,
	               String picture, String animation) {
	    this.id = id;
	    this.name = name;
	    this.title = title;
	    this.descr = descr;
	    this.picture = picture;
	    this.animation = animation;               	
	}	
	
	public String getAnimation() {return(animation);}
	public String getDescription() {return(descr);}
	public String getName() {return(name);}
	public String getPicture() {return(picture);}
	public String getTitle() {return(title);}
}