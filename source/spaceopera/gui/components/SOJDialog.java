//The class SODialog is the parent class for dialog style classes in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//

//reworked by SR-040224 to change in future to Swing

package spaceopera.gui.components;

import java.awt.event.*;
import javax.swing.*;

import spaceopera.gui.SpaceOpera;
import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.ShipYard;

/**
 * This class is used to enable the close button for dialogs
 */
public class SOJDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	// Methods
	public void enableCloseButton(JDialog me) {
		me.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
	}

	public SOJDialog(SpaceOpera so) {
		super(so.getDisplay(), true);
	}

	public SOJDialog(ShipYard s) {
		super(s);
	}

//	public SOJDialog(ColonyDetail s) {
//		super(s);
//	}

}