//The class ImageCanvas is a wrapper class for images in the SpaceOpera game
//Copyright (C) 1996-2008 The SpaceOpera Team
//
//This program is free software; all accompanying material like
//sound files, images, or prose is also free; you can redistribute
//it or modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//For information about SpaceOpera and its authors, please visit
//the SpaceOpera Web Site at http://spaceopera.sourceforge.net/
//
package spaceopera.gui.components;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.image.ImageProducer;
import java.io.File;
import java.net.URL;

import javax.swing.JFrame;

import spaceopera.gui.window.ColonyDetail;
import spaceopera.gui.window.PlanetDetail;
import spaceopera.gui.window.ShipYard;
import spaceopera.gui.window.SpaceOperaDisplay;
import spaceopera.test.Fullscreen;
import spaceopera.universe.SOConstants;

/** The container class for images (for colonies and planets).
 */
public class ImageCanvas extends Canvas implements SOConstants {

    private static final long serialVersionUID = 1L;
	private Image image;
    private String imgPath;
    private boolean imageLoadError = false;
    private boolean loadOK = false;
    private URL source = null;
    private Toolkit t = Toolkit.getDefaultToolkit();
    private Object myObject;
    private Container parent;
    private MediaTracker mediaTracker;


//    // Methods
//	public synchronized Dimension getImageDimensions(Image im) throws ImageNotFoundException {
//		int width;
//		int height;
//		while ((width = im.getWidth(this)) < 0) {
//			try {
//				wait();
//			} catch (InterruptedException e) {/* ignore this error */
//			}
//			if (imageLoadError) {
//				throw new ImageNotFoundException(im.getSource());
//			}
//		}
//		while ((height = im.getHeight(this)) < 0) {
//			try {
//				wait();
//			} catch (InterruptedException e) {/* ignore this error */
//			}
//			if (imageLoadError) {
//				throw new ImageNotFoundException(im.getSource());
//			}
//		}
//		// XXX: only for SUNSYSTEM?
//		// return new Dimension(SUNSYSTEMFRAMEX, SUNSYSTEMFRAMEY);
//		return new Dimension(0, 0);
//	}

    public void display() {
        if (loadOK) {
          repaint();
        }
    }

    public ImageCanvas(JFrame any, String path) {
        parent = (Frame) any;
        imgPath = path;
     }
    
//    public ImageCanvas(SpaceOperaDisplay sod, String path) {
//       parent = (Frame) sod;
//       imgPath = path;
//    }
//
//

//
//
//    public ImageCanvas(ColonyDetail cd, String path) {
//       parent = (Frame) cd;
//       imgPath = path;
//    }

    
    public ImageCanvas(Panel p, String path) {
       parent = p;
       imgPath = path;
    }



    public synchronized boolean imageUpdate(Image img, int infoFlags,
        int x, int y, int width, int height)     {
        if ((infoFlags & ERROR) != 0)  {
           imageLoadError = true;
        }
        return true;
    }


    public void init() {

        File file;
        mediaTracker = new MediaTracker(parent);
        setBackground(Color.black);
             
        image = this.createImage(this.getWidth(), this.getHeight()); // e.g. 247*250
        file = new File(imgPath);
        try {
          source = file.toURL();
          myObject = source.getContent();
        } catch (Exception e) {
          System.err.println("Could not load image file: " + file + ", " + e);
        }


        if (myObject instanceof ImageProducer) {
           image = t.createImage((ImageProducer)myObject); //e.g. 240*320
           //XXX: distorted aspect ratio fixed, but either image is too narrow or part of the image is not displayed...
           //image = image.getScaledInstance(this.getWidth(),this.getHeight(),image.SCALE_AREA_AVERAGING);
           image = image.getScaledInstance(this.getWidth(),-1,Image.SCALE_SMOOTH);
        } else {
           System.out.println("URL " + source + "(" + imgPath +") didn't give a ImageProducer");
        }
        mediaTracker.addImage(image,0);
        try {
            mediaTracker.waitForAll();
        } catch (Exception e) {
           System.out.println("Error at Mediatracker: " + e);
        }
        loadOK = true;
    }


    public void paint(Graphics g) {
       if (image != null) {
           g.drawImage(image, 0,0, null);
       }
    }


    public void setImage(Frame any, String path) {
        parent = (Frame) any;
        imgPath = path;
     }
    
//    public void setImage(ShipYard sy, String path) {
//       parent = (Frame) sy;
//       imgPath = path;
//    }
//
//    public void setImage(SpaceOperaDisplay sod, String path) {
//        parent = (JFrame) sod;
//        imgPath = path;
//     }
    
    

    public void update(Graphics g) {
        g.drawImage(image,0,0,null);
    }

}